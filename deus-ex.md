# 0. Metadata

## 0.0. Versioning

| Date          | Révision                          |
|-              |-                                  |
| 2025-01-06    | Création (AGDQ 2025) - Run Any%   |

# 1. Fiche technique

Sortie :
- PC :
  - AN : 22 juin 2000
  - EUR : 22 septembre 2000
- MAC :
  - 7 juillet 2000
- PS2 :
  - AN : 29 mars 2002
  - EUR : 18 juillet 2002
- PlayStation Network : EUR : 16 mai 2012

Plateforme : PC, MAC, PS2

Développé par : Ion Storm

Édité par :
- Eidos Interactive (PC, PS2)
- Aspyr Media (MAC)

Genre : Immersive Sim

Réalisateurs :
- Warren Spector
- Harvey Smith

Compositeurs :
- Alexander Brandon
- Dan Gardopée
- Michiel van den Bos

# 2. Histoire

(Pépom sans vergogne de wikipédia)

## 2.0. Prélude

La première moitié du XXIe siècle est marquée par la formation de nombreux
groupes terroristes comme la NSF (National Secessionist Force) aux États-Unis ou
le groupe Silhouette en France. Pour combattre ce péril international, les
Nations unies ont étendu leur influence sur le globe et ont mis en place
l'UNATCO (United Nations Anti-Terrorist Coalition).

Le monde des années 2050 sombre progressivement dans le chaos : une maladie, la
peste grise, décime la population. Un vaccin, l'ambroisie, a déjà fait ses
preuves, mais n'existe qu'en très faibles quantités et n'est prescrit qu'aux
personnes soi-disant « vitales à la société » ; seuls les politiques, le
personnel militaire, les hommes riches et influents, les scientifiques et les
intellectuels y ont accès.

La nanotechnologie a fait de grands progrès depuis les années 2040, les
machines-outils, les trousseaux de clés, les cartes de crédit ont été replacés
par des nanorobots capables de s'autorépliquer et de s'agencer à la commande...
Réussir à les intégrer dans un être humain fût la prouesse technologique
majeure sur laquelle s'attarde le jeu. Un tel exploit cependant coûte très cher
et seules les agences gouvernementales peuvent se permettre de l'offrir à leurs
agents. Le joueur incarne un de ceux-ci.

## 2.1. Le jeu

Le jeu commence à proximité du quartier général de l'UNATCO, sur Liberty
Island : des terroristes NSF viennent d'envahir la zone et ont même capturé un
agent méca-modifié de l'agence, Gunter Hermann. J.C Denton, un agent
nano-modifié intervient pour mettre un terme aux agissements des criminels et
capturer le chef des terroristes. JC est ensuite envoyé en mission à New-York
où il travaille en parallèle avec son frère Paul, lui-même premier agent
nano-modifié, ainsi que l'agent Anna Navarre, afin de détruire un générateur
électrique dans le quartier de Hell's Kitchen et pister des cargaisons
d'ambroisie à l'aéroport LaGuardia de New York. Il apprend alors que Paul est
complice des NSF et que d'autres instances contrôlent à la fois la Peste Grise
et l'Ambroisie, manipulant les puissances mondiales par un effet de menace,
d'offres et de demandes. JC rencontre Juan Lebedev, un autre leader des NSF, et
peut potentiellement le tuer ou affronter l'agent Navarre, envoyée en support.

De retour à l'UNATCO, JC apprend par Manderley qu'il a activé le 'killswitch'
de Paul, arrêt d'urgence qui le fera s'auto-détruire dans les 24h. Manderley
envoie JC à Hong-Kong s'occuper d'un hacker, Tracer Tong, mais chemin faisant,
Jock, le pilote de l'hélicoptère qui l'emmène en mission, le ramène à Hell's
Kitchen pour découvrir Paul, agonisant. Celui-ci demande à JC d'envoyer un
message au groupe terroriste français Silhouette, alliée des NSF, afin de
prévenir d'une attaque imminente. En repositionnant les paraboles et en
envoyant le message, JC devient également un ennemi de l'UNATCO et Walton
Simons, agent de la FEMA et manifeste supérieur de Manderley, active à son tour
son 'killswitch', avant de le capturer lui et éventuellement Paul.

JC se réveille dans une prison appartenant au Majestic 12 (MJ12), une entité
gouvernementale de recherche extra-terrestre, mais est libéré par Daedalus, un
prototype d'IA de communication basé sur le projet Echelon. Il s'aperçoit alors
que cette prison est située sous l'état-major de l'UNATCO. Après avoir demandé
l'aliance de ses amis à l'UNATCO, il est confronté à Manderley et éventuellement
Anna Navarre si elle n'avait pas été tuée auparavant. les deux frères sont
envoyés à Hong-Kong pour désactiver leur 'killswitch' avec l'aide du hacker
Tracer Tong. Afin de gagner sa confiance, JC doit mettre la fin à la guerre
entre deux triades, la Voie Lumineuse, gérée par Gordon Quick et protégeant
Tong, et la Flèche Ardente, gérée par un nouveau chef, Max Chen. La Flèche
Ardente est persuadée que la Voie Lumineuse a assassiné son ancien leader et
volé les plans d'une arme technologique, la Dent-de-Dragon, et dispose d'un
avantage tactique. JC enquête d'abord auprès de Maggie Chow, ancienne actrice
et directrice de la société de nanotechnologies VersaLife. Il acquiert la
preuve qu'elle a tué Max Chen, volé la Dent-de-Dragon et fait porter le chapeau
à la Voie Lumineuse. S'introduisant dans VersaLife, JC vole la ROM de la
Dent-de-Dragon, puis dévoile la supercherie à Max Chen qui se fait attaquer par
les MJ12. Il permet ainsi la réunification des deux triades, et gagne la
confiance de Tracer Tong qui désactive le 'killswitch'. JC retourne à
VersaLife, encore plus surveillé, pour récupérer les schématiques du
Constructeur Universel, qui produit la Peste Grise. A la demande de Daedalus,
il détruit également le Constructeur Universel.

Grâce aux informations du C.U., Tracer Tong remonte la piste aux Illuminati, en
particulier Stanton Dowd. Celui-ci terré à New-York désormais sous loi martiale,
demande à JC de détruire sur un chantier naval le PCRS Cloud, un navire chinois
affretant la Peste Grise par le biais d'explosifs, placé à des endroits
stratégiques. JC rencontre de nouveau Dowd, qui le renvoie en France à Paris
pour rencontrer Nicolette Duclare, fille d'Elizabeth DuClare, décédée, elle-même
amante de Morgan Everett, tous deux étant Illuminati. Sans plus d'information,
JC rencontre d'abord Chad Daumier, le leader de Silhouette, dans les catacombes
sous la place Denfert-Rochereau, et ancien amoureux de Nicolette. Celle-ci est
localisée dans une boîte de nuit aux Champs-Élysées. Dans le manoir abandonné
d'Elizabeth DuClare, JC trouve une salle secrète et envoie un message à Morgan
Everett. Ce dernier l'envoie à la cathédrale de Payens, pour localiser l'or des
Templiers et récupérer des informations sur l'Ambroisie. Ce faisant, JC
rencontre et affronte Gunther, qui le pourchasse depuis sa fuite de l'UNATCO et
le tient responsable à juste titre de la mort de Navarre.

JC apprend par Everett que la Peste Grise était à la base un produit pouvant
aider à l'acceptation de nano-modifications. L'ancien Illuminatus Bob Page a
détourné cet usage afin de créer la Peste Grise pour prendre contrôle du monde.
Morgan Everett envoie JC aux Etats-Unis pour protéger d'un assaut des MJ12, un
groupe de scientifiques dissidents de la zone 51 appelé X-51 qui tentent de
reconstruire un Constructeur Universel pour produire, cette fois-ci, de
l'Ambroisie. JC arrive tout juste pour repousser un assaut des MJ12. Everett
tente de contrôler les télécommunications américaines avec Daedalus, mais Bob
Page contre cette attaque avec sa propre IA, Icarus. De l'affrontement naît une
IA surpuissante et unique, Helios, capable de contrôler toutes les
télécommunciations mondiales.

Gary Savage, responsable des scientifiques, a un dernier plan : récupérer
auprès de sa propre fille Tiffany, capturée par les MJ12, l'emplacement d'une
base sous-marine contenant les plans du Constructeur Universel, ce qui
permettrait de finaliser le leur. Une fois l'information envoyée, Bob Page,
commençant à paniquer, envoie un missile depuis la base de Vanderberg pour
détruire X-51. JC arrive à réorienter le missile vers la Zone 51, véritable
blockhaus et quartier général de Bob Page. Il s'oriente vers ce dernier lieu
afin de l'affronter. Par là, il est contacté par Helios qui lui propose de
fusionner avec lui afin de devenir un demi-dieu surveillant l'humanité ; par
Tracer Tong qui essaie de le convaincre d'envoyer une onde électromagnétique
surpuissante pour détruire toute électricité dans le monde, poussant l'humanité
à revenir à un Moyen-Âge et recommencer une révolution industrielle ; et par
Everett lui demandant de détruire Bob Page et laisser les Illuminati reprendre
contrôle du monde dans l'ombre. La charge est laissée à JC de choisir comment
guider le monde.

# 3. Commentaire

## 3.0. Général

### 3.0.0. Mécaniques de jeu

Deus est un jeu qui mélange plusieurs genres : RPG, infiltration, tir à la
première personne. Ce melting-pot des genre est appelé "immersive sim".

On retrouve la plupart des gimmick de ce genre comme des points d'expérience
qui permettent de monter en niveau des compétences comme le tir, qui permet
d'être plus précis plus vite ; le crochetage, qui permet de crocheter plus vite
et réduire la consommation en crochet, le hacking, etc.

Au cours du jeu il faudra faire attention aux patrouilles ennemies et caméras
de sécurité au risque de déclencher un combat ou une alarme résultant en un
combat.

Les combats eux-mêmes peuvent être résolus soit en se cachant, soit en
affrontant les ennemis avec des armes de corps-à-corps, des armes de poings,
des armes de feu lourdes, des explosifs ou des armes de démolitions.
Le jeu se déroulant dans le futur, il existe plusieurs armes fictionnelles
capable d'infliger de lourds dégâts. Il existe des ennemis de toutes sortes,
notamment cyborg et robots en plus d'humain qui peuvent être combattus avec des
armes à impulsions (grenade IEM).

Petite spécificité, JC Denton, le personnage-joueur ne dispose pas d'une barre
de vie unique mais de plusieurs selon ses membres : tête, torse, deux bras et
deux jambes. Si la santé de ses bras tombe à zéro, l'usage des armes est compris
tandis que pour les jambes cela handicape le déplacement voire force à ramper.
Si la tête ou le torse tombe à 0 de vie, le personnage meure et l'on doit
recharger la partie.

On peut bien sur sauvegarder et charger à tout moment, et c'est d'ailleurs
excessivement utile en run (voir plus loin pour plus d'info).

En plus de tout cela, JC dispose d'augmentations bioniques appelées
nano-modifications qui permettent selon l'augmentation activée d'augmenter la
force, la vitesse, de réduire le son, de réduire les dégâts subis, de réduire
le coût en énergie desdites augmentation, etc. Chaque augmentation activée
consomme de l'énergie, qui peut être réchargé via des cellules énergétiques (un
consommable) ou auprés d'un robo-mécanique.

La vie quand à elle peut être rechargée via divers consommables comme de la
nourriture, des trousses de soin ou également un robo-médecin (qui soigne
intégralement).

L'inventaire est limité : il fait 30 cases les objets pouvant être aussi petit
que 1x1 ou aussi grand que 2x5.

### 3.0.1. Techniques émergentes

**save/load** : pas une technique à proprement parler mais il se passe des
trucs chelou quand on sauvegarde et qu'on charge la partie immédiatement après.
Certains truc ne sont pas pris en compte. Également, les contrôles sont pris en
compte pendant le chargement de la partie mais pas la physique ou les dégâts.

Ça tue également l'inertie, ce qui permet d'éviter les dégâts de chute.

À noter également que si le joueur modifier le fichier `user.ini` pour y ajouter
un raccourci personnalisé pour sauvegarder et charger la partie, il est possible
d'effectuer ces actions dans des moments où cela ne devrait pas être possible,
d'où l'ouverture de nombreux autres glitches.

---

**Dead-man walking** : après un **Revive**, si le joueur se soigne légèrement
tout en restant en négatif, le jeu va rentrer dans cet état. Lorsqu'il est dans
cet état, les IA ignorent complètement JC. N'importe quel dégât subit dans cet
état va toutefois tuer JC.

Il faut se soigner à chaque zone où on recontre des ennemis, d'où la duplication
aggressive de barres chocolatées.

**Elevator skip** : en détruisant un corps (préférablement celui du
personnage-joueur), le "fantôme" du personnage passe à travers tout ce qui n'est
pas permanent, dont les ascenceurs.

**Free pistol training** : Quand on crée le personnage, il dispose de 5000
points de compétences initialement et a déjà un niveau dans la compétence armes
de poing. On peut enlever ce niveau pour obtenir 1575 points et commencer le
jeu avec 6575 points. Cependant le jeu démarre quand même avec un niveau de
compétence dans les armes de poing.

**Ghost knife** : parfois un couteau de lancer repop après passer une zone de
chargement. La raison n'est pas bien connue des speedrunner. Le nombre de
munition est de 0 et on peut quand même lancer le couteau.

**Grenade jump** : en lançant la grenade sur ses pieds, quand elle touche le sol
il faut sauter. On gagne la vitesse de la grenade qui rebondit, ce qui nous
permet de franchir de sacrés obstacles.

**Grenade climbing** : on peut fixer les grenades au mur (mécanique normale)
pour en faire une mine, mais du coup elle obtient une boîte de collision du coup
le personnage joueur peut monter dessus.

**Grenade clip** : en se tenant près d'une grenade lancée contre une surface
verticale, quand elle se retourne le personnage-joueur se retrouve poussé dans
le mur. Parfois ça tue, mais parfois ça permet de passer au traver d'une surface
fine.

**Instant Lockpicking** : on commence à lockpick, on ouvre le menu, on attends
genre 10-20 secondes, on ferme le menu. Le lock aura été dévérouillé, ne
consommant qu'un seul outil de crochetage.

**Infinite Skill Points** : il y a plusieurs instances dans le jeu où la
progression déclenche un gain de skill mais qui, par oubli des développeur,
peut être répété, faisant gagner autant de point que l'on peut spammer.

**Item duplication** : en associant l'action de ramasser un objet sur la molette
de la souris, le jeu va prendre en compte l'action de ramasser plusieurs fois
dans la même itération du jeu ce qui aura pour effet de ramasser un objet
autant de fois ; duplicant ainsi l'objet initial.

**Item stacking** : permet de stacker des items non stackable dans l'inventaire.
Pour cela, prendre un item, commencer à le déplacer dans l'inventaire puis
fermer le menu. L'item existe toujours dans l'inventaire et est toujours
utilisable mais le prochain item ramassé prendra sa place comme s'il n'existait
pas.

**Laser bypass** : faire face à un laser, **save/load** et marcher pendant le
chargement. On aura marché par-delà le laser sans l'activer.

**Revive glitch** : pour revenir à la vie, c'est très simple. D'abord il faut mourir.
Ensuite il faut **save/load**. Wallah.

**Skill duplication glitch** : on peut dupliquer les points de compétence qu'on
est en train d'assigner. Il faut :
- **save/load**
- assigner les points de compétence
- **save/load**
On a ainsi les points assignés mais également encore disponibles.

**Superjump** : la seule instance utilisée en run d'un empilement d'effet
d'augmentation. En gros, quand on rentre dans suffisemement de sous-menu du
menu principal alors qu'une augmentation est active, le jeu réaplique l'effet
de l'augmentation plusieurs fois. On va s'en servir avec l'augmentation de
vitesse (et de saut, du coup) pour faire des saut bien plus grand qu'on ne
devrait déjà les faire avec l'augmentation de base.

## 3.1. Any% (last update: AGDQ 2025)

### 3.1.0. Informations

#### 3.1.0.0. Description

Runné sur PC.

Temps :
- WR - jelmeree : **30m 45s** (LRT) - **32m 25s** (RTA)
- 11ème - Vahist : **33m 50s** (LRT) - **n/a** (RTA)

Type de run : C'est bugué et c'est pété

#### 3.1.0.1. Règles

##### 3.1.0.1.0. Général

Les images par secondes sont limités à 120 ou moins. Le nombre doit être visible
à l'écran.

Bien que le temps pris en compte soit hors temps de chargement, l'abus de
save/load pour gagner du temps (car les déplacements sont toujours pris en
compte malgré le délai) est interdit. Utiliser la technique de save/load est
acceptée uniquement pour les tricks spécifiques à la run.

Le jeu peut être joué en difficulté "Facile" ou "Réaliste", le runner joue en
facile.

N'impporte quelle fin peut être choisie bien que :
- la fin dite "Moyen-âge" soit admise comme plus rapide ;
- la fin dite "Illuminati" soit admise comme plus lente d'environ 3 minutes ;
- la fin dite "Ascension" soit admise comme plus lente d'environ 5 minutes.

N'importe quelle sauvegarde chargée pendant la run doit avoir été faite pendant
la run.

Les runners peuvent modifier le fichier de configuration `user.ini` pour avoir
accès à plusieurs touches pour sauvegarder et charger est autorisé.

##### 3.1.0.1.1. Chronomètre

Le chronomètre démarre lorsque le joueur prend le contrôle du personnage-joueur
pour la première fois.

Le chronomètre s'arrête lorsque le joueur perd le contrôle du personnage-joueur
dès le début du chargement de n'importe quelle fin.

### 3.1.1. Route

Commence le jeu ; **Free pistol training** suivit d'un **Skill duplication
glitch**. On va avoir :
- Armes lourdes - avancé
- Armes de poings - basique (ça c'est "normal")
- Nage - Maître

On récupère le PEG à Paul.

Skip Liberty Island en manipulant le comportement d'un ennemi avec la gas
grenade qu'on récupère dans la caisse.

Tir dessus le garde pour ne pas déclencher un dialogue.

Plein de gens à aller voir pour le scénar. On va obtenir les couteau de lancer
sur le cadavre pour pouvoir skip les cutscenes car quand un personnage prend
des dégât son dialogue s'arrête.

Skip dialogue Mandeleiv.

On sort on loot les corps sur la route pour la mini-arbalète, la cigarette et
la barre de chocolat.

À *Battery Park* : dodge Navarre pour éviter le dialogue.

On regarde le sol pour skip une animation de fight.

**Laser bypass** avant le métro.

Go basement NSF pour récupérer un amplificateur d'augmentation (pour level up
des augmentations plus tard).

Ensuite C'EST SUPER IMPORTANT : on va se tuer.

**Revive glitch** deux fois pour looter son propre corps et dupliquer son stuff.

Récupérer la grenade pour **grenade jump**.

**Dead-man walking**.

Cassage de coffre pour récuperer des grenades et une canette d'augmentation.

Détruire le générateur into **grenade jump** x2 ou 3 (not safe).

Retour à UNATCO, trigger conversation entre Mendeleiv et Simons, pendant ce
temps on va activer l'augmentation (de course, bien entendu, c'est un speedrun
vous vous attendiez à quoi, on va pas prendre la discretion) au niveau max grace
à la duplication des amplificateurs d'augmentation.

Retour à Mendeleiv, et en sortant on va **item duplication** les cellules
d'énergie, les medpacks et les barres de chocolat. On va les dupliquer
régulièrement au cours de la run.

À *Battery park* on va voir le premier **Elevator Skip** puis plus loin le
premier **Grenade clip**.

Plus loin dans la base NSF, on récupère le lance-roquette, nécessaire pour un
skip à la fin du jeu.

Ensuite, **Superjump**.

À noter, les items single-use comme le lance-roquette peuvent actuellement être
conservés et réutilisés si on quitte le niveau avant que l'animation de
débarassage se termine.

Par la suite, le runner va enchaîner autant que faire se peut les **Elevator
skip**s, les **grenade clip**s, les **superjump**s et la conservation du
lance-roquette.

À noter que certains dialogue peuvent être skip si on saute au moment où le
dialogue devrait se déclencher. C'est normal du coup si vous le voyez sauter par
moment.

Retour à *Hells's Kitchen*, on skip pas mal d'allers-retours avec un saut bien
placé.

Après le signal satellite, le retour (enfin l'aller, du coup) à Paul après le
dialogue demande d'utiliser une save glitché, qui est préparé par les runners
avant la run. Apparamment c'est chiant à décrire, donc confère speedrun.com.
Anyway on va se tuer après le dialogue 1 avec Paul et load la save glitché.

Normalement on est censé se faire capturer vachement plus loin et sans notre
inventaire en plus mais grâce à la save glitché on se retrouve en prison et
avec notre stuff.

À Hong Kong, on peut actuellement skipper une énorme partie du jeu, parce qu'on
peut aller directement à l'objectif principal.

On va avoir un problème une fois qu'on rend la quête parce que le jeu se
rend compte qu'on a grugé. Il va chercher à décharger le PNJ qui permet de
continuer la quête principale. Cependant il ne décharge pas un PNJ qui a été
chargé moins de 5 secondes auparavant. On va donc setup un OoB pour charger le
PNJ en mémoire et pour lui parler en moins de 5 secondes.

Retour à *New York*, rien de nouveau pour *Hell's Kitchen*.

Dans le shipyard on se retue dans la ventilation pour dupliquer ses objets.

On fait le truc "normalement".

Retour à Dowd, puis go to Paris.

Catacombes, glitches classicos.

Au manoir DuClare, il y a un clip hyper précis à la fin du labyrinthe.

On rush les illuminatis.

À *Vandenberg Aiforce Base* on peut déclencher la fin du niveau en tirant sur
le déclencheur. Apparamment celui-ci est aussi activé par les collisions de
balles.

Ensuite, on vient au secours un hôtage... ah too bad.

On continue d'abuser des glitches traditionnels dans la base sous-marine.

Donc pourquoi on avait besoin du lance-roquette ? C'est pour tirer à travers une
grille, pour tuer un PNJ 5 étages plus bas. Oui la roquette apparait devant
le personnage-joueur et s'il y a un obstacle suffisament fin, devant lui aussi.

Le PNJ c'est l'objectif, du coup on a l'hélico qui repop instantanément.

Zone de fin, on est sur du glitch classique.

# 4. Trivia

n/a

# 5. Références

Deus Ex video tuto : https://www.youtube.com/playlist?list=PLSI6-MGQkaDLKv8tZ8huhnGwhqY8aK2fj

Deus Ex wiki (exploits) : https://deusex.fandom.com/wiki/Exploits_(DX)#Free_skill_points

Run de l'ESAWinter24 par le même runner : https://www.youtube.com/watch?v=lFqm-vnhqD4

Speedrun.com : https://www.speedrun.com/dx

Wikipedia : https://fr.wikipedia.org/wiki/Deus_Ex_(jeu_vid%C3%A9o)
