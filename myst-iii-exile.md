# 0. Fiche technique

Sortie : 2001 (PC), 2002 (console)

Plateforme : Mac OS, Windows, XBox, Playstation 2

Développé par : Presto Studios

Édité par : Ubi Soft

Genre : First person point-and-click

~~Concepteur : Entre autres Rand Miller (Atrus)~~

Compositeur : Jack Wall

# 1. Histoire

(Wipidia FTW:)

## 1.0. Prélude

Confère Myst I et II. Exile suppose la bonne fin dans les opus précédents.

## 1.1. Jeu

Exile est la suite de Riven, 10 ans plus tard. Atrus et Catherine se sont
installés dans l'Âge de Tomahna et ont même une fille, Yeesha.

Atrus invite le joueur, qui incarne toujours un personnage inconnu, pour lui
présenter le projet qu'il entretient depuis de longues années : un nouvel âge
pour la civilization en ruine D'ni, pour qu'elle puisse renaître. Ce nouvel âge
s'appelle Releeshahn.

Malheureusement, au moment ou Atrus souhaite montrer le livre, un individu que
l'on finira par connaître sous le nom de Saavedro fait irruption chez Atrus,
met le feu à sa maison et vole Releeshahn en laissant tomber au sol un livre
de liaison vers J'nanin.

En le poursuivant, on finit par trouver plusieurs pages de son journal qui donne
son nom et l'on apprend également que Saavedro devait être le mentor de Sirrus
et Achenar, les fils d'Atrus, mais qu'il a subit leur machiavelisme et a fini
par se retrouver piégé sur J'nanin et ses âges liés pendant 20 ans. Il a
finalement pu retourner sur son Âge natal, Narayan, mais pour y trouver ce qu'il
croit être ruine et désolation. Pour se venger, il veut faire connaître à Atrus
ce qu'il a subit, en lui enlevant l'âge si précieux de Releeshahn.

J'nanin était un âge créé par Atrus pour former ses fils. Ils devaient y trouver
3 symboles, chacun situés dans 3 âges liés à J'nanin : Voltaic, l'âge de
l'énergie, Amateria l'âge de la matière, et Edanna l'âge de la nature. C'est
donc cette route que doit suivre le joueur pour retrouver ces symboles et
accéder à l'Âge de Narayan sur lequel Saavedro s'est réfugié.

Lorsque l'on rencontre enfin Saavedro, il se voit décçu d'avoir piégé avec lui
non pas Atrus mais le joueur, et lui dit qu'il est piégé sur Narayan avec lui.
À moins de résoudre l'énigme que lui-même n'a pas résolu depuis vingts ans,
c'est la fin.

Avec l'aide du journal d'Atrus, le joueur peut résoudre les deux énigmes finales
et choisir la fin du jeu.

La fin canonique consiste à piéger Saavedro une ultime fois, seule solution pour
qu'il consente à rendre le livre de Releeshahn. Ensuite, le joueur lui laisse
rejoindre son peuple qui, il s'avère, est en vie ; tout en se protégeant de lui.

# 2. Commentaire

## 2.0. Informations diverses

Runné sur PC

Catégorie : Best Ending

Temps :
- WR : **10m59s** par MarvinSORR
- 2e : **11m14s** par Admiral_Flapjack

Run du par coeur, mais avec un petit programme ça va vite.

Chronomètre :
- Démarre au départ d'une nouvelle partie
- Termine en parlant à Atrus et Catherine avec le livre de Releeshahn dans
l'inventaire et sachant que Saavedro a été libéré

Règle additionnelle : l'utilisation de AutoHotkey est autorisée pour binder les touches.

## 2.1. Mécaniques de jeu

Un seul écran de jeu. On clique là où on veut aller et l'écran suivant nous y
ammène.

Le gros du jeu passe par la résolution d'énigme. Il y a plein de mécanismes
éparpillés ça et là et d'indices laissés pour qu'on les comprenne.

J'nanin est un peu un retour aux sources car il agit comme un hub, telle que
pouvait être l'île de Myst dans le jeu original.

## 2.1. Les âges de Exile

- Tomahna, l'Âge de résidence de Atrus et Catherine
- Releeshahn, l'Âge dont on doit récupérer le livre
- J'nanin, l'Âge-hub de Exile
- Amateria, Voltaic et Edanna, les âges des symboles
- Narayan, l'Âge natal de Saavedro

## 2.2. Techniques

Du par coeur et de l'optimisation principalement.

L'utilisation de AutoHotkey permet de remapper ses touches pour accélerer un
peu le mashing des déplacement et soigner des tendinites.

## 2.3. Route

10fast20u

Honnêtement, j'abandonne d'avance l'idée d'expliquer les énigmes. Meubler par
l'histoire.

Normalement il n'y a pas de random dans Exile.

# 3. Trivia

# 4. Références

Lien speedrun.com : https://www.speedrun.com/myst_iii_exile

Wikipedia : https://en.wikipedia.org/wiki/Myst_III:_Exile

# Annexe A. Versioning

| Date       | Révision             |
|------------|----------------------|
| 2020-01-05 | Création (AGDQ 2020) |
