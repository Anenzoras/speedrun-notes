# 0. Metadata

## 0.0.  Versioning

| Date          | Révision                              |
|-              |-                                      |
| 2022-06-25    | Update structurelle                   |
| 2018-06-xx    | Création (SGDQ 2018) - Run Extra Mode |

# 1. Fiche technique

Sortie : (1992)
Plateforme : Game boy
Développé par : HAL Laboratory
Édité par : Nintendo
Genre : Plateformer

# 2. Histoire

    (Merci Wiiiiikiiipeeediiiiiaaaa !)

Le scénario du soft met en scène le roi Dadidou, qui a dérobé toute la nourriture de Dream Land ; celle-ci devait pourtant servir à une fête. Kirby, un habitant de Dream Land, a décidé de venir en aide au royaume et récupérer la nourriture.

Pas grand-chose à dire de plus.

# 3. Commentaire

## 3.0. Informations diverses

Runné sur PC
Catégorie : Extra Mode (qui est le mode de difficulté caché qui est bien trop difficile)
WR is **11m33s** by tanuki256.
PB eBloodyCandy 11ème à 11m50s
Run du full skill.
La run démarre quand on appuie sur "Start Game" à l'écran titre et termine au dernier hit sur le Roi Dadidou.
L'extra mode rajoute des ennemis à plein d'endroit, les rends plus rapide, change des pattern, change les pattern des boss. Globalement le jeu est cocaïné.

## 3.1. Mécaniques de jeu

(Wiki)
Kirby's Dream Land se joue dans les grandes lignes comme les autres jeux de plates-formes en défilement horizontal de l'ère des consoles 8 et 16 bits. Kirby doit utiliser ses capacités naturelles et des objets pour atteindre la fin de chaque niveau. Comme dans la plupart des jeux de plates-formes des années 1980, le joueur peut cumuler des points et obtenir, à certains seuils, des vies supplémentaires. Le jeu ne possédant pas de système de sauvegarde, ces scores ne sont pas enregistrés.
La plupart des niveaux possèdent également, à leur moitié, un mini-boss. Tous possèdent un boss à leur fin.
À chaque fois que Kirby touche un ennemi, un boss ou un projectile, il perd l'un de ses six points de vie. S'il les perd tous, il perd une vie et recommence au début de la zone parcourue. S'il perd toutes ses vies, le jeu est terminé. Mais le joueur a la possibilité de continuer la partie en choisissant CONTINUE afin de recommencer au début du niveau avec cinq nouvelles vies ou bien de mettre fin à la partie en choisissant END et il revient à l'écran titre.
Kirby peut marcher, nager, sauter, voler et avaler des ennemis et des objets. Il peut se gonfler voler autant de temps que le joueur le souhaite en avalant de l'air, ce qui lui permet d'atteindre des objets et des endroits inaccessibles. À tout moment, le joueur peut recracher de l'air pour atterrir. Ce dégagement d'air peut blesser des ennemis de base ou détruire des blocs. Lorsqu'il nage, Kirby peut frapper les ennemis et n'a pas besoin de remonter à la surface pour respirer.
La technique principale d'attaque de Kirby est associée à sa capacité d'avaler ennemis et objets. Il peut les garder dans sa bouche et les renvoyer sous forme d'étoile pour blesser des ennemis et boss ou détruire des blocs. S'il avale certains objets, il peut régénérer ses points de vie.
Il existe des zones secrète, qui aident généralement le joueur.
Le jeu a également un boss rush à la fin, avant le boss final.

## 3.2. Techniques

Rien de spécifique. On va juste skipper toutes les zones secrètes qui ralonge la run.

## 3.3. Route

### 3.3.0. Général

Route standard, pas de glitch, juste du skill.

# 4. Trivia

Sans doute, j'en ai pas.

# 5. Références

Lien speedrun.com : https://www.speedrun.com/kdl
Wikipedia : https://fr.wikipedia.org/wiki/Kirby%27s_Dream_Land
