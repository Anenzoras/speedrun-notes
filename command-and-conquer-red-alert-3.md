# 0. Metadata

## 0.0.  Versioning

| Date          | Révision                                      |
|-              |-                                              |
| 2022-06-25    | Update structurelle                           |
| 2019-06-21    | Création (SGDQ 2019) - Run Soviet Campaign    |

# 1. Fiche technique

Sortie :
- 2008-08 (Windows) 2008/2009 pour toutes les plateformes

Plateformes : Microsoft Windows, Xbox 360, PlayStation 3, OS X
Développé par : EA Los Angeles  
Édité par : Electronics Art  
Genre : RTS  
Compositeur : Frank Klepacki (et d'autres)

# 2. Histoire

## 2.0. Préambule

L'histoire se déroule dans un univers à peu près contemporain, dans une réalité alternative.

## 2.1. Red Alert 1

Dans Red Alert 1, Einstein revient dans le passé pour tuer Hitler avant la création du régime Nazi.
Les nazi ne montent pas au pouvoir, mais l'autre conséquence est l'émergence du bloc de l'est plus tôt que prévu, qui escallade dans une guerre de Guerilla qui opposera les soviet aux alliés (europe et US).

La fin canonique est la fin de la campagne alliée.

## 2.2. Red Alert 2

La fin de RA 1 va engendre un traité de paix entre alliés et soviets qui tiendra quelques années jusqu'en 1972 où les soviets lancent soudainement une attaque massive contre les US. On retourne dans un affrontement alliés vs soviets.

La fin canonique est également la fin de la campagne alliée.

## 2.3. Red Alert 3

Le jeu démarre immédiatement après l'extension de Red Alert 2 où la Russie est en train de perdre la guerre. Les commandants russes utilisent leur propre machine à voyager dans le temps pour tuer Einstein, qu'ils jugent responsable de leur défaite grâce à ses inventions. Cela fonctionne car en revenant à leur époque, ce sont eux qui dominent le conflit alliés-soviets.

Toutefois l'absence de recherches nucléaires permet au Japon d'émerger comme puissance alternative et s'engage dans le conflit pour dominer le monde.

Encore une fois, la victoire canonique est celle des alliées.

# 3. Commentaire

## 3.0. Informations diverses

Runné sur Windows, patch officiel, version 1.12.

Catégorie : Soviet Campaign Coop - Hard

Times:
- WR: **1h 15m 08s** IGT by Raelcun and CovertMuffin

Run de jeu de stratégie. Du coup abus d'IA (un peu), de script (un peu) et surtout de position/placement et unités spéciales.

Timer démarre lorsque l’on prend contrôle des unités dans chaque mission et termine quand l'écran "Victorious!" apparaît.

## 3.1. Mécaniques de jeu

Un jeu de stratégie en temps réel. Vue top down.

On contrôle des armées et des bâtiments.

Une ressource, le minerai, utilisée pour produire tout le reste.

De l'énergie, produite par les centrales et utilisée pour alimenter les bâtiments.

3 factions ayant chacun leur propres bâtiments et unités.

Des armées qui s'affrontent.

Des unités spéciales.

Des superarmes destructrices.

Des protocoles top secret (= compétences spéciales) gagnée par niveau d'urgence (= grosso modo de l'experience)

Les unités victorieuses au combat gagnent des promotions, qui les rendent bien plus puissantes.

Les campagnes du jeu de base ont la spécificité d'être faits pour la coop, du coup les deux joueurs contrôlent 2 armées indépendante excepté que les ressources sont mises en commun.  
En solo, le commandant 2 est remplacé par une IA incarné par un commandant IG. Les commandants IG de factions différentes ont tendance à se taunter entre eux.

L'unité spéciale Soviet est Natasha, une commando.

## 3.2. Techniques

## 3.3. Route

### 3.3.0. Commun

Commune à toute les missions, on va chercher la méthode la plus rapide pour atteindre les objectifs de chaque scénarios, même si cela signifie terminer la partie avec 1 unité à 1 pv.

### 3.3.1. Soviet 01: Leningrad - The Shrike and the Thorn

Objectif : Sécuriser et protéger la forteresse de Leningrad de l'invasion japonaise.

Cette mission hautement scriptée sert d'introduction. Après avoir pris le contrôle de la forteresse, les ennemis arrivent par vagues et doivent ensuite être éliminés.

On anticipe beaucoup sur les placements des vagues.

### 3.3.2. Soviet 02: Krasna 45 - Circus of Treachery

Objectif : Ammener Natasha au silo de missile.  
Update : Détruire les bâtiments Japonais.

Le commandant qui contrôle Natasha l'ammène au silo le plus vite possible et défoncera un bâtiment par la suite puis un autre avec capacité spéciale.

L'autre commandant libère les ours et crée une diversion pour passer. Il défoncera le troisième bâtiment avec capacité spéciale.

### 3.3.3. Soviet 03: Vladivostok - Taking Back Ice-Harbor

Objectif : Détruire les ennemis.

Première mission soviet avec une base. On nettoie l'île puis on nettoie le reste de la carte.

Straitforward, pas particulièrement d'abus. L'unité phare ici est le stringray, qui est la meilleur unité dispo à ce moment.

### 3.3.4. Soviet 04: Geneva - March of The Red Army

Objectifs : Détruire les alliés, protéger le QG du général, (optionel : capturer les banques suisses (coucou Loos).

On utilise des stingray pour une partie de la mission, mais le gros des forces seront des tank Hammer et des lance-roquette V4.

### 3.3.5. Soviet 05: Mykonos - The Science of War

Objectifs : Construire une base, détruire les forces alliés et capturer un centre scientifique allié.  
Update : Défendre le centre.

On a enfin accès à la meilleur unité soviet : les twinblades (les hélico trop pété, objectivement).

On va faire la map avec que ça. Et des stingrays.

### 3.3.6. Soviet 06: Von Esling Airbase - No Traitors Tomorrow

Objectifs : Détruire les forces alliés et le traître.

Ici petite spécificité : On reçoit 100k de pognon de la part du Kremlin en début de partie et Krukov va nous trahir et piquer tout ce qu'on a 3 minutes après.

L'idée c'est de dépenser as fuck juste pour pouvoir revendre les trucs ensuite et se refaire du pognon vite.

Ensuite mass Twinblades pour EZ win.

Pour l'antiair on fait une troupette de MiG Fighters.

### 3.3.7. Soviet 07: MT. Fuji - To Tame a Living God

Objectif : Assassiner l'empereur japonais.

Début standard.

On reçoit les Tank Apocalypse, les meilleurs tanks du jeu.

Plusieurs ingé pour capturer des bâtiments neutre qui donnent du pognon et on va faire mass infantery flak, car très efficace contre les japonais.

En gros Commander 1 défend pendant un moment, Commander 2 attaque.

Ensuite Commander 1 anticipe sur les arrivés successives des autres commandeurs japonais.

### 3.3.8. Soviet 08: Easter Island - The Stone-Faced Witnesses

Objectifs : Construire une force d'embuscade pour tuer l'emissaire allié.  
Update : Détruire la contre-offensive alliée.  
Update : Détruire Cherdenko

Twinblades CPT. Toujours.

### 3.3.9. Soviet 09: New York City - Blight on the Big Apple

Objectif : Détruire la statue de la liberté.

Après la première phase pas vraiment optimisable, Commander 1 fait des MiG, Commander 2 des Twinblades et Kirov.

Time à l'écran "Victorious!".

# 4. Trivia

Beaucoup, et la flemme.

# 5. Références

Wikipedia : https://en.wikipedia.org/wiki/Command_%26_Conquer:_Red_Alert_3

Speedrun.com : https://www.speedrun.com/cncra3
