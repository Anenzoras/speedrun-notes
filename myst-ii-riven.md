# 0. Fiche technique

Nom alternatif : *Riven - The Sequel to Myst*

Sortie : 1997 (plein de dates selons les pays et les plateformes)

Plateforme : PC - Saturn - Playstation - Pocket PC

Développé par : Cyan

Édité par : plein de gens

Genre : First person point-and-click

Concepteur : Entre autres Rand Miller (Atrus)

Compositeur : Robyn Miller

# 1. Histoire

(Wipidia FTW:)

## 1.0. Prélude

Confère Myst. Riven part de la bonne fin dans le premier opus.

## 1.1. Jeu

Riven est la suite directe de Myst. Tellement directe que l'on prend le jeu
quasiment juste après la dernière discussion avec Atrus dans le premier âge.
Il nous dit qu'il lutte contre son propre père, Gehn, maître des écritures
comme lui et capable d'écrire des Âges, toutefois incomplets. Ces âges se
désintègrent lentement tel l'âge de Riven qu'Atrus essaie de maintenir en vie,
ce qui requiert toute son attention. Le problème, c'est que sa femme, Catherine
(dont il est parfois fait mention dans le premier jeu)
est otage de Gehn dans cet âge.

Il nous invite à aller la libérer pour lui, et essayer dans le même temps
d'enfermer Gehn une fois pour toute dans un âge prison, conçut spécialement par
ses soins. En effet, Catherine et lui avaient déjà essayé d'emprisonner Gehn
dans cet âge en brûlant tout livre de liaison entre Riven et ailleurs.
Toutefois, piégée par ses fils, Catherine a été contrainte de retourner à Riven
puis fut capturée par Gehn.

Atrus nous y rend donc avec un journal et un livre "prison", similaire à s'y
méprendre à un livre de liaison, mais dont on ne peut s'échapper. Le but est de
piéger Gehn à l'intérieur, de libérer Catherine et d'activer "un signal" pour
qu'Atrus nous rejoigne avec le livre de Myst.

Toutes sortes de péripéties intervienne pendant le jeu mettant des bâton dans
les roues du joueurs mais finalement, dans la fin canonique, il s'acquitte
fièrement de sa mission.

Tel le premier, plusieurs fin possible, toutes les autres étant mauvaises à
plusieurs degrés.

# 2. Commentaire

## 2.0. Informations diverses

Runné sur PC

Catégorie : Any%

Temps :
- WR: **5m51s** par zaustus

Run du par coeur, mais avec un petit programme ça va vite.

Chronomètre :
- Démarre au clic sur "Play Riven"
- Termine au dernier input

Règle additionnelle : sur la version ScummVM, l'utilisation de AutoHotkey est
utilisable pour binder les touches. Aucun autre script est autorisé.

## 2.1. Mécaniques de jeu

Un seul écran de jeu. On clique là où on veut aller et l'écran suivant nous y
ammène.

Le gros du jeu passe par la résolution d'énigme. Il y a plein de mécanismes
éparpillés ça et là et d'indices laissés pour qu'on les comprenne.

## 2.1. Les âges de Riven

- Riven, là où se passe quasiment tout
- Âge 233, le QG de Gehn
- Tay, l'Âge des Rebelle de Riven
- D'ni, l'Âge de départ, où se trouve Atrus

## 2.2. Techniques

Du par coeur et de l'optimisation principalement.

L'utilisation de AutoHotkey permet de remapper ses touches pour accélerer un
peu le mashing des déplacement et soigner des tendinites.

## 2.3. Route

10fast20u

Honnêtement, j'abandonne d'avance l'idée d'expliquer les énigmes. Meubler par
l'histoire. Il faut juste savoir qu'il y a 2 énigmes random dans la run et
qu'on a besoin de 2 combinaisons de chiffres (D'Ni du coup) pour les résoudre.
Chaque combinaison est dans un journal de Gehn.

# 3. Trivia

Il existe une scène dans le jeu où Gehn, le méchant, chante un opéra. Pour voir
cet easter egg, il faut à divers endroit du jeu cliquer sur certains items
précis. Les items sont :
- un oeuf dans le laboratoire de Gehn sur Riven
- un bol dans la prison de Tay
- un sous-marin qui vole dans la vigie du village de Riven
- un ascenseur spécifique de Riven
- un bout de roche spécifique de Riven

Puis ensuite aller voir Gehn, l'emprisonner et aller cliquer sur une banière
dans sa chambre.

# 4. Références

Lien speedrun.com : https://www.speedrun.com/riven

Wikipedia : https://en.wikipedia.org/wiki/Riven

# Annexe A. Versioning

| Date       | Révision             |
|------------|----------------------|
| 2020-01-05 | Création (AGDQ 2020) |
