# 0. Metadata

## 0.0. Versioning

| Date       | Révision                                     |
|-           |-                                             |
| 2021-07-01 | Création (SGDQ 2021) - Run Any% - No Void    |

# 1. Fiche technique

L'histoire du développement du mod original commence en 2004, où l'équipe était
déçu de la sortie de Half-Life: Source, qui se contentait d'être un reskin
basique du jeu, sans corriger certains des défauts. Plusieurs équipes
indépendantes se sont alors attelés à faire un remake de Half-Life de ce nom.

Le jeu a connu plusieurs vies, d'abord mod sous le moteur Source 2004, puis sous
le moteur source 2007 jusqu'à ce que les deux team indépendante qui avaient la
même volonté de faire ce remake fusionnent. En se rendant compte que la date
initiale de sortie (2009) leur faisait rusher le developpement, l'équipe choisit
de complètement revoir leurs objectifs, favorisant un polissage plus poussé du
jeu. Ils choisirent également de complètement retravailler la dernière partie du
jeu, Xen, qui avait subit un développement trop bref comparé aux ambitions de
Valve à l'époque de Half-Life.

Sortie :
    Version gratuite : 14 septembre 2012
    Version Steam en accès-anticipé : 5 mai 2015
    Version 1.0 : 6 mars 2020

Plateforme :  Windows, Linux

Développé par : Crowbar Collective

Édité par : Crowbar Collective

Genre : FPS

Moteur : Source 2013+

# 2. Histoire

    (Merci Wiiiiikiiipeeediiiiiaaaa !)

## 2.0. Introduction

Black Mesa est un gigantexte complexe scientifique top secret installé dans une base militaire désaffectée et totalement enterrée sous la surface. Au sein de ce centre de recherche, Gordon Freeman est un docteur en physique et chercheur au Laboratoire des matériaux anormaux. Accessoirement, il a suivi avec succès les exercices de survie en milieu hostile au parcours d'obstacles de Black Mesa, ce qui explique sa compétence en maniement d'armes.

## 2.1. Chapitre 1 - Arrivée à Black Mesa

Gordon Freeman rejoint son lieu de travail grâce au tram de Black Mesa.
(C'est l'intro du jeu, que l'on passe dans le speedrun.)

## 2.2. Chapitre 2 - Matériaux anormaux

Alors qu'il participait à une expérience sur un mystérieux échantillon de cristal, Freeman ouvre involontairement une brèche interdimensionnelle vers un monde parallèle, Xen, peuplé de créatures extraterrestres. Des aliens d'espèces et de races diverses font irruption un peu partout dans le centre et attaquent sauvagement son personnel. La salle de test est partiellement détruite par la résonance en chaîne, mais Freeman, équipé d'une combinaison de protection en milieu hostile (HEV), parvient à en sortir indemne après avoir été pris dans une tempête de portails.

## 2.3.  Chapitre 3 - Conséquences imprévues

Après s'être remis d'un court évanouissement, il prend conscience de la catastrophe... Le centre de Black Mesa est jonché de cadavres et des créatures particulièrement voraces pourchassent les survivants. Gordon, appuyé par d'occasionnels collègues et agents de sécurité du centre, les affronte sans autre but que celui de survivre dans l'espoir de pouvoir regagner la surface pour chercher de l'aide.

## 2.4. Chapitre 4 - Complexe administratif

Freeman cherche à atteindre la surface et doit pour cela traverser une zone infestée d'aliens.

## 2.5. Chapitre 5 - Nous sommes menacés

Au cours de ces péripéties, Gordon va découvrir que le gouvernement des États-Unis tente d'étouffer l'affaire en envoyant sur place un commando de l'Unité de combat en environnement hostile. De nombreux soldats très bien équipés déferlent à l'intérieur du centre dans le but de contenir l'invasion extraterrestre, mais aussi de réduire au silence tout le personnel de Black Mesa, afin qu'il ne témoigne jamais de ce qu'il a vécu. De nombreux chercheurs sont massacrés sans ménagement. Ne pouvant plus attendre de secours de la part de l'extérieur, Freeman, sur les conseils de ses collègues survivants, se fixe un nouvel objectif : tenter d'atteindre le complexe Lambda de l'autre côté du centre où il pourra trouver le moyen de se rendre sur Xen afin de mettre fin à l'invasion.

## 2.6. Chapitre 6 - Chambre de combustion

Gordon Freeman fuit par les chambres de test pour atteindre une Chambre de combustion de réacteurs de fusée. Il lui faudra réussir à activer les réacteurs pour venir à bout des trois tentacules qui lui barrent la route.

## 2.7. Chapitre 7 - Allumage

Freeman devra remettre les rails sous tension pour pouvoir poursuivre sa progression, mais cette mission ne sera pas un parcours de santé : un Gargantua garde l'accès aux bobines de Tesla.

## 2.8. Chapitre 8 - Sur un rail

Gordon utilise un système ferroviaire désaffecté investi par les militaires pour atteindre la surface, où se trouve un bâtiment d'où il pourra lancer une fusée transportant le satellite Lambda. Ce satellite est la tentative des scientifique pour fermer le portail dimensionnel ouvert entre Xen et la Terre.

## 2.9. Chapitre 9 - Appréhension

Freeman progresse dans des souterrains et des entrepôts désaffectés contrôlés par l'armée, les Black Ops et infestés de xéniens. Il sera capturé par des militaires à la fin du chapitre.

## 2.10. Chapitre 10 - Traitement de déchets

Privé de ses armes, Gordon, laissé pour mort par l'armée dans une fosse de destruction de déchets, se fraye un chemin dans l'inhospitalier centre de traitement des déchets où quelques aliens rodent.

## 2.11. Chapitre 11 - Une éthique douteuse

Dans ce chapitre de transition, Freeman finit par atteindre un laboratoire dans lequel quelques équipements laser sont à l'étude. Pour en sortir, il doit trouver des collègues capables de passer l'identification rétinienne pour lui ouvrir l'accès à l'extérieur. Mais de nombreux soldats et aliens se mettront sur son chemin...

## 2.12. Chapitre 12 - Tension en surface

Gordon Freeman traverse une vaste zone désertique et montagneuse située en partie à la périphérie de Black Mesa. L'armée contrôle chaque secteur et tentera par tous les moyens de l'éliminer.

## 2.13. Chapitre 13 - Oubliez Freeman

Les militaires évacuent la zone alors que l'Air Force commence à bombarder le site. Les fuyards sont poursuivis par de nombreux xéniens qui commencent à s'installer dans les bâtiments du centre devenus déserts.

## 2.14. Chapitre 14 - Réacteur Lambda

Freeman parvient finalement à atteindre le complexe Lambda, mais le chaos y règne. Les téléporteurs ne manqueront pas de mettre Gordon Freeman à l'épreuve, mais il faut absolument réactiver le réacteur Lambda, trouver les scientifiques survivants et les protéger pour lui permettre d'atteindre la dimension Xen.

## 2.15. Chapitre 15 - Xen

(Ce chapitre et les suivants jusqu'à l'épilogue ont été intégralement révisé pour le remake.)

Gordon Freeman est téléporté sur Xen, un univers singulier très différent de la Terre. Le premier chapitre est une introduction au monde extraterrestre, qui vise à présenter et faire ressentir l'étrangeté de cet univers lointain. Il permet aussi de prendre en main le module de grand saut.

On pourra appercevoir de nombreux restes de l'équipe de scientifique envoyée en reconnaissance dans le monde, avant la réaction en chaîne.

## 2.16. Chapitre 16 - L'Antre du Gonarch

Dans ce chapitre, Gordon Freeman combattra le Gonarch, un monstre gigantesque très similaire dans sa biologie aux crabe de têtes. Le chapitre entier alternera des phases de combats et de course poursuite dans l'antre de la créature, jusqu'à aboutir du Gonarch (ou peut-être pas si l'on trouve une sortie alternative).

## 2.17. Chapitre 17 - L'Intrus

Chapitre le plus remarquable de la révision, l'Intrus est aussi très long, et montre les Vortigaunt sous un jour nouveaux. Ils sont en fait maintenus en esclavage par l'entité maître de ce monde et ses sbires, les Contrôlleurs. Le village Vortigaunt laisse place à l'antre des Gargantuas, où il faudra s'en enfuir vite, puis l'usine organique où sont conçus les Grognards.

Ce chapitre marque pleinement dans la saga Half-Life l'ascension de Freeman en tant que libérateur, qui lui vaudra dans Half-Life 2 son status de messie aux yeux des Vortigaunts.

## 2.18. Chapitre 18 - Nihilanth

Gordon Freeman combat ici le maître des habitants de Xen. Après un long et terrible affrontement, il en vient à bout.

## 2.19. Fin de Partie

Le G-Man est un personnage énigmatique que Freeman peut appercevoir à de nombreux moments du jeu, mais avec lequel il n'interragit pas directement. À travers diverse déduction, on peut savoir que le G-Man est bien plus influent qu'on ne pourrait le penser, et également responsable de nombreux événements dans le complexe.

En fin de jeu toutefois, ledit G-Man rejoint Gordon Freeman, lui explique la situation et lui laisse l'illusion d'un choix :
- Accepter de travailler pour lui ;
- Affronter une armée entière de xéniens sans aucune chance de succès.

D'après Half-Life 2, Gordon Freeman accepte son offre et est mis dans un état de stase en attendant son affectation.

# 3. Commentaire

## 3.0. Général

### 3.0.0. Mécaniques de jeu

FPS

Des armes, cac / armes "standard" de FPS / armes plus expérimentales et des bidules aliens. On verra des utilisations TRÉS détournées de certaines armes.

Mécaniques de vie/armure qui se rechargent via soit des collectibles soit des distributeurs, soit d'autres distributeurs (oui). Un certain pourcentage des dégâts pris est encaissé par l'armure, proportionnellement à la quantité d'armure possédée.

Du fun.

Mécanique de crouch jump, pour faire un saut accroupi, pour passer à travers certains obstacles.

Une mécanique de long jump apparaît juste avant d'aller à Xen. Pour activer le long jump il faut utiliser 2 fois la touche de saut successivement.

Xen update a ajouté une mécanique de sliding quand on court puis s'accroupi, on slide. Ceci réduit la friction et est utile à connaître pour les techniques émergentes.

### 3.0.1. Techniques émergentes

Notes : la vélocité maximum en courant c'est 320 unités hammer.

**Bunny hoping** : on saute + straffe et on regarde dans la direction du straffe = vitesse augmente. Slider nous faisant perdre de la friction, c'est encore mieux accroupi. Le gain de vitesse est plus bas que le BH de Half-Life mais tout de même décent.

**Superslide** : alors en fait le slide est pété. En fait 'faut juste faire des straffe moves en slide et on s'arrête jamais. Perso j'appelle ça le superslide pour le differencier de la mécanique de base mais y'a pas de nom. Le **Superslide** a l'avantage de ne pas bumper contre de petits obstacles sur la route mais de glisser sur eux.
Note : en ce qui concerne la turning speed, **Bunny hoping** > **Superslide**

**Straffe Jump** : exploitation d'un bogue du moteur Hammer (et donc Source), en sautant + straffe + direction on peut vraiment accélérer son saut dans un mouvement circulaire.

**Grenade boost** : quand on lance une grenade, on les dégoupille, et elles ont un délai. Si on les garde en main longtemps, elles explosent quand on relâche. Le truc c’est que l’explosion nous boost radicalement les sauts. Par ailleurs, plus on prend des dégâts de l’explosion sur la vie (et non l'armure) plus le boost est fort.

**Blast boost** : plus généralement, une propulsion par n'importe quelle explosion.

**Tau boost** : en utilisant le recul offert par le Fusil Tau chargé, on bénéficie d’un boost de vitesse.

**Save deletion** : glitch disponible sur certaines version de Source, qui doit son nom à la première façon de le faire. Le glitch essaye de provoquer le reload de la carte comme si elle venait juste d'être chargée de 0. Le moteur appelle un script spécifique à chaque map pour fixer vie/armure et armes selon sa configuration, en plus d'autres petits bonus.

**Save/load buffering** : l'action de sauvegarder et charger une partie en même temps. Cela a plusieurs effets kiss-cool notamment quand le personnage-joueur est coincé dans un item, cela permet de le faire passer à travers des "Brush" (grosso modo les surfaces) et donc souvent le sol, pour passer outre des parties du jeu.
Note : le Save/Load buffering est aussi utilisé pour ouvrir certaines portes dans Tension de Surface et Oubliez Freeman.

**NPC Prop Teleport** (nom non officiel) : on peut utiliser des props qu'on a grabbé au préalable en donnant des coup sur la tête des NPC en pleine animation pour les faire se téléporter et accélérer ladite animation.

**Ladder speeding** : en montant une échelle, crouch + utiliser + avancer nous fait monter plus vite.

**Abs box abuse** : grâce au crouch jump, une des hitbox du personnage-joueur change (elle devient plus fine mais plus haute). Parfois la hitbox touche un plafond et permet alors de faire des **Save/load buffering**.

**Corner boost** : quand on prend certains angles avec une haute vitesse, cela permet de transférer la vitesse horizontale en vitesse verticale et inversement, selon angles et direction d'arrivée.

**Prop clipping** : on peut parfois clipper à travers le sol si on utilise un gros prop (genre un baril) et un save/load.

**Prop boost** : on peut parfois abuser de la mécanique de grab des props pour gagner de la vitesse. Beaucoup moins efficace et un poil plus de setup que dans HL mais tout de même.

**Floor Warp** : glitch qui consiste en le personnage-joueur passant à travers le sol dans certaines transition de carte. Pour ce faire, il faut que le PJ se coince dans le décor alors que la carte transitionne.

### 3.0.2. Associations de touches

Grâce à la console, il est possible d'associer une touche à des commandes plus complexes qu'un simple déplacement. Voici les commandes généralements utilisés sur une touche :

- `jump` associé à la molette de la souris (important pour le **Bunny Hop**) ;
- `"save quick; load quick"` associé à une touche pour faire une sauvegarde rapide et charger celle-ci instantanément (important pour le **Save Deletion** et le **Save/Load buffering**) ;
- `load autosave` pour charger une sauvegarde auto et `load quick01` pour charger l'avant-dernière sauvegarde rapide sont parfois nécessaire ;
- `"save :; reload"` est la méthode optimale pour faire un **Save Deletion**.
- `"toggleconsole; unpause"` pour activer la console et tout de même avoir le jeu non pausé est nécessaire pour faire un **Quantum Pause**.

## 3.1. No void

### 3.1.0. Informations diverses

#### 3.1.0.0. Description générale

Runné sur PC.

Temps :
- WR : **1h 09m 59s 010ms** IGTA - **1h 34m 05s 680ms** RTA par JohannVonVorst
- (2ème : **1h 12m 52s 200ms** IGTA - **1h 33m 35s 190ms** RTA the_kovic)

Type de run : LE. BORDEL.
(Run du swag et du skill avec des techniques fumées et des glitchs.)

Notes :
- Existe des sous catégories avec scripts (notamment pour faciliter le bunny hop).

#### 3.1.0.1. Règles

##### 3.1.0.1.0. Général

La run doit être complétée sans scripts (il existe des script pour rendre trivial le **Bunny Hop**, le **Ladder Boost**, le **Spam E**, etc.).

Toutes les commandes de la console des développeurs sont interdites, exceptées :
- `cl_showpos`
- `cl_showfps`
- `cl_auto_crouch_jump`
- `r_portalsopenall`
- `fps_max`
- `reload`
- `kill`
- `unpause`

Il est autorisé d'associer une commande à plusieurs touches (par exemple sauter sur espace et la molette de la souris).

Il est autorisé d'associer plusieurs commandes à une seule touche (par exemple `bind c "reload; fps_max 30"`).

L'utilisation de mods est interdite. Le remplacement de maps est interdit.

##### 3.1.0.1.1. Version

Les runs doivent être joués sur la version 0.9 du jeu.

##### 3.1.0.1.2. Glitchs

Le personnage-joueur et ses hitboxes ne doivent pas être entouré par le Void.

Le Void est défini par tout espace dans la carte qui n'est pas entouré par des solides.

##### 3.1.0.1.3. Chronomètre

Le chronomètre commence au load à la 1ère map du chapitre Matériaux Anormaux (bm_c1a0a).

Le chronomètre termine au dernier coup sur le crystal dans la tête du Nihilanth.

Le temps de référence est celui donné par SourceSplit, un logiciel utilisé pour ne pas compter les temps de chargement.

### 3.1.1. Route

#### 3.1.1.0. Général

C’est le bordel. Mais 4 points à retenir :
- On Bunny Hop / Superslide systématiquement (quand pas de meilleur déplacement disponible) ;
- Se rattraper sur une échelle ne provoque aucun dégât après une longue chute.

#### 3.1.1.1. Chapitre 1 - Arrivée à Black Mesa

Les règles permettent de démarrer après.

#### 3.1.1.2. Chapitre 2 - Matériaux anormaux

**First tick save load** si on save + load sur le premier tick du jeu, ça reset la carte à l'état de base prévu par le moteur hammer, notamment utilise pour ne pas attendre un ascenseur plus loin.

Main technique de mouvement avec **Bunny Hop** + **Superslide**

**NPC Prop Teleport** pour le garde

Le bureau est chelou : on peut se coincer dedans. L'avantage : **Save/load buffering** nous permet de passer à travers le sol. Ce trick est hardware dépendant, il faut trouver combien de temps passer dans les loads permet de passer à travers le sol soit même.

Test chamber normal.

#### 3.1.1.3. Chapitre 3 - Conséquences imprévues

Grosso modo normal. **NPC Prop Teleport** ne fait pas gagner de temps car il passe tout son dialogue avant le scan rétinien.

**Ladder speeding**

[On va vite...]

On va faire un petit skip avec un Houndeye qui va nous propulser.

[On va vite...]

**Save delete** dès qu'on change de map pour se give vie et flingues.

[On va vite...]

On prend l'ascenceur, mais on prend pas l'ascenseur finalement. (Nouvelle strat : à la grenade).

[On va vite...]

**Quantum Pause** pour passer le fossé avec un grenade boost.

#### 3.1.1.4. Chapitre 4 - Complexe administratif

**Abs box abuse** pour passer l'arc électrique.

[On va vite...]

~~Double **Grenade boost** grâce à une **Quantum pause** pour faire un launch sur le balcon et arriver à la fin du chapitre.~~ New strat: Zombie boost grâce à la **Quantum pause**, suivi de air straffe.

#### 3.1.1.5. Chapitre 5 - Nous sommes menacés

Alors y'a deux routes.

- Ancienne : fait tout le chapitre quasi-normalement avec 2-3 **Grenade boost**s
- Nouvelle : clip dans une porte.

Nouvelle + compliquée, voici l'explication :

On va faire la première salle, prendre le baril pour faire un **Save/load buffering** en étant en l'air avec le tonneau contre nous. Cela va avoir pour effet de décaler une des hitbox du personnage-joueur et on va pouvoir faire passer un bout de hitbox à travers la porte pouvoir se coincer dedans !
Ensuite, re **Save/load buffering** pour passer à travers.

Voilà le chapitre est skip.

#### 3.1.1.6. Chapitre 6 - Chambre de combustion

On va se bloquer dans la porte de l'ascenseur pour faire un clip et passer à travers le sol du coup. Il survit grâce à un corner boost.

**Quantum pause** pour **Grenade boost** pour toute la ligne de tram et faire un launch assez difficile en rebondissant sur un obstacle.

[On va vite...]

Passer au dessus des déchets radioactifs pour attérir dans l'ascenseur.

**Save delete** au chargement de la nouvelle map pour refill vie et mun.

On continue dans la chambre et...

En fait on va se coincer dans le sol en attirant les tentacules puis on va faire un blast boost avec le C4 avec un save/load pour se projeter dans le sol.
On attend que les tentacules perdent aggro le garde plus haut et du coup laissent la place, puis on va **Save/load buffering** pour sortir du sol.

Boom chapter skip.

#### 3.1.1.7. Chapitre 7 - Allumage

**Save delete** pour refill et se faire téléporter en fait.

On grab un baril pour faire un **Prop clipping** puis on se blast boost avec des mines puis **Save/load buffering** pour passer la porte fermée.

Ensuite 'faut passer à travers l'ascenseur qui normalement ne s'active que si on a le tram. Save/load à un endroit précis pour activer le level transition et se coincer dans le sol. Ensuite **Grenade boost** + save/load pour sortir et tomber.

#### 3.1.1.8. Chapitre 8 - Sur un rail

**Prop boost** pour gagner de la vitesse.

Petit skip à l'aide du Barnacle au plafond et de l'air straffing.

On fait le puzzle normalement mais on récupère une toolbox utilisée pour skip la porte fermée en l'absence du tram.

Du coup rebelote, **Prop clipping** into **Blast boost** into save/load, into level transition into **Save delete**.

On fait la map-ci normalement [On va vite...]

Next map, on va bait le corps-à-corps avec un garde puis **Quantum pause** alors qu'il nous tape pour en fait
accumuler de la vitesse à cause du recul et voler en OOB.

Ensuite on va se placer sur un solide invisible qui est le plafond de la suite du niveau, pour clipper à travers (classique on se bloque + **Save/load buffering**).

#### 3.1.1.9. Chapitre 9 - Appréhension

Chapitre fait relativement normalement excepté quelques **Straffe jump** et **Blast boost** bien placés et un **Prop clipping** into **Save/load buffering** pour tomber dans une vent et skipper une partie d'une map.

#### 3.1.1.10. Chapitre 10 - Traitement de déchets

On va skip le début avec un prop qu'on place en face d'un mur coulissant, ce qui a pour effet de désactiver sa hitbox, puis faire un **Prop clipping** grâce à un save/load avec celui-ci, puis **Save/load buffering** pour passer à travers le sol.

[On va vite...]

On va prendre un baril et faire un **Prop clipping**/**Save/load buffering** pour passer une carte.

[On va vite...]

Sur un des tapis roulant qu'on prend en sens inverse on va justement **Blast boost** pour passer.

À la fin du chapitre on va se placer avec précision pour accrocher le trigger de changement de map.

#### 3.1.1.11. Chapitre 11 - Une éthique douteuse

**Save Delete** pour refill.

Bon...

Ça fait longtemps qu'on n'a pas skip un chapitre...

Donc...

On prend une sorte de bombonne, on la fait clipper dans le mur, puis on va se bloquer sur un mur et faire un **Floor Warp**. On récupère la bombonne, on l'envoit sur le toit ; on **Blast boost** sur le toit et ensuite on fait un**Prop clipping** pour trigger la fin de la carte.

Éventuellement parler du Tau Gun pendant les loading screens.

**Save delete** pour refill et faire la dernière carte tranquille.

#### 3.1.1.12. Chapitre 12 - Tension en surface

C'est comme Half-Life 1 mais pas vraiment : on va faire les mêmes skip mais pas de la même façon.

À partir de maintenant on va quasiment **Save delete** chaque carte pour se refill en munition expérimentale pour le Tau Gun et en explosifs.

**Save delete** into **Grenade boost** into **Tau boost** into une adaptation du même skip que Half-Life 1.

Bon c'est dingue, ça va faire **Grenade boost** + **Tau boost** tant que faire ce peut pour sortir en haut de la carte puis dans le tunnel.

(Eh psst regardez au fond c'est le logo de Black Mesa)

On va faire un **Quantum Pause** quand le millitaire nous tape pour faire le même skip que Half-Life 1 pour le passage de la falaise.

Ensuite, on passe encore en haut de la carte, on n'est plus à ça prêt.

Encore une fois, on skip quelques trucs. On passe au dessus de la carte, etc.

Le passage de la "Ex-porte qui soigne", on va le passer via un **Prop clipping**.

Comme dans HL1, on skip le passage du bombage du Gargantua.

**Crash à la fin de Surface Tension** (c'est normal)

#### 3.1.1.13. Chapitre 13 - Oubliez Freeman

[On va vite...]

Dans le passage avec l'Ichtyosaure on va rebondir sur lui et **Grenade boost**.

Le reste c'est du classique **/X/ Boost**.

#### 3.1.1.14. Chapitre 14 - Réacteur Lambda

Comme pour *Conséquences Imprévues*, on prend l'ascenceur mais sans l'ascenseur.

[On va vite...]

Arrivé dans la carte du hub du réacteur lambda, on peut skip grace à un **Prop clipping** into **Save/load buffering**

Pour le skip du réacteur, c'est moins impressionant que HL1 mais c'est le même délire. Le **Quantum pause** est utilisé pour se faire booster par l'AOE d'un Vortigaunt.

À la fin du pilier de téléportation, **Grenade Boost** pour éviter une autre salle.

On récupère le Module de Grand Saut... ça va devenir encore plus rigolo. Surtout qu'il n'y a plus de dégats de chute sur sol.

#### 3.1.1.15. Chapitre 15 - Xen

Bon.

Gravité différente.

Monde un peu ouvert.

Bisou je menvol !

Xen est fait plus ou moins normalement, les trucs fous c'est juste des sauts craqués du slip.

Vers la fin de Xen et le passage avec les racines, on va se démerder pour skip une caverne et en fait arriver DERRIÈRE la transition de niveau pour passer sur la map suivante.

Sinon après c'est relativement normal.

La salle explosive, on va faire un petit clip dans le plafond pour sortir du trigger de décès.

#### 3.1.1.16. Chapitre 16 - L'Antre du Gonarch

Là part contre, gros **Blast Jump** pour skipper l'intro où on rencontre le Gonarch.

On continue sur la map 2, jusqu'à ce qu'on arrive à la phase avec le canyon. **Blast jump** avec le C4 pour passer de l'autre côté et skipper quasi toute la carte.

P'tits skip en ne déclenchant pas les triggers dans le chant des cristaux.

Arrivé à la phase poursuite, on va skipper quasiment tout avec un **Tau Boost** bien placé.

Ensuite on va se propulser OOB avec un **Blast jump** et un à un endroit ou la texture est un peu perméable pour ensuite aller toucher le trigger de fin de carte.

#### 3.1.1.17. Chapitre 17 - L'Intrus

**Save delete** pour se remettre inbound.

**Blast boost** pour skipper la map 1 avec le village Vortigaunt.

Pas mal de boosts pour les Gargantuas, normal.

Première salle de l'usine, on clip grace à un **Blast boost** et on passe à travers le sol via un **Save/load buffering**

Ensuite gros enchaînements de **Blast boost** pour skip plusieurs sections, successivement.

Jusqu'au moment où on pose 3 mines et 2 C4 pour skiper une grosse phase baston. Idéalement on tombe dans le tuyau avec la vitesse.

Gros gros skip aussi avec le passage Ichtyosaure où on va s'arranger pour tomber directement dans le dernier conduit avec un énorme **Blast Boost** + **Tau boost**.

Sur la phase avec les écrans de force bleus à désactiver, on voudrait idéalement chercher à passer avec un bon **Blast Boost** de C4, mais sinon la backup c'est de le faire normal.

P'tit skip avec des **Tau boost** juste après le premier gros saut avec bumper.

Énormes **Blast boost** pour skipper toute la phase de montée avec des bumpers à débloquer.

Énormes **Tau boost** + **Blast boost** + **Quantum pause** pour skipper le premier étages de l'Ascension. Rebelotte avec des coups de grunts en plus pour le deuxième étage.

On est obligé de faire le troisième normalement, mais comme on attends pas les plateformes, rebellote again pour passer le dernier étage.

Le reste est assez explicite.

#### 3.1.1.18. Chapitre 18 - Nihilanth

**Save delete** puis grosse marave. P'tit trick : un save/reload sur une autre arme skip l'animation de recharge du lance-roquette.

Time au dernier coup sur le cristal dans sa tête.

#### 3.1.1.19. Fin de Partie

Normalement le time s'est arrêté.

# 4. Trivias

Y'en a beaucoup. Beaucoup de clin d'oeil au jeu original, à l'univers, etc.

# 5. Références

Lien speedrun.com : https://www.speedrun.com/bms
Wikipedia
- https://fr.wikipedia.org/wiki/Black_Mesa_(mod)
- https://en.wikipedia.org/wiki/Black_Mesa_(video_game)

Note : le wiki source run n'inclut pas d'infos sur Black Mesa.
