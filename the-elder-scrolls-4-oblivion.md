# 0. Metadata

## 0.0.  Versioning

| Date          | Révision                  |
|-              |-                          |
| 2022-06-25    | Update structurelle       |
| 201x-yy-zz    | Création (?) - Run Any%   |

# 1. Fiche technique

Sortie :
- (2006-03-24) Windows, XBox 360 (international)
- (2007-04-26) Playstation 3 (international)

Plateforme : Windows - PS3 - Xbox 360
Développé par : Bethesda Game Studios  
Édité par : 2K Games
Genre : Action-RPG  
Compositeur : Jeremy Soule

# 2. Histoire

## 2.0. Prélude

La saga des The Elder Scrolls se déroule dans le monde fictif de Tamriel. La saga est riche et extrêmement chargée en lore, ce qui fait que je ne mentionnerai qu’une infime fraction de ce qui est.

*La flemme*

## 2.1. Jeu

Le jeu commence comme tous les Elder Scrolls : emprisonné. Le personnage-joueur se trouve dans la prison impérial et aurait fini exécuté s'il ne se trouvait pas dans la cellule qui contient un passage secret. L'empereur Uriel Septim lui-même cherche à fuir car menacé par des assassins et vous vous retrouver à vous échapper avec lui et ses gardes Lames.

Il finira malgré tout assassiné au terme de l'évasion (qui sert au passage de tutoriel). Il vous demande juste avant de trouver Jauffre, et de lui remettre l'amulette des rois, artefact que seul les Septims peuvent porter.

Jauffre, chef des lames indiquera au héros qu'il y a encore un héritier en vie, un bâtard du nom de Martin qu'il vous demande de secourir avant qu'il ne lui arrive quoi que ce soit. Arrivé à Kvatch, la ville dans laquelle il hâbite, le PC se rend rapidement compte que la ville est assiégé par des Deadras (grosso modo des démons dans l'univers TES). Après avoir nettoyé la zone et le portail démoniaque (porte d'Oblivion), Martin accepte de vous suivre.

De retour chez Jauffre, il est attaqué par les mêmes gonzes qui ont tué l'empereur, et qui volent l'amulette des rois. Tout le monde part du coup se planquer au Temple des nuages, la forteresse des Lames en Cyrodiil (la province dans laquelle se déroule le jeu).

À partir de là commence une enquête pour comprendre ce qu'il se passe, ce qui nous mets sur la piste d'un culte au seigneur Daedra de la destruction Mehrunes Dagon, qui ne venir en Tamriel que si les empereurs sont décédés. Le chef de ce culte est Mankar Camoran.

Après avoir infiltré ce culte pour tenter de récupérer l'amulette, seul outil permettant de banir Mehrunes Dagon définitivement, Mankar se barre dans une dimension alternative (le paradis de Camoran). Le PC s'échappe récupérant au passage le Mysterium Xarxes, fait son rapport et Martin nous indique que pour rejoindre Camoran il va falloir plusieurs trucs:
- Le sang de Talos, trouvable dans les catacombes de Sancre Tor ;
- Une grande pierre de Welkynd, un crystal chargé d'énergie trouvable dans la ruine dwemer de Miscarcand ;
- Un artefact Daedrique ;
- Une grande pierre sigilaire (un crystal qui maintient les portails Daedrique ouverts (portes d'Oblivion)).

À la suite de plusieurs péripéties pour récupérer tout ça, Martin ouvre un portail vers le paradis de Camoran pour le tuer et récupérer l'amulette. Martin nous indique ensuite que la seule façon de stopper Mehrunes Dagon de débarquer en Tamriel est de l'amener avec l'amulette au temple des Neufs dans la Cité Impériale.

Là-bas, il se sacrifie pour banir le seigneur Daedra une bonne fois pour toute.

# 3. Commentaire

## 3.0. Informations diverses

Runné sur PC, version classique

Catégorie : Main Quest no OoB.

Times:
- WR: **26m30s** loadless (**33m48s** RTA) by Mirocu
- BubbleDelFuego 4ème à 27m37s / 35m00s resp.

Run incompréhensible du swag avec des glitchs dans tous les sens mais pas aussi pire que Skyrim.

Timer démarre lorsque l’on voit le premier message du tuto et se termine à la dernière ligne de dialogue de Martin Septim avant sa phase scriptée.

Sont interdits :
- Les out of bounds ou les clips.
- Les commandes consoles
- Les mods

## 3.1. Mécaniques de jeu

Alors, le jeu est un action RPG en vue à la première personne. Vous pouvez courir, sauter, marcher, activer le mode discrétion, vous battre avec les armes que vous pouvez équiper et lancer des sorts. Il y a quelques subtilité à un peu tout, mais globalement c’est ça.

Vous avez une barre de vie, une barre de mana et une barre d’endurance.

Les équipement que vous ramasser sont parfois enchantés, vous rendant encore plus puissant.

Monter de niveau se fait par compétence : quand vous utilisez ou entraînez une compétence, cette compétence gagne de l’expérience. Chaque fois que la compétence gagne un niveau, votre personnage gagne de l’expérience globale. Quand votre personnage gagne un niveau, vous avez l'opportunité de booster 3 caractéristiques qui influencent lesdites compétences.

De reste, le monde d'Oblivion est vaste, jouer-y pour tout découvrir.

## 3.2. Techniques

La charactéristique la plus importante du speedrun est évidemment la vitesse.

**Arrow Dupping** (v1.0) : Il faut faire une action avec animation (punch, sortir une arme...). Pendant l'animation, go menu, déséquiper les flèches, dropper n'importe quel item à dupliquer et quitter le menu. Ça ne fonctionne pas toujours sur les gros items et ça duplique autant d'item qu'il y a de flèches équippées.

**Scroll Dupping** :
- 1) Avoir une ou plusieurs copies du même parchemin, le nombre déterminant la quantité de copies générées par la duplication.
- 2) Ouvrir son inventaire
- 3) Mettre le stack de parchemin en surbrillance et le sélectionner une fois.
- 4) Mettre en surbrillance un item mais ne pas l'équiper et le droper.
- 5) Fermer l'inventaire et comtempler le resultat.

**Potion glitch** (utilisée avec le skooma) : La limite du jeu du nombre de potion bue dans un certain intervale de temps est de 5. Toutefois, on peut boire des potions depuis son inventaire et depuis un raccourcis et la limite est séparée pour les deux ; ce qui fait qu'en en prenant de chaque façon, on peut se retrouver à pouvoir en prendre 8 plutôt que 5.

**Wrong Warp** : Charger une partie alors que l'on entre dans quasiment n'importe quelle zone séparée par chargement fera que le personnage ainsi chargé sera celui de la sauvegarde, ce qui fait généralement gagner du temps.

Quicksave/quickload permet parfois de deaggro les mobs et du coup on peut FT.

Frapper les NPC permet de sauter des dialogues ou des scripts. On se rend pour qu'ils redeviennent amicaux.

## 3.3. Route

Race : High Elf parce que grand du coup rapide (oui).

Signe astrologique : Destrier parce que Rapidité

Classe : Scout ou Nightblade parce que Rapidé et Acrobatie/Athlétisme.

Donc, déjà on s'échappe le plus vite possible (on preshot aussi les ennemis).
Au passage on ramasse des flèches et pendant une cutscene, **Arrow Duping** sur des potions et parchemins puis parce que plus simple **Scroll Duping** sur des parchemins et des crochets.

On va se magner à chopper du Skooma (drogue qui notemment augmente la vitesse).

Go Weynon voir Jauffre pendant qu'on duplique le Skooma.

On va passer tout le jeu à boire du Skooma avec le **Potion Glitch**.

Go checher Martin, fermer la porte d'Oblivion. Attention ! Corner boost. Attention 2 ! Sauvegarde avant la chambre sigilaire.

Go Miscarcand pour récupérer une grosse pierre de Welkynd puis **wrong warp** à Weynon revoir Jauffre.

FT Temple des Nuages.

Go récuperer le masque de Clavicus Vile (Umbra -> Quête -> Rendre Umbra).

Rendre le casque à Martin.

Questline de l'aube mythique juste faite rapidement. À la fin, wrong warp au Temple.

Quête des espions expédiées.

Porte de Bruma fermée en vitesse avec un **wrong warp** into go chercher le sang de Talos into rendre ça et la grosse pierre en même temps.

Grosse porte d'oblivion par **wrong warp** encore.

Go Paradis.

Go fin du jeu.

# 4. Trivia

Bon y’a genre pleins de trivia à chopper sur TES4 mais la flemme.

# 5. Références

- Lien speedrun.com : http://www.speedrun.com/oblivion
- Wikipedia : https://fr.wikipedia.org/wiki/The_Elder_Scrolls_IV:_Oblivion
- UESP : http://en.uesp.net/wiki/Oblivion:Oblivion
