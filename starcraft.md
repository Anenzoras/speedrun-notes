Starcraft
===

# 0. Metadata

## 0.0. Versioning

| Date          | Révision                                              |
|-              |-                                                      |
| 2024-06-21    | Update (SGDQ 2024) - Run Brood War Terran Campaign    |
| 2024-01-12    | Update (AGDQ 2024) - Run Brood War Zerg Campaign      |
| 2022-06-25    | Update structurelle                                   |
| 2019-06-21    | Création (SGDQ 2019) - Run Brood War Protoss Campaign |

# 1. Fiche technique

Sortie :
- 1998-03-31 (Classic) : Microsoft Windows, Classic Mac OS, macOS, Nintendo 64
- 2017-08-14 (Remastered) : Windows, MacOS

Plateforme : Windows, MacOS

Développé par : Blizzard Entertainment

Édité par : Blizzard Entertainment

Genre : RTS

Créateurs : Chris Metzen, James Phinney

Compositeur : Glenn Stafford, Jason Hayes and Tracey W. Bush

# 2. Histoire

## 2.0. Prélude

(À éventuellement détailler)

Starcraft commence en l'an 2499. L'humanité a colonisé le secteur Kropulu,
distant de la terre et le peuple local se fait appeler Terran. Les colonies
humaines sont principalement contrôlées par la confédération Terrane.

Il existe également dans le secteur deux races aliens qui maîtrisent le
voyage spacial : les zergs et les protoss. Les zergs sont une race insectoide
animale avide de domination qui s'étend rapidement sur tous les mondes où ils
s'échouent. Les protoss sont une race de nobles guerriers psyoniques qui
chassent les zergs.

Les Terrans ignorent leur existance jusqu'à ce que les zergs infestent la
planète Chau Sara et que les protoss les détruisent, rendant par la même
occasion la planète stérile.

## 2.1. StarCraft

(À détailler pour d'autres run que les campagnes BW)

### 2.1.0. Début

La première campagne commence immédiatement après la destruction de Chau Sara

### 2.1.1. Campagne Terrane

La campagne Terrane se concentre surtout sur Jim Raynor, un marshall de la
planète Mar Sara, Sarah Kerrigan, une fantôme au service d'Arcturus Mengsk, un
terroriste aux yeux de la confédération. La campagne sert surtout d'introduction
aux personnages et à l'univers du jeu.

Mar Sara se faisant attaquer par les zergs et abandonner par la confédération,
les locaux sur place se tournent vers Arcturus Mengsk et ses Fils de Khoral
pour les sauver. Raynor, Kerrigan et Mengsk vont alors combattre la
confédération, partiellement responsable de l'infestation de Mar Sara et
remplacer le gouvernement. Toutefois Raynor se rend peu à peu compte que Mengsk
ne vaut pas mieux que les confédérés et décide, lorsque Kerrigan est laissée
pour morte par Mengsk sur la capitale Terrane de Tarsonis attaquée par les
zergs, de s'échapper avec ceux qui deviendront plus tard les Rebelles de Raynor.

### 2.1.2. Campagne Zerg

La campagne Zerg suit les aventures de Kerrigan après sa capture puis son
infestation par les Zergs. Elle devient la meilleure agent de l'overmind,
le maître-esprit qui contrôle tous les zergs. La campagne termine sur les zergs
envahissant Aiur, la planète natale des Protoss.

### 2.1.3. Campagne Protoss

La campagne Protoss démarre là-dessus et traite de la lutte pour défendre le
monde natal d'Aiur, ainsi que de l'alliance entre l'exécuteur et templier
Protoss Tassadar, et le Templier Noir Zeratul. Les deux castes sont ennemies
depuis longtemps mais vont peu à peu se rapprocher face à la menace Zerg
commune.

Lors de la dernière phase de l'attaque d'Aiur, l'Overmind lui-même vint infester
Aiur, et seul le sacrifice de Tassadar permis de le vaincre.

## 2.2. StarCraft: Brood War

### 2.2.0. Information

Brood War se déroule imédiatement après les événements de Starcraft.
L'extension démarre cette fois par la campagne Protoss.

### 2.2.1. Campagne Protoss

La campagne Protoss démarre sur Aiur, alors que les zergs sont encore bien trop
présent pour rester sur la planète. Les protoss partent alors en exil sur
Shakuras, la planète où se sont installés les Templiers Noirs. Là les Protoss
vont découvrir que les Zergs les ont suivis. Malgré tout, la matriarche
des templiers noirs Raszagal accepte leur venue et ils s'allient contre les
Zergs.

En détruisant un cérébrate zerg, Kerrigan arrive sur la planète et prévient
qu'un nouvel Overmind se développe sur Char. Elle-même a regagné sa conscience
et refuse de subir le contrôle de l'Overmind à nouveau. En parallèle, pour
assurer la défense de Shakuras, les Protoss doivent activer un vestige de la
race ayant créé les Protoss et les Zerg : le temple Xel'naga.

Les missions de la campagne s'enchaînent sur récupérer les cristaux Uraj et
Khalis ainsi que détruire l'Overmind naissant. Les cristaux sont nécessaire pour
activer le temple Xel'naga. Toutefois, avant de pouvoir activer le temple, on
apprend qu'Aldaris, un templier, s'est révolté de l'alliance entre Zeratul et
Artanis (l'héritier de Tassadar) et Kerrigan et doit être maté.

Dans son interrogatoire, Kerrigan intervient et le tue. Révulsé par cet acte,
Zeratul chasse la reine zerg de Shakuras.

La dernière mission conduit Artanis et Zeratul à utiliser Uraj et Khalis pour
activer le temple et détruire toute présence Zerg sur Shakuras.

### 2.2.2. Campagne Terrane

La campagne Terrane suit les actions du Directoire de la Fédération Terrienne,
une force expéditionnaire originaire de la Terre envoyé dans le secteur Kropulu.

Les forces terriennes sont arrivés pour acquérir les territoires Terrans, qu'ils
estiment appartenir au Directoire. Au contact des Protoss et des Zergs,
l'expédition commandée par l'amiral Gerard DuGalle et le vice-amiral Alexei
Stukov leur somme de cesser toute aggression, faute de quoi ils seront détruit.

C'est peu ou prou comment il se mettent à dos tout le secteur.

De par leur intervention rapide, de meilleures ressources que le Dominion se
remettant de la guerre et avec l'aide de rebelles de l'ancienne
Confédération commandée par le lieutenant Samir Duran, les forces du DFT
conquièrent un à un tous les postes clés du Dominion. Ils mettent la main sur le
disrupteur psy de Tarsonis que Duran urge de détruire mais Stukov intervient au
préalable, pensant pouvoir s'en servir comme arme.

Le DFT part à l'assaut de Khoral pour démettre l'empereur Mengsk, qui va
s'échapper avec l'aide de Jim Raynor ! Le DFT les poursuit jusqu'à Aiur, mais
ils ne peuvent empêcher leur échappatoire à cause des Zergs que Duran n'a pas pu
retenir.

DuGalle apprend alors que Stukov est parti pour Braxis, leur avant-poste, où il
y a emporté le disrupteur psy en secret. Outré par cette trahison, DuGalle
ordonne son exécution, que Duran applique. Mortellement blessé après leur
rencontre, Stukov parvient à convaincre DuGalle dans son dernier souffle que
Duran est un infiltré Zerg, peut-être même infecté, et qu'il les a manipulé
depuis le début.

Les forces du DFT parviennent au dernier moment d'empêcher les Zergs d'activer
le mécanisme d'autodestruction du disrupteur psy, cible de Duran, et partent à
l'assault de Char pour prendre le contrôle du nouvel l'Overmind Zerg avec l'aide
des psionique terrans. Une fois le combat remporté et l'Overmind mis en
esclavage, Duran présente Kerrigan à DuGalle. La Reine des Lames le mets en
garde, promettant de ralier tout Kropulu à sa cause pour les bouter hors du
secteur.

### 2.2.3. Campagne Zerg

La campagne Zerg commence immédiatement après, et suit Sarah Kerrigan dans sa
mission de vaincre le Directoire de la Fédération Terrienne pour reprendre le
contrôle des Zergs.

Initialement planquée sur Tarsonis avec Duran, elle reprend le contrôle des
cérébrates de la zone. Ses forces sont affaiblies à cause des disrupteur psy du
DFT. Elle négocie ensuite la coopération de Jim Raynor, Fenix et de Arcturus
Mengsk contre les forces du DFT. À contrecœur, ils acceptent, bien que méfiants,
mais convaincus qu'ils doivent s'allier pour contrer le DFT. L'alliance des
factions parvient à détruire le disrupteur, permettant à Kerrigan de reprendre
contrôle sur ses forces.

Ils s'attaquent ensuite à Moria, la planète la plus riche du système pour
obtenir les ressources nécessaire pour libérer Korhal, puis ils libèrent Korhal.

Une fois la majorité des forces du DFT détruite, et certaine de pouvoir s'en
sortir par elle-même avec ce qui reste sur Char, elle brise son alliance avec
ses compagnons. Elle s'attaque ensuite à Tarsonis à nouveau pour reprendre le
contrôle de la planète.

Ensuite, elle a besoin de la matriarche Raszagal pour l'étape suivante de son
plan, ce qui nécessite d'infiltrer Shakuras pour capturer la Protoss. Avec cet
otage, Kerrigan force Zeratul à intervenir sur Char pour tuer l'Overmind ; les
lames des templiers noirs seuls pouvant blesser un cérébrate ou lui-même.

Une fois accompli, Zeratul demande la libération de la matriarche, ce que
Raszagal elle-même refuse. Zeratul comprend alors que Kerrigan a corrompu
Raszagal depuis bien avant sa venue sur Shakuras pour que les Protoss se plie à
sa volonté.

Zeratul appelle alors à l'aide un groupe de survivant Protoss, emprisonant
Raszagal dans une chambre de stase et se préparant à un rappel dimensionnel vers
Shakuras (une téléportation en gros) ; tandis que des forces du DFT, Protoss et
Terranes approchent de Char. Kerrigan ordonne à ses forces de fondres sur les
survivant de Zeratul pour les empêcher de s'échapper. Dans un acte de désespoir,
Zeratul brise la chambre de stase et met à mort Raszagal lui offrant un destin
préférable à la corruption Zerg. Dans son dernier souffle, la matriarche lui
dit qu'il l'a libérée de son contrôle, et lui demande de veiller sur les
templiers noirs.

Kerrigan, impressionnée par le choix de Zeratul, le laisse partir de Char.

(Plot de la mission bonus : Zeratul détecte dans sa fuite vers Shakuras un
signal Protoss sur une lune inconnue. Il découvre alors un laboratoire sous
contrôle de Duran dans lequel existe un hybride Zerg/Protoss en stase. Duran lui
révèle alors qu'il a eu bien des noms dans le dernier millénaire, et qu'il ne
sert pas Kerrigan mais un pouvoir bien plus grand encore. Zeratul s'échappe à
nouveau, troublé par cette révélation.)

En orbite autour de Char convergent les trois flottes précédentes, et le combat
est rude alors que ses forces sont au sol ; mais les flottes sont également dans
un piteux état. Kerrigan parvient à l'emporter et devient alors maîtresse
incontestée des Zergs, la Reine des Lames.

### 2.2.4. Epilogue

Dans leur échappatoire, toutes les forces du DFT sont annihilés, le Directoire
sur Terre ignorera alors à jamais ce qu'il advint dans le secteur Kropulu.

Toutes les forces ayant subis des lourdes pertes, chaque peuple retourne panser
ses plaies dans leurs territoires respectifs, attendant alors la prochaine
menace qui plane sur le secteur.

Zeratul et Raynor, quant à eux, disparaissent pour un temps.

# 3. Commentaire

## 3.0. Général

### 3.0.0. Mécaniques de jeu

Un jeu de stratégie en temps réel. Vue top down.

On contrôle des armées et des bâtiments.

Deux ressources :
- Minerai : la ressource de base, utilisée pour produire tout
- Gaz Vespène : la ressource un peu avancée, utilisée pour faire des unités et
  bâtiment un peu plus puissants.

3 races ayant chacun leur propres bâtiments et moyen de productions.

Des péons qui construisent la base/récoltent.

Des armées qui s'affrontent.

Du fun.

Les Zergs ont deux spécificités :
- un seul bâtiment de production, la Couveuse (ou le Terrier, ou la Ruche, dans
  ses améliorations avancées), qui produit des larves qui peuvent muter en
  unités. Une larve est produite toutes les 20 *StarCraft Seconds* environ,
  jusqu'à 3 larves maximum par couveuse.
- les bâtiments ne peuvent être posés que sur du sol avec du Creep (une espèce
  de substance alien), à l'exception de la Couveuse et de l'Extracteur.
  Le Creep est généré depuis la Couveuse ainsi que les Colonies de Creep et leur
  améliorations.

Les Protoss ont deux spécificités :
- à l'exception du Nexus, il ne pouvent construire des bâtiment que sur des zone
  d'énergie posées par des Pylones.
- les unités et bâtiment ont des points de bouclier qui se rechargent au cours
  du temps et des points de vie qui ne régénèrent pas.

### 3.0.1. Techniques émergentes

Les runs n'ont pas de glitches.

Le gros des gains de vitesse vient d'une très bonne exécution et également des
particularités spécifique aux scénarios de la campagne. En général, chaque
scénario peut être rempli de manière plus ou moins conventionnelle, mais en
abusant au maximum des connaissances des cartes, scripts, comportement de l'IA
ou encore resources initialement disponibles. On parle généralement de *cheese*
dans ce genre de situation.

**Dialog skip** (nom non officiel) : un gain de vitesse non négligeable
également va consister à sauvegarder et charger la partie pour certains dialogue
in-game qui ralentissent la mission en cours, soit parce que le joueur n'a pas
le contrôle des unités pendant ce temps, soit parce qu'un personnage est en
pleine animation. Le sauvegarde/chargement va couper court au dialogue pour
laisser le runner jouer.

Certains dialogues mettent en pause le jeu, rendant le joueur usuellement
incapable de faire un save/load. Toutefois, dans certaines situation, le runner
va maintenir le bouton de la souris enfoncé sur le bouton de Menu, et le
relacher dès que le dialogue commence. Cela a pour effet de donner accès au
menu (normalement impossible), et donc au save/load.

## 3.1. Brood War - Protoss Campaign (last update: SGDQ 2019)

### 3.1.0. Informations diverses

#### 3.1.0.0. Description générale

Plateforme : PC

Version : Remastered

Times:
- WR: **47m 57s** RTA by KingDime

Type de run : c'est un RTS, du coup abus d'IA (un peu), de script (un peu) et
surtout de position / placement des unités spéciales.

#### 3.1.0.1. Règles

##### 3.1.0.1.0. Général
##### 3.1.0.1.1. Chronomètre

Le chronomètre démarre lorsque l’on appuie sur OK en lançant la campagne.

Le chronomètre termine au dernier écran "Victoire!".

### 3.1.1. Route

#### 3.1.1.0. General

Commune à toute les missions, on va chercher la méthode la plus rapide pour
atteindre les objectifs de chaque scénarios, même si cela signifie terminer la
partie avec 1 unité à 1 pv.

#### 3.1.1.1. Protoss 01: Escape from Aiur

Objectif : ammener Zeratul à la balise à l'autre bout de la carte.

Les autres unités ne sont pas nécessaire, du coup on va juste les utiliser pour
tanker les zergs.

#### 3.1.1.2. Protoss 02: Dunes of Shakuras

Objectif : Détruire la base zerg orange.

Probablement la mission la plus proche de la réalisation casual.

On va cependant ignorer la base zerg violette, juste en protégeant. Les unités
favorites ici sont les templiers noirs, qui font des gros dégâts sol-sol et les
dragons, descent en sol-sol et sol-air.

#### 3.1.1.3. Protoss 03: Legacy of the Xel'Naga

Objectif : Détruire les cérébrates zergs qui gardent le temple Xel'Naga. Les
cérébrates ne peuvent être détruit que par des templiers noirs.

Ici on va essayer de prévenir les drops des zergs avec les corsaires, et grosso
modo on va attaquer avec full dragon + nuage corsaire et rajouter des templars
vers la fin.

#### 3.1.1.4. Protoss 04: The Quest for Uraj

Objectif : ammener Kerrigan au Crystal Uraj.

On ammène Kerrigan au crystal Uraj.

Oui.

Oui, oui.

En même temps elle peut cloak et a 400 pv.

Donc oui.

#### 3.1.1.5. Protoss 05: The Battle of Braxis

Objectif : Détruire tout les générateur pour pouvoir s'échapper du blocus.

On va vraiment prendre des commando suicides pour foncer sur les générateurs.

Mention spécial à la fin : on utilise la capacité illusion des templars pour
augmenter la tankiness et utiliser le recall de l'arbiter pour invoquer la
puissance de feu pour détruire le dernier générateur.

#### 3.1.1.6. Protoss 06: Return to Char

Objectif : Détruire l'Overmind OU ammener un worker au crystal Khalis.

La route la plus rapide ici est de récupérer Khalis.

On va donner la vision avec une unité volante pour détruire deux bâtiments de
défense côté zerg et on drop un worker pour qu'il récupère le crystal.

#### 3.1.1.7. Protoss 07: The Insurgent

Objectif : Tuer ou mind-control les 3 Aldaris.

Fun fact : seul tuer le vrai à l'autre bout de la map compte, les deux illusions
on s'en fout.

Alors.

Là.

C'est drôle.

Un peu.

On prend deux archon, un dropship et on va mind control le vrai Aldaris.

#### 3.1.1.8. Protoss 08: Countdown

Objectif : Amener Zeratul et Artanis avec leur crystaux au temple Xel'naga.

Update : Une fois dedans, protéger le temple pendant 30 minutes in-game.

Cette mission est celle faite de manière la plus "règlo". On va d'abord utiliser
Artanis et Zeratul pour clean la zone autour du temple. Puis on va préparer la
défense en posant quelques canon à photons et c'est parti.

La meilleur unité ici sera le templier, car la psionic storm fait beaucoup de
dégâts et permet de nettoyer les Gardiens zergs, hors de portée des Photons.

On gagne la partie quasi sur le fil si tout va bien.

Note : le temple possède 5k pv (aucun bouclier).

## 3.2. Brood War - Terran Campaign (last update: SGDQ 2024)

### 3.2.0. Informations diverses

#### 3.2.0.0. Description générale

Plateforme : PC

Version : Remastered

Temps :
- shox - WR : **31m 59s** RTA

Type de run : c'est un RTS, du coup abus d'IA (un peu), de script (un peu) et
surtout de position / placement des unités spéciales.

#### 3.2.0.1. Règles

##### 3.2.0.1.0. Général

Remporter la campagne Terran, par tous les moyens.

Mettre en pause est autorisé, mais découragé.

Sauvegarder et charger est autorisé, tant qu'il s'agit de la même run.

##### 3.2.0.1.1. Chronomètre

Le chronomètre démarre lorsque:
- soit l’on appuie sur OK sur l'écran de sélection de mission avec la mission 1.
  sélectionnée ;
- soit l'on appuie sur OK en sélectionnant la campagne avec un profil vierge ;
- soit l'on clique sur DuGalle dans le menu si la campagne Protoss est terminée
  et la campagne Terran n'a pas commencé.

Le chronomètre termine dès l'écran "Victoire!" apparait à la fin de la dernière
mission.

### 3.2.1. Route

#### 3.2.1.0. Général

Commune à toute les missions, on va chercher la méthode la plus rapide pour
atteindre les objectifs de chaque scénarios, même si cela signifie terminer la
partie avec 1 unité à 1 pv.

La bio-ball terrane est pétée dans Brood War parce que médics+stim marines c'est
pété.

#### 3.2.1.1. Terran 01: First Strike

Objectifs :
- Détruire le Centre de Commandement ennemi ;
- Duran doit survivre.

On prend un SCV + toutes les units into rush vers Duran pour le récupérer.
En plus de ça on a accès au Gaz Vespene qu'on va récolter pour pouvoir
rechercher le mode siège.

On profite du fait que l'IA a une capacité de détection limitée pour cloak Duran
et l'envoyer dans un angle mort commencer à taper le Centre de Commandement.
Duran va également body block les unités et empêcher qu'elles puissent passer.

On prend un barraquement qu'on soulève pour rediriger une patrouille de marines
dessus alors qu'on rush avec les unités de départ + quelques SCV pour passer en
force.

Une fois la zone clear, les tanks siègent et font le taf.

#### 3.2.1.2. Terran 02: The Dylarian Shipyards

Objectifs :
- Voler les Cuirrassés ;
- Vaincre la force d'intervention du Dominion.

Il faut ammener un pilote (une unité spéciale de cette mission) à la balise de
chaque cuirrassé.

On va juste passer en force en fait en abusant du ciblage auto par aggro de
l'IA sur les ghosts + blind un char avec un médic.

À partir de l'étape 3 de la mission, on a trop de pilotes par rapport au nombre
de balises. C'est plus rapide de tuer les pilotes et les troupes que de voir
l'envoi de renfort à l'étape suivante (d'où la nuke).

On va abuser de la capacité *Lockdown* (Verrou) des *Ghosts* (Fantômes).

Dernière étape de la mission, on va micro pour préparer la concave des
battlecruiser et mieux shooter les Cannons Yamato

#### 3.2.1.3. Terran 03: Ruins of Tarsonis

Objectifs :
- Détruire les ruches Zergs pour pacifier les essaims ;
- Ammener Duran au disrupteur Psy ;
- Duran doit survivre.

On replace le command center pour optimiser les ressources (C'EST SUPER
IMPORTANT XD).

À noter : sur cette map, les réactions des IA sont assez RNG, en particulier
pour l'IA violette et marron.

L'académie est construite pour rechercher la portée des marines, mais en plus
elle est placée à un endroit spécifique pour optimiser le déplacement des SCV à
la rafinerie x)

Alors là en même temps :
- il s'agit d'utiliser Duran au mieux pour clear le terrain pour que les 3
  marines avec lui détruisent la ruche zerg orange ;
- une partie des troupe bait la rush violette pour proxy bunker qui a juste
  la portée nécessaire pour l'atteindre en bas de la pente ;
- proxy rax la ruche marron + bunker (la rax wall le choke point);
- proxy factory la ruche rouge pour des tanks (la facto wall la choke des
  ultralisk, mais pas des zerglings, cependant l'IA ne réagit pas si on attaque
  que la ruche).

On va ignorer la plupart des autres batiments.

On ammène Duran à la balise juste avant que les ruches soient détruites pour
gagner du temps.

#### 3.2.1.4. Terran 04: Assault on Korhal

Objectifs :
- Détruire les Laboratoires de Physique ennemis ;
- OU ALORS détruire les Silos Nucléaires ennemis ;

(Selon l'objectif rempli, la mission suivante change de version.)

On va partir sur détruires les silos. Les *Wraith* (Ombres) vont clear assez
vite les tanks du dessus pendant que la factory pose un atelier et recherche le
mode siège.

Dès que c'est fait on soulève le Centre de Commandement (c'est super important),
et les autres building pendant que la facto fait un tank avec les ressources.

Les tanks sont placés à un spot ultra précis pour clear les silos avec le mode
siège sans aggro le reste de l'IA.

Les silos étant neutre, les ennemis ne cherchent pas à les défendre outre
mesure.

Le rax et le labo technique encaissent les coups du bunker pendant que le
Centre de Commandement va se poser à côté du dernier Silo à Missile, pour le
capturer.

#### 3.2.1.5. Terran 05: Emperor's Fall

#### 3.2.1.5.0. Général

Objectif : détruire le Centre de Commandement de Mengsk.

Cette mission a deux version, selon la façon dont la mission 04 a été terminée.

Si, lors de la mission 04 les *Silos Nucléaires* sont détruits,
la mission 05 sera la version **Ground Zero**.

À l'inverse si, lors de la mission 04 les *Laboratoires de Physique* sont
détruits, la mission 05 sera la version **Birds of War**

Il n'est pas nécessaire de faire les deux version pour valider la run.

#### 3.2.1.5.1. 05a: Ground Zero

En théorie on devrait faire l'autre version de la carte, cependant on
sélectionne cette version depuis le menu.

On va cheese le début du niveau avec des mines araignées précisément placées
pour éviter que des bombes nucléaires envoyées par des fantômes scripté ne
détruisent les renforts au moment où ils arrivent.

Cette phase est un peu rng due au comportement des mines araignées.

Le marine et les VCS sont mis de côtés pour body block les derniers fantômes
et éviter qu'ils ne posent un missile nucléaire (yes confirmé par shox).

On va alors bénéficier de ce surplus d'unité qui n'est pas censé être prévu par
la mission pour rusher le Centre de Commande de Mengsk. Les Wraiths (Ombres)
vont tanker les coups à la place des dropships.

#### 3.2.1.5.2. 05b: Birds of War

En théorie on devrait faire cette version de la carte, cependant on sélectionne
l'autre version depuis le menu.

#### 3.2.1.6. Terran 06: Emperor's Flight

Objectif : détruire le Centre de Commandement de Raynor.

Le début est scripté, mais du coup le comportement des unités est incontrôlable
du coup un peu RNG. Idéalement, on veut que 4 tanks restent en vie avec beaucoup
de PV et 3 c'est bien aussi mais un peu plus lent pour charger les dropship.
Il peut en rester 2 et c'est pas ouf.

Fun fact : l'IA protoss de cette mission ne s'active pas tant qu'on ne pose pas
un bâtiment. Du coup on ne pose pas de bâtiment.

On utilise le vaisseau scientifique pour mettre une matrice défensive sur un
transport ; utiliser les Valkyries qui restent pour tanker aussi et drop les
tanks et les marines sur le Centre de Commandement.

#### 3.2.1.7. Terran 07: Patriot's Blood

Objectifs :
- Trouver et tuer l'Admiral Stukov ;
- Duran doit survivre ;
- Éteindre le réacteur du disrupteur Psy avant qu'il ne surcharge.

Phase 1 :

- Grosso modo : on rushe.
- On essaye de garder les marines en vie pendant un temps et on les sacrifie
  ensuite.
- Normalement, les marines sont censés ouvrir les portes verrouillées en allant
  sur des balises. Cependant, si tous les marines sont morts, Duran peut ouvrir
  les portes lui même sans aller aux balises.
- On va rush le reste de la phase 1 avec Duran en tankant tant qu'on peut.

Phase 2 :

- On va prendre deux marines, qu'on va stimme régulièrement pour le bonus de
  vitesse
- Un groupe de marine + médic va attirer l'attention des Zerg.
- Un des marines va chercher un deuxième groupe de marines + médic pour faire
  pareil
- Le dernier marine fonce plus loin pour trigger le script pendant que le
  groupe rapplique.
- Manipulation des ennemis pour les attirer dans le piège.
- Un marine pour bait les terrans infestés (qui explosent au contact).
- Idéalement on veut garder les 2 médics en vie mais c'est pas facile.
- Grosso modo, on rush la fin de la mission.

#### 3.2.1.8. Terran 08: To Chain the Beast

Objectifs :
- Ammener un médic à chacune des 4 balises autour de l'Overmind ;
- Tuer les 3 cérébrates Zergs pour affaiblir les défenses de l'Overmind
  (optionel)

On va faire une bioball et des dropships.

Une rax et le labo technique sont soulevés pour les amener en haut de la carte
et tanker les colines de spores pour laisser passer des dropship. On prend 2 VSC
pour tanker quelques coups aussi.

On va faire tout ce qu'on peut pour drop les 4 médic sur les balises.

## 3.3. Brood War - Zerg Campaign (last update: AGDQ 2024)

### 3.3.0. Informations diverses

#### 3.3.0.0. Description générale

Plateforme : PC

Version : Remastered

Temps :
- 7thAce - WR : **1h 22m 03s** LRT - **1h 24m 29s** RTA

Type de run : c'est un RTS, du coup abus d'IA (un peu), de script (un peu) et
surtout de position / placement des unités spéciales.

#### 3.3.0.1. Règles

##### 3.3.0.1.0. Général

Remporter la campagne Zerg, par tous les moyens.

La mission secrète, *Dark Origin*, n'est pas requise.

Mettre en pause est autorisé, mais découragé.

Sauvegarder et charger est autorisé, tant qu'il s'agit de la même run.

##### 3.3.0.1.1. Chronomètre

Le chronomètre démarre lorsque:
- soit l’on appuie sur OK sur l'écran de sélection de mission avec la mission 1.
  sélectionnée ;
- soit l'on appuie sur OK en sélectionnant la campagne avec un profil vierge ;
- soit l'on clique sur Kerrigan dans le menu si la campagne Terran est terminée
  et la campagne Zerg n'a pas commencé.

Le chronomètre termine dès l'écran "Victoire!" apparait à la fin de la dernière
mission.

### 3.3.1. Route

#### 3.3.1.0. General

Commune à toute les missions, on va chercher la méthode la plus rapide pour
atteindre les objectifs de chaque scénarios, même si cela signifie terminer la
partie avec 1 unité à 1 pv.

#### 3.3.1.1. Zerg 01: Vile Disruption

Objectif(s) :
- Secourir les 7 Ruches.
- Vaincre les ennemis sur la dernière île.

Prendre les zerglings du départ (l'unité la plus rapide disponible de cette
mission) et les envoyer jusqu'à la fin aussi vite que possible en traversant les
7 Ruches pour activer les triggers.

#### 3.3.1.2. Zerg 02: Reign of Fire

Objectif(s) :
- Détruire le Disrupteur Psy.
- Le VCS doit survivre.

Prendre le VCS de départ et aller rallier (= activer les triggers) de toutes
les unités Zerg héroiques de la carte. Les unités héroiques sont des unités
spéciales de la campagne bien plus forte que les unités classiques.

On prend ces unités pour foncer au Disrupteur Psy au fond de la base Terran,
en splittant au préalable pour bait l'IA.

#### 3.3.1.3. Zerg 03: The Kel-Morian Combine

Objectif(s) :
- Miner 10 000 de minerai.
- Fenix doit survivre.

On va faire que des drones sur cette mission, l'attaque puis la défense seront
assurés par Fenix et les unités de départ.

On pète la première base pour faire la notre, puis on en pète deux de plus pour
en faire au total 3.

Il faut se protéger des attaques ennemies sachant que des attaques bonus trigger
à chaque pallier de 2000 de minerais récoltés.

#### 3.3.1.4. Zerg 04: The Liberation of Korhal

Objectif(s) : détruire toutes les structures ennemies.

La mission commence avec 10 000 de minerai.

Macro game clasique, surtout avec le kick-start de minerai : on pond trouzemille
couveuse, trouzemille drone, on fait les upgrade et on spamme les unités pour
déglinguer les ennemis.

#### 3.3.1.5. Zerg 05: True Colors

Objectf(s) :
- Détruire toutes les structures ennemies.
- Tuer Edmund Duke.
- Tuer Fenix.

La mission se déroule en 3 phases.

Phase 1 : attaque en traître

Pendant 6 minutes, c'est scripté, on peut attaquer allègrement nos anciens
alliés sans riposte. On va principalement cibler les bâtiments de production.

On va aussi builder l'économie et des unités pour se préparer à la phase 2.

Phase 2 : contre-attaque

Les ennemis lancent une contre-offensive.

Phase 3 : coup de grâce

On commence par dégommer la base du Protoss bleu et Fenix et ensuite la base du
Terran rouge et Edmund Duke.

Selon le runner, c'est la première des deux missions qui demandent le plus de
compétences de RTS de la campagne.

#### 3.3.1.6. Zerg 06: Fury of the Swarm

Objectif(s) : tuer tous les 30 scientifiques du DFT.

Au début de la mission, c'est scripté, il y a une très grosse attaque normalement
indéfendable qui détruit les deux bases du Nord, pour les besoins de l'histoire.

Cependant, il est possible de body-block les envahisseurs avec une technique à
base de zerglings. On prend les zerglings et on les aligne sur le pont en hold
position. Étrangement, le script ordonne les ennemis de simplement se déplacer
vers la base, pas de se déplacer en attaquant.

Cette technique sauve 1 minute 30. Au bout d'un moment, les zerglings ennemis
despawn et on peut tuer les lurker sans problèmes.

Ensuite, le runner construit une grosse armée de zerglings et les drop sur la
base du Terran blanc, moins bien défendue que les bases Zerg marron et orange
à côté.

#### 3.3.1.7. Zerg 07: Drawing of the Web

Objectif(s) :
- Amener Duran à chaque balise Zerg (5)
- Duran doit survivre

Cette mission est extrêment précise sur les déplacement. Duran peut se rendre
invisible à l'ennemi à moins qu'un détecteur soit à proximité. Les zerglings
sont utiliser pour détruire les détecteur afin que Duran puisse s'infiltrer
tranquilement.

#### 3.3.1.8. Zerg 08: To Slay the Beast

Objectif(s) :
- Détruire l'Overmind en utilisant des templiers noirs
- Il faut pouvoir créer des templiers noirs

On défend pendant un moment, à la suite de quoi on build des Ultralisk, des
templiers noirs et un saboteur, qu'on va drop à un point précis de la base
adverse. Le saboteur va spammer Dark Swarm qui protège contre les attaques à
distance pendant que les ultralisk / templiers noirs déglinguent l'Overmind.

#### 3.3.1.9. Zerg 09: The Reckoning

Objectif(s) :
- Détruire la base Protoss en moins de 30 minutes.
- Récupérer la Matriarche Raszagal et Zeratul

Au début de la mission, on va drop de l'armée et un drone à un point vide de la
carte pour pondre une base pour ramener des unités plus facilement par la suite.

Les gardiens grapillent les défenses une part une, jusqu'à ce que 3 destructeurs
soit détruit, à la suite de quoi on peut charger la base et tout dégommer.

#### 3.3.1.10. Secret: Dark Origin (mission secrète)

*Cette mission n'est débloquée que si la mission précédente est terminée avec
plus de 5 minutes restantes sur le minuteur.*

*Additionellement, en run usuelle, cette mission n'est pas jouée car on peut la
skip avec la sélection de mission par le menu. On verra le résultat de
l'incentive.*

Objectif(s) :
- Trouver des informations sur la signature énergétique
- Zeratul doit survivre

Mission un peu drôle, on va utiliser l'Archon Noir avec son Contrôle Mental pour
prendre le contrôle de 2 Tank et d'un Vaisseau laboratoire. Le vaisseau lab va
utiliser la Matrice Défensive sur Zeratul et un Archon Noir pour qu'ils tracent
à travers les défenses ennemis et mind control un transport.

On va monter Zeratul dans le transport pour bypass la carte et l'amener à la
dernière balise à la fin.

#### 3.3.1.11. Zerg 10: Omega

Objectif(s) : détruire toutes les bases ennemies (= les structures de prod).

Ben grosso modo, c'est une game de StarCraft quoi.

On va faire 60+ drones ("la plus grosse éco en speedrun de SC 1 ou 2") puis
faire des Hydra en continu (200+ au long de la mission) pour dégommer les bases
dans cet ordre :
- Protoss bleu ;
- Terran blanc (DFT) ;
- Terran rouge (Dominion).

Selon le runner, c'est la deuxième des deux missions qui demandent le plus de
compétences de RTS de la campagne.

# 4. Trivia

Beaucoup, et la flemme.

# 5. Références

Mission summaries:
- episode 5 : https://starcraft.fandom.com/wiki/StarCraft_Episode_V
- episode 6 : https://starcraft.fandom.com/wiki/StarCraft_Episode_VI

Notes du runner (shox) episode V : https://docs.google.com/document/d/1pWpWM4C6p6m5634tJhEeEtRs5JZSVyF1KWijZgDeNys/edit#heading=h.ch33c1w7zmq5

Timeline sc : https://starcraft.fandom.com/wiki/Timeline

Speedrun.com : https://www.speedrun.com/scbw/

Wikipedia : https://en.wikipedia.org/wiki/StarCraft
