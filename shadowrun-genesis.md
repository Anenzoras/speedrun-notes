# 0. Metadata

## 0.0.  Versioning

| Date          | Révision                          |
|-              |-                                  |
| 2022-06-25    | Update structurelle               |
| 2019-05-xx    | Création (RPGLB 2019) - Run Any%  |

# Info

Plateforme: Genesis

WR: **33m53s** par TascoDLX

# Route

PC : Joshua
Son frère Michael.

Class : Shaman (parce qu'on vend le stuff, du coup si on a besoin pour fight c'est les spells).

Go bar où Michael crèchait. Go vendre son stuff de départ au lieu de faire des runs. Go retour bar où on paye la facture et on a accès à des infos (sur Tabatha Shale, Caleb Brightmore et le Dr. Heaversheen).

Go voir Tabatha Shale qui nous parle d'un fixer où Michael a pris le job mais pas elle. Ici des gens débarque et vous savez pas ce qu'il advient d'elle. On trouve un holopix de David Owlfeather, un amérindien, ranger dans les alentours de Seattle.

Go voir le vendeur d'arme qui a équippé Michael. Des gens débarquent encore et changent de cible pour nous descendre. C'est plus rapide de game over donc on game over (on se fais rez à l'hopital le plus proche) (-12.5% de thune).

On va embaucher plusieurs runner (on va vendre le stuff du premier (Ricky) pour embaucher le deuxième).
Ricky va être très utile le long de la run pour son sort d'invisibilité qui fait qu'on ne voit pas les personnages.

Achat d'un passeport pour accéder à la zone où on peut trouver David.

David nous dit qu'il a peut d'info mais qu'il peut trouver un mec pour nous en dire plus sur ce qu'il se passe. Il nous demande un service sur retrouver un certain Aragorn (aucun rapport, il est fils unique).

Vendre le stuff de la shadowrunneuse (3000Y !) et achat de d'espace mémoire pour le decking.

Go virer la shadowrunneuse et en embaucher une autre (une deckeuse).

Go aller voir Caleb Brightmore (fixer). Il nous apprend que la mission de Michael se déroulait dans une zone interdite et la scene du crime lui est donc inaccessible. Il nous renvoit vers un elfe nommé Frosty qui était le contact de Michael.

Ensuite hack dans la matrice pour se faire mass thune ainsi que d'ouvrir la porte. On revend les infos qu'on a choppé dans la matrice au gonz.

Go trouver Mortimer qui a des infos sur Aragorn et sur Mako, un ennemi. Aragorn a tenté une run solo contre la corporation Mitsuhama et est retenu prisonier parce que la corporation pense qu'il sait des choses.
On va donc à Mitsuhama le liberer (étage 4 désactivation des caméras, étage 5 le gonz).

On ramène Aragorn à David (passeport again). Il nous dirige vers "Spirit Eyes", un shaman. On en profite pour aller à la hutte des guérisseurs, car on sait (en tant que runner) y trouver le meilleur ami de Michael, Stark, très mal en point. Le guérisseur ne peut le sauver que si on trouve de l'aide.

On va trouver Spirit Eyes, qui peux nous aider à trouver le tueur de Michael, il doit cependant faire un rituel complexe et l'on doit lui fournir des ingrédients/ressources. D'abord un corne de gargouille.

On va chercher l'aide du Dr. Heaversheen pour sauver Stark. Elle nous indique qu'il a besoin d'une transplantation du Coeur pour être sauvé, mais vue qu'il a plein d'implants, il faut un coeur cybernétique. Elle vous demande de voler le prototype dans le bâtiment de Fuchi (une autre corporation).

On va voller le coeur donc (étage 3) puis on va mourrir pour aller plus vite (on vire les gonz pour aller plus vite).

Stark rétabli, il a des trucs à faire du coup il se barre mais on peux le retrouver. On va le chercher, vendre son stuff (sauf les medkits), chasser une gargouille et livrer la corne.

Spirit Eyes a maintenant besoin de la fourrure d'un chien d'enfer (Hellhound). Qu'on va aller chasser de ce pas et qu'on l'ui rend.

Spirit Eyes a maintenant besoin de la plume d'un serpent à plume. Il faut que cette plume soit donnée et non prise.

On retourne dans la zone sauvage pour trouver le dragon. On passe chercher un cadeau pour les elfes chez qui on va devoir entrer plus tard.

On va chercher le dragon qui se fait attaquer, il nous donne la plume. Il dit aussi qu'il nous aidera plus tard, quand la situation sera désespérée.

On vire Stark et on meurt. On recherche Stark et on va maintenant faire la quête des elfes qui nous ont demandés de retrouver Ilène. On la libère de prison.

On retourne voir Spirit Eye. Les esprits nous parlent de Thon le gros ennemis du jeu.

On retourne chez les elfes ou ils nous parlent un peu du grand méchant et on peut enfin rencontrer Frosty.

Elle nous révèle le reste. Elle veut aussi nous mettre en contact avec Harlequin (!), qui veut empêcher Thon de venir dans ce monde.

On va interroger Mako, un mec qui a été au service du grand méchant et qui a dirigé l'opération pour tuer Michael. Il nous dirige vers Ito Ogami qui l'a remplacé par la suite.

On va tuer Ito aussi. On récupère quelques trucs dont de la thune et l'information que Renraku a une carte d'où se situe Thon. On va réembaucher Ricky et on va chercher la carte. On meurt pour aller plus vite.

On va enfin pouvoir rencontrer Harlequin. Il nous invite à chercher Vigore et Jarl, deux lieutenant de Thon pour trouver l'autre partie de la carte.

Réembaucher Ricky. Aller chercher l'autre partie de la carte, qu'on rend à Harlequin. Il nous indique où trouver le big boss.

On y va. On tue ce qu'on peut tuer. Le dragon vient nous aider. On gagne. Fin.

https://www.speedrun.com/shadowrun_genesis/guide/oc3xm
https://en.wikipedia.org/wiki/Shadowrun_(1994_video_game)
