# 0. Fiche technique

Sortie :
- 1993 - 1994 - 1995 - 1996 (plein de dates selons les pays et les plateformes) pour le jeu original
- 2015 pour realMyst: Masterpiece Edition

Plateforme : plein (jeu original), PC - Android - iOS (realMyst: ME)

Développé par : Cyan (et plein d'autres)

Édité par : Cyen (et plein d'autres)

Genre : First person point-and-click

Concepteur : Entre autres Rand Miller (Atrus)

Compositeur : Robyn Miller

# 1. Histoire

(Wipidia FTW:)

## 1.0. Prélude

Le joueur prend le rôle d'un inconnu qui tombe sur un livre étrange appelé
Myst. En lisant le livre, l'individu trouve une description très détaillée d'un
monde-île nommé Myst. En plaçant la main sur la dernière page, le joueur est
envoyé dans le monde décrit et n'a alors d'autre choix que d'explorer l'île.

## 1.1. Jeu

Au delà des méchanismes et diverses énigmes qui parsemment l'île, le joueur
trouve deux livres dans la bibliothèque, bâtiment central de l'île. En plaçant
une page déchirée correspondante trouvable à côté d'eux, le joueur réalise que
ce sont deux livres similaires à celui qui l'a ammené ici. Exception étant que
les livres sont incomplets et contiennent chacun déjà un individu enfermé à
l'intérieur. Ils peuvent s'exprimer via cette sorte d'image animée de fin de
livre.

Les individu sont frères, fils d'un certain Atrus et chacun accuse l'autre de l'avoir piégé dans le livre et déchiré les pages pour le garder enfermé. Chacun
encourage le joueur à le libérer, et promet une récompense le cas échéant. Sans
autre but, le joueur doit progresser dans 4 âges différents, en quelques sorte
des mondes créés de toutes pièce via des livres, et y récupérer les différentes
pages cachées à l'intérieur. Les pages bleues correspondent au livre de Sirrus
et les rouges à celui de Achenar.

De résolution d'énigme en exploration d'Âge, tous plus différents les uns que
les autres, on y découvre un peu l'histoire de ceux-ci ainsi que des frêres,
révélant petit à petit leur comportement avaricieux et violent. Achenar semble
être la brute du duo tandis que Sirrus est plutôt sournois.

Lorsque 5 pages correspondantes ont été apportés au même livre, le frêre révèle
alors l'existence d'une dernière, cachée derrière la cheminée. Pour activer le
mécanisme de cette cheminée, il faut une combinaison spécifique contenue dans
un livre de la bibliothèque dont il donne un indice pour trouver le livre et
la page donnant la bonne combinaison.

Derrière la cheminée, le joueur trouve bien deux pages : une rouge, une
bleue, mais également un livre blanc. En ouvrant le livre, on y
découvre Atrus dans l'âge de D'ni, qui nous urge de ne pas tenter de le
rejoindre. Il révèle que c'est lui qui a enfermé ses fils, car coupables de
terribles crimes, mais que l'ironie du destin a voulu que lui aussi se
retrouve piégé car il manque une page à son livre de liaison, arrachée par ses
fils.

Il nous demander de la lui ramener, suite à quoi il pourra finalement se
débarrasser des livres de ses fils. La page blanche est cachée sur l'île de Myst
et une fois transmise à Atrus, que l'on rejoint dans l'âge de D'Ni, il nous
explique qu'après avoir brûlé les livres de ses fils, il aurait une tâche à
nous confier si notre bonté le veux bien. En attendant, le joueur peut à ce
moment parcourir librement l'île de Myst.

Ceci est la bonne fin, canonique. Il y a 3 mauvaises fins. D'abord 1 pour
chaque fils lorsque l'on complète son livre. Le frêre sauvé nous enferme à sa
place et brûle le livre. Ensuite la dernière lorsque l'on rejoint Atrus
hâtivement sans récupérer sa page manquante et l'on se retrouve enfermé avec
lui.

# 2. Commentaire

## 2.0. Informations diverses

Runné sur PC

Catégorie : All Pages

Temps :
- WR: **12m59s810ms** par Gelly

Run du par coeur, mais avec realMyst:ME pas mal d'abus.

Chronomètre :
- Démarre à la première frame de déplacement sur l'île de Myst
- Termine quand Atrus tiens la page blanche dans sa main

All pages, règle : toutes les pages qui ne se trouvent pas derrière la cheminée doivent être récoltées et ammenées à leur livre respectifs. Il y a au total cinq pages bleues, cinq rouges et une blanche.

## 2.1. Mécaniques de jeu

Un seul écran de jeu. On clique là où on veut aller et l'écran suivant nous y
ammène.

Le gros du jeu passe par la résolution d'énigme. Il y a plein de mécanismes
éparpillés ça et là et d'indices laissés pour qu'on les comprenne.

realMyst a toutefois, contrairement au Myst original, un mode de déplacement
libre à l'aide du clavier et de la souris. Ce mode ne limite plus le joueur
aux écrans fixes.

## 2.2. Techniques

Le plus souvent, du par coeur et de l'optimisation principalement. Le mode
principal de déplacement est au clic car plus rapide.

Toutefois, le mode de déplacement libre n'est pas exempt d'utilité, car il y a
plusieurs endroit glitchés où l'on peut passer à traver le décor. Ces OoB sont
combiné au changement d'option pour rétablir le mode de déplacement au clic, ce
qui conduit le jeu à replacer le personnage au point d'écran fixe le plus
proche, à notre avantage.

## 2.3. Route

La run est vraiment ultra visuelle. À part les OoB qui rendent le tout funky,
les énigmes sont justes faites en connaissant déjà la solution.

Pour n'importe qui a joué au jeu, c'est standard sinon cf soluce (ça se voit
que j'ai la flemme et pas le temps, là ?).

# 3. Trivia

C'est l'un des deux concepteur du jeu et le CEO de Cyan qui joue le rôle
d'Atrus, qu'il reprendra par ailleurs dans le reste de la série.

# 4. Références

Lien speedrun.com : https://www.speedrun.com/realmyst

Wikipedia : https://en.wikipedia.org/wiki/Myst

# Annexe A. Versioning

| Date       | Révision             |
|------------|----------------------|
| 2020-01-05 | Création (AGDQ 2020) |
