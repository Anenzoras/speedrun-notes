# 0. Versioning

| Date          | Révision                                  |
|-              |-                                          |
| 2023-01-05    | Update (AGDQ 2023) - Run Nosferatu Any%   |
| 2020-01-09    | Création (AGDQ 2020) - Run Nosferatu Any% |

# 1. Fiche technique

Sortie :
- 2004-11

Plateforme : PC (Windows)

Développé par : Troika Games

Édité par : Activision

Genre : First person RPG

Compositeur : Rik Schaffer

Moteur : Source

# 2. Histoire

## 2.0. Prélude

Vampire: The Masquerade – Bloodlines prend place dans un Los Angeles début 21e
siècle. Le jeu utilise l'univers de jeu de rôle World of Darkness, qui dépeint
un monde dans lequel vampires, loup-garou, démons, et d'autres créatures
surnaturelles dessinent les contours de l'histoire de l'humanité.

Les vampires sont liés par un code pour maintenir leur secret, la Mascarade,
interdisant l'utilisation des capacités surnaturelles en présence d'humain,
ainsi que pour maintenir le peu d'humanité qu'il leur reste en évitant de tuer
des innocents. Les vampires sont divisés dans plusieurs clans avec leurs
caractéristiques et capacités propres.

Outre cela, les vampires sont eux-même une société ultra-politisée, luttant sans
arrêt pour le pouvoir.

## 2.1. Jeu

Le jeu permet de choisir un personnage parmi 7 clans : Brujah, Gangrel,
Malkavian, Nosferatu, Toreador, Tremere, Ventrue. On débute le jeu lors de notre
Étreinte, le rituel pour transformer un humain en Vampire. On apprend que notre
Parent (celui qui nous a transformé) a enfrein la Mascarade en ne demandant pas
l'autorisation au Prince de la Camarilla, une organisation luttant pour
appliquer systématiquement la Mascarade. Le Prince manque de nous tuer par
sanction mais, sous la pression de Niles Rogriguez, va finalement nous prendre
sous son aile.

Pour faire très court, il va nous confier plusieurs missions, entrecoupées par
les luttes de pouvoir de petits groupuscules vampirique comme des plus gros. Au
cours de la quête principale, on sera ammener à lutter contre ou coopérer
notamment avec :
- la Camarilla
- les Anarch, un groupe indépendant qui lutte pour la liberté des Vampires
- les Kuei-Jin un équivalent asiatique des Vampires
- le Sabbat, des vampires assoifés de pouvoir qui se fichent de la Mascarade

La quête principale nous conduit à rechercher un ancien sarcophage mésopotamien
qui a récemment été exhumé d'un ancien tombeau et qui, selon les rumeurs de la
nuit, contiendrait un antédiluvien, un puissant vampire de troisième génération.
Les pouvoirs des vampires sont déterminés par leur génération par rapport à
Cain, le Père des Vampires. La deuxième génération est éteinte, mais la
troisième est légendaire pour avoir laissé un héritage à divers endroits du
globe et surtout pour avoir enfanté les 11 clans vampiriques.
L'apparition de cet antédiluvien potentiel est pour de nombreux superstitieux
l'annonciateur de la Géhenne, l'Apocalypse qui mettra fin aux jours des
vampires.

L'on rencontrera également Beckett, un puissant vampire vieux de 300 ans qui
recherche l'Histoire des vampires, rarement consignée, et qui ne croit pas à
ces choses là. Individualiste, il conseillera au joueur de s'écarter des groupes
et des luttes de pouvoir, seul chemin vers lequel vivre sa non-vie dignement. Il
donnera aussi de nombreux détails sur l'univers du jeu. Il faut savoir que
Beckett est le personnage emblèmatique de White Wolf, le studio à l'origine
du jeu de rôle papier.

Après diverses péripéties et résolution de problèmes pour chaque groupe ou
personnage qui pourrait nous rapprocher du but, on finit par récuperer le
sarcophage, puis la clé pour l'ouvrir, non sans toutefois devoir faire un choix
profond d'allégeance envers l'un des groupes cités plus haut.

La fin révèle qu'à l'intérieur du sarcophage, Jack, notre mentor au début du
jeu, avait au préalable remplacé le corps tant convoité par des explosifs, qui
détruiront ceux qui l'ouvriront à ce moment là, fusse le joueur, le prince ou
les deux, selon la fin choisie.

Et en vrai y'a encore PLEIN de PUTAIN de DÉTAILS que j'ai pas cité parce que ce
jeu est trop bien 'faut y jouer pour l'histoire, c'est fou. Genre Cain serait
dans le jeu, OUI !

# 3. Commentaire

## 3.0. Général

### 3.0.0. Mécaniques de jeu

FPS, RPG.

On a des armes de càc, des armes à distance. Au càc on a une vue TPS.

Barre de santé, 3 types de dégâts.

Barre de sang (= mana pour les capacités vampiriques). On la recharge en...
buvant du sang.

Un feuille de perso avec des stats et des attributs, qui influe sur des
caractéristiques qui serviront dans les phases de gameplay.

Résolutions alternatives : persuasion, séduction, intimidation...

Pouvoirs vampiriques, 3 pouvoirs par clans (certains ont des déclinaisons du
coup plusieurs capacités), ainsi que Blood Buff, qui fixe les attributs
physiques à 5, commune elle à tout les clans.

Masquarade : on a le droit à 5 rupture de masquarade sinon game over. On brise
la masquarade en suçant le sang en public ou en utilisant une capacité
surnaturelle.

Humanité : si on tue des innocents, on perd en humanité. Plus l'humanité est
basse, plus on risque de perdre le contrôle et laisser place à la Bête, un état
de rage. Les checks de rage deviennent fréquent si l'on manque de sang.

### 3.0.1. Techniques émergentes

En fait y'en a plein. Ben ouais c'est le moteur Source.

#### 3.0.1.0. Général

Les personnages féminins se déplacent plus vite. 'me demandez pas pourquoi.

Comme beaucoup de jeu, l'accélération est dépendante du *framerate*, donc avec
un meilleur *framerate*, il est plus facile d'accumuler et conserver sa vitesse.

La plupart des animations toutefois jouent plus rapidement si les FPS sont bas.
Également, la physique peut être très capricieuse si les FPS sont élévés.

À l'instar de nombreux jeux *Source*, les runner vont manipuler le nombre
d'images par seconde à l'aide de la commande console `fps_max <number>`,
généralement avec une assignation de 2 touches, une pour baisser les FPS et
l'autre pour les rétablir.

#### 3.0.1.1. Mouvements

##### 3.0.1.1.0. All-stars

**Bunny Hopping** : on saute + straffe et on regarde dans la direction du
straffe = vitesse augmente. Slider nous faisant perdre de la friction, c'est
encore mieux accroupi.

Vampire utilise une version du moteur Source qui patche le **Bunny Hop** par
rapport à Half-Life, mais celui-ci reste toutefois exploitable.

**Strafe Jump** : exploitation d'un bogue du moteur Hammer (et donc Source), en
sautant + strafe + direction on peut vraiment accélérer son saut dans un
mouvement circulaire.

Cette technique est aussi patchée, peu exploitée hors **Bunny Hop**.

**Damage Boosting** : prendre des dégâts d'explosions ou d'arme à feu peut
propulser le personnage à l'opposé, et cela est évidemment exploitable.

##### 3.0.1.1.1. Spécificités

**Wigling** ou **Zigzaging** : les personnages se déplacent un peu plus vite en
avant avec des *strafes* gauches et droites répétitivement que sans.

**Wallstrafing** : avec *Avant* et un *strafe* contre un mur avec un certain
angle, les personnages peuvent accumuler plus de vitesse.

**Vent Boosting** : en sautant dans un conduit d'aération incliné vers le haut,
les personnages translatent leur vitesse vertical en vitesse horizontale
(c'est un **Corner boost** grosso modo).

**Climbing Ladders Fast** : avec un certain angle et un *strafe* alors qu'on
monte à une échelle, on peut grimper plus vite que sans.

**Crouching/Ducking & Hitbox Shifting** : s'accroupir change la hitbox du
personnage. Plus précisément, celle-ci se *déplace vers le haut* et non se
rétrécie lorsque le personnage est en l'air. Cela peut alors aider à monter sur
des plateformes autrement inaccessibles avec un saut normal, mais surtout
permet d'accrocher certains déclencheurs normalement impossible à atteindre
depuis l'emplacement du joueur. (voir le skip dans l'Asylum).

**Save/Load Clipping** : Alors que le personnage-joueur est coincé dans un
objet, après une sauvegarde et un chargement rapide, le personnage est déplacé
vers le haut. Il est possible de téléporter le personnage vers le haut si cette
séquence d'actions est répétée alors que le personnage est bloqué dans l'objet.

#### 3.0.1.2. Préparation de run

**Experience Glitch** : exclusif à la version "Official 1.1". Ce glitche permet
aux joueurs d'attribuer plus de points d'expériences à la création de personnage
que prévu par les développeurs. Il suffit juste de revenir au début de la
création de personnage alors que les points sont attribués pour qu'au retour sur
l'écran d'attribution, les points soient remboursés alors que les attributs sont
augmentés.

## 3.1. Run Nosferatu Any% (AGDQ 2023)

### 3.1.0 Informations diverses

#### 3.1.0.0. Description générale

Plateforme : PC.

Temps :
- WR: **29m 34s** RTA **27m 50s** IGT par speedfriend
- Runner - 2e: **31m 39s** RTA **29m 42s** IGT par Vysetra

Type de run : Run du glitch, du skill et du fun.
(Alt., run en 4 parties : ça va glitch, ça va vite, ça va skip, ça va time)

#### 3.1.0.1. Règles

##### 3.1.0.1.0. Général

Le jeu doit être joué sur la version officielle 1.1 ou 1.2.

Le jeu doit être patché pour être jouable. Speedrun.com fournit 3 fichiers à
mettre à jour (`vampire.exe`, `engine.dll`, et `shaderapidx9.dll`).

##### 3.1.0.1.2. Chronomètre

Le chronomètre démarre à l'appuie sur "Left-click to continue" sur la première
explication de tutoriel ("Use the mouse to look around." etc.).

Le chronomètre prend fin à la transition de niveau vers le loading screen quand
on active l'ascenseur pour monter chez Lacroix.

### 3.1.1. Route

#### 3.1.1.1. Général

C'est le bordel.

Avant même la run on prépare un personnage pété grâce au glitch des points de
compétences sur la fiche de perso.

#### 3.1.1.1. Tutoriel

On passe le tuto.

#### 3.1.1.2. Santa Monica

On va acheter les outils de crochetage, mais l'utilité principale ne sera pas
vraiment ce que vous pensez.

On rentre dans l'Asylum pour préparer un petit sequence break avec Jeannette,
on va faire la quête du pendentif de Thérèse sans l'avoir reçue puis en revenant
le séquence break s'enclenche et le jeu est confus sur le stade de la quête.

On fait la suite "normalement" jusqu'à l'entrepôt ou grâce à Obfuscate et des
saut bien placés on va très vite.

#### 3.1.1.3. Downtown

On va parler à Miles et l'envoyer chier.

On va faire toutes les quêtes principales jusqu'à la quête de recherche des
Nosferatu à peu près normalement, juste très très vite.

Exception faite du manoir de Grout, où on va manipuler un malkavien fou pour lui
sauter sur la tête puis en haut de la salle des lumières murales pour skip le
fait de faire le tour pour monter.

#### 3.1.1.4. Hollywood

##### 3.1.1.4.0. Nouvelle route (AGDQ 2023)

On va *sequence break*, bébé.

Normalement on doit mettre la main sur 2 cassettes qui contiennent des preuves
sur les Nosferatus avant d'être invité chez eux, mais en abusant de calculs de
collisions un peu foireux avec un tonneau, le gardien du cimetière Gary et un
coin de mur, on va passer à travers le décors à côté l'entrée secrète des
Nosferatus.

Ce *sequence break* s'appelle le **Warren Skip**.

##### 3.1.1.4.1. Ancienne route (AGDQ 2020)

Pareil, on va aller vite mais déjà on va sequence break pour la deuxième partie
de la cassette pour aller à l'étage du cyber café directement.

Après le boss Tzimice, on va faire le premier étage des égouts jusqu'à sortir et
rentrer par une porte fermée, mais avec le bon angle on peut l'attraper. Là, le
jeu suppose qu'on est rentré par la route normale, et nous warp au "bon" spawn.

On va ensuite turbovite vers les Nosferatu.

#### 3.1.1.5. Chinatown

En passant par dessus une barrière avec des saut du schnaps, on skip le reste du
jeu chez les Kuei-Jin en confusant le jeu au maximum. En tuant le boss Kuei-Jin,
qui intervient dans le segment de fin de jeu, ça déclenche tous les events de
fin de jeu et nous précipite vers le niveau de fin.

# 4. Trivias

Le secret de polichinelle du jeu concerne le conducteur de taxi, qui en tant que
mécanique de jeu sert surtout de *stage select* pour rejoindre les différentes
zones jouables du jeu. Il y a plusieurs indices *très* marqués qui indiquerait
qu'il s'agit en fait de Cain, qui observe sans doute vos actions et
influencerait fortement les événements :
- Le pouvoir *Apex* des Malkavian l'identifie comme un être surnaturel ;
- Dans l'épilogue du jeu, on l'apperçoit discuter avec Jack, le mentor, comme
  s'il avait observé le déroulement de tous les événements depuis le début ;
- Il y a plusieurs fichiers audios dans le jeu qui sont liés au personnage et
  qui contiennent le nom *cain*, comme descriptif.

Et, sinon, euh... plein d'autre trivias, je les ai plus en tête.

# 5. Références

Speedrun.com : https://www.speedrun.com/vtmb

Wiki speedrun : https://lurk.moe/vtmb/doku.php

Wikipedia : https://fr.wikipedia.org/wiki/Vampire:_The_Masquerade_-_Bloodlines
