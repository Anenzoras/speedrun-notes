# 0. Metadata

## 0.0.  Versioning

| Date          | Révision                                      |
|-              |-                                              |
| 2022-06-25    | Update (RPGLB 2023) - Run All Main Quests     |
| 2022-06-25    | Update structurelle                           |
| 2019-01-xx    | Création (AGDQ 2019) - Run All Main Quests    |

# 1. Fiche technique

Sortie :
- 2002-05-01 (Windows (en))
- 2002-09-05 (Windows (fr))
- 2002-06-06 (XBox (en))
- 2002-11-14 (XBox (fr))

Plateforme : PC, XBox

Développé par : Bethesda Softwork

Édité par : Ubi Soft Entertainment

Concepteur : Todd Howard

Compositeur : Jeremy Soule

Genre : Action-RPG

# 2. Histoire

## 2.0. Univers

*(Extrait de Wikipédia:)*

Morrowind se déroule dans l'Univers des Elder Scrolls, et partage donc le lore
avec le reste de la saga. Le jeu se situe pendant l'ère troisième, 20 ans après
Daggerfall et 6 ans avant Oblivion.

Le jeu se déroule sur l'île volcanique de Vvardenfell, appartenant au royaume de
Morrowind. Morrowind a un roi, mais il est faible. Chaque région est contrôlée
par sa baronnie locale : maison Hlaalu pour le Sud-Ouest avec la ville de
Balmora, maison Rédoran pour le Nord-Ouest avec Ald'rhun, maison Telvani pour le
Nord-Est avec la ville Sadrith Mora, elle-même divisée en fiefs. Le Sud-Est est
désert et ne semble contrôlé par personne.

Ces maisons divisées font face à un ennemi beaucoup plus puissant qu'elles, la
maison Dagoth, dont le siège est au cœur du volcan du Mont Écarlate. Une
barrière, le Rempart Intangible, a été érigée pour empêcher les monstres de
Dagoth de tout envahir.

Bien que l'univers des Elder Scrolls soit riche et marqué par un contenu
résolument épique, Morrowind se distingue par son caractère calme, parfois
contemplatif et voire parfois triste et mélancolique. Terre d'origine des
Dunmers, les Elfes Noirs, Morrowind est un territoire peuplé à cinquante pour
cent de Dunmers de souche et à cinquante pour cent d'étrangers.

## 2.1. All Main Quest

Morrowind contient de nombreuses quêtes diverses mais la quête principale se
concentre sur la lutte entre le Tribunal, un triumvirat d'êtres quasi-divins qui
règnent sur Morrowind, et la divinité Dagoth Ur. Autrefois allié du Tribunal,
Dagoth Ur et ses disciples, la Sixième Maison, étendent leur emprise depuis le
Mont Écarlate, le centre volcanic de Vvardenfell. Dagoth Ur a utilisé le Cœur de
Lorkhan, un artéfact très puissant, pour se rendre immortel et cherche
maintenant à chasser l'occupation de la Légion Impériale de Morrowind.

Après un orage et un étrange rêve, le personnage-joueur *(abrégé par la suite PC
pour player character)* commence sa quête dans la ville de Seyda Neen.
Initialement prisonier arrivant tout droit de l'empire à bord d'un bâteau, il
est libéré par l'empereur actuel de Tamriel, Uriel Septim VII. Le pc choisit à
ce moment son nom, son apparence, son sexe, sa race et sa classe, et reçoit une
mission impériale de remettre un paquet à un certain Caius Cosadès.

Une fois trouvé et le paquet remis, Cosadès nous recrute en temps que novice de
l'ordre des Lames, qui sont des soldats et espions d'élite au service de
l'empereur. Il envoie le PC enquêter sur des disparition et des secrets de
citoyens de Vvardenfell, qui concernent en particulier la Sixième Maison et les
prophéties des Cendrais (*Ashlanders*) sur Nérévar ou le Nérévarine. Il est plus
tard révélé que la libération du PC et sa mise sous les ordres de Cosadès était
dû à une intuition de l'empereur qui pensait que le PC pouvait être le
Nérévarine, la réincarnation d'un légendaire héros Dunmer nommé Indoril Nérévar.
Ou, dans le cas contraire, quelqu'un qui puisse être un imposteur convaincant
pour s'en servir comme pion politique. Le PC sera au fur et à mesure du jeu
chargé de découvrir les prophéties et de les accomplir pour finalement défaire
Dagoth Ur et son culte.

Les prophéties des peuples Dunmer nomades qui vivent dans les Terres Cendrées
prédisent que le Nérévarine accomplira sept prophéties. La première est qu'il
naîtra un jour certain de parents incertains. La deuxième, qu'il sera immunisé
à la maladie Corprus (peste des cendres), une maladie divine créée par Dagoth
Ur. Le PC ayant été choisi parce qu'il avait rempli la première condition finira
par devenir également immunisé à la maladie après l'avoir contracté et survécu à
un remède expérimental. Le PC s'attèle donc à remplir les suivante.

La troisième est un test pour trouver Lunétoile (Moon-and-Star), l'anneau
symbolique initialement porté par Nérévar qui tue instantanément son porteur si
ce n'est lui-même. Après avoir trouvé et équipé l'anneau, le PC reçoit une
vision d'Azura, l'ancien Prince Daedric de l'Aube et du Crépuscule, qui lui
confirme qu'il s'agit bien de la réincarnation de Nérévar.

La quatrième prophétie indique que le Nérévarine unira les Grandes Maisons
Dunmer et la cinquième qu'il unira les clans des Cendrais, tous sous sa banière.
Après avoir été déclaré "Hortator" par les maisons et "Nérévarine" par les
clans, le PC est officiellement nommé Nérévarine par le Tribunal du Temple qui
normalement persécute quiconque veuille se faire passer pour la réincarnation de
Nérévar.

Le Nérévarine est invité au palais du Dieu-Roi poète Vivec, une des trois
divinité qui forme le triumvirat, pour discuter de l'assaut contre la forteresse
de Dagoth Ur et de son culte au sein du Mont Écarlate. Vivec confie au PC le
gantelet "Garde Spectrale" (*Wraithguard*), un ancient artéfact Dwemer qui
permet d'utiliser les outils Broyeur et Lamentation (*Sunder* et *Keening*). Ces
anciens outils Dwemer permettent de canalyser la puissance du Cœur de Lorkhan,
ce qui explique le statut quasi-divin du triumvirat. Les outils peuvent
également détruire le Cœur, mais sans l'absence du gantelet, ils infligeront une
blessure mortelle à leur porteur.

Le joueur voyage dans le Mont Écarlate, jusqu'à la citadelle de Dagoth Ur, et
massacrant son culte, remplissant ainsi la sixième prophétie. Après avoir
confronté Dagoth Ur, le PC triomphe mais se rend compte que Dagoth Ur étant
immortel, il peut revenir d'entre les morts tant que le Cœur de Lorkhan est
intact. Cherchant au cœur même de la montagne, le PC trouve enfin le Cœur de
Lorkhan et le détruit, brisant ainsi le lien entre Dagoth Ur et ses pouvoirs.
La chambre dans laquelle le Cœur reposait ainsi que toute la caverne finit par
s'écrouler et en conséquence le Mont Écarlate est purifié de la maladie des
cendres, et la Sixième Maison disparaît. Le PC remplit ainsi la septième et
dernière prophétie. La suite de quête se termine sur une vision d'Azura
remerciant son champion de son aide.

# 3. Commentaire

## 3.0. Général

### 3.0.0. Mécaniques de jeu

Un Action-RPG qu'il est bien, avec :
- pléthore de compétences et des caractéristiques qui définissent notre
  personnage et ce qu'il peut faire
- une barre de vie
- une barre de mana
- une barre d'endurance
- des armes et des sorts.

### 3.0.1. Techniques émergentes

On se déplace 1.4x plus vite diagonalement donc on s'en sert généralement au
début quand on n'a pas une bonne vitesse.

Sortir des limites du jeu se fait de 2 façons :
- Maintenir une direction alors qu'on rentre dans une cellule d'intérieur (après
  un chargement, quoi)
- Se placer derrière un mur très fin et faire une sauvegarde rapide alors qu'on
  marche dans cette direction.

Le Menuing c'est important !

**Weapon Switching Glitch** (a.k.a. The Sunder/Keening Cheat) : étrange bug
quand on quickswitch d'armes avec Sunder ou Keening : des stats augmentent de
façon permanente.
Quand on quickswitch Keening, la vitesse et l'agilité augmentent.
Quand on quickswitch Sunder, la force, l'endurance et la chance augmentent.

**Version switch** : La run all quest (et peut-être d'autres) change de version
du jeu en cours de run (1.0 -> GoTY Edition). Ce n'est pas un exploit à
proprement parler, mais cela permet de sécuriser pleins de trucs en ce qui
concerne les bugs ou défauts de performance du jeu et d'utiliser des mécaniques
ajoutées par les patchs.

Conséquences :
- Empêche le *spell selecting bug* qui peut supprimer un sort du joueur
- Rends disponible le bouton "Max offer" dans les échanges avec les vendeurs
- Possible de mettre le niveau de difficulté du GoTY à -100 (plus facile de OS
  les mobs)
- Plus grande distance d'escorte des PNJ

## 3.1. All Main Quests (update RPGLB 2023)

### 3.1.0. Informations diverses

#### 3.1.0.0. Description générale

Plateforme : PC

Version du jeu : Hybrid (On fait le début en v1.0 et après Sunder avec la
version GOTY)

Catégorie : any% All main quests

Temps (RPGLB 2023) :
- Volvulus - WR : **12m 45s** LRT - **16m 50s** RTA
- musical_daredevil - 2ème : **16m 44s** LRT - **22m 04s** RTA

Style : Run du swag et du skill avec des techniques fumées et des glitchs.

#### 3.1.0.1. Règles

*(Tirées de speedrun.com)*

##### 3.1.0.1.0. Général

Il faut finir toutes les 19 quêtes de la trame principale, peu importe l'ordre.

Les quêtes sont les suivantes (accompagnées d'une numérotation pour plus tard
dans les notes) :
- Report to Caius Cosades (1.)
- Antabolis Informant (2.)
- Gra-Muzgob Informant (3.)
- Vivec Informants (4.)
- Zainsubani Informant (5.)
- Meet Sul-Matuul (6.)
- Sixth House Base (7.)
- Corprus Cure (8.)
- Mehra Milo and the Lost Prophecies (9.)
- The Path of the Incarnate (10.)
- Redoran Hortator (11-H1.)
- Hlaalu Hortator (11-H2.)
- Telvanni Hortator (11-H3.)
- Ahemmusa Nerevarine (11-C1.)
- Erabenimsun Nerevarine (11-C2.)
- Zainab Nerevarine (11-C3.)
- Urshilaku Nerevarine (11-C4.)
- Hortator and Nerevarine (12.)
- The Citadels of the Sixth House (13.)

Les mods ne sont pas autorisés.

Les scripts et les commandes de la console ne sont pas autorisées.

##### 3.1.0.1.1. Chronomètre

Le chronomètre démarre quand le joueur peut déplacer son personnage.

Le chronomètre termine après soit:
- en utilisant un "Rappel" vers là où est Azura,
- en perdant contrôle du personnage après s'être approché d'Azura.

### 3.1.1. Route

#### 3.1.1.0. Général

On commence le jeu en v1.0 (j'imagine que les glitches sont plus simples à
faire) et on passe sur la version GOTY après le **Weapon Switching** avec
Sunder.

On va toujours courir

On va utiliser à répétition un couple de sort/effets magiques appelés "Marque"
et "Rappel" qui permettent de pour le premier de placer une marque à un endroit
du monde et pour le dernier de s'y téléporter depuis n'importe où.

On utilise d'autres sorts de téléportation :
- "Intervention d'Almsivi" qui téléporte au temple le plus proche;
- "Intervention Divine" qui téléporte au culte impérial le plus proche.

Les runners vont généralement conserver la fenêtre de caractéristique ouverte
en jeu (mécanique de base du jeu) pour avoir toujours une indication sur l'état
de leurs caractéristique (notemment à cause du **Weapon Switching**).

Dès qu'on a suffisamment de Rapidité avec ledit glitch, la plupart des niveaux
en intérieur sont traversé via OoB (il suffit de quicksave/quickload en
marchant contre un mur).

#### 3.1.1.1. Caius Cosadès (1.)

##### 3.1.1.1.0. Libération

- Chara-création :
    - Sexe : Male (?)
    - Race : Rougegarde (+ rapide et Adrenaline Rush)
    - Signe astro : Destrier (+10 speed)
    - Classe : Aventurier (custom) pour choisir :
      - Intelligence (default) > Speed
      - Heavy Armor (default) > Athletics
      - Armorer (default) > Enchant
      - Long blade (default) > Acrobatics
      - Axe (default) > Speechcraft
    (Important car on va devoir level up par la suite et ça nous permet de level
    up plus vite)
- On pique le plat qu'on pose avant de se faire arrêter et qu'on reprend
- On prend l'anneau de Fargoth, Adrenaline Rush, on parle à Sellus pour la
  mission
- On vend l'anneau, on part vendre le truc qu'on a piqué et avec l'argent on
  achète 2 parchemins d'intervention d'Almsivi, un parchemin d'unlock, un
  crochet
- On court vers Tarhiel, on récupère ses parchemins (Acrobatie +1000 pour 7s) et
- TP temple (Balmora).

##### 3.1.1.1.1. Setup et Rapport to Cosadès (1.)

- Depuis Balmora : parchemin jump pour aller à Odrosal. On ramasse une lance
  dwemer et Keening pour le **Weapon Switching** (et la quête principale)
- **Weapon Switching** pendant qu'on va à Vemynal chercher Sunder
- OOB juste en marchant en arrière au chargement parce qu'on va vite.
- On tape avec le lockpick pour pas péter d'armes (durabilité).
- On récupère le stuff de Dagoth Vemyn : entre autres Sunder et une amulette qui
  permet entre autre de léviter.
- **Weapon Switching** avec Sunder (on drop Keening pour pas overdo it) pour
  être full pété.
- VERSION CHANGE: quicksave, on quitte le jeu v1.0, copie de sauvegarde dans le
  dossier des sauvegarde version GOTY et lancement du jeu version GOTY.
- TP temple avec le deuxième parchemin
- Levitation avec l'anneau + rush vers Caldera, Manoir de Ghorak
- OOB into vol de stuff into revente de ce stuff dans le même temps
- Go to Verick Gemain le vendeur en face pour vendre le reste et acheter une
  amulette de Rappel des flêches et un arc.
- Go back to Balmora, Temple pour :
  - acheter un sort d'Intervention d'Almsivi et un sort de Marque et Fortitude
  - créer un sort de buff (+200 Speed AOE, +100 Personality Self)
  Le sort est utile pour avoir une disposition max avec tous les PNJ et donc
  faire certaines quêtes plus vite et le speed boost pour une quête d'escorte.
- Go to échassier des marais Balmora, posage de marque
- Go to Seyda Neen récupérer un objet de quête à Andrano Ancestral Tomb pour une
  quête qu'on a pas encore (Crâne de Llevule Andrano pour l'informateur
  Gra-Muzgob)
- Rapport à Cosadès (fin de la première Main Quest)

#### 3.1.1.2. Antabolis l'informateur (2.)

- Marque chez Cosadès
- Go to Arkngthand pour chercher une boîte à puzzle Dwemer
- Recall (Cosadès)
- Go guilde des guerriers pour Antabolis pour la quête
- Recall (Cosadès)
- Rapport Cosadès
- Train Speechcraft (Eloquence) pour level up (niveau 3).

#### 3.1.1.3. Gra-Muzgob l'informatrice (3.)

- Go mage guild, vendre les 2 amulettes de Dagoth Vemyn, acheter une pierre
  d'âme (Dremora) et 2 parchemin d'Intervention Divine
- Go Gra-Muzgob (on lui file le crâne)
- Achat du sort Lévitation
- TP via guilde des mages vers Ald-ruhn
- TP temple (idéallement initiée avant le dialogue précédent)
- Go to auberge d'Ald-ruhn pour acheter le sort "Commanding Touch" qui permet
  (entre autre) de forcer les gens à suivre le joueur
- Enchantement des bottes avec ce spell pour pouvoir le caster rapidement plus
  tard
- Recall
- Level 3 pour la suite
- Rapport à Cosadès

#### 3.1.1.4. Vivec Informants (4.)

- Go to Échassier
- Go to Vivec
- Informant Huleeya : On gruge le jeu avec les bottes enchantées (on devrait
  normalement l'escorter)
- Informante Addhiranirr : On tue le mec des taxes pour que l'informante nous
  parle.
- TP Temple
- Informante Mehra Milo : On va piquer le livre "Progress of Truth" et lui
  parler
- Recall (Cosadès)

#### 3.1.1.5. Zainsubani Informant (5.)

- Échassier to Ald'ruhn
- Bonjour Zainsubani, donne tes infos, au revoir
- Recall (Cosadès)

#### 3.1.1.6. Meet Sul-Matuul (6.)

- Échassier to Ald'ruhn
- Échassier to Khuul
- Go to Urshilaku
- Marque avant d'entrer
- Récupérer l'arc "Bonebiter Bow of Sul-Senipul" pour juste après
- Recall (devant Urshilaku)
- On parle à Zabamund pour parler à Sul-Matuul
- On parle à Sul-Matuul, on lui file l'arc.
- Marque (Sul-Matuul)
- On parle à Nibani Maesa

#### 3.1.1.7. Le bordel commence

À partir de maintenant, la run qui suivait la main quest de manière à peu près
linéaire va commencer à partir dans tous les sens pour compléter des objectifs
des quêtes avant même de les recevoir et par conséquent faire pleins de quêtes
en même temps.

- Go to Cavern of the Incarnate pour l'anneau "Moon-and-Star" (10. The Path of
  the Incarnate, la quête est finie normalement)
- Go to Nerano Ancestral Tomb pour tuer le vampire Calvario (11-C3. Zainab
  Nerevarine)
- Go to Zainab Camp pour parler à Kaushad et faire avancer cette quête (11-C3.
  Zainab Nerevarine)
- Go to Tel Vos tuer Aryon (methode alternative pour la quête 11-H3. Telvanni
  Hortator où une méthode acceptée par la maison Telvanni pour résoudre un
  grief c'est par le meurtre)
- Go to Tel Mora tuer Mistress Dratha (11-H3. Telvanni Hortator)
- Go to Ahemmusa Camp pour parler à Sinnammu Mirpal et GRUGER la quête
  d'escorte avec les bottes de "Command" (11-C1. Ahemmusa Nerevarine)
- Recall (Sul-Matuul)
- Parler à Sul-Matuul pour finir la quête 11-C4. Urshilaku Nerevarine
- TP Temple (Gnisis)
- Échassier des marais vers Ald-ruhn
- Go to Manor District / Venim Manor pour parler à Varvur Sarethi et le libérer
  (la tech des bottes ne marche pas, du coup on la buff avec le spell qu'on a
  créé) (11-H1. Redoran Hortator)
- Marque (Ald-ruhn / Manor District) pendant que que Sarethi court
- Go to Athyn Sarethi pour lui rendre la quête d'escorte et valider son
  approbation pour devenir Hortator (11-H1. Redoran Hortator)
- Go demander l'approbation des autres conseillers de la maison Redoran
  entrecoupé par des Recall pour aller plus vite (11-H1. Redoran Hortator)
- Go provoquer en duel Bolvyn Venim (11-H1. Redoran Hortator)
- Échassier des marais vers Balmora
- Marque (Cosadès)
- Parler à Cosadès (fin: 6. Meet Sul-Matuul et début: 7. Sixth House Base)
- Go to Ilunibi Caverns pour tuer Dagoth Gares et contracter la Peste des
  Cendres (le Corprus) (7. Sixth House Base)
- Recall (Cosadès)
- Parler à Cosadès (7. Sixth House Base et 8. Corprus Cure)
- Échassier des marais vers Vivec
- Marque (Vivec)
- Échassier des marais vers Molag Mar
- Go to Erabenimsun Camp tuer Ulath-Pal, Ahaz, Ranabi, Ashu-Ahhe et persuader
  Gulakhan Han-Ammu de devenir le nouvel Ashkhan du clan (11-C2. Erabenimsun
  Nerevarine)
- Go to Tel Fyr parler à Divayth Fyr puis Yagrum Bagarn puis re Divayth Fyr
  et avoir une potion pour être guéri de la peste (8. Corprus Cure)
- TP Culte impérial (Wolverine Hall)
- Go to Sadrith Mora / Tel Naga pour tuer Master Neloth (11-H3. Telvanni
  Hortator)
- Go to Tel Aruhn
- Go tuer Gothren (11-H3. Telvanni Hortator)
- Go libérer Falura Llervu de manière non conventionnelle en crochetant la
  serrure de la cage et on gruge l'escorte encore avec les bottes de "Command"
  (11-C3. Zainab Nerevarine)
- Go to Zainab Camp pour parler à Kaushad et finir la quête (11-C3. Zainab
  Nerevarine)
- Recall (Vivec (Échassier))
- Échassier des marais vers Balmora
- Go parler à Caius Cosadès (8. Corprus Cure et 9. Mehra Milo and the Lost
  Prophecies)
- Recall (Vivec (Échassier))
- Go Dren Plantation tuer Orvas Dren (11-H2. Hlaalu Hortator)
- Go to Ules Manor parler à Nevena Ules (11-H2. Hlaalu Hortator)
- Go to Vivec / Arena pour tuer Bolvyn Venim (11-H1. Redoran Hortator)
- Go to Vivec / St. Olms Canton Plaza / Yngling Manor pour tuer Yngling
  Half-Troll parce que flemme de payer la somme qu'il veut (11-H2. Hlaalu
  Hortator)
- Go to Vivec / St. Olms Canton Plaza / Haunted Manor pour parler à Dram Bero
  (11-H2. Hlaalu Hortator)
- TP Temple (Vivec / Temple)
- Go to Omani Manor parler à Velanda Omani (11-H2. Hlaalu Hortator)
- TP Temple (Vivec / Temple)
- Go to Ministry of Truth pour aller libérer Mehra Milo (9. Mehra Milo and the
  Lost Prophecies)
- TP Temple (Vivec / Temple)
- Go Ebonheart pour parler à Blatta pour pouvoir aller à Holamayan (9. Mehra
  Milo and the Lost Prophecies)
- Voyage en bâteau vers Holamayan pour parler à Gilvas Barelo (9. Mehra Milo and
  the Lost Prophecies)
- Recall (Vivec)
- Voyage en bâteau vers Tel Branora
- Go parler à Therana, la seule survivante des Telvanni pour devenir Hortator
  (11-H3. Telvanni Hortator)
- Recall (Vivec)
- Échassier des marais vers Balmora puis vers Ald-ruhn
- Go parler à Athyn Sarethi pour devenir Hortator (11-H1. Redoran Hortator)
- Recall (Vivec)
- Go Vivec / Vivec Hlaalu Plaza / Curio Manor pour parler à Crassius Curio et
  devenir Hortator (11-H2. Hlaalu Hortator)

#### 3.1.1.8. La fin

C'est bon, le reste est dans l'ordre (un peu obligé)

- Go to Vivec / Temple Canton pour parler à Tholer Saryoni (12. Hortator and
  Nerevarine)
- Go to Vivec / Palace of Vivec pour parler à Vivec et recevoir Wraithgard
  (12. Hortator and Nerevarine)
- Échassier des marais vers Balmora puis Ald-ruhn
- Go to Dagoth Ur's Citadel pour finir le jeu (13. The Citadels of the Sixth
  House)
- Time Soon TM

# 4. Trivia

Beaucoup trop, demandez à Synahel.

# 5. Références

Wikipedia : https://fr.wikipedia.org/wiki/The_Elder_Scrolls_III:_Morrowind

Wiki ultra complet : https://en.uesp.net/wiki/Morrowind:Morrowind

Timeline Elder Scrolls : https://www.ign.com/wikis/elder-scrolls-online/Elder_Scrolls_Timeline

Lien speedrun.com : https://www.speedrun.com/morrowind
