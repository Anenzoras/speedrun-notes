# 0. Metadata

## 0.0. Versioning

| Date       | Révision                                                     |
|-           |-                                                             |
| 2022-06-25 | Création (SGDQ 2022) - Run Inbound No Saveload Abuse (TAS)   |

# 1. Fiche technique

Sortie initiale : 19 avril 2011

Plateforme(s) : Linux, OS X, Windows, PlayStation 3, Xbox 360, Nintendo Switch

Développé par : Valve Corporation

Édité par : Valve Corporation

Distributeur :  Electronic Arts (boîte)
                Valve Corporation (Steam)

Compositeur :   Mike Morasky,
                Jonathan Coulton,
                The National

Genre : Puzzle-plateformer

# 2. Histoire

(Merci Wiiiiikiiipeeediiiiiaaaa !)

## 2.0. Contexte

La série Portal se déroule dans le même univers que Half-Life qui est une série de jeux vidéo de tir à la première personne développé par le même éditeur, Valve. Les événements de Portal se passent entre le premier et le deuxième opus des Half-Life. L'histoire de Portal 2 se déroule, quant à elle, « longtemps après » les événements de Portal et de Half-Life 2.

Avant Portal, le laboratoire Aperture Science menait des expériences déterminant la capacité des sujets humains à naviguer en toute sécurité dans de dangereuses « chambres de test », jusqu'à ce que l'intelligence artificielle régissant le laboratoire, GLaDOS, libère une neurotoxine mortelle à travers tout le centre, tuant sujets de test et scientifiques, à l'exception de Doug Rattmann. À la fin du premier opus, la protagoniste Chell détruit GLaDOS et échappe momentanément à l'établissement. Mais elle y est reconduite par une forme invisible à la voix robotique, identifiée plus tard par le scénariste Erik Wolpaw comme le « Robot Escorteur ». Une bande dessinée promotionnelle, Lab Rat, montre l'ex-employé d'Aperture Science, Doug Rattmann, qui a utilisé des graffitis pour guider le joueur dans Portal, en train de placer Chell en biostase pour lui sauver la vie, jusqu'au début de Portal 2.

## 2.1. Campagne Solo

Chell se réveille dans ce qui semble être une chambre d'hôtel. Il s'agit en fait d'une chambre de relaxation longue durée. Une voix de robot la guide à travers un test cognitif avant qu'elle ne soit rendormie. Lorsqu'elle se réveille de nouveau, de nombreuses années se sont écoulées et le laboratoire d'Aperture Science est délabré et envahi par la végétation. Wheatley, un robot chargé de l'entretien du personnel en stase, déplace la salle, une des centaines stockées dans un immense entrepôt, et aide la protagoniste à s'échapper en passant par des salles de tests. Durant leur fuite, ils réveillent accidentellement GLaDOS (principalement à cause de Wheatley) qui s'empresse de les séparer et de reconstruire le laboratoire.

Cela fait, GLaDOS soumet Chell à de nouvelles courses d'obstacles jusqu'à ce que Wheatley la fasse s'échapper et sabote la production de la neurotoxine ainsi que l'usine de fabrication des tourelles avant de se confronter à GLaDOS, après quoi Chell effectue un « transfert de noyau » en plaçant Wheatley à la tête du laboratoire. Cependant, le robot devient ivre de puissance et affirme avoir fait tout le travail tandis que Chell n'a rien fait, ce à quoi GLaDOS, maintenant impuissante, répond en défendant Chell. Wheatley place ensuite GLaDOS sur un module alimenté par une pomme de terre. GLaDOS déclare alors que Wheatley est une « sphère d'amortissement de l'intelligence », qui produit en grande quantité des pensées illogiques, la rendant moins intelligente, afin d'empêcher GLaDOS de tenter d'éliminer toute vie au sein du centre. Refusant d'admettre ceci, Wheatley pousse Chell et GLaDOS dans une cage d'ascenseur menant au plus bas niveau des laboratoires, des kilomètres sous terre. À partir de là, Chell monte à travers les laboratoires en construction et les décors passent lentement du style des années 1950 à celui vu au début du jeu. Des enregistrements audio du fondateur d'Aperture Science, Cave Johnson, sont diffusés périodiquement et apprennent au joueur que Johnson est devenu de plus en plus aigri et dérangé au fur et à mesure que son entreprise perdait argent et prestige à cause de l'espionnage industriel de Black Mesa, la société de Half-Life et qu'il est mort empoisonné par de la poussière de lune, après quoi son assistante Caroline est devenue cobaye pour une expérience consistant à transférer l'esprit humain à l'intérieur d'un ordinateur, et elle est finalement devenue GLaDOS. Chell et GLaDOS s'unissent alors à contre-cœur pour arrêter Wheatley avant que son incompétence détruise les laboratoires. Au cours de leur avancée, GLaDOS se rend compte, troublée, de sa véritable identité, celle de Caroline.

Ainsi Chell et GLaDOS reviennent à la zone moderne et font face à Wheatley qui est contrôlé par le logiciel de GLaDOS et leur font passer des tests mortels auxquels Chell échappe. Pendant leur confrontation finale, Chell réussit à brancher trois processeurs corrompus sur Wheatley, déclenchant la procédure de « transfert de noyau » et permettant à GLaDOS de se brancher comme processeur de substitut. Le réacteur nucléaire du laboratoire étant sur le point d'exploser, le toit s'effondre laissant place au ciel de la nuit permettant à Chell de placer un portail sur la surface de la Lune. Ce portail crée une dépressurisation aspirant Wheatley et Chell à l'intérieur de l'autre portail, présent dans la chambre de test. GLaDOS ramène Chell sur Terre, où elle s'évanouit, et laisse Wheatley en orbite dans l'espace.

Lorsque Chell se réveille, GLaDOS affirme avoir découvert de précieuses leçons sur l'humanité en étant Caroline, puis elle supprime les fichiers de Caroline et retourne à son attitude habituelle. Cependant, partant du postulat que la meilleure solution est souvent la plus simple, elle refuse de tuer Chell, ayant du mal à le faire, et la libère. Chell traverse alors le bâtiment au moyen d'un ascenseur bercée par la chanson Cara Mia Addio diffusée par les tourelles robotiques des laboratoires. À la surface, elle se retrouve dans un champ de blé après être sortie d'un hangar en tôle ondulée, puis un cube carbonisé, que le joueur avait été obligé de détruire alors qu'il avait été un compagnon dans le premier opus, est expulsé par la porte derrière elle.

La scène après le générique montre Wheatley qui, flottant désespérément dans l'espace avec un autre processeur corrompu obsédé par l'espace autour de lui, regrette sincèrement d'avoir trahi Chell et ce malgré le fait qu'il a été envoyé dans l'espace.

# 3. Commentaire

## 3.0. Général

### 3.0.0. Mécaniques de jeu

Puzzle-plateformer en vue à la première personne.

On peut sauter.

On peut s'accroupir.

On peut combiner les 2 pour des sauts accroupis.

On peut prendre des objets en main et les jeter.

Des interrupteur, des bumpers...

Et surtout : DES PORTAILS.

La seule "arme" du jeu est un *Portal Gun*, un objet qui permet de placer des portails bidirectionels sur des surface spécifiques.

Le possède une mécanique de lasers qui, bien qu'ils puissent blesser le joueur, peuvent être envoyé dans des récepteurs spécifiques pour activer des mécanismes. Les lasers peuvent être envoyé dans les portails.

Le jeu possède aussi une mécanique de pont lumineux, une plateforme semi-transparente bleue sur laquelle le joueur peut entrer en collision (et marcher, si sur le sol). Le pont lumineux peut être envoyé dans les portails.

Vers la moitié du jeu, une mécanique de gel est introduite. Tous les gels sont appliqués sur des surfaces et peuvent être enlevé avec de l'eau. Le gel bleu amplifie les sauts et permet de rebondir, le gel orange permet d'augmenter sa vitesse maximale et de prendre beaucoup d'inertie et le gel blanc permet de placer des portails sur des surfaces normalement non-conforme. Tous les gels peuvent être envoyés dans les portails.

### 3.0.1. Techniques émergentes

En fait y'en a plein. Ben ouais c'est le moteur Source.

*All time legendary tricks*

**Bunny hoping** : on saute + straffe et on regarde dans la direction du straffe = vitesse augmente. Slider nous faisant perdre de la friction, c'est encore mieux accroupi.

Cette technique est patchée dans Portal 2. Elle est encore utilisable mais le gain de vitesse est incroyablement réduit.

**Straffe Jump** : exploitation d'un bogue du moteur Hammer (et donc Source), en sautant + straffe + direction on peut vraiment accélérer son saut dans un mouvement circulaire.

Cette technique est aussi patchée, de la même manière.

---

**Accelerated Back Hopping (ou ABH)** (et assimilés) : lorsque Valve a patché le bunny hoping dans le moteur Source, ils ont involontairement introduit un glitch dans le moteur qui, lorsque le personnage dépasse une vitesse absolue, le jeu essaye de pousser le joueur :
- Si le joueur se déplace dans une direction, le jeu le pousse dans la direction opposée ;
- Si le joueur ne presse pas de touche de déplacement, le jeu le pousse dans la dernière direction de poussée enregistrée.

C'était la technique de déplacement phare de Portal 1, mais complètement patchée dans Portal 2.

**Button Save Glitch** : Avec un save/load au moment précis où l'on active un interrupteur, au sortir du chargement l'état de l'interrupteur est activé même si aucun objet physique n'est en contact avec lui.

**Corner boost** : quand on prend certains angles avec une haute vitesse, cela permet de transférer la vitesse horizontale en vitesse verticale et inversement, selon angles et direction d'arrivée.

**Collision Boosting** : terme un peu plus général pour translater sa vitesse en rentrant dans des objets ou surfaces.

**Excursion Funnel Flying** : aussi appelé **ExFu** ou encore **Crouch Flying Glitch** ou **CFG**, c'est un glitch qui permet de conserver l'état "En vol" alors que le joueur n'est plus dans une cheminée de vol (les tourbillons bleus ou oranges).

Pour activer le glitch :
- Marcher ou tomber dans une cheminée.
- Attendre d'avoir atteint le plus loin du trajet et être bloqué et attendre que la vitesse (consultable avec la commande `cl_showpos`) soit 0.
- Ensuite, changer son état d'accroupissement (passer de debout à accroupi ou inversement) et sortir de la cheminée.
- Une fois sorti, le personnage ne sera plus soumis à la gravité jusqu'à soit :
    - réentrer une cheminée;
    - enlever le portail qui a permis de manipuler la cheminée (si c'est le cas);
    - changer l'état d'accroupissement.
- Toutefois, faire un `save/load` alors que le glitch est actif permet par la suite de s'accroupir ou se redresser sans désactiver le glitch.

Notes sur le déplacement pendans le vol :
- On peut se déplacer librement en gravité nulle avec des air-strafes. Le cap de vitesse de 300 UPS est toujours présent par contre, donc dépasser cette vitesse fera perdre tout air control et la seule façon de modifier son inertie est de toucher un mur ou tout autre surface.
- La plupart des moyen de déplacement en run va impliquer des collision pour translater sa vitessse.

(Plus d'info : https://wiki.sourceruns.org/wiki/Excursion_Funnel_Flying)

**Invincibility Glitch** : lors de la salle "Wakeup" qui se situe à la toute fin du chapitre 1, les speedrunners ont trouvé une méthode très avancée pour pouvoir skip 6s (oui, 6) de fin de cutscene. Sauf qu'il y a un effet kiss-cool : quand la cutscene démarre, le jeu définit le joueur comme invulnérable, chose qui devrait s'inverser lorsque la cutscene termine. Cependant, le fait de court-circuiter la fin de la cutscene n'annule alors pas l'invulnérabilité, qui est conservée jusqu'à la fin du jeu.

(Plus d'info : https://www.youtube.com/watch?v=wYHMzPKktXg)

**LAPMG** : le **Large Angled Panel Momentum Glitch** est une façon de gagner beaucoup de vitesse très rapidement, modérément difficile. Elle nécessite de placer un portail sur une surface à 45° et un autre duquel le joueur désire être éjecté. Il faut rentrer dans le portail incliné et se déplacer en arrière pour revenir ; cela a pour effet de se faire projeter hors de l'autre portail.

**Laser Switch Glitch** : ce glitch repose sur le fait qu'un récepteur à laser reste activé pendant une fraction de seconde si un laser est dépolacé du récepteur. On peut abuser de cette mécanique en déplaçant un laser très vite sur un autre récepteur pour activer une porte qui a besoin que les 2 récepteurs soient actifs simultanément.

**Objects Edge Glitch** : technique qui consiste à faire passer un prop déplaçable à travers d'autres prop statiques au sortir d'un portail.

**Pause abuse** : état pendant lequel le jeu est apparemment en pause mais où la physique continue de fonctionner. Elle permet dans certaines situations d'amplifier un mouvement, comme un saut par exemple.

**Portal Bumping** : permet de placer un portail derrière un autre portail sur le même plan (et donc techniquement passer à travers le mur).
Note : c'est possible uniquement si la surface opposée est à 32 hammer units de distance ou moins du premier portal.

**Portal Stand** : technique qui consiste à revenir dans un portail pendant qu'un projectile à portail de l'autre couleur est projeté. Permet d'enchaîner les déplacements sans vraiment faire les énigmes correctement.

**Propulsion Gel Crouch Boost Glitch** : technique qui permet d'atteindre plus rapidement la vitesse maximale sur le gel accélérateur. Pour ce faire, il faut s'accroupir, marcher 0.25s sur le gel et se redresser. Cela permet de réduire la durée d'accélération d'une seconde (oui).

**Reportaling** : c'est un glitch qui existe dans les 2 jeux. À cause d'une mécanique qui tente de déplacer le joueur hors d'un portail alors que celui-ci ferme, le joueur sera projeté avec une forte vitesse.
Pour pouvoir exécuter ce glitch dans Portal 2, il faut utiliser une surface verticale ou inclinée pour pouvoir avoir ce boost. Cela ne fonctionne pas si les deux portails sont horizontaux.
Il faut placer les deux portails au sol, sans y entrer au début. Il faut ensuite choisir celui qui nous éjectera. Ensuite faire face à un mur ou pente sur lequel on peut placer un portail et rentrer dans le portail choisi. Juste avant de passer au travers, replacer ce portail sur le mur d'en face. Si c'est fait correctement, le jeu propulsera significativement le joueur vers le haut.

Extension du glitch : **Super Reportal**

En gros c'est un **Reportal** mais bien plus rapide et énervé à faire. Tout repose sur parvenir à se coincer dans une surface avec un portail, puis lancer plusieurs fois le portail dans lequel on n'est pas coincé (ce que j'appelle la *charge*), puis déplacer le portail dans lequel on est coincé sur une surface depuis laquelle on veut être propulsée. On est alors éjecté de ce portail avec une vitesse proportionnelle au nombre de fois où on a *chargé* le saut (200 u/s par portail lancé apparemment).

**Save/load buffering** : l'action de sauvegarder et charger une partie en même temps. Cela a plusieurs effets kiss-cool notamment quand le personnage-joueur est coincé dans un item, cela permet de le faire passer à travers des "Brush" (grosso modo les surfaces) et donc souvent le sol, pour passer outre des parties du jeu.

**Stuck launch** : consiste en se coincer entre 2 props statiques pour accroître sa vélocité. Le jeu considère que l'on tombe mais le moteur ne change pas la position du joueur ce qui fait qu'au moment de se décoincer la vitesse verticale vers le bas est très grande. On l'utilise généralement avec un portail pour se propulser quelque part.

**Wallstrafing** : on se déplace un peu plus vite en strafant contre un mur.

**Wrong warp** : On peut utiliser des sauvegardes du mode challenge en même temps qu'une sauvegarde du mode normal. Cela provoque des réactions bizarre du jeu qui, si l'on termine un challenge, peut warper le joueur à la fin du chapitre de la sauvegarde normale.

## 3.1. Run Inbound No Saveload Abuse

### 3.1.0 Informations diverses

#### 3.1.0.0. Description générale

Runné sur PC.

Temps :
- WR : **56m57s617ms** RTA par *AngerySnek*
- TAS : **-undisclosed-** (à peu près -7m par rapport au WR apparemment) par *Can't Even* and friends

Type de run : LE SKILL (et quelques glitches)

Note: Le Saveload Abuse permet de faire des wrong warp en utilisant des sauvegarde du mode challenge en même temps qu'une sauvegarde du mode normal est
active. Ceci est non autorisé dans cette catégorie.

#### 3.1.0.1. Règles

##### 3.1.0.1.0. Général

Il faut rester dans les limites du jeux pour la durée de la run, à l'exception où le jeu requiert de sortir des limites.

Les glitches qui demandent un setup particulier avant le début de la run sont interdits (ex: **Movable Portals**).

Les quicksave et quickload sont autorisés, sauf s'il permettent d'accomplir une action/glitch précis(e) (**Superreportal**, **Clipping**, **Save buffering**, etc). Cette règle s'applique aussi à la commande `restart_level`.

Les scripts sont interdits.

Il est possible d'utiliser une sauvegarde spéciale pour passer la séquence d'introduction où Chell se réveille et où Wheatley la libère de sa chambre de relaxation. Si c'est le cas, il faut ajouter une certaine durée au chronomètre (environ 4 ou 5 minutes selon la sauvegarde utilisée).

##### 3.1.0.1.1. Commande de la console

L'utilisation de la commande `fps_max` est autorisée, mais uniquement entre les valeurs 30 et 999.

Toute commande protégée par la commande `sv_cheats 1` est interdite.

Les commandes `phys_timescale`, `kill`, ou tout autre altération du jeu sont interdites, sauf si explicitement mentionnées comme autorisées.

Changer la valeur de `m_yaw` pour stopper les mouvements de la souris dans la direction horizontale ou verticale est interdit.

L'utilisation du plugin SAR (affiche des infos additionnelles dans un coin de l'écran) est autorisée.

Commandes spéciales autorisées :
- `vm_debug`
- `cl_forwardspeed` between 0 and 175
- `cl_backspeed` between 0 and 175
- `cl_sidespeed` between 0 and 175
- `sar_hud_velocity_precision` between 0 and 2
- `sar_hud_default_padding_x` (any value)
- `sar_hud_default_padding_y` (any value)
- `sar_hud_default_font_index` (any value)
- `cl_showpos 1`
- `+movedown` (only for a walk key)
- `cl_fov`, but only between 45 and 140
- `mat_ambient_light`
- `mat_fullbright`
- `developer 1`
- Les commandes SAR:
  - `sar_hud_angles`
  - `sar_hud_position`
  - `sar_hud_velocity`
  - `sar_fast_load_preset full/normal/etc`
  - `ui_loadingscreen_transition_time 0`
  - `ui_loadingscreen_fadein_time 0`
  - `ui_loadingscreen_mintransition_time 0`

##### 3.1.0.1.2. Chronomètre

Le chronomètre démarre lorsque le joueur prend le contrôle de Chell (au moment où la crosshair apparaît).

Le chronomètre prend fin lorsque la Lune est touchée par le projectile à portail.

Le temps passé en **Pause Abuse** doit être ajouté au chronomètre.

### 3.1.1. Route

#### 3.1.1.0. Général

La run Inbound est une run qui suit grosso modo le parcours standard du jeu. Le jeu étant fermé sur une grande partie et sans s'autoriser de OoB, la plupart des salles vont être faites sans pouvoir s'y céder, bien que peu soient fait tout à fait normalement.

On va abuser de plein de petit glitches subtils qui ne sont pas facile à voir mais qui vont grandement accélerer le passage des salles.

Sur les phases où le jeu est un peu plus ouvert (l'ancien Aperture et la fin du jeu, grosso modo) on va un peu plus s'amuser sequence-break.

Au cours de la run, il y a certains endroit où on va si vite qu'il faut attendre quelques instants parce qu'un dialogue est en court pour accéder à la suite. Parfois même, c'est des manipulations pour que le dialogue se termine pile quand un ascenseur atteint le trigger de changement de niveau (très utilisé dans les premiers chapitres).
D'autres fois, on évite des triggers spécifiquement pour skip des dialogues.

À noter qu'il possible de zapper le prologue et une partie du chapitre 1 pour la run, bien que les régles demandent d'ajouter un délai au chronomètre en contrepartie.

Du reste, classique des jeux Valve, on **Bunny Hop** à la moindre occasion, sauf bien sûr si une meilleure technique de déplacement se présente.

#### 3.1.1.1. Chapitre 1

Ça va vite et ça applique surtout le principe de synchronisation des dialogues.

Le premier glitch de la run arrive à la moitié du chapitre 1 : on fait passer un cube à travers la porte. En gros on approche le cube très près du portail ce qui fait croire au jeu qu'on essaye de le faire transiter mais ce faisant, les collisions sont simplifiés et le cube passe au travers de la porte à la place parce qu'on n'est pas exactement dans le portail.

Un peu plus loin, il va falloir se placer à un angle et position très précise pour passer un portal derrière une porte et skip le fait de ramasser Wheatley.

Skip de cutscene du chapitre 1 et donc déclenchement de l'**Invincibility Glitch** (https://www.youtube.com/watch?v=wYHMzPKktXg)

Skip dialogue setup juste avant de récupérer le vrai Portal Gun.

Maintenant on s'ammuse.

#### 3.1.1.2. Chapitre 2

Ça va vite.

**Laser Switch Glitch** avec 2 portails bien placés.

Ça va vite. On marche sur les murs.

Setup pour **Super Reportal** (x5) dans `Pit Flings` pour complètement skip la salle.

#### 3.1.1.3. Chapitre 3

On fait des trucs un peu vite mais rien de glitchy.

P'tit dialog skip un peu compliqué à voir donc bon.

Une salle, on veut se faire propulser par la plateforme qui bouge.

(Note : on peut utiliser l'**Invicibility Glitch** pour faire la salle avec l'acide et le pont lumineux mais l'ancienne route reste plus rapide.)

On se repose sur l'**Invicibility Glitch** dans la salle avec les tourelles.

Ça va vite. Pas de glitches massifs visiblement.

#### 3.1.1.4. Chapitre 4

Ça va vite avec quelques tricks mais sans plus.

Setup pour dialog skip très chiant aussi.

Dans *Column Blocker* :
- **Chorus skip** : la petite salle là en haut, on va mettre un portail dedans avec un setup compliqué, y aller et clip à travers le niveau via **Portal Bumping**
- **Jerry Skip** : avec un setup ULTRA précis, on peut passer à travers l'ascenceur

Salle du laser avec 3 récepteurs : **Laser Switch Glitch**.

Dans la salle ou Wheatley nous fait nous échapper, petit glitch pour un peu accélerer les choses mais je comprends pas vraiment ce qu'il se passe. Juste on arrive en se calant dans le mur à faire poper la plateforme lumineuse plus vite ce qui permet de sauter au dessus.

Quelques petites opti de déplacements pour skip des micros passages mais rien d'abusif pour la fin du chapitre.

#### 3.1.1.5. Chapitre 5

Alors ça va être un peu sombre parce qu'on fait un petit skip qui en effet secondaire zappe l'activation des lumières.

On avance, on avance...

Au moment de switcher le modèle de tourelle pour une tourelle défectueuse, en fait non, on va skip ça avec un portail à travers le mur (setup vachement précis).

Plus loin pour les neurotoxines on peut tirer un portail à travers la porte (c'est comme ça c'est même pas un glitch) ce qui va un peu plus vite.

Quand on switch GladOS avec Wheatley, au moment où l'ascenseur pop on peut en fait se placer au dessus, ce qui permet d'éviter d'être coincé à l'intérieur et sauter directement dans le trou en dessous quand l'ascenseur remonte et skipper toute la séquence de dialogue de Wheatley soûl de pouvoir.

#### 3.1.1.6. Chapitre 6

Ce chapitre est généralement un run killer.

Par contre on rentre dans les niveaux ouverts et là ça va devenir très le funk.

Enchaînement de portails. Quand le sas gigantesque est en train de s'ouvrir, on va passer par le haut avec un portail pour skipper monter l'escalier.

Map suivante, un setup craqué au poil de fesse pour poser un portail qu'on ne devrait pas pouvoir poser et skiper un ascenseur. Ensuite on va faire un **Reportal** pour skiper encore 15s avec la vitesse obtenue.

Pas mal de tricks pour aller très vite notemment un **Button Glitch**, on récupère le cube qui devait servir à activer ledit button et utiliser le cube pour clipper à travers le plafond et skipper une salle.

Niveau suivant : encore un **Reportal**.

Des gros moves à base de portails.

Note : on récupère pas GladOS-patate mais le jeu nous la filera quand même au chapitre suivant.

#### 3.1.1.7. Chapitre 7

Placement très précis de portail et **Reportal** pour skipper un niveau.

Sur 2 niveaux on avance vite avec des tricks de mouvement facile à piger.

Un **Reportal** au niveau de l'introduction du gel à portail.

On avance vite, un peu.

**Lemonade skip** (du nom du dialogue Johnson "When life gives you lemon...") avec un **Super Reportal** x14 avec un setup assez fou pour se coincer suffisamment longtemps.

**Stuck launch** à la map des 3 gels, pour skipper les 3 gels. Le setup est un peu compliqué mais ça passe pas mal.

(Fun fact, dans la salle du haut après le launch, on peut bumper contre le tuyau de gel qui transporte le gel bleu (le gel de saut).)

On est obligé de regarder le poster sur les paradoxes pendant qqs secondes, c'est un trigger.

On va faire un setup de **Super Reportal** x9 pour se propulser hors du chapitre.

#### 3.1.1.8. Chapitre 8

Dès la première salle, skip en lançant la tourelle-cube car sinon Wheatley bloque la porte et parle pendant 15 secondes.

Salle 2 : on va trigger l'**Excursion Funnel Flying**, état qu'on va abuser dès que possible jusqu'à la fin du jeu.
Aussi petit skip de dialogue mais on est pas à ça prêt.

Note : le **ExFu** glitch va nous permettre de faire les deux derniers chapitres assez incroyablement aisément.

On va toutefois le perdre sur le niveau "Polarity", il va falloir le refaire 2 salles plus loin.

La première salle d'après, c'est marrant, on peut mettre un portail sur une surface pas blanche. Du coup on s'en sert.

Un peu plus loin avec le Flying glitch, il faut atteindre la fin d'une zone ouverte avec une cheminée, toucher la cheminée pour annuler le glitch et tomber vraiment sur le côté pour skip une partie du niveau et aussi éviter une zone de mort.

La salle suivante, on récupère le glitch et il faut bien, alors qu'on se dirige vers la fin du niveau, regarder l'easter egg de P-Body parce que ça va plus vite.

Désac du **ExFu** encore, réac du **ExFu** niveau suivant et on conserve le **ExFu** pour le chapitre suivant.

#### 3.1.1.8. Chapitre 9

Avec le fly on va skip UN PAQUET de trucs sur ce chapitre et c'est cool.

Ça va esquiver plein de trucs sur 3 niveaux et à la fin de "Finale 3" on reperd le fly, juste avant le boss.

Pour le bossfight, y'a des setup très précis pour le early-cycle. En gros Wheatley va devoir subir 3 coups de bombes avant de pouvoir être vaincu. En faisant le fight normalement, il faut attendre chaque cycle que Wheatley balance des bombes.

Sauf que dès le premier cycle, il balance 3 bombes, ce qui fait qu'on va placer 2 portails de telle sorte qu'on jongle avec, et on en replace un au moment où on souhaite toucher Wheatley. Le **Bomb juggle** est le seul moyen d'accélérer le bossfight.

On peut également shooter la lune avant de perdre le contrôle de Chell et du coup gagner un tout petit peu de temps sur le time.

# 4. Trivia

Beaucoup trop, demandez à Synahel.

# 5. Références

Lien speedrun.com : https://www.speedrun.com/portal_2

Wiki source run, grosse référence du speedrun de ces moteurs : https://wiki.sourceruns.org/wiki/Category:Portal_2

Wikipedia : https://fr.wikipedia.org/wiki/Portal_2

Run Inbound No SLA expliquée :
- https://www.speedrun.com/portal_2/guide/6td23
- https://www.speedrun.com/portal_2/guide/vwqk8

Skip de cutscene du chapitre 1 et Invincibility Glitch : https://www.youtube.com/watch?v=wYHMzPKktXg

Excursion Funnel Flying : https://wiki.sourceruns.org/wiki/Excursion_Funnel_Flying)

Notes du TAS de Portal 2 (SGDQ 2022): https://docs.google.com/document/d/1Dzve6wHShTf0SvT2wnd05QafCe0x34a9D6EoY2v-K14/edit
