# 0. Metadata

## 0.0. Versioning

| Date       | Révision                             |
|-           |-                                     |
| 2024-04-04 | Mise à jour (GUF 2024) - Run Any%    |
| 2021-01-05 | Mise à jour (AGDQ 2021) - Run Any%   |
| 2018-06-01 | Création (SGDQ 2018) - Run Any%      |

# 0. Fiche technique

Sortie :
- 1998-11-19 (Windows)
- 2001-11-09 (PlayStation 2)
- 2013-01-25 (Linux)

Plateforme : PC, PlayStation 2

Développé par : Valve Corporation

Édité par : Sierra Entertainment

Genre : FPS

# 2. Histoire

    (Merci Wiiiiikiiipeeediiiiiaaaa !)

## 2.0. Introduction

Black Mesa est un gigantexte complexe scientifique top secret installé dans une
base militaire désaffectée et totalement enterrée sous la surface. Au sein de ce
centre de recherche, Gordon Freeman est un docteur en physique et chercheur au
Laboratoire des matériaux anormaux. Accessoirement, il a suivi avec succès les
exercices de survie en milieu hostile au parcours d'obstacles de Black Mesa, ce
qui explique sa compétence en maniement d'armes.

## 2.1. Chapitre 1 - Arrivée à Black Mesa

Gordon Freeman rejoint son lieu de travail grâce au tram de Black Mesa.
(C'est l'intro du jeu, que l'on passe dans le speedrun.)

## 2.2. Chapitre 2 - Matériaux anormaux

Alors qu'il participait à une expérience sur un mystérieux échantillon de
cristal, Freeman ouvre involontairement une brèche interdimensionnelle vers un
monde parallèle, Xen, peuplé de créatures extraterrestres. Des aliens d'espèces
et de races diverses font irruption un peu partout dans le centre et attaquent
sauvagement son personnel. La salle de test est partiellement détruite par la
résonance en chaîne, mais Freeman, équipé d'une combinaison de protection en
milieu hostile (HEV), parvient à en sortir indemne après avoir été pris dans une
tempête de portails.

## 2.3.  Chapitre 3 - Conséquences imprévues

Après s'être remis d'un court évanouissement, il prend conscience de la
catastrophe... Le centre de Black Mesa est jonché de cadavres et des créatures
particulièrement voraces pourchassent les survivants. Gordon, appuyé par
d'occasionnels collègues et agents de sécurité du centre, les affronte sans
autre but que celui de survivre dans l'espoir de pouvoir regagner la surface
pour chercher de l'aide.

## 2.4. Chapitre 4 - Complexe administratif

Freeman cherche à atteindre la surface et doit pour cela traverser une zone
infestée d'aliens.

## 2.5. Chapitre 5 - Nous sommes menacés

Au cours de ces péripéties, Gordon va découvrir que le gouvernement des
États-Unis tente d'étouffer l'affaire en envoyant sur place un commando de
l'Unité de combat en environnement hostile. De nombreux soldats très bien
équipés déferlent à l'intérieur du centre dans le but de contenir l'invasion
extraterrestre, mais aussi de réduire au silence tout le personnel de Black
Mesa, afin qu'il ne témoigne jamais de ce qu'il a vécu. De nombreux chercheurs
sont massacrés sans ménagement. Ne pouvant plus attendre de secours de la part
de l'extérieur, Freeman, sur les conseils de ses collègues survivants, se fixe
un nouvel objectif : tenter d'atteindre le complexe Lambda de l'autre côté du
centre où il pourra trouver le moyen de se rendre sur Xen afin de mettre fin à
l'invasion.

## 2.6. Chapitre 6 - Chambre de combustion

Gordon Freeman fuit par les chambres de test pour atteindre une Chambre de
combustion de réacteurs de fusée. Il lui faudra réussir à activer les réacteurs
pour venir à bout des trois tentacules qui lui barrent la route.

## 2.7. Chapitre 7 - Allumage

Freeman devra remettre les rails sous tension pour pouvoir poursuivre sa
progression, mais cette mission ne sera pas un parcours de santé : un Gargantua
garde l'accès aux bobines de Tesla.

## 2.8. Chapitre 8 - Sur un rail

Gordon utilise un système ferroviaire désaffecté investi par les militaires
pour atteindre la surface, où se trouve un bâtiment d'où il pourra lancer une
fusée transportant le satellite Lambda. Ce satellite est la tentative des
scientifique pour fermer le portail dimensionnel ouvert entre Xen et la Terre.

## 2.9. Chapitre 9 - Appréhension

Freeman progresse dans des souterrains et des entrepôts désaffectés contrôlés
par l'armée, les Black Ops et infestés de xéniens. Il sera capturé par des
militaires à la fin du chapitre.

## 2.10. Chapitre 10 - Traitement de déchets

Privé de ses armes, Gordon, laissé pour mort par l'armée dans une fosse de
destruction de déchets, se fraye un chemin dans l'inhospitalier centre de
traitement des déchets où quelques aliens rodent.

## 2.11. Chapitre 11 - Une éthique douteuse

Dans ce chapitre de transition, Freeman finit par atteindre un laboratoire dans
lequel quelques équipements laser sont à l'étude. Pour en sortir, il doit
trouver des collègues capables de passer l'identification rétinienne pour lui
ouvrir l'accès à l'extérieur. Mais de nombreux soldats et aliens se mettront
sur son chemin...

## 2.12. Chapitre 12 - Tension en surface

Gordon Freeman traverse une vaste zone désertique et montagneuse située en
partie à la périphérie de Black Mesa. L'armée contrôle chaque secteur et
tentera par tous les moyens de l'éliminer.

## 2.13. Chapitre 13 - Oubliez Freeman

Les militaires évacuent la zone alors que l'Air Force commence à bombarder le
site. Les fuyards sont poursuivis par de nombreux xéniens qui commencent à
s'installer dans les bâtiments du centre devenus déserts.

## 2.14. Chapitre 14 - Réacteur Lambda

Freeman parvient finalement à atteindre le complexe Lambda, mais le chaos y
règne. Les téléporteurs ne manqueront pas de mettre Gordon Freeman à l'épreuve,
mais il faut absolument réactiver le réacteur Lambda, trouver les scientifiques
survivants et les protéger pour lui permettre d'atteindre la dimension Xen.

## 2.15. Chapitre 15 - Xen

Gordon Freeman est téléporté sur Xen, un univers singulier très différent de la
Terre. Le module de grand saut se révèlera indispensable pour atteindre le
premier téléporteur.

## 2.16. Chapitre 16 - L'Antre du Gonarch

Dans ce chapitre, Gordon Freeman combattra le Gonarch, le monstre qui engendre
les crabes-de-tête. Seule sa mort permettra d'accéder au second téléporteur
indispensable à sa progression.

## 2.17. Chapitre 17 - L'Intrus

Ici, Gordon Freeman errera successivement à la périphérie et à l'intérieur
d'une fabrique d'armes où les esclaves Vorts, surveillés par les contrôleurs,
donnent vie de façon étrange aux Grognards. Gordon Freeman aura à mener de
rudes combats pour trouver les 4 téléporteurs restants.

## 2.18. Chapitre 18 - Nihilanth

Gordon Freeman combattra le maître des habitants de Xen. Après un long et
pénible combat entre lui et ses subordonnés, il en vient à bout.

## 2.19. Fin de Partie

Le G-Man est un personnage énigmatique que Freeman peut appercevoir à de
nombreux moments du jeu, mais avec lequel il n'interragit pas directement. À
travers diverse déduction, on peut savoir que le G-Man est bien plus influent
qu'on ne pourrait le penser, et également responsable de nombreux événements
dans le complexe.

En fin de jeu toutefois, ledit G-Man rejoint Gordon Freeman, lui explique la
situation et lui laisse l'illusion d'un choix :
- accepter de travailler pour lui ;
- affronter une armée entière de xéniens sans aucune chance de succès.

D'après Half-Life 2, Gordon Freeman accepte son offre et est mis dans un état
de stase en attendant son affectation.

# 3. Commentaire

## 3.0. Général

### 3.0.0. Mécaniques de jeu

FPS

Des armes, cac / armes "standard" de FPS / armes plus expérimentales et des
bidules aliens. On verra des utilisations TRÉS détournées de certaines armes.

Mécaniques de vie/armure qui se rechargent via soit des collectibles soit des
distributeurs (oui).

Du fun

### 3.0.1. Techniques émergentes

En préparation, on crée un raccourci clavier vers une touche pour fixer les fps_
max à 5 et rétablir, un autre pour les fps_max à 15. Les NPCs s’animent plus
vite moins y’a de fps d’où le 15. Le 5 est spécifique à certains déplacements
pour ne pas subir les dégâts de chutes. De plus y’a certains bugs indésirables
qui se produisent avec de trop grandes valeurs de FPS à certains passages (les
ascenseurs).

**Blast boost** : terme général avec lequel je décris une propulsion par
n'importe quelle explosion. Par ailleurs, plus on prend des dégâts de
l’explosion sur la vie (et non l'armure) plus le boost est fort.

**Bunny hoping** : on saute + straffe et on regarde dans la direction du
straffe = vitesse augmente. La vitesse est reset si on dépasse une certaine
valeur alors qu'on appuie sur `forward`, du coup on s’auto cap la vitesse en
saut accroupi quand on va assez vite sans appuyer sur le déplacement.

**Corner boost** : lors de grosse chute, se rattraper sur un rebord très
incliné permet de transformer sa vitesse verticale en vitesse horizontale et
ainsi annuler les dégâts de chute.

**Edgebug** : un glitch qui permet de cancel les dégâts de chute en tombant
pile sur le bord d'un bloc. Le jeu annule la vitesse mais ne comprend pas
qu'on n'est plus en l'air, donc pense qu'on n'a pas touché le sol.

**Grenade boost** : quand on lance une grenade, on les dégoupille, et elles ont
un délai. Si on les garde en main longtemps, elles explosent quand on relâche.
Le truc c’est que l’explosion nous boost radicalement les sauts.

**Infinite Health Door Glitch** : expliqué au moment où ça arrive.

**Ladder boosting** : avancer + straffer sur une échelle = 2x plus vite.

**Nuke effect** : si une explosion se produit dans une zone réduite, l'impact
de l’explosion est plus grand.

**Object Boosting** : une technique qui exploite un bug de poussée/tractation
d'objet qui conduit à gagner de la vitesse (jusqu'à 2000 UPS). Il suffit de se
mettre de profil, de straffer et d'appuyer-relacher sur la touche de drag/push.

**Save/load skip** : pendant le temps nécessaire au jeu pour finaliser un
chargement de sauvegarde, le joueur peut se déplacer alors que certaines
collisions ne sont pas appliquées. Cela permet avec sauvegarde + chargement
rapide bien placé de ne pas activer certains lasers en passant au travers
(notamment des mines murales).

**Straffe Jump** : exploitation d'un bogue du moteur de Quake (et donc de
Half-Life), en sautant + straffe + direction on peut vraiment accélérer son saut
dans un mouvement circulaire. C'est la base du **bunny hoping**.

**Tau Jump** : en utilisant le recul offert par le Fusil Tau chargé, on
bénéficie d’un saut complètement fumé en ligne droite.

**Weapon duplication glitch** : si on **save/load** pendant l'animation de mort
d'un ennemis qui fait tomber une arme au sol, on duplique l'arme (et donc les
munitions) à chaque **save/load**.

## 3.1. Any% WON Scriptless (dernière mise-à-jour : GUF 2024)

### 3.1.0 Informations diverses

#### 3.1.0.0. Description générale

Runné sur PC, sur la version originale boîte (dite *WON*).

Temps :
- WR : **26m 09s 297ms** LRT - **26m 33s 290ms** RTA par Muty
- (en théorie) 183ème : **38m 36s** LRT - **39m 36s** RTA par mt5xdd

(mt5xdd n'a pas de temps sur speedrun.com mais son PB est disponible sur
twitch ; et cela le place 183/377).

Type de run : LE. BORDEL. (Run du swag et du skill avec des techniques fumées
et des glitchs.)

Pour info, la run est "*scriptless*" cat il existe des sous catégories avec
scripts (notamment pour faciliter le **bunny hop**).

#### 3.1.0.1. Règles

Une démo des inputs doit être enregistrée pour que la run soit valide. Les
runners utilisent un outils qui modde le jeu pour pouvoir le faire :
`Bunnymod XT`.

(La commande en question est : `_bxt_save_runtime_data_in_demos 1`.)

La valeur `fps_max` ne doit jamais dépasser 100.

Il est interdit d'utiliser des inputs ou des sauvegardes de run précédentes.

La majorité des commandes de la console de développement sont interdites, avec
quelques exceptions (la liste est assez longue en vrai). On peut noter les
commandes qui permettent d'afficher les FPS ou bien la vitesse du joueur.

La niveau de difficulté ne peut pas être modifiée en court de run.

Il est interdit d'utiliser la commande `wait` ou bien de binder plusieurs
commande à une seule touche. Il est toutefois autorisé de binder une seule
commande sur une touche (notamment la commande `fps_max <integer>`).

##### 3.1.0.2. Chronomètre

Le chronomètre démarre dès que la map 'c1a0' est chargée.

Le chronomètre s'arrête au coup final sur le Nihilanth (à la première frame de
l'éclair bleu).

### 3.1.1. Route

#### 3.1.1.0. Général

C’est le bordel. Mais quelques points à retenir :
- la run est jouée en "Facile" ;
- on **Bunny Hop** systématiquement (quand pas de meilleur déplacement
  disponible) ;
- on diminue les FPS dès qu'une cutscene ingame est obligatoire (notamment les
  animations des gardes qui ouvrent les portes) ;
- on diminue les FPS drastiquement sur certaines chutes ;
- se rattraper sur une échelle ne provoque aucun dégât après une longue chute.

#### 3.1.1.1. Chapitre 1 - Arrivée à Black Mesa

Les règles permettent de démarrer après.

#### 3.1.1.2. Chapitre 2 - Matériaux anormaux

Go fast jusqu'à l'experiment, on fait un saut sur une échelle.

Dans l'antichambre avant l'expérience, on manipule le comportement des
scientifiques pour qu'ils se déplacent chacun plus près de leur scanners
respectifs.

Au moment de mettre le cristal dans la machine, on **Object Boost** pour se
placer au bon endroit pour le départ du chapitre suivant. Au bon endroit en
l'air on save/load pour passer toute la séquence de téléportation.

#### 3.1.1.3.  Chapitre 3 - Conséquences imprévues

On ratterrit direct dans la salle de contrôle, zappant une cutscene.

On trace jusqu'au reste du jeu, chemin standard mais fait tarpin vite jusqu'à
l'ascenseur oblique.

Ascenseur oblique : les ascenseurs c'est long, même avec des crabes de têtes
pour nous distraire du coup on saute, en prenant bien garde de ne pas prendre
de dégâts de chute.

Un peu plus loin on prend de l'énergie pour un skip plus tard et on zappe un
fossé avec un **Object Boost**.

On active l'ascenseur à travers le mur pour zapper un bout d'animation.

#### 3.1.1.4. Chapitre 4 - Complexe administratif

On rammasse direct la vie et le shield et on skip le coupage du courrant en
bruteforçant le passage.

On va ultra-vite et plus loin on tire sur un garde pour le forcer à nous suivre
plus vite (notamment parce qu'il veut nous taper). Manipulation avec des
grenades pour qu'il ouvre la porte plus vite aussi.

Petit skip de la montée aux échelles à la fin pour atteindre la dernière sans
l'animation du scientifique qui décède.

#### 3.1.1.5. Chapitre 5 - Nous sommes menacés

ATTENTION GROS SKIP !

On tire sur le scientique pour interrompre son animation, on se place à côté de
lui, ~~on lance une grenade sous la porte vers laquelle il va aller,~~ on
récupère de la vie, on passe à travers les restes du scientifique pour
atteindre une zone vers laquelle on est censé arriver bien plus tard.

On active la porte, on prend la vie pour damage boost et on se dirige vers la
suite du jeu.

#### 3.1.1.6. Chapitre 6 - Chambre de combustion

Skip de l'ascenseur avec l'échelle into rush **grenade boost** + **bunny hop**
parce que plus rapide que le train into skip grace à la vitesse into
*insérez_musique_d'ascenseur*.

Skip du passage à l'interieur de la chambre pour activer directement le
carburant et l'électricité.

Manipulations d'ennemis et saut compliqué plus loin à base de **Corner Boost**
into conservation de la vitesse into retour en Corner Boost.

Soin à travers le mur into **Object Boost** into **Blast Boost** into suite du
jeu.

**Corner boost** pour pas mourir.

Retour au main event, into pression de bouton à travers le mur into on se barre
pour pas crever et aller chercher de la vie pendant l'animation.

On revient et on continue jusqu'au produit toxique où on fait un move que je
pige absolument pas pour rejoindre le conduit.

#### 3.1.1.7. Chapitre 7 - Allumage

Bienvenue dans allumage.

C'était bien.

#### 3.1.1.8. Chapitre 8 - Sur un rail

On fait tout le truc des trains sans train.

Premier skip avec les mines murales, sur lesquelles on peut grimper.

Bunny hop compliqués into **Object Boost** avec lequel on doit garder sa
vitesse un long moment.

À un moment **save/load** pour passer à travers les rayons détecteurs des mines.

À un autre moment saut très chelou mais qui fait qu'on ne prend pas de dégâts
de chute.

À la surface, manipulation des ennemis avec les grenades into activation de la
fusée à travers le murs, into fonçage se soigner pendant l'animation.

#### 3.1.1.9. Chapitre 9 - Appréhension

Petit skip au début : on tombe direct dans le bon côté.

Gros skip de l'Ichtyosaure.

Petit skip des Ichtyosaures.

**Object boosting**, coucou le G-Man.

Manipulation des ennemis pour vite parler au scientifique.

Vers les black ops : **Grenade Boost** vers le haut into fonçage sous porte de
garage into piège into soin pendant la cutscene.

#### 3.1.1.10. Chapitre 10 - Traitement de déchets

Montée ultrafast into crowbar into cassage de barreau into early cycle escape.

Circle jump into **bunny hop** pour la vitesse into traitement des déchets.

On prend le C4 (c'est super importantTM).

Petit skip en montant sur un levier.

C4 pour un **blast boost** + **save/load** pour les rayons.

#### 3.1.1.11. Chapitre 11 - Une éthique douteuse

Petit skip au début into C4 **blast boost** into grosse manipulation d'IA à
l'aide de grenades.

**Weapon duplication glitch** sur un mec pour le fusil
mitrailleur-lance-grenade.

On récupère le Tau gun (c'est très important).

Manipulation comportement scientifique.

#### 3.1.1.12. Chapitre 12 - Tension en surface

Alors là, skip avec mega gros **Tau boost** pour s'envoyer vers la gauche
(idéalement, il faut viser le tuyau et on peut ne pas mourrir et en plus
continuer avec la vitesse ; sinon il faut viser l'échelle).

Plus loin C4 **blast boost** pour skip une échelle.

Dans le bords de la montagne, re mega gros **Tau boost** into circle jump pour
passer toute la séquence (idéalement on arrive dans le tuyau).

Quand on ressort, **Tau boost** pour passer au dessus du niveau.

Pareil plus loin.

2 strats pour la zone bombée, une safe, une moins safe, les deux avec un petit
**Tau boost**.

**Tau boost** plus loin avec conservation de vitesse puis **grenade boost** pour
passer au dessus du niveau.

**Tau boost** plus loin pour passer au dessus du niveau encore.

Alors là, manipulation d'ennemis pour faire l'**Infinite Health Door glitch** :
on effraie un garde avec des grenades, on se calle contre une porte avec un truc
de soin à côté, on attend que le garde flippé ouvre la porte, ce qui nous
blesse. Sauf que cette porte est buggé, et sa valeur de dégât vaut -1. Du coup
elle nous modifie la santé de --1 par tic donc +1 par tic. Donc ça soigne ; et
ce au-delà du maximum de 100.
Nécessite de ne pas avoir d'armure.

On vise 2000+ points de vie environ.

À partir de maintenant, la technique de déplacement privilégié est le **Blast
Boost** into **Bunny Hop**.

Ensuite gros damage boost et **bunny hop** pour gagner du temps et skip le kill
du Gargantua + le blast de la porte (on peut le faire nous même).

#### 3.1.1.13. Chapitre 13 - Oubliez Freeman

Damage boost pour skip le tank et rush.

Damage boost pour skip les blacks ops.

#### 3.1.1.14. Chapitre 14 - Réacteur Lambda

Grenade avant que le scientifique ouvre la porte pour manipuler et ne pas
l'avoir dans le passage.

Skip en clippant à travers le mur grâce à un **blast boost**.

**Grenade boost** (avec **Nuke effect**) into deuxième **grenade boost** pour
skiper la montée de l'eau du réacteur.

Manipulation d'IA pour aller plus vite.

Boost pour sauter directement dans le téléporteur et skipper toute la phase
stamina.

#### 3.1.1.15. Chapitre 15 - Xen

Direct arrivé, **Tau Boost** pour tomber direct là où on veut.

#### 3.1.1.16. Chapitre 16 - L'Antre du Gonarch

On shot pour attirer le Gonarch, et on préshot les C4 pour le tuer.

On le dépasse après la phase 2 pour préshot le morceau de sol à détruire.

#### 3.1.1.17. Chapitre 17 - L'Intrus

Immédiatement **grenade boost** pour skipper une zone puis rush après le tp via
**tau boost** et **blast boost**.

Dans l'usine, rush puis cumul de **grenade boost** pour atteindre le tp.

#### 3.1.1.18. Chapitre 18 - Nihilanth

Fait à peu près normalement, juste très vite, sauf **grenade boost** à la fin
pour le dernier hit quand la tête du boss s'ouvre.

#### 3.1.1.19. Fin de Partie

Normalement le time s'est arrêté.

# 4. Trivia

Beaucoup trop, demandez à Synahel.

# 5. Références

Lien speedrun.com : https://www.speedrun.com/hl1

Wiki source run, grosse référence du speedrun de ces moteurs : https://wiki.sourceruns.org/wiki/Category:Half-life

Wikipedia : https://fr.wikipedia.org/wiki/Half-Life
