# 0. Metadata

## 0.0. Versioning

| Date       | Révision                                                     |
|-           |-                                                             |
| 2023-01-06 | Mise-à-jour (AGDQ 2023) - Version Anniversary Run Any%       |
| 2022-10-16 | Création (RPGLB 2022) - Version The Lost Chapter Run Any%    |

# 1. Fiche technique

Sortie :
- 14 septembre 2004
- Septembre 2005 (The Lost Chapters)

Plateforme :  Windows, Linux

Développé par :
- Big Blue Box Studios (base game)
- Lionhead Studios (The Lost Chapters / Anniversary)

Édité par : Microsoft Game Studios

Genre : Action RPG

Notes :
- *The Lost Chapter* est une réédition du jeu qui ajoute du contenu additionnel,
  notamment après la fin du jeu original.
- *Anniversary* est une version remasterisée du jeu, incluant *The Lost
  Chapters*. qui change le système de sauvegarde et améliore massivement les
  graphismes. Le jeu conserve une grande partie des bugs de la version
  originale toutefois.

# 2. Histoire

    (Merci Wiiiiikiiipeeediiiiiaaaa !)

## 2.0. Univers

Fable prend place dans le continent d'Albion, un patchwork de cité-état séparées
par des plaines et forêts sans loi.

L'entiereté du territoire était il y a longtemps un royaume entier contrôlé par
une ancienne lignée dont le dirigeant portait le titre et nom d'Archon. On
réfère usuellement au régime par l'Ancien Royaume. Le première règne d'Archon
était pacifique et prospère mais la source de son pouvoir était une ancienne et
puissante épée (l'Épée des Ages) qui graduellement le corrompit, lui et
éventuellement le royaume entier. Au moment où le jeu prend place, le monde a
lentement détérioré depuis les jours de l'Ancien Royaume.

L'une des plus proéminente institutions d'Albion désormais est la Guilde des
Héros. La Guilde est un centre d'apprentissage et d'entraînement pour des
Héros, des mercenaires renommés, actifs dans tous les recoins d'Albion. Les
Héros sont recrutés comme voleurs, soldats, guards, secours et protecteurs ; la
Guilde n'appliquant aucun jugement moral quant au comportement de ses Héros.

## 2.1. Intrigue

### 2.1.0. Enfance

Le jeu commence sur l'enfance du personnage principal, le jour de
l'anniversaire de sa sœur Theresa. Après une brève phase tutorielle, des
bandits raident le village dans lequels ils habitent, Oakvale, tuant le père et
capturant la sœur et la mère. Un vieux Héro, Maze, sauve le garçon et le
recrute pour l'entraîner en tant que Héros dans la Guilde.

### 2.1.1. La Guilde et l'adolescence

Les années passent via plusieurs ellipse, entre lesquelles des phases de
tutoriel plus complètes apprennent au joueurs les mécaniques de combat,
notemment. On y fait aussi la connaissance de Whisper, une jeune fille, qui
partage le même dortoir que le Héros. Elle deviendra rapidement son amie et son
rival.

Une fois sa formation terminé, à l'age adulte, le Héros peut entamer plusieurs
quêtes mineures.

### 2.1.2. L'âge adulte

Quelques années passent encore alors que le Héros augmente son renom et ses
compétences, jusqu'à ce que Maze informe le Héros qu'une voyante aveugle vit au
sein d'un camp de bandits près d'Oakvale (le village existe toujours, il a été
reconstruit). Maze conseille au Héros d'infiltrer le camp de bandit. Le Héros
s'exécute, mais à sa surprise, la voyante est en fait sa grande sœur recuillie
par Twinblade, un ancien Héros et désormais Roi Bandit. Après un combat avec
celui-ci, le Héros a le choix de le tuer ou l'épargner.

Peu après dans la vie du Héros, alors que son renom augmente, il est invité à
combattre dans l'Arène où il rencontre le légendaire Héros Jack of Blades qui
gère les combats d'arène. Pour son dernier combat, Jack fait se confronter le
Héros et sa rivale Whisper et offre 10 000 pièces d'or en récompense s'il la
tue. Le Héros peut l'épargner ou non.

Le Héros apprend peu après que Jack of Blades lui-même a détruit Oakvale
pendant son enfance. Aidé par Theresa, le Héros apprend que leur mère est en
vie dans la Prison de Bargate. Après tenter de sauver celle-ci, le Héros est
capturé et passe deux années en prison avant de s'en échapper. Il est alors
révélé que Maze est un traître et travaille avec Jack, qui a de nouveau capturé
Theresa. Après avoir vaincu Maze, le Héros combat Jack, pendant lequel sa mère
se fait tuer. Jack révèle alors que l'Épée des Ages ne peut être brandie
uniquement si elle reçoit le sang d'Archon. Après la mort de leur mère, le
Héros et Theresa sont les deux derniers descendants d'Archon ; et si Jack les
tue tous les deux, l'Épée sera encore plus puissante. Après avoir vaincu Jack,
le Héros doit choisir entre garder l'Épée en tuant sa sœur ou bien la bannir
pour toujours en la jetant dans le portail généré par la mort de Jack.

Par la suite de son choix, et en fonction de l'alignement du Héros, il y a un
total de 4 fins différentes. Le joueur peut poursuivre le jeu après les crédits
s'il le désire.

### 2.1.3. The Lost Chapter

Dans l'édition The Lost Chapter, l'histoire continue.

Après la défaite de Jack, le Héros doit trouver un passage dans les Northern
Wastes pour aider un autre Héros légendaire nommée Scythe (que l'on côtoie
brievement dans le jeu de base). La mission de Scythe est d'empêcher un grand
mal inconnu de revenir. On sera rejoint par la suite dans l'investigation par
une autre Héroïne, Briar Rose / Églantine.

Si le Héros a banni l'Épée des Ages, il aura l'opportunité d'obtenir une autre
Épée Légendaire de même facture et au pouvoir similaire, mais alignée vers le
Bien plutôt que vers le Mal : la Larme d'Avo.

Après une série de quête pour obtenir de plus amples informations sur ce mal
nouveau et comment le combattre, il est révélé qu'il s'agit de Jack of Blades,
revenu d'entre les morts. Le Héros doit alors défaire Jack une deuxième fois,
tandis qu'il arbore désormais la forme d'un dragon.

Après un combat acharné, le Héros utilise le masque de Jack pour capturer son
âme, suite à quoi Scythe lui indique que le combat n'est pas fini et qu'il doit
détruire le masque. À l'instar de l'édition standard, le Héros un choix final :
porter le masque pour acquérir le pouvoir de Jack, se faisant corrompre dans le
processus, ou alors détruire le masque, et avec lui Jack, pour toujours.

### 2.1.4. Annexe

Il est dit qu'Avo est un Dieu qui réprésente l'alignement Bon.

# 3. Commentaire

## 3.0. Général

### 3.0.0. Mécaniques de jeu

Action RPG en vue à la troisième personne.

Mécanique de vie/mana standard. On récupère du mana progressivement, du reste
il y a toujours des consommables pour recharger la vie (bouffe, potion) ou le
mana (potion).

Mécanique d'expérience séparée en 4 catégories :
- l'expérience générale
- l'expérience de Force
- l'expérience d'Addresse
- l'expérience de Volonté
L'expérience est acquise en effectuant une action dans le domaine
correspondant. La Force régit le combat physique, l'Adresse le combat à
distance, la furtivité et le marchandage et la Volonté régit la magie.
L'expérience génale s'acquiert en faisant n'importe quelle action et en tuant
un ennemi.

Le leveling se gère via une interaction avec un autel dans la Guilde des Héros
où l'on peut dépenser son expérience dans trois domaines différents pour
améliorer son personnage : la Force, l'Adresse et la Volonté. L'expérience
correspondant au domaine est assignée en priorité avant d'utiliser l'expérience
générale.

Armes standard de RPG : épée, arcs...

Il existe également une mécanique de combo. Plus le personnage attaque sans se
faire toucher, plus il accumule de la combo, qui s'incrémente. À chaque rang de
combo, le personnage peut effectuer une attaque puissante, bien plus ravageuse.
Également, l'expérience gagnée est directement proportionnelle au rang de
combo : avoir un rang de combo plus élevé permet d'acquérir de l'expérience
plus vite.

Il existe des potions à expérience pour chaque catégorie. Ces potions donnent
1000 points d'expérience, cependant elles sont aussi influencées par le rang de
combo.

Des portails de téléportation sont disponibles à divers endroit d'Albion pour
se déplacer instantanément vers un autre portail déjà découvert.

Du fun.

### 3.0.1. Techniques émergentes

OoB en utilisant un bug de colision avec une créature invoquée au bon endroit
(récupérer l'Épée des Ages early).

Il existe des bugs de sauvegarde corrompue qui permettent de transférer ses
stats et compétence vers une autre sauvegarde. Ce bug est généralement interdit
en run.

**Hotshot glitch** : glitch pour le level up. Pendant le level up, on peut
défaire le level up d'une compétence si on le souhaite. Cela rembourse
l'expérience dans la catégorie d'expérience. Le glitch consiste en spammer et
le bouton de level up et de level down, ce qui rend le jeu confus sur le nombre
de point d'expérience et permet d'acheter plus de niveau que l'expérience
disponible ne le permet.

On se retrouve cependant avec de l'expérience négative au final (on peut voir
ce glitch comme un emprunt).

Le **hotshot glitch** n'est pas disponible sur la version *Anniversary*.

**Menu manipulation** (version *Anniversary* uniquement) : un glitch qui recoupe
pas mal d'applications, de la duplication d'item à l'échange d'expérience.

À la base, le glitche consiste, dans un menu, à faire croire au jeu qu'on a
sélectionné un objet à la place d'un autre. Cela permet de vendre des objet que
l'on ne possède pas (essentiellement donc, on les duplique), ou d'acheter des
compétences que l'on ne pourrait pas obtenir en l'état.

La méthode demande de sélectionner un élément du menu pour avoir accès au menu
contextuel qui lui est lié. Si c'est un objet chez un  marchand par exemple, le
menu contextuel aura "Vendre", "Vendre 10", "Vendre 50", "Vendre tout". Si c'est
une compétence à acheter, le menu aura : "Acheter" ou "Rembourser". Une fois
l'élément sélectionné donc, il faut simplement slider sur un autre élément,
alors que le menu contextuel reste affiché (avec les **mêmes** options,
**inchangées**), et valider, selon ce que l'on souhaite faire.

Lorsque l'on achête par ce biais des compétences trop chères, le jeu va nous
donner de l'expérience négative, que l'on peut annuler par un simple
remboursement (le jeu remet le compteur à 0). Dans les faits, on a donc généré
de l'expérience.

**Load warping** (version *Anniversary* uniquement) : c'est un glitch qui
permet de transférer l'avancement d'une sauvegarde dans une autre. Il faut pour
cela charger la-dite sauvegarde alors que le personnage est en train d'entrer
dans une zone différente. Cela a pour effet de transférer le contenu de la
sauvegarde dans la zone en question. On s'en sert dans les fait pour se
teléporter à une zone précédemment visitée.

**Summon clipping** : consiste en l'abus de hitbox entre le sort *Charge
Assassine* et le sort *Invocation* pour passer à travers certains murs. Il est
utilisé entre autre pour récupérer l'Épée des Âges bien avant le moment où on
devrait pouvoir le faire.

## 3.1. The Lost Chapter Run Any% (RPGLB 2022)

### 3.1.0. Informations diverses

#### 3.1.0.0. Description générale

Runné sur PC.

Temps (RPGLB 2022) :
- WR : **1h 05m 17s** LRT - **1h 15m 14s** RTA par VRehim
- Runner, 9ème : **1h 13m 59s** LRT - **1h 23m 56s** RTA par Biglaw

Type de run : pépouze, avec 2-3 passages funky quand même

#### 3.1.0.1. Règles

*(Tirées de speedrun.com, toutefois les règles ne concernant pas le jeu en
lui-même ont été exclues)*

##### 3.1.0.1.0. Général

Les sauvegardes qui commencent à 00h 00m à l'introduction à Oakvale sont
acceptées en tant que point de départ de run pour éviter aux joueurs de créer
un profil à chaque fois. Cependant, il est demandé au runner de revenir au menu
principal et charger depuis le menu principal entre les tentative de run. Ceci
permet d'éviter un glitch de corruption de sauvegarde qui tranfère les stats
d'une sauvegarde à l'autre.

Dans le cas où le runner crée un nouveau profile, il doit fermer et réouvrir le
jeu, sinon cela peut provoquer une corruption de profil qui transfère stats,
temps de jeu et créatures invoqués sur l'autre profil.

Les macros de plusieurs actions sur la même touche ne sont pas autorisées.

Les boutons turbos ne sont pas autorisés.

##### 3.1.0.1.3. Chronomètre

Le chronomètre démarre à la fin de la première sauvegarde automatique.

Le chronomètre se termine à lorsque s'affiche "Quête Accomplie" pour la
bataille finale (Dragon Jack).

### 3.1.1. Route

#### 3.1.1.0. Général

Courir c'est lent, roulade avant ça va plus vite.

Après avoir acquis "Charge Assassine", c'est ce sort qui va plus vite.

Grosso modo, le jeu est linéaire donc pas de grosses surprises, même si 2-3
trucs funky.

#### 3.1.1.1. Avant l'arène

À la fin du tuto, acquérir "Charge Assassine", **hotshot glitch** pour l'avoir
niveau 2.

Après la quête de la ferme, on va utiliser l'expérience acquise pour lvl up la
force et la magie (notemment pour avoir un niveau dans le sort invocation et le
reste dans le sort Berserk).

Une fois fait, on va utiliser un glitch de hitbox avec une créature invoquée
pour récupérer l'Épée des Ages dans la Salle du Destin de la Guilde avec un
clip dans le décors.

Petit glitch plus loin pour passer à travers la porte du camp des bandits avec
une charge + roulade bien placée.

Avant le combat contre Twinblade, bien récupérer une potion d'XP Volonté.

Pareil pour une porte de pierre à Witchwood (les portes de pierres sont des
portes qui s'ouvrent seulement sous une certaine condition, et qui contiennent
généralement une récompense derrière).

Après le combat contre le gros loup-garou, on consomme les potions d'XP
conservées (force + volontés) où on devrait avoir un multiplicateur de combo à
20 (environ 40k xp volonté (2 popo)) 20k force (1 popo)).

Description level up qui suit :
- Force > Physique lvl 5
- Addresse > Vitesse lvl 4
- Volonté >
    - Embrasement lvl 4
    - Poussée magique lvl 3
    - Bouclier physique lvl 2 (déjà acquis ?)
    - Berserk lvl 2 (déjà acquis ?)
    - Puissance magique lvl 4

Puis **hotshot glitch** pour maximiser Embrasement.

#### 3.1.1.2. L'arène

Pour l'arène, vu que c'est une succession de combat qui s'enchaîne sans
coupure, c'est le premier vrai endroit du jeu où on peut efficacement augmenter
son combo d'XP.

On va abuser de deux sorts :
- Le sort Bouclier Physique, qui permet d'encaisser des dégâts sur le mana
  plutôt que la vie. L'effet est sympa mais le vrai bonus c'est que se faire
  toucher ne cause pas d'animation de récupération ni perte de combo.
- Le sort Poussée magique, qui inflige des dégâts et renverse les ennemis.
  L'effet est sympa mais le vrai bonus c'est le petit bug qui vient avec :
  quand un ennemi est tué et tombe au sol, le sort va quand même s'activer sur
  son cadavre et augmenter le bonus de combo.

On va aussi utiliser Embrasement niveau max pour tout tuer très vite, et voilà
une arène rondement menée.

Evidemment, au maximum du combo, on va utiliser tous les consommables qui
donnent de l'XP (bouffe et potion) pour de l'expérience à gogo.

#### 3.1.1.3. Cassage de cul

Une fois cette expérience acquise, on va juste maxer le perso et foncer jusqu'à
la fin du jeu.

- Force > Physique lvl 7
- Addresse > Vitesse lvl 7
- Volonté >
    - Multifrappe lvl 4
    - Poussée magique lvl 4
    - Bouclier physique lvl 3
    - Berserk lvl 4
    - Charge assassine lvl 4
    - Pluie de flèche lvl 4
    - Puissance magique lvl 7

Après on fonce littéralement jusqu'à la fin, je compte sur ma mémoire pour
expliquer le reste.

## 3.1. Anniversary Any% (AGDQ 2023)

### 3.1.0. Informations diverses

#### 3.1.0.0. Description générale

Runné sur PC.

Temps (AGDQ 2023) :
- WR : **57m 25s** LRT - **1h 04m 17s** RTA par VRehim
- Runner, 3ème : **58m 02s** IGTA - **1h 09m 05s** RTA par SeraVenza

Type de run : pépouze, avec 2-3 passages funky quand même.

#### 3.1.0.1. Règles

*(Tirées de speedrun.com, toutefois les règles ne concernant pas le jeu en
lui-même ont été exclues)*

##### 3.1.0.1.0. Général

Les sauvegardes qui commencent à 00h 00m à l'introduction à Oakvale sont
acceptées en tant que point de départ de run pour éviter aux joueurs de créer
un profil à chaque fois. Cependant, il est demandé au runner de revenir au menu
principal et charger depuis le menu principal entre les tentative de run. Ceci
permet d'éviter un glitch de corruption de sauvegarde qui tranfère les stats
d'une sauvegarde à l'autre.

Dans le cas où le runner crée un nouveau profile, il doit fermer et réouvrir le
jeu, sinon cela peut provoquer une corruption de profil qui transfère stats,
temps de jeu et créatures invoqués sur l'autre profil.

Le jeu doit être joué en difficulté "Poulet".

Les macros de plusieurs actions sur la même touche ne sont pas autorisées.

Les boutons turbos ne sont pas autorisés.

Les équipements du DLC ne doivent pas être utilisés ou achetés en run.

##### 3.1.0.1.3. Chronomètre

Le chronomètre démarre à la fin de la première sauvegarde automatique.

Le chronomètre se termine lorsque s'affiche "Quête Accomplie" pour la
bataille finale (Dragon Jack).

### 3.1.1. Route

#### 3.1.1.0. Général

**Le *hotshot glitch* n'est pas disponible dans la version *Anniversary* !**

La langue la plus rapide c'est le français, cocorico !

Courir c'est lent, roulade avant ça va plus vite.

Après avoir acquis "Charge Assassine", c'est ce sort qui va plus vite, couplé au
bonus de vitesse de Berserk.

Grosso modo, le jeu est linéaire donc pas de grosses surprises, même si 2-3
trucs funky.

#### 3.1.1.1. Avant l'arène

À la fin du tuto, acquérir "Charge Assassine" niveau 1.

Après la quête de la reine guêpe, il faut aller à Bowerstone faire de la
duplication d'objet.

Explication du menuing du marchand :
- vendre ses potions de vie et de résurrections
- acheter une potion de volonté, toutes les pommes et un anneau de marriage.
- **Menu Manipulation** pour dupliquer et vendre des potions de volonté et des
  anneaux de marriage.
- Racheter les potions de vie et de volonté.
- Acheter la canne à pêche et la pelle.

On se retrouve à environ 100k de pognon, 200 potions de chaque et surtout assez
d'expérience pour le menuing suivant.

Explication du menuing des compétences (full abus **Menu manipulation**) :
- Force : Physique niveau 5 ;
- Addresse :
    - Précision niveau 7 (max.) ;
    - Vitesse lvl 7 (max.)
- Volonté :
    - Multifrappe niveau 4 (max.) ;
    - Embrasement niveau 4 (max.) ;
    - Poussée magique niveau 2 ;
    - Invocation niveau 1 ;
    - Bouclier physique niveau 2 ;
    - Charge assassine niveau 4 (max.) ;
    - Berserk niveau 4 (max.) ;
    - Pluie de flêches niveau 2 ;
    - Puissance magique niveau 5 ;

Le problème du glitch pour les compétences, c'est que l'élément du menu qui
subit le **Menu Manipulate** n'est pas celui qui est vraiment en train de se
faire modifier, du coup c'est très confusant.

Une fois fait, on va utiliser le **Summon clipping** pour récupérer l'Épée des
Ages dans la Salle du Destin de la Guilde avec un clip dans le décors.

Ensuite **Load warping** à l'entrée de Greatwood.

Petit glitch plus loin pour passer à travers la porte du camp des bandits avec
une charge + roulade bien placée.

Récupérer l'arc de maître juste après le combat contre Twinblade, à la guilde.

Petit clip à travers une porte de pierre à Witchwoow pour une quête (les portes
de pierres sont des portes qui s'ouvrent seulement sous une certaine condition,
et qui contiennent généralement une récompense derrière).

Sinon grosso modo c'est speed% jusqu'à l'arène.

#### 3.1.1.2. L'arène

Juste avant de rentrer, on va manipuler le garde pour que le dialogue se déroule
plus vite.

Pour l'arène, vu que c'est une succession de combat qui s'enchaîne sans
coupure, c'est le premier vrai endroit du jeu où on peut efficacement augmenter
son combo d'XP.

On va abuser de deux sorts :
- Le sort Bouclier Physique, qui permet d'encaisser des dégâts sur le mana
  plutôt que la vie. L'effet est sympa mais le vrai bonus c'est que se faire
  toucher ne cause pas d'animation de récupération ni perte de combo.
- Le sort Poussée magique, qui inflige des dégâts et renverse les ennemis.
  L'effet est sympa mais le vrai bonus c'est le petit bug qui vient avec :
  quand un ennemi est tué et tombe au sol, le sort va quand même s'activer sur
  son cadavre et augmenter le bonus de combo.

On va aussi utiliser Embrasement niveau max pour tout tuer très vite, et voilà
une arène rondement menée.

Evidemment, au maximum du combo, on va utiliser tous les consommables qui
donnent de l'XP (bouffe et potion) pour de l'expérience à gogo.

À la fin de l'arène, on fait un **Load warping** pour terminer plus vite. Le jeu
estime que, comme on est sorti du dernier round, c'est qu'on a gagné.

#### 3.1.1.3. Cassage de cul

Une fois cette expérience acquise, on va juste maxer le perso et foncer jusqu'à
la fin du jeu.

- Force : Physique lvl 7 (max.)
- Volonté :
    - Poussée magique lvl 4 (max.)
    - Bouclier physique lvl 3
    - Pluie de flèche lvl 4 (max.)
    - Puissance magique lvl 7 (max.)

Après on fonce littéralement jusqu'à la fin, je compte sur ma mémoire pour
expliquer le reste.

# 4. Trivias

No sé.

# 5. Références

Lien speedrun.com : https://www.speedrun.com/fabletlc

Wikipedia : https://en.wikipedia.org/wiki/Fable_(2004_video_game)

Hotshot glitch : https://youtu.be/G5DlJTu93cY?t=4565
