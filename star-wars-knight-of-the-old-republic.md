# 0. Metadata

## 0.0. Versioning

| Date       | Révision                        |
|------------|---------------------------------|
| 2020-08-15 | Création (SGDQ 2020) - Run Any% |

# 1. Fiche Technique

Sortie et plateformes :
- 2003 : Windows, XBox
- 2013-05-30 : IOS
- 2014-12-21 : Android

Développé par : BioWare

Édité par : LucasArt

Genre : RPG

# 2. Histoire

## 2.0. Préambule Star Wars

Star Wars est un univers de science fiction connu notamment pour la Force, un
pouvoir surnaturel et les jedi, des individus qui manipulent ce pouvoir.
L'univers a initialement été construit par une trilogie cinématographique
sortie entre 1977 et 1983. Par la suite de nombreux films, livres voir même
jeux sont venus étayer la franchise. L'univers mets régulièrement en scène des
combats spaciaux ainsi que des duels de jedi, se battant à l'aide la Force et de
sabre lasers.

Le jeu se déroule 3 956 ans avant les événements de *Un nouvel espoir* , le tout
premier film sorti.

## 2.1. Prologue

Il y a très longtemps, dans une galaxie lointaine, très lointaine...

Juste avant le début du jeu, Malak et Revan, deux Jedis prometteurs, mènent
la lutte contre les mandalorien qui livrent une guerre acharnée contre la
République Galactique. Brillant stratège, Revan remporte des victoires clés qui
vont lui permettre de repousser l'ennemi jusque dans les confins de la galaxie.
Le contact sera perdu avec eux.

Durant quelque années, la République va lentement se reconstruire. Soudain
Revan et Malak reviennent, sauf que les Jedis méritant sont devenus de
puissants Jedi noirs, des Sith. Ils vont à leur tour mener des assauts sans
merci contre la République, et tuer ou ralier de nombreux Jedis à la cause Sith
par la même occasion.

L'ordre Jedis décide d'envoyer une de ses plus compétente représentante,
Bastilla Shan, possèdant une rare capacité de médiation de combat, pour arrêter
les meneurs Sith. Sa troupe va intercepter et triompher sur Darth Revan, le
Maître des Sith.

Cependant Darth Malak, l'apprenti de Revan, récupère le contrôle de l'armada
Sith et poursuit l'offensive. Tandis qu'il prend par surprise le vaisseau de
Bastilla, l'Endar Spire, en orbite autour de Taris, la République galactique est
sur le point de s'effondrer.

## 2.2. Jeu

### 2.2.0. L'Endar Spire

On débute le jeu (après création de personnage) sur l'Endar Spire, en tant que
recrue spéciale. Pris dans la tourmente de l'assaut surprise, le personnage doit
s'échapper et le jeu s'en sert de prétexte pour le tutoriel. Le joueur va finir
par évacuer le vaisseau, accompagnant officier reconnu de la République, Carth
Onasi. Il s'agit du premier sur neuf compagnon que le jeu propose pour former
le groupe de personnages contrôlés.

### 2.2.1. Taris

L'évacuation au moyen d'une capsule de sauvetage les fait s'échouer sur la
planète Taris. Les personnages se donnent pour objectif de retrouver Bastilla,
indispensable à la survie de la République et de s'échapper dans la foulée de
Taris. Taris est rapidement mis sous blocus Sith qui a le même objectif de
retrouver la Jedi.

Entre plusieurs visions de Darth Revan, rencontres avec d'autre personnages
qui le rejoindront (Mission Vao et Zaalbar) et quêtes secondaire, le
personnage-joueur finit par
retrouver Bastilla. Avec son aide et l'intervention de Canderous Oro, un
mercenaire qui cherche à s'échapper de son employeur Davik, seigneur du crime
local, les compagnons vont récuperer des codes d'accès Sith dans la forteresse
Sith  qui leur permettront de passer le blocus. Il vont également mettre un
terme aux agissement de Davik et voler son vaisseau, l'Ebon Hawk.

Les compagnons rejoints par Canderous s'échappent de Taris et mettront le cap
sur Dantoine.

### 2.2.2. Dantoine

Dantoine est une planète fermière qui pourtant possède un conseil jedi.
Bastilla informe le conseil de la réussite de sa mission contre Revan, ainsi
que la participation du personnage dans l'évacuation. De plus, de par ses
visions le personnage semble extrèmement sensible à la force et potentiellement
un atout de poids dans la lutte contre Revan. Enfin, Bastilla et le
personnage-joueur semblent liés par une connection par la Force.

À contrecoeur mais lucide quant au besoin de le former, les Jedis vont ouvrir
le personnage-joueur à la force et c'est à ce moment du jeu que le personnage
doit sélectionner une classe de jedi (cf gameplay). Le personnage-joueur doit
remplir plusieurs mission pour terminer sa formation, ayant par celles-ci
l'opportunité de sauver une Jedi, Juhani, du côté obscur.

Au terme de la formation, le joueur doit fouiller une très ancienne ruine,
auparavant explorée par Revan et Malak, afin de comprendre ce qu'ils y ont
trouvé et ce qui les aurait fait basculer du côté obscur.

Ils découvrent un savoir ancien qui appartenait à la race dite des Bâtisseur et
trouve une cart stellaire incomplète qui menerait à une structure connue comme
la Forge Stellaire. Après compte-rendu au conseil Jedi, le conseil décide de
donner pour mission aux compagnons de retrouver les autres morceaux de carte
sur les planètes Tatoine, Kashyyyk, Manaan et Korriban.

À partir de là les compagnons utilisent l'Ebon Hawk comme quartier général et
sont libres de leur déplacement sur les 4 planètes (+ Yavin contenu exclusif
lol).

### 2.2.3. La quête de la Carte Stellaire

#### 2.2.3.0. Résumé général

Le jeu se découpe maintenant en quatre parties relativement indépendantes pour
chaque planète. Chaque planète a une petite intrigue intrinsèque qui la relie
obligatoirement à la recherche du fragment de carte contenue sur celle-ci. Il
y a également quelques quêtes secondaires par-ci par-là et parfois du
backtracking.

#### 2.2.3.1. Tatoine

Sur Tatoine, il y a un conflit entre la Corporation Czerka et les hommes des
sables, entre la première qui essaye de miner la planète tandis que les
deuxièmes luttent contre les étrangers qui envahissent leur planète.

Au terme de la quête on peut accéder à la sous-zone qui contient la Carte
Stellaire.

#### 2.2.3.2. Kashyyyk

Sur Kashyyyk, les wookies sont réduis en esclavage par la Czerka avec la
collaboration du chef de clan. Quelques rebelles luttent contre cette situation
révoltante. La résolution de la situation conduit, quel que soit le camps
rejoint à l'accès de la Carte Stellaire.

#### 2.2.3.3. Manaan

Sur Manaan, monde océanique des Selkath, la planète neutre est visitée à la fois
par les forces Sith et Républicaines. Le groupe va être mis à la recherche d'une
station d'extraction de kolto avec laquelle la République a perdu le contact.

#### 2.2.3.4. Korriban

Juste avant de rejoindre la quatrième planète, l'Ebon Hawk est pris dans une
embuscade par le Léviathan. Il s'agit du vaisseau amiral de Saul Karath, ancien
mentor de Carth Onasi et bras droit de Darth Malak. Le groupe apprend que
l'académie Jedi de Dantoine est détruite. Le groupe parvient à tuer Saul et
s'échapper mais c'est alors qu'arrive Darth Malak qui révèle au
personnage-joueur qu'il est Revan, étrangement amnésique. Pour permettre
au groupe de s'enfuir, Bastilla se lance seule contre le Sith.

Le groupe peut s'enfuir mais Bastilla finit capturée.

La seule solution est de trouver la dernière carte sur la planète des Sith.
Le groupe devra s'infiltrer parmi eux jusqu'à trouver la carte, ce qui les
éprouvera à la noirceur des siths.

### 2.2.4. Lehon et la forge stellaire

La carte recomposée révèle que la Forge Stellaire se situe dans un système
appelée Lehon. Après plusieurs péripétie ou le groupe s'échoue sur une planète
inconnue à cause d'un champ de disruption originaire de celle-ci, croise
Bastilla brisée au côté obscur par la torture de Malak et réveille une ancienne
civilisation.

Libéré de la disruption, l'Ebon Hawk charge la Forge Stellaire. Dans la
fin canonique, Revan ramène
Bastilla du côté lumineux, triomphe de Malak et sauve la république.

La seconde fin montre Revan succombant à nouveau au côté obscur et reprennant
les rennes de l'armada Sith des mains calcinées de Malak et menant la République
à sa chute.

# 3. Commentaire

## 3.0. Général

### 3.0.0. Mécaniques de jeu

KotOR est un RPG occidental, fortement inspiré dans son système de jeu de
Donjons & Dragons 3.5. On contrôle un groupe de personnage-joueurs.

Le personnage principal a le choix entre 3 classes de personnages pour démarrer,
et devra changer pour des niveaux futur en 1 parmi 3 classes de jedi:
- Soldat, focalisé sur le combat
- Éclaireur, à mi-chemin entre soldat et voyou
- Voyou, focalisé sur les compétences utilitaires
- Jedi Gardien, jedi focalisé sur le combat
- Jedi Sentinelle, jedi à mi-chemin entre gardien et conseiller
- Jedi Conseiller, jedi focalisé sur l'utilisation de la force

On retrouve les 6 attributs emblématiques des D&D:
- Force, qui améliore les performances au combat corps-à-corps
- Dextérité, qui améliore les performances à distance et la class d'armure d'esquive
- Constitution, qui améliore la vie et les resistances physiques
- Intelligence, qui permet d'avoir plus de points de compétences et améliore les resistances mentalles
- Sagesse, qui améliore la manipulation de la force et la résistance à celle-ci
- Charisme, qui améliore la manipulation de la force et la résistance à celle-ci

On retrouve par contre des compétences différentes :
- Informatique : pour pirater des systèmes
- Démolition : pour placer des mines
- Discrétion : pour se cacher
- Vigilance : pour détecter les gens qui se cachent
- Persuasion : pour convaincre les gens
- Réparation : pour réparer les droides
- Sécurité : pour crocheter des trucs
- Premiers soins : pour se soigner à l'aide de trousses

La gestion des pouvoirs de force est également différente. À chaque niveau de
jedi, celui-ci peut obtenir 1 ou 2 pouvoir additionel parmi une liste
prédéfinie.

Les combats se déroulent en temps réels par système de rounds qui simule le
tour par tour d'un système D&Desque. Chaque personnage à le droit à un certains
nombre d'actions par round.

Le jeu étant un RPG, il y a beaucoup d'opportunité de jouer diplomatiquement
et de résoudre certaines situation par discussions également.

### 3.0.1. Techniques émergentes

https://www.speedrun.com/kotor1/guides

Les principales sont :

**Anywhere Menu Glitch (AMG)** : Anywhere Menu Glitch est un glitch qui permet
d'avoir un contrôle limité sur les personnages alors que le jeu est dans un menu
ou dans une cutscene. C'est depuis ce glitch que sont déclenchés de nombreux
autres.

**Unskippable Cutscene Skip** : Glitch grace à l'AMG, on peut skip des cutscene
que en fait on devrait pas pouvoir.

**Save Buffering** : Glitch qui consiste en l'abus du fait que la sauvegarde
place le joueur dans un état de non-combat, non-trigger, non-cutscene et
non-conversation pour passer outre un ou plusieurs de ces éléments.
*NOTE*: La sauvegarde rapide dans KotOR a un memory leak et les runs prévoient
*dans la route* les moments optimaux pour fermer le jeu et le relancer afin
de nettoyer la mémoire.

### 3.0.2. Règles globales du speedrun

- Cliquer sur la barre de menu (SST) ou underclocker l'écran pour augmenter la
distance de *Save teleporting* est banni.

- Aucune sauvegarde créée avant la run ne doit être utilisée pendant celle-ci.

- L'utilisation excessive de quicksaves pour abuser le suppresseur de chargement
est banni. C'est interprêter comme niquer son RTA pour améliorer son IGT.

- Pour nettoyer les memory leaks et prévenir n'importe quelle manipulation
pré-run, le jeu doit être quitté et relancé entre chaque run.

- L'utilisation des mods est interdite.

## 3.1. Any%

### 3.1.0 Informations diverses

Plateformes privilégiés :
- PC

Temps :
- WR : **41m32s910ms** RTA - **34m14s570ms** IGTA par ChaosDrifting (ChaosDrifter)

Type de run : FULL GLITCH SA MÈRE

Chronomètre (RTA) :
- Démarre en appuyant sur "Play" *après* la création du personnage.
- Termine sur l'écran noir *après* le coup final sur Malak.

Règles :
- Completer le jeu via n'importe quelle fin dans n'importe quelle difficulté.

### 3.1.1 Route

https://www.speedrun.com/kotor1/guide/hmjxg

#### 3.1.1.0. Création de personnage

Classe : Voyou (parce que sécurité)

Attributs :
    - STR: 18
    - DEX:  8
    - CON: 15
    - INT:  8
    - WIS: 14
    - CHA:  8

Skills :
    - Computer Use: 2
    - Persuade: 4
    - Security: 4

Don : "Weapon Focus: Melee Weapons"

#### 3.1.1.1. Le reste

Il se passe tellement de truc que j'ai pas envie de noter. CF le guide sr.com.

Grosso modo on va rien faire comme prévu.

# 4. Trivia

- Kotor est une ville fortifiée sur la côte adriatique de Monténégro (ben quoi ?)

# 5. Références

- [Wikipedia](https://fr.wikipedia.org/wiki/Star_Wars:_Knights_of_the_Old_Republic#La_qu%C3%AAte_des_Cartes_Stellaires)

- [speedrun.com/kotor](https://www.speedrun.com/kotor)
