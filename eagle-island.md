# 0. Metadata

## 0.0. Versioning

| Date          | Révision              |
|-              |-                      |
| 2022-06-25    | Update structurelle   |
| 2020-01-05    | Création (AGDQ 2020)  |

# 1. Fiche technique

Sortie : 11 juin 2019

Plateforme : PC (Windows)

Développé par : Pixelnicks

Édité par : Screenwave Media

Genre : plateformer 2D rogue-lite metroidvania

# 2. Histoire

## 2.0. Prélude

Quill, accompagné de ses chouettes Koji et Ichiro est en voyage sur la mer quand
il est pris dans une tempête et fini par s'échouer sur un rivage.

## 2.1. Jeu

En explorant l'île, Ichiro se fait capturer par un aigle géant appelé Armaura.

On croise le Dr. Oliver Ornis, dont sa chouette Feather a elle-même été capturée
et il révèle que l'aigle est un danger pour divers oiseau de l'île. Il
demande de chercher l'aide de divers oiseaux-totems fantastiques de l'île dont
les pouvoirs légendaires permettraient d'aider à combattre Armaura et donne à
Quill donne un gant de fauconnier pour que l'on puisse se battre avec Koji.

Les totems sont :
- Zephara, totem de foudre (Koji prolonge son attaque quand il touche un ennemi)
- Magira, totem de feu (Koji explose à l'impact)
- Icora, totem de glace (Koji gèle les ennemis, qui tombent au sol et éclatent)

Une fois récupérés, Quill va combattre Armaura pour le distraire pendant que le
Dr. Ornis répare son dirigeable pour pouvoir le bombarder. Quill n'est
malheureusement pas assez fort et Armaura le jette dans une plaine. À son
réveil, Quill entend la voix de "l'Oracle" qui lui révèle que Armaura n'est
peut-être pas aussi méchant qu'il le croit.

On va récupérer divers powerups tandis qu'il nous donne des détails sur Armaura.

Powerups :
- nager
- double jump
- charge attack

Au début, sur l'île, les humains vivait en harmonies avec les pouvoirs des
totems et la protection d'Armaura, mais les hommes devinrent de plus en plus
avaricieux. Armaura finit par les attaquer, après quoi les survivants se
cachèrent dans les sanctuaires. Ils inventèrent les canons. Armaura a été
corrompu(e) quand ils l'ont emprisonné(e). L'Oracle souhaite la sauver, mais
pour cela il faut lui rappeler qui elle était.

Il nous révèle aussi que le scientifique (le docteur Ornis), offensif, ne va
pas nous aider à la ramener. Pire, lorsqu'on sauve Armaura, il tire un coup de
canon qui la/le tue. Pire encore, alors que les totems sont libérés, il les
capture pour les utiliser à ses fins. Mais le pire du pire, à notre refus, il
capture Koji. Heureusement, après la défaite d'Armaura, Ichiro est libéré et
va nous aider à la place de Koji pour le combat.

On va courir après le Dr. Ornis, qui va réactiver les fontaines élémentaires
pour charger l'île en énergie. On arrive trop tard à chaque fois, mais une fois
cela fait, le chemin vers la machine qui enferme les totems est ouvert, et on
va les libérer.

Bien sûr le Dr. Ornis n'est pas content du coup on lui casse la figure. Sur le
point de nous tuer, Koji se sacrifie ce qui déclenche la colère des totems, qui
finissent par enfermer le Dr. dans une prison de glace.

Impressionnés par le sacrifice de Koji, ils ressemblent leurs pouvoirs pour le
ressuciter, ce qui le transforme en oiseau protecteur de l'île, successeur
d'Armaura.

# 3. Commentaire

## 3.0. Informations diverses

Runné sur PC

Catégorie : Any % - Official Seed Core Rules

Temps :
- WR : **47m56s** RTA - **43m06s** IGTA par KarlF
- 3e : **48m59s** RTA - **44m00s054ms** IGTA par MrDino023

Run du skill et du par coeur.

Chronomètre (RTA) :
- Démarre en appuyant sur "Play Official Seed"
- Termine sur la transition d'écran après le tir d'Ice Bird Totem sur Ornis

Official Seed : Jeu rogue-lite donc avec des seed. Le jeu a une seed officiel
pour les speedruns

Core Rules : Le jeu permet d'adapter un peu ses règles (à la Celeste). Le
speedrun utilise les règles par défaut.

## 3.1. Mécaniques de jeu

Plateformer 2D Metroid-Vania Rogue-lite

On contrôle Quill, qui a un familier faucon (Koji ou Ichiro) qui s'envoie sur
les ennemis. On peut envoyer Koji dans les 8 directions cardinales mais on
s'immobilise lorqu'on le fait.

Quill peut marcher, sauter et s'accrocher aux corniches. Il a 3 pv (6 en
facile).

On récupère des powerups qui change l'attaque de Koji, mais qui coûte des
manaroches (des points de mana en gros). Quill a 8 points de mana max.

Les graines d'or servent à unlock des coffres, les pièce d'argent à acheter
des choses à l'oiseau marchand.

Quill peut équiper jusqu'à 4 runes qui modifient passivement ses
caractéristiques. Elles sont limitées à un donjons et ont une durée de vie.

En cas de chain-kill, les ennemis droppent des manaroches ou même des coeurs,
pour regagner des pvs.

L'overwold est aléatoire, les donjons sont générés procéduralement.

Dans les donjons, certaines portes sont vérouillées et nécessite d'avoir tué un
certain nombre de monstre dans le donjon.

Dans les donjons avancés, certains mobs droppent des clés pour ouvrir certaines
portes vérouillées.

## 3.2. Techniques

Aucune particulière (!)

## 3.3. Route

Run très visuelle, la route est optimisée mais n'a pas d'élément hors-du-commun
par rapport à un jeu casu.

On utilise la seed officiel pour connaître précisément l'aléatoire des donjons
et avoir une route fixe.

Évidemment, pour coller aux limites de mobs tués par portes, il y a un compte
relativement précis d'ennemis à tuer et c'est prévu.

On va prendre quelques passages secrets pour aller plus vite et abuser de double
sauts à un moment.

# 4. Trivia

# 5. Références

Lien speedrun.com : https://www.speedrun.com/eagleisland

Lien steam : https://store.steampowered.com/app/681110/Eagle_Island/

