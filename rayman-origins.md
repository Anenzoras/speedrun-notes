# 0. Fiche technique

Sortie : 2011-2012 (plein de dates selons les pays et les plateformes)

Plateforme : PS3, Xbox 360, Wii, PS VITA, 3DS, Windows, Mac OS X

Développé par : Ubisoft Montpellier

Édité par : Ubisoft

Genre : Plateformer Metroid-Vania

Concepteur : Michel Ancel

Compositeur : Christophe Heral

# 1. Histoire

(Wipidia FTW:)

## 1.0. Prélude

Il y a fort longtemps, la Croisée des Rêves a été peuplée par le puissant
Polokus, dont tous les rêves deviennent réalités. Mais le calme de ce monde est
aujourd'hui brisé : les créatures malfaisantes issues des cauchemars de Polokus,
jusqu'alors enfermées dans la lugubre Lande aux Esprits Frappés, s'échappent de
leur prison souterraine pour semer à nouveau le chaos dans la Croisée des Rêves.

## 1.1. Jeu

Pour sauver son univers, le héros sans bras ni jambes Rayman doit de nouveau
partir à l'aventure. Il est accompagné de son meilleur ami Globox et de deux Ptizètres. Ils commencent par parcourir les cinq premiers mondes pour libérer
les cinq fées. Sur le chemin, ils libèrent les Électoons prisonniers et
échangent des Lums au Magicien pour obtenir plus d'Électoons.

Une fois les cinq mondes parcourus et les cinq fées libérées, celles-ci ouvrent
des passages vers les quatre mondes suivants pour que les héros aillent aider
les quatre rois. Ceux-ci, sous l'influence du mal, se sont transformés en
monstres. Les héros battent les monstres, pour permettre aux rois de retrouver
leur apparence normale.

Les fées ouvrent alors un passage vers le Cumulo Grincheux, une immense usine
dans les nuages. Les héros le parcourent et découvrent que le chef des méchants
et en réalité le Magicien. Celui-ci tente de les tuer en leur envoyant des
copies robotiques de boss battus précédemment. Comme cela échoue, il tente de
s'enfuir d'abord à pied, puis à bord d'un bateau volant. Mais lors de la fuite,
le bateau entre en collision avec le cœur de l'usine. Celui-ci est détruit et
l'usine explose intégralement. Les héros retombent alors sur terre.

Ils peuvent en plus poursuivre des Coffrapattes dans des niveaux spéciaux, afin d'obtenir des Dents de la mort. Une fois toutes les dents obtenues, les héros
peuvent entrer dans la Lande aux Esprits Frappés. Après un parcours assez
difficile, les héros arrivent au fond de la Lande et affrontent Big Mama, un
monstre gigantesque. Une fois vaincue, celle-ci reprend son apparence normale :
la dernière fée.

# 2. Commentaire

## 2.0. Informations diverses

Runné sur PC

Catégorie : any%

Temps :
- WR : **52m53s** by Twyn_o
- PB Mr.Tiger : 1h02m17s [pas sur les leaderboard pour le moment]

Run du méga skill, pas vraiment d'abus... ou presque :-p

Chronomètre :
- Démarre à la sélection du fichier de jeu
- Fini au déclenchement de la cutscene de fin dans "Shoot For The Stars"

## 2.1. Mécaniques de jeu

Un plateformer

Rayman commence en pouvant marcher et sauter, et sauter depuis les murs
(il s'y accroche un petit temps d'ailleurs). Il récupèrera des pouvoirs
additionnels en libérant les fées (attaquer, hélico, devenir petit, nager
courir sur les murs)

Metroid-vania oblige, certaines cages ne sont brisable qu’avec des pouvoirs
obtenus plus tard dans le jeu, du coup backtracking (euh je suis plus sûr en
fait).

Après on a le classique des jeux de plateformes : plateformes qui bougent
plateformes invisibles qui apparaissent, plateformes qui disparaissent quand on
marche dessus, objets cachés qui apparaissent uniquement si on va à un certain
endroit.

Les lums, les trucs jaunes qu'on ramasse, sont des indices visuels pour la suite
du niveau. Ils servent également à gagner des electoons à la fin du niveau si on
en ramasse un certain nombre. Les electoons sont des bouboules violettes qui
permettent de dévérouiller les niveaux suivants.

Si on se fait toucher, on meurt et recommence au dernier checkpoint. Les coeurs
en bouteille permettent de se faire toucher une fois additionnelle si on les
ramasse. Lorsqu'on en a déjà un, ça fait des lums en plus.

## 2.2. Techniques

Rien de bien spécifique au speedrun, du par coeur et de l'optimisation
principalement.

On va principalement tuer les ennemis soit en leur sautant dessus, soit avec
l'attaque en courant.

On optimise le ramassage de lums pour en avoir pile le bon nombre et aller au
plus vite de l'animation de fin de niveau (assez longue mine de rien).

Des damages boost là où ça compte.

**Death abuse** : La grosse technique abusive consiste à profiter d'une
mécanique de jeu qui propose explicitement au joueur de passer un niveau s'il
meurt sept fois sur dans un même tableau.

**Coop abuse** : Le jeu permet de jouer en coop. Hors en coop quand un joueur
sort de l'écran, il décède. On peut abuser de cette technique pour tuer les 2
personnages dans les niveaux où cela n'est pas forcément possible et donc trigger
un Death Abuse.

## 2.3. Route

Normal mais optimisée.

La run death abuse sur les niveau trop long.

*Le* death abuse essentiel se situe au milieu du jeu (à peu près) où l'on arrive
à un niveau bloqué. Dans l'histoire, il faut libérer 4 rois (== refaire d'autres
niveau dans les mondes déjà visités qui apparaissent à ce moment là). Sauf que
Coop Abuse et on s'en tire gratos.

# 3. Trivia

On peut skip l'animation de fin de level, mais c'est frame perfect.

# 4. Références

Lien speedrun.com : https://www.speedrun.com/rayman_1

Wikipedia : https://fr.wikipedia.org/wiki/Rayman_Origins

# Annexe A. Versioning

| Date       | Révision            |
|------------|---------------------|
| 2019-09-25 | Création (ZFM 2019) |
