# 0. Metadata

## 0.0.  Versioning

| Date          | Révision                              |
|-              |-                                      |
| 2022-06-25    | Update structurelle                   |
| 2018-10-xx    | Création (ZFM 2018) - Run Any% Easy   |

# 1. Fiche technique

Sortie : 2013

Plateforme : Windows, OS X (07-25) ; iOS, Android (09-26), Linux (10-30)

Développé par : Harebrained Schemes

Édité par : Harebrained Schemes

Genre : Tactical RPG

Moteur : Unity

# 2. Histoire

(Wipidia FTW:)

## 2.0. Shadowrun en général

Shadowrun Returns est tiré du système de jeu de rôle papier Shadowrun qui prend place dans un univers de science-fantasy, mélangeant cyberpunk et magie.

Shadowrun prend place plusieurs décénies dans le futur (2050 pour la première édition, pour l'actuelle 2080). La fin du cycle long du calendrier mésoaméricain a eu lieu dans le "Sixième Monde", avec des créatures mythologiques apparaissant alors et des formes de magie émergeant spontanément. Un grand nombre d'humain ont "goblinisé" en orks et en trolls, tandis que beaucoup d'autres sont nés en temps qu'elfes, nains, et même des créatures plus exotiques.
En Amérique du Nord, des peupls indigènes ont découvert que leurs cérémonies traditionnelles leur permettait de commander des esprits puissants. Les rituels, associés avec un nouveau mouvement "Ghost Dance" les a permis à prendre le contrôle de la plus grande partie de l'Ouest des États-Unis et du Canada ; où ils ont formé la fédération des Nations des Natifs Américains. Seattle est resté sous contrôle des États-Unis comme une cité-état enclavé par traité. La plupart des sources du jeu prennent place ici et supposent que les campagnes utiliseront la ville comme setting.

En parallèle de ces développements magiques, l'avancée temporelle va de pair avec les avancées technologiques et sociales, également associées à une science-fiction cyberpunk. Des mégacorporation contrôlent les vies de leurs employés et commandent leurs propres milices ; les dix plus grosses ayant par ailleurs l'extraterritorialité (droit actuel des ambassades). Les avances technologiques font que les cyberwares (des prothèse mécaniques) et des biowares (des organes augmentés) deviennent chose commune. Le Crash des Ordinateurs de 2029 conduisent à la création de la Matrice, un réseau d'ordinateur mondial avec lequel les utilisateurs peuvent intéragir via interface neuronale directe. Lorsque des conflits surviennent, les corporations, les gourvernements, les syndicats du crime organisé, et eventuellement des individus aisés sous-traitent le sale boulot à des spécialistes, qui effectuent des shadowruns (*wink wink*), autrement dit des missions remplies par des gens remplaçables sans identités ou qui veulent rester sans nom. Les plus compétents de ces spécialistes, appelés shadowrunners, ont gagné la réputation du travail fait et bien fait. Ils ont développé une capacité pour rester en vie et prospérer dans ce monde de Shadowrun.

## 2.1. Jeu (Dead Man's Switch)

Le jeu permet de créer et de jouer des campagne dans son gameplay, mais est toutefois fourni avec la campagne "Dead Man's Switch".

### 2.1.0. Prologue

Le joueur contrôle un shadowrunner qui reçoit un message pré-enregistré de l'un de ses vieux complice shadowrunner lui aussi, Sam Watts. Le message a été envoyé à cause d'un dead man's switch (une puce biomécanique qui se déclenche à la mort de l'hôte). Sam déclare qu'il a 100,000 nuyens (la monaie de Shadowrun) en récompense pour le joueur s'il traduit en justice (quelle que soit la forme de cette justice) la personne responsable de sa mort. Le message déclenche un flashback qui sert de tutoriel de combat.

### 2.1.1. Le tueur

Acceptant l'offre, le runner se dirige vers Seattle où il découvrira que Sam est la dernière victime de l'Éventreur d'Emerald City, un tueur en série qui ôte via chirurgie les organes de ses victimes. Peu après le runner rencontre Jake Armitage (Trivia nul: le protagoniste du jeu Shadowrun sur SNES) qui lui donne une piste et le conseil d'aller voir les lieux du crime.

Une fois cela fait, on découvre que Sam a une sœur jumelle. On découvre également que la dernière personne à l'avoir vu en vie est Coyote, une barmaid/shadowrunneuse qui par ailleurs manque à l'appel depuis qu'elle a accepté son dernier contrat. Après une courte investigation, le runner rejoint le petit ami de Coyote pour une mission de sauvetage dans un gang de dealer de puces BTL ("Better-Than-Life", une sorte de drogue psycho-informatique).

Avec l'aide de Coyote et à la suite de plusieurs missions où l'on parvient petit à petit à grapiller des informations sur le tueur de Sam, notemment de l'ADN, une run dans la matrice remonte la piste jusqu'à un certain Silas Forsberg qui se fait passer pour mort et a pris l'identité du docteur Holmes à l'asile psychiatrique proche. Le runner découvre également le point commun des meurtres : les victimes ont toutes reçu des transplantations d'organe qui appartenait à la mère de Sam.

Entre temps, après avoir parlé à Jessica Watts, la sœur de Sam, on après que ceux-ci n'étaient plus en contact depuis longtemps. De plus la plupart des informations au sujet de Sam tendent à dire que ce n'était pas le meilleur des hommes, puisqu'il était alcoolique notoire et avait tendance à dilapider le peu d'argent qu'il parvenait à obtenir. Jessica mentionne notamment qu'ils vivaient des jours paisibles et comfortables avant la mort de leur père mais qu'après cela et malgré les efforts de Sam pour vivre décemment, il a finit par craquer sous la pression et dépensa l'argent de sa famille dans la drogue et l'alcool. Il s'est finalement tourné vers les shadowruns pour joindre les deux bouts et nourir ses besoins auto-destructeurs.

Après avoir traqué et tué Silas, le joueur apprend ou comprend qu'il avait été commandité par Jessica Watts.

### 2.1.2. Le commanditaire

Le runner fait face à Jessica peu après l'enterrement de son frère pour lui demander des comptes mais elle s'enfuit. Le runner découvre alors qu'elle est un membre haut-placé de l'Universal Brotherhood, une organisation international du Nouvel Age qui attire les parias de la société. Par ailleurs, on rencontre également brievement Lynne Telestrian, qui ferait également partie des cercles supérieurs et qui veille sur Jessica.

Le joueur comprend assez qu'il a affaire à un mélange détonnant d'une corporation et d'une secte. Le runner et Coyote parviennent à la suite d'une run d'infiltration à accéder aux zones interdites du chapitre de l'organisation à Seattle. Cela révèle que l'Universal Brotherhood est juste une façade pour un culte bien plus sombre qui essaye de créer une ruche d'insecte-esprit, des créatures invulnérables qui cherchent à détruire toute vie pour prospérer.

On apprend alors que Jessica est un shaman, qui part ailleurs et l'une des rare à connaître la vrai nature de l'Universal Brotherhood et elle libère des insectes qui ne peuvent être tuer pour mettre fin aux jours du runner. L'équipe parvient tout de même à s'enfuir grâce à l'aide d'une femme nommée Marie-Louise, deckeuse de son état. Elle était emprisonnée pour devenir "la reine", de la même manière qu'une ruche d'insecte.

### 2.1.3. L'invasion

Une fois l'équipe sortie des enfer, le petit ami de Marie-Louise, un decker expert sous l'alias de Baron Samedi, contacte le runner pour organiser une shadowrun contre Telestrian Industries pour voler un échantillon du Projet Aegis; une arme chimique capable de tuer les insectes esprits. À la suite d'une longue run, le joueur parvient à acquérir l'échantillon mais est capturé lors de l'extraction, et est conduit face à James Telestrian III, le PDG de la mégacorporation, pour interrogation. Par ailleurs pendant cette run, le joueur peut découvrir que le père de Telestrian a eu des relations extra-conjugales avec Melinda Watts, engendrant ainsi les enfants Watts.

Quand il est alors révélé que Marie-Louise, que le runner a sauvé, n'est autre que la fille de Telestrian, il décide qu'au lieu de punir le joueur il va l'engager pour diriger l'équipe qui va déployer le Projet Aegis accompagné de l'elfe immortel Harlequin. On apprend entre autre que l'invocation d'un insecte esprit nécessite un être humain qui va accueillir l'esprit de l'insecte et dont le corps va être horrible altéré. Telestrian explique ensuite que le rituel de Jessica pour invoquer une reine insecte esprit dans ce monde nécessite un individu lié par le sang. Marie-Louise était ainsi une candidate de choix, au vu du lien de parenté entre elle et Jessica (rappel : adultère). Si Jessica parvenait à accomplir le rituel, cela resulterait en une invasion globale des être extra-dimensionels.

Telestrian donne au runner et à Harlequin un fusil à pompe capable de tirer des capsules remplies avec ce qui reste du composé Aegis dans le but de tuer les insectes esprits déjà invoqués. L'équipe infiltre à nouveau le chapitre de l'Universal Brotherhood, puis la ruche et combattent jusqu'à atteindre le cœur du sanctuaire interne. Là les runners découvrent que Lynne, la sœur de Telestrian s'est portée volontaire pour accuillir la reine, vu qu'elle aussi a un lien de sang avec Jessica. L'équipe parvient à interrompre le rituel, blessant sérieusement Jessica et tuant la plupart des insectes esprit dans la ruche. La reine esprit abandonne Jessica et le joueur a l'option de l'arrêter ou de la tuer. Lynne survit mais est arrêtée et éventuellement conduite dans un hôpital psychiatrique.

Le jeu termine lors de la sortie de la run avec une bonne partie des personnages rencontrés pendant le jeu. Le joueur peut discuter avec chacun pour plus d'informations. Harlequin nous apprend notamment que le Brotherhood a d'autres chapitres dans le monde, chacun habritant potentiellement une ruche. Une fois que le joueur prend contact avec le numéro donné par Sam pour rapporter son succès, un autre message pré-enregistré de celui-ci demande au runner de dire à sa sœur qu'il est désolé pour ce qu'ils ont vécus ensemble, et finit par révélé qu'il n'avait pas d'argent depuis le début.

### 2.1.4. Épilogue

L'épilogue sous forme de texte décrit les événements qui suivirent la campagne, et liant ce qu'il s'est produit avec le lore de Shadowrun.

La couverture médiatique des événements n'ont pas mentionné les shadowrunners ou les insectes esprits ; probablement dû à l'influence du Brotherhood. L'aegis a éventuellement finit par être produit en masse en un produit appelé "Fluorescing Astral Bacteria-3" ou "FAB-3". La ruche de l'Universal Brotherhood de Chicago a émergée et la ville est en grande partie scellée derrière des murs pour tenir les insectes à l'intérieur. Le FAB-3 a été utilisé quelques temps plus tard pour nettoyer la ville de l'infestation.

# 3. Commentaire

## 3.0. Informations diverses

Runné sur PC

Catégorie : any% Easy

WR is 1h51m42s RTA by tetronic

Run de RPG, du coup du routing et du lore.

Timer démarre au premier mouvement après avoir chargé "Down and Out" et fini après avoir raccroché le téléphone et au fade vers les crédits.

## 3.1. Mécaniques de jeu

C'est un RPG et les combats se joue en Tactical.

Le joueur peut dépenser de l'expérience (appelé karma) dans plusieurs caractéristiques.

- Body (= la vie = 10pv par rang).
- Quickness (chance to hit distance, chance to pas être toucher cac)
    - Ranged Combat (chance to hit ranged)
        - Pistol (1)
        - SMG (fusil auto) (1)
        - Shotgun (1)
        - Rifle (1)
    - Dodge (réduit chance d'être touché (physique))
- Strength (chances pour toucher cac/lancer, portée grenade)
    - Close Combat (chance to hit melee/unarmed, capacities)
        - Melee (1)
        - Unarmed (1)
    - Throwing Weapons (chance to hit grenade, capacities)
- Intelligence (decking: chance to hit, chance to dodge)
    - Biotech (soin++ avec un medkit)
    - Decking (chance to hit, capacities)
        - ESP control
    - Drone control
        - Drone Combat
- Willpower (chance to hit magic, chance to dodge magic)
    - Spellcasting (Mage)
    - Chi casting (Adept)
- Charisma (Control spirits, chance to hit conjuring spell, etiquettes)
    - Spirit summoning
        - Spirit control
    - Conjuring (chance to hit + barrier)

(1) chance to critical X, capacités X et équipement X, X étant le type d'arme associé.

Après c'est un RPG, du coup on parle au gens on interragit avec des trucs et on fait des quêtes.

Certains skills débloquent certaines options de dialogue.

## 3.2. Techniques

Alors. Du part coeur. C'est tout.

Build :

    Homme. Humain. Custom.
    Skills :
    - Body 3
    - Quickness 4
        - Ranged Combat 4
    - Intelligence 3
        - Decking 3
    - Charisma 2 (étiquette : sécurité)

## 3.3. Route

https://drive.google.com/file/d/0B3hRqcfMGu5aVnB0SkFULXM3NXY2al92WU5MWldBT0x6Um5Z/view

# 4. Trivia

Jeu kickstarté, la plupart des personnages ont été créés par des backers.

Jake Armitage est le protagoniste du jeu SNES.

Harlequin est un personnage phare du lore de Shadowrun, l'un des personnage les plus actifs dans la lutte contre les Horreurs. Il est immortel, insensible à la plupart des menaces physiques.

# 5. Références

Lien speedrun.com : https://www.speedrun.com/Shadowrun_Returns

Wikipedia : https://en.wikipedia.org/wiki/Shadowrun_Returns
