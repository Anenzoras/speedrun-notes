# 0. Metadata

## 0.0. Versioning

| Date          | Révision                          |
|-              |-                                  |
| 2025-01-06    | Création (AGDQ 2025) - Run No OoB |

# 1. Fiche technique

Sortie : 15 mars 2001

Plateforme : PC (Windows)

Développé par : Piranha Bytes

Édité par :
- Shoebox
- Xicat Interactive

Genre : Action-RPG

Compositeur : Kai Rosenkranz

# 2. Histoire

(Pompé de wikipédia:)

## 2.0. Prélude

La série des Gothic se déroule dans un monde médiéval *dark fantasy*.

Le roi Rhobart II dirige le royaume de Myrtana. Il a réussi à le préserver de
tous les dangers excepté des invasions d'orques. Pour poursuivre sa lutte contre
ces derniers et forger les armes pour équiper ses guerriers, il manque
cruellement de minerai de fer. Tout homme reconnu coupable, même pour le délit
le plus futile, est condamné aux travaux forcés dans les mines du royaume. Afin
d'empêcher toute évasion, le roi ordonne la création d'une barrière magique
autour du complexe minier. Malheureusement, un problème survient lors de la
conception de la barrière magique et le complexe minier se trouve totalement
coupé du royaume. Il est toujours possible d'envoyer des personnes et des biens
vers le complexe minier et de recevoir les cargaisons de minerai de fer en
provenance de ce dernier. Par contre il n'est plus possible aux personnes qui y
vivent de quitter le complexe minier. Profitant de la situation, les condamnés
prennent le pouvoir, créant de fait une colonie minière. Le roi Rhobart II se
voit contraint de négocier avec les nouveaux dirigeants de la colonie, leur
faisant parvenir tous les biens qu'ils désirent en échange des cargaisons du
précieux minerai.

## 2.1. Jeu

Dans ce contexte, le héros du jeu est reconnu coupable (pour un crime non
explicité dans le jeu) et envoyé vers la colonie minière. Avant d'y pénétrer,
il reçoit une lettre cachetée avec mission de la remettre aux mages du feu.

Sans armes et sans protection, le héros doit faire face aux périls de la
colonie.  Dans la pure tradition des jeux développés par Piranha Bytes, le
personnage principal n'a pas de nom. On l'appelle simplement le héros sans
nom (HSN). La colonie abrite trois camps humains. Le vieux camp et son château
sont dirigés par Gomez. C'est lui qui contrôle le commerce avec le roi
Rhobart II. Le nouveau camp, dans les rizières, abrite des rebelles dirigés par
Lee. Le troisième camp abrite la secte de la Fraternité sur laquelle veille
Y´Berion.

Parmi les personnages importants sont Diego, mentor du HSN ; Thorus, chef des
gardes du vieux camp ; Gorn, un des principaux lieutenants de Lee dans le camp
rebelle ; Milten Plescott, jeune magicien du feu au vieux camp ; et Xardas
expert émérite en magie du feu et nécromancie.

En se mêlant au conflit entre les trois factions, le héros va progressivement
découvrir une menace d'un ancien Dieu, qu'il va finir par combattre et détruire,
engeandrant la destruction de la barrière et libérant les prisoniers.

# 3. Commentaire

(Mauvaise qualité)

## 3.0. Général

### 3.0.0. Mécaniques de jeu

Action RPG plus ou moins classique.

De la vie, du mana, des stats, des skills.

On peut lancer des sorts avec des parchemins.

Des potions.

Des armes, des armures, etc.

A noter, il y a une touche action pour tout faire, ce qui est très rigide comme
gameplay au vue des standard aujourd'hui (exemple, pour attaquer il faut :
sortir son arme, maintenir *action* et appuyer sur une touche de direction).

Le jeu est très difficile, il y a beaucoup d'opportunité de clamser pour rien,
sans préparation.

### 3.0.1. Techniques émergentes

Le moteur original du jeu est fixé à 21 images par secondes. En maintenant une
touche enfoncée, les FPS augmentent jusqu'à atteindre 255 images par seconde.

Les runners vont souvent maintenir une touche enfoncée pour les dialogues du
coup.

Sauter ça va plus vite.

Loot en mode combat ça va plus vite.

---

**No map glitch** (nom non officiel, abrégé en **m** pour le raccourci) :
ouvrir la carte alors qu'on n'a pas de carte reset la velocité du personnage.
On va s'en servir pour ouvrir le menu dans des situations où ce ne sera pas
nécessairement possible (genre en chute, par exemple). On va aussi s'en servir
pour carrément annuler une chute.

C'est le glitch principal du jeu, duquel certains autre vont découler.

---

**Catapult glitch** : mettre le jeu en pause à la bonne frame alors que le
personnage commence à marcher et relacher la pause propulse le joueur dans la
direction en question.

**Chain jump** : maintenir avant et la touche de saut ; et ensuite **m** à
interval régulier pour reset la vélocité, et refaire un saut.

**Manual clip** (je sais pas comment l'appeler autrement) : maintiens de la touche de saut + avancer + **m** en même temps pour glisser et clip à travers
le sol. Posez pas de question, ça marche.

**Save/load** : comme beaucoup de jeu, sauvegarder et charger à des effet très
bénéfique. On notera ici le reset du comportement de la majorité des créature
du jeu, à l'exception de certains humains et orcs.

## 3.1. No OoB (last update: AGDQ 2025)

### 3.1.0. Informations

#### 3.1.0.0. Description

Runné sur PC.

Temps :
- WR - Blackhuf : **09m 40s 659ms** (LRT) - **09m 46s 160ms** (RTA)
- 15ème - PokerFacowaty : **11m 52s 894ms** (LRT) - **12m 05s 829ms** (RTA)

Type de run : C'est bugué et c'est pété

#### 3.1.0.1. Règles

##### 3.1.0.1.0. Général

https://www.speedrun.com/notr/guides/jq5aa

Le jeu a besoin d'être patché pour runner le jeu correctement.

Les FPS ne doivent pas être restreintes en dessous de 20.

##### 3.1.0.1.1. Chronomètre

Ca démarre quand on appuie sur "nouvelle partie" ;

Ça s'arrête quand l'écran devient noir après planter la dernière épée dans le
dernier pilier.

### 3.1.1. Route

On va grimper en haut de la montagne pour setup un skip.

**Catapult glitch** into **m** into save into **m** into load pour reset la
caméra into **m** pour reset la chute.

Manipulation d'IA pour éviter de se faire attaquer.

Loot le coffre (des parchemins).

**Chain jump** pour passer au dessus de la collision de la porte du temple.

Dodge le squelette.

**Manual clip** dans la pente et passer à travers la porte.

**Chain jump**, activer le levier. **Save/load** pour reset le comportement de
l'ennemi.

Prêtre 1 : sauter contre un mur et straffer jusqu'à ouvrir la porte ;
**save/load** juste avant le dialogue pour pas le déclencher (grace au straffe)
ni que le prêtre utilise des sorts. Refaire le même straffe jump contre le
pilier pour skip l'animation, déclencher le piège, attendre que le mob
passe dessus et **save/load** jusqu'à ce qu'il meurre. Loot.

Récupérer potions. Attention au mob dans le noir. Attentions aux mobs pas dans
le noir.

Prêtre 2 : activer le mécanisme + cancel animation avec battle mode +
**save/load** pour stop l'anim du mob qui a poppé ; refaire ça une fois ; la
troisième fois, juste cancel l'animation. Plusieurs techniques possible
ensuite, le runner passe dessus le mécanisme puis backdash immédiatement à la
fin du dialogue et attent que les squelettes casse la djeule au prêtre parce
qu'ils attaquent la cible la plus proche.

En attendant, on setup le mana et on prépare les sorts avec les parchemins.

**save/load** pour reset les squelettes, loot le prêtre, unlock la porte.

Équiper un spell, marcher vers la porte du 3ème spell, se caser contre la porte
et lancer la transformation en chien qui va nous faire clip à travers la porte.

Prêtre 3 : straffe contre le pilier de gauche fais skipper la cutscene. Le prêtre nous ignore parce qu'on est un chien. Se transformer back derrière la
table. On attire le prêtre sur un piège qui l'insta-kill. Loot le spell, le
prêtre et la potion de mana.

Ensuite faire l'amour au mur into transformation into clip. Se retransformer
plus loin avant le prêtre 4 into **chain jump**.

Prêtre 4 : plusieurs méthodes pour le tuer mais qui se résument à : l'attirer sur le piège pour qu'il décède. Loot en vitesse et go.

Série de sauts précis qui font peur au-dessus des piques plus loin.

Prêtre 5 : déclencher le dialogue au bord du vide, puis sauter immédiatement sur
le côté. Le prêtre devrait être coincé. Maintenir le clic gauche enfoncé pour
locker la caméra sur l'ennemi, reculer un peu et masher **m**. Il est possible
de looter l'ennemi au moment où il meurt grâce au mashing.

(Backup si on a pas réussi à looter, sortir de la salle et revenir fait
respawn le cadavre au milieu).

On continue **chain jump** pour éviter de se faire tuer par des humains qui ne
reset pas leur comportement avec **save/load**.

Skip de cutscene avec un jump bien placé.

Au boss de fin, save puis pause buffer pour déclencher sa cutscene mais avec le
menu ouvert into load au moment où on déclenche la cutscene. Après ce reload
toutes les créatures ne peuvent plus intéragir avec nous mais le personnage est
toujours actif.

Planter les épées qu'on a récupéré sur les prêtres dans les pillier et time.

# 4. Trivia

n/a

# 5. Références

No OoB guide : https://www.youtube.com/watch?v=M3Q_EPfJC50

Speedrun.com : https://www.speedrun.com/gothic

Wikipedia : https://fr.wikipedia.org/wiki/Gothic_(jeu_vid%C3%A9o)
