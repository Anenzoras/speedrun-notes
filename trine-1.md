# 0. Metadata

## 0.0. Versioning

| Date       | Révision                                               |
|-           |-                                                       |
| 2023-05-26 | Création (SGDQ 2023) - Enchanted Edition Run Any% NG+  |

# 1. Fiche technique

Sortie (Jeu original) :
- Microsoft Windows : 3 juillet 2009
- PlayStation 3 :
  - EU : 17 septembre 2009
  - NA : 22 octobre 2009
- OS X : 2 novembre 2010
- Linux : 12 avril 2011

Sortie (Enchanted Edition) :
- PC : 24 juillet 2014
- PlayStation 4 :
  - EU : 17 décembre 2014
  - NA : 23 décembre 2014
- Wii U :
  - NA : 12 march 2015
  - EU : 26 march 2015
- Nintendo Switch : November 9, 2018

Plateforme : Windows, Mac, Linux, PS 3, PS 4, Wii U, Switch

Développé par : Frozenbyte

Édité par : Nobilis

Compositeur : Ari Pulkkinen

Genre : Action-aventure puzzle-plateformer

Notes : *Enchanted Edition* est un remake du premier Trine fait avec le moteur
du deuxième jeu.

# 2. Histoire

## 2.0. Univers

Le jeu se déroule dans un univers de high fantasy où la féérie et la magie sont
de mise.

Le monde de Trine est très inspiré de l'ambiance des comptes de féé.

## 2.1. Intrigue

*(Meeeeeeeeerciiiiiii Wikipedia !)*

Dans un royaume autrefois prospère, des hordes de morts-vivants ont quitté leurs
tombes et envahissent le pays. Les populations fuient et l’Académie Astrale, le
sanctuaire des magiciens, est laissée à l’abandon...

À l’exception de trois personnages :
- une voleuse (Zoya) attend son heure pour s’emparer du précieux trésor gardé au
  cœur de l’Académie ;
- un magicien (Amadeus) peureux s’est réfugié dans l’immense bâtiment pour
  rester à distance des horribles monstres ;
- et un brave chevalier (Pontius) s’est juré de protéger l’Académie Astrale des
  morts-vivants, bien qu’il ne sache pas vraiment ce qu’est un mort-vivant.

Alors que la Voleuse s’apprête à s’emparer de l’artefact ancien, sa main reste
prisonnière de l’une des trois faces de l’objet.

Attirés par la lueur émanant du trésor, le Magicien et le Chevalier se
précipitent dans le sanctuaire et tentent de lui reprendre l’objet. Au moment où
leurs mains se posent sur les deux autres faces de l’objet, les trois
personnages se retrouvent brusquement liés les uns aux autres d’une façon
inconfortable.

Finalement, le Magicien se souvient : cet objet s’appelle le Trine, une relique
pouvant « unir les âmes ». Un seul d’entre eux peut exister physiquement tandis
que les deux autres demeurent reclus dans le Trine. Il se rappelle également que
l’artefact est lié à la légende du Gardien, dont la sépulture est située sous
l’Académie Astrale.

À la recherche d’un moyen de séparer leurs âmes entremêlées, la Voleuse,
le Magicien et le Chevalier unissent leurs talents respectifs pour se frayer un
chemin jusqu’aux catacombes. Sur la tombe du Gardien, le Magicien déchiffre des
inscriptions anciennes : autrefois, trois reliques (une pour l’esprit, une pour
l’âme et une pour le corps) étaient protégées par trois gardiens qui se
servaient de ces objets pour faire régner la paix sur le royaume. Le Magicien en
conclut que réunir les trois artefacts devrait leur permettre de rompre le sort
les liant les uns aux autres.

D’après les inscriptions, l’une des trois reliques serait cachée dans le château
du vieux roi. Le trio s’y rend (non sans affronter quelques morts-vivants au
passage) et entreprend de fouiller les lieux. Ils finissent par découvrir le
journal du roi, lequel indique que les artefacts de l’esprit et du corps sont
gardés dans des ruines lointaines, ancien repère des gardiens où les trois
reliques furent créées.

En explorant les ruines, les trois aventuriers découvrent que le Trine,
l’artefact de l’âme, fut un jour séparé des deux autres artefacts par un
tremblement de terre. La catastrophe épargna l’artefact de l’âme mais souilla
ceux de l’esprit et du corps, engendrant l’apparition d’une tour démoniaque qui
réveilla les morts. Malgré les attaques d’un énorme mort-vivant, les trois
aventuriers parviennent à atteindre le sommet de la tour et à réunir les trois
reliques, libérant leurs âmes. La magie engendrée par l’union des objets
magiques fait alors disparaître toutes les créatures démoniaques du royaume et
revenir les habitants qui acclament la Voleuse, le Magicien et le Chevalier
comme leurs nouveaux héros.

# 3. Commentaire

## 3.0. Général

### 3.0.0. Mécaniques de jeu

Action-aventure puzzle-plateformer

Le gimmick du jeu est qu'il est en 3D cependant les personnages ne se déplacent
qu'en 2D.

Le deuxième gimmick du jeu est qu'on joue trois personnage à la fois, mais pas
en même temps. Il y a une touche pour changer de personnage actif.

Le guerrier est un combattant corps-à-corps très centré sur le cassage de
bouche, la voleuse est une archère et dispose aussi d'un grappin qui s'accroche
aux plateformes en bois pour se déplacer plus aériennement et le magicien ne
combat pas mais fait apparaître des caisses ou des planches de bois pour les
puzzles. La physique du jeu lui permet également, via télékinésie, de tout de
même pouvoir tuer les ennemis.

Mécanique de vie/mana standard. Les monstres tués droppent des potions de vie et
de mana qui rechargent leur ressource respective quand ramassés.

Les checkpoints régulier rechargent également les points de vie et mana et
rescussitent les personnages mort, avec plus ou moins d'efficacité en fonction
du niveau de difficulté.

L'expérience s'acquiert sour forme de potions droppés par les ennemis ou de
à récolter dans le niveau. Toutes les cinquantes potions récoltés le groupe
gagne un niveau et par ce biais un point de compétence.

Les points de compétence sont donc partagés entre tous les personnages et
peuvent être répartis pour débloquer leurs compétences. Celles-ci peuvent leur
donner des attaques additionnelles, plus de projectiles, plus d'objet à invoquer
ou différent...

Il existe également des objets / artefacts à récolter tous le long du jeu qui
améliore les personnages, soit par leurs dégâts, soit leur nombre de points de
vie, soit leur nombre de points de mana, etc.

Sinon à part les secret, les niveaux sont linéaires et s'enchaînent.

Du fun.

### 3.0.1. Techniques émergentes

**Infinite charge** : permet de faire une charge de Pontius mais sans que le jeu
interrompe la charge. Pour ce faire il faut commencer une charge avec le
chevalier et dans les premières frames intervertir deux objets dans le menu.
Ça suffit pour activer l'état glitché.

Visiblement, changer d'item désactive le timer qui compte la charge jusqu'au
changement de personnage.

**Door clip** : lorsque l'on est bloqué dans certaines arènes obligatoire du
jeu, (et à d'autres endroit) on peut passer outre les murs pour ne pas faire
l'arène. Il faut switcher entre Guerrier, Voleuse et Magicien puis revenir au
menu tout en se déplaçant et le jeu nous ejecte vers la suite.

À priori, les personnages sont placées dans une position différente au
chargement de la partie, et est légèrement manipulable en alternant les 3
personnages car la commutation influence la position de spawn.

**Triangle clip** : il est possible de passer à travers le décors à certains
endroits du jeu en abusant du grappin de Zoya contre le triangle du mage.
À priori, grappiné le triangle essaye de nous pousser à une distance minimale
de celui-ci, et la force générée est suffisante pour passer à travers le niveau.

Apparemment possible car certains personnages perdent leur collisions au
changement.

## 3.1. Any% NG+ (SGDQ 2023)

### 3.1.0. Informations diverses

#### 3.1.0.0. Description générale

Runné sur PC.

Runnés en NG+ c'est à dire avec une sauvegarde complète ; niveau maximums, tous
les objects et compétences débloquées.

Temps (SGDQ 2023) :
- ssqice - WR : **16m 03s 640ms** IGT
- GrosHiken - 2ème : **16m 05s 940ms** IGT

Joué en mode facile.

Type de run : NIIIIOOOOUUUUUMMMM

#### 3.1.0.1. Règles

*(Tirées de speedrun.com, toutefois les règles ne concernant pas le jeu en
lui-même ont été exclues)*

##### 3.1.0.1.0. Général

Finir les niveau du premier au quinzième.

Les runs doivent être complétés sur l'*Enchanted Edition*.

##### 3.1.0.1.1. Chronomètre

Les runners utilisent LiveSplit pour chronométrer le temps IGT avec précision.
Les splits sont automatiques.

Le chronomètre démarre quand le bouton "Skip" est pressé au niveau 1.

Le chronomètre se termine losque le joueur ne contrôle plus les personnages à la
fin du niveau 15.

Le chronométrage RTA est optionnel.

### 3.1.1. Route

#### 3.1.1.0. Général

Courir c'est lent. L grappin c'est lent. Le petit grugeage avec le mage et une
planche pour accélérer c'est lent. Voler c'est plus pratique et on va le faire
tout le temps.

« Comment ça voler ? » ? Oui (confère **Infinite charge**). On va l'utiliser
avec des planches du mage pour se launch et on va faire ça tout le temps.

Bon occasionnellement on utilisera le grappin avec la voleuse pour gagner un
petit boost de vitesse et aller un peu plus vite. Technique que l'on applique
généralement instinctivement en casual.

Niveau itemization, c'est Pontius qui a tous les items utiles de vie/mana parce
qu'il est tout le temps en charge.

Les 3 clip et glitches majeurs s'enchaînent en continu.

#### 3.1.1.1. Astral Academy

Tant qu'on n'a pas fini le niveau on n'a pas les 3 personage à la fois donc pas
de **Infinite Charge** donc on va utiliser les accélérations du grappin, un
petit tricks avec le mage qui va plus vite si une planche lui tombe sur la
figure et on va charger avec le guerrier. Vu qu'on a tous les pouvoirs (NG+)
c'est possible.

#### 3.1.1.2. Academy Hallways

C'est un oiseau ? C'est un avion ? Eh bien non, c'est GrosHiken ! Enfin... ses
personnages plutôt.

Les 3 glitches/clips démarrent à ce niveau et s'enchaînent très rapidement.

#### 3.1.1.3. Wolvercote Catacombs

RAS à part les 3 techniques.

#### 3.1.1.4. Dragon Graveyard

RAS à part les 3 techniques.

#### 3.1.1.5. Crystal Caverns

Le niveau il est pété, lol, le plafond ça n'existe pas. Une fois passé en OoB,
on va utiliser la compétence de planage du bouclier du guerrier pour esquiver
tout le niveau.

C'est un clip assez similaire au **Door clip**.

#### 3.1.1.6. Crypt of the Damned

RAS à part les 3 techniques.

#### 3.1.1.7. Forsaken Dungeon

En plus de la gruge usuelle, il y a un **Triangle clip** à un moment avant une
des arènes, un **Triangle clip** pendant l'arène et un **Infinite charge**
instantanément après.

#### 3.1.1.8. Throne of the Lost King

RAS à part les 3 techniques.

#### 3.1.1.9. Fangle Forest

RAS à part les 3 techniques.

#### 3.1.1.10. Shadowthorn Thicket

RAS à part les 3 techniques.

#### 3.1.1.11. Ruins of the Perished

Euh.

Alors.

Oui.

Le niveau il est pété, lol, il existe un plafond. Mais on peut quand même OoB
par dessus et après ben c'est tout droit.

Clip similaire au **Door clip** à nouveau, avec la nuance qu'il faut se placer
légèrement sur la droite avant sinon la caméra ne suit pas et ne permet pas de
joindre le bord du niveau.

#### 3.1.1.12. Heartland Mines

Le **Infinite charge** au milieu du niveau est un peu dissident et peut soit se
coincer, soit se faire tomber  dans le vide, soit tout va bien.

#### 3.1.1.13. Bramblestock Village

RAS à part les 3 techniques.

#### 3.1.1.14. Iron Forge

RAS à part les 3 techniques.

#### 3.1.1.15. Tower of Sarek

Semi-autoscroller, peut-être le seul niveau fait à peu près normalement.

Ça se voit pas mais la lave monte.

# 4. Trivias

No sé.

# 5. Références

Lien speedrun.com : https://www.speedrun.com/trine1

Wikipedia : https://fr.wikipedia.org/wiki/Trine
