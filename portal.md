# 0. Metadata

## 0.0.  Versioning

| Date          | Révision                                  |
|-              |-                                          |
| 2022-01-05    | Update (SGDQ 2023) - Run Airboat%         |
| 2022-06-25    | Update structurelle                       |
| 2019-01-xx    | Création (AGDQ 2019) - Run Any% Inbounds  |

# 1. Fiche technique

Sortie : 2007-10-10 (Windows, XBox 360), 2007-12-11 (PS3)

Plateforme : PC, XBox 360, PS3

Développé par : Valve Software

Édité par : Valve Software, Microsoft Game Studios

Genre : Puzzle-FPS

# 2. Histoire

*Brievement, parce que l'univers est vachement riche mais il faut jouer au jeu
pour que les subtilitées frappent.*

La protagoniste, Chell, se réveille dans une chambre blanche. Ses seules
indications lui sont communiquées par messages audios par GLadOS, l'IA qui
contrôle le "Centre d'Enrichissement d'Aperture Science". Aperture Science
étant une société en charge de recherches sur la téléportation personnelle.

Chell doit effectuer des tests dans des chambres de tests construites à cet
effet, avec pour but final un gâteau et une fête. Les premiers tests consistent
en un tutoriel de la physique du jeu, mais bien vite la protagoniste s'arme du
fameux "Portal Gun", un fusil qui permet de créer deux portails de
téléportation connectés. Les salles augmentent rapidement en complexité.

Au cours du jeu, l'attitude initialement aimable de GLadOS se change peu à peu
en indifférence désagréable et l'ambiance s'entoure de cynisme. Arrivé à la
dernière salle de test, GLadOS place la protagoniste sur une plateforme se
dirigeant droit vers un incinérateur, auquel Chell échappe en usant de son
portal gun.

Arpentant plus librement les couloirs d'Aperture Science pour échapper à GLadOS
qui cherche à la retenir, des révélations sont faites sur la société. Les
chemins empruntés conduisent Chell droit vers le corps matériel de GLadOS et la
demoiselle finit par détruire ses quatres processeurs ce qui désactive l'IA.

L'explosion qui s'ensuit propulse Chell hors du complexe, à la surface, et elle
tombe inconsciente alors que son corps est tracté par un être inconnu.

# 3. Commentaire

## 3.0. Général

### 3.0.0. Mécaniques de jeu

Vue FPS.

Une seule arme : un portal gun. Il permet de créer 2 portails connectés entre
eux, à travers lequel on peut entrer à souhait.

Pas d'indication de pv. En cas de perte de vie, celle-ci remonte toute seule.

Quelques salles de tests ont des ennemis, mais le gros du jeu est fait tout
seul.

Du fun

### 3.0.1. Techniques émergentes

#### 3.0.1.0. Général

Comme de nombreux jeu du moteur *Source*, les runners vont avoir tendance à
changer les associations de touches pour faciliter la plupart des tricks. Le
saut va généralement est lié à la molette de la souris pour faciliter les
**Bunny Hop** ou techniques assimilés.

#### 3.0.1.1. Mouvements

##### 3.0.1.1.0. All-stars

**Bunny Hopping** : on saute + straffe et on regarde dans la direction du
straffe = vitesse augmente. Slider nous faisant perdre de la friction, c'est
encore mieux accroupi.

***À noter*** : Portal utilise une version du moteur Source qui patche le **
Bunny Hop** par rapport à Half-Life, mais, ironiquement, le correctif est
exploitable et même plus efficace. En effet, quand le moteur détecte que le
joueur dépasse sa vitesse limite (300 hu/s) (hu = Hammer Unit), il va pousser
le joueur dans une direction. Précisément :
- si le joueur tient une touche de déplacement pressée, la direction opposée ;
- si aucune touche est pressée la direction opposée à celle de l'orientation de
  la caméra.

Avec les bonnes conditions, on peut donc abuser de cette poussée pour gagner de
la vitesse avec les techniques décrites dans la section `Spécificités`.

**Strafe Jump** : exploitation d'un bogue du moteur Hammer (et donc Source), en
sautant + strafe + direction on peut vraiment accélérer son saut dans un
mouvement circulaire.

Cette technique est aussi patchée, peu exploitée hors **Bunny Hop**.

**Damage Boosting** : prendre des dégâts d'explosions ou d'arme à feu peut
propulser le personnage à l'opposé, et cela est évidemment exploitable.

##### 3.0.1.1.1. Spécificités

**Accelerated Backwards Hopping** ou **ABH** : c'est la principale mécanique
de mouvement des runs de Portal (exception du glitchless).

Il faut une vitesse initiale (généralement obtenue en sautant en avant et en
faisant 180° en l'air) puis lacher la touche avant et s'accroupir. Ensuite il
faut sauter systématiquement au moment ou on touche le sol. Chaque saut
augmente la vitesse et éventuellement les strafes aussi.

**Accelerated Forward Hopping** ou **AFH** : sur le même principe d'abus du
correctif, ici on va toutefois maintenir une touche de déplacement pour faire
croire au jeu qu'on se déplace en arrière (ce qui nécessite une vitesse
initiale plus grande pour compenser le déplacement arrière de la pression de la
touche). Le jeu nous pousse en avant. (exemple : *any% inbounds* Chambre 10 &
11).

**Accelerated Sideway Hopping** ou **ASH** : peu voire pas utilisé dans les run.
C'est comme l'**ABH** mais en tournant à 45° et en maintenant 2 directions
(arrière et l'une des direction latérale).

### 3.0.1.1. Glitches

**Edgeglitch** : L'**Edgeglitch** est réalisé en se tenant au dessus ou à côté
de l'arête d'un portail, parfois en se coinçant quelque part. La caméra du
portal gun va être disloquée de celle du joueur et cela permet de shooter des
portails de l'autre côté du portail déjà ouvert alors que l'on ne s'y situe pas.

C'est compliqué à piger si on ne voit pas comment le jeu gère ça (il existe des
commandes console pour afficher dans une petite fênetre la caméra réelle).

**Saveglitch** : Le **Saveglitch** est un dérivé de l'**Edgeglitch**.
L'**Edgeglitch** doit spécifiquement rester dans la bulle qui permet de le
réaliser. Le saveglitch permet toutefois de conserver cet état de façon
permanente.

L'avantage de cet état est notemment la possibilité de se déplacer (et donc
déplacer la caméra disloquée) et également une physique différente. En effet,
le moteur calcule légèrement différement les collisions lorsqu'on se situe dans
un environement proche du portail (*Portal Physics Bubbles*), notamment :
- Les collisions avec les murs/sol/plafond sont seulement calculées dans la
  proximité du portail glitché
- Seuls les objets (les portes, etc.) placés en face du portail ont une
  collision (on peut passer à travers le reste).

Le glitch est annulé en passant à côté ou dans un portail.

**Large Angle Glitch** (**LAG**), **Acute Angle Glitch** (**AAG**), et
**Vertical Angle Glitch** (**VAG**) : ce sont des variations du même exploit.

C'est un abus du code qui empêche le joueur de se coincer dans un portail.
Lorsque que le jeu pense que l'on se bloque quelque part en passant un portail
il tente de nous ramener à travers le portail pour nous décoincer. Sauf que
n'importe quel déplacement fait après être sorti du portail et avant que la
correction se déclenche est appliquée, mais à l'envers comme si on était sorti
de l'autre portail (oui c'est compliqué).

Pour déclencher le glitch, le placement du portail est extrèmement particulier,
je n'en parlerai pas. Il faudrait linker le guide complet pour le chat pour
ceux qui veulent.

**Item saveglitch** : Pour faire court, c'est un saveglitch mais avec un item
du jeu à la place du joueur. Lorsque ce glitch est réalisé, ça met le bazar dans
toute la physique du jeu sauf celle du joueur.

**Void Clip** : Le void clip est un glitch classique du moteur Source qui
se produit quand l'espace occupé par le joueur est en collision avec un *prop*
pendant une sauvegarde/chargement, ce qui conduit la physique à être un peu
débile au chargement. Le seul usage dans Portal est le Button SaveGlitch.

**Button SaveGlitch** (BSG) (merci alex_sr pour l'explication) : C'est un glitch
spécifique pour la salle 1, et il s'agit d'un Void Clip particulier. Pour le
faire, il faut récupérer une radio dans la salle des portails alternatif et
tenir sur le bouton qui active la sortie. En plaçant la radio sur le bouton et
en faisant des sauvegardes/chargements répété, la physique du jeu devient
confuse et considère que c'est la radio qui presse le bouton au lieu du joueur
(alors que c'elle-ci n'est pas assez lourde). Le joueur peut donc sortir de la
salle en 1 cycle sans utiliser le cube.

## 3.1. Run Any% Inbounds (AGDQ 2019)

### 3.1.0 Informations diverses

#### 3.1.0.0. Description générale

Plateforme : PC

Temps :
- WR : **10m 39s 900ms** RTA no loads par Msushi
- 4ème (runner AGDQ 2019) : **10m 50s 520ms** RTA par wowie_allie
- 5ème (runner AGDQ 2019) : **10m 51s 720ms** RTA par ConnorAce

Style : Run du swag et du skill avec des techniques fumées et des glitchs.

#### 3.1.0.1. Règles

##### 3.1.0.1.0. Général

Le jeu doit être fini sans que la caméra ou que les portails placés quittent les
limites fermées du jeu.

Le jeu doit être fini sans scripts, macros ou cheats (`sv_cheats` doit être à
0).

Le jeu doit être fini d'une traite, sauvegarde et chargement rapides sont
permis.

En cas d'*OOB* avantageux par accident, il faut charger la dernière sauvegarde.

##### 3.1.0.1.2. Chronomètre

Le chronomètre démarre quand la mire apparaît à l'écran.

Le chronomètre termine quand la mire disparaît à la mort de GLaDOS.

### 3.1.1. Route

#### 3.1.1.0. Chambre 0

ABH et jetage de caisse[0]. Jusque là, ça va.

*[0] Optimisation: BSG avec le réveil*

#### 3.1.1.1. Chambre 1

BSG

#### 3.1.1.2. Chambre 2 et 3

Ça commence à devenir débile. On va faire un saveglitch, ce qui skippe tout le
reste de la chambre 2 et 3.

#### 3.1.1.3. Chambre 4 et 5

Saveglitch pareil et on abuse de la mécanique de non collision.

#### 3.1.1.4. Chambre 6 et 7

Faites normalement.

#### 3.1.1.5. Chambre 8

Technique difficile à l'ABH, backup avec un portail en haut du niveau sur le
bout de mur qui dépasse.

#### 3.1.1.6. Chambre 9

No glitch mais un portail sous la porte parce qu'on peut.

#### 3.1.1.7. Chambre 10

AFH et possible **Door Launch** pour gagner du temps (on tape le haut de la
porte (qui a un angle) avec de la vitesse ce qui change la direction du vecteur
vitesse).

#### 3.1.1.8. Chambre 11 et 12

AFH pour récup le portal gun puis Saveglitch à l'infini

#### 3.1.1.9. Chambre 13

Saveglitch

#### 3.1.1.10. Chambre 14

Move stylé du skill.

#### 3.1.1.11. Chambre 15

Long saveglitch de la fumance (la fin est potentiellement trollesque d'ailleurs)

#### 3.1.1.12. Chambre 16

Route compliqué où on doit garder une tourelle pour la salle suivante et
conserver le saveglitch.

#### 3.1.1.13. Chambre 17

On récupère la tourelle pour activer un bouton puis on la crame à la place du
companion cube.

#### 3.1.1.14. Chambre 18

Saveglitch compliqué en blind pour zaper toute la phase de reportal.

#### 3.1.1.15. Chambre 19

Double **LAG**, c'est le bordel dans tous les sens.

#### 3.1.1.16. Escape 0

Saut précis pour saveglitch

#### 3.1.1.17. Escape 1

Saveglitch dans tout les sens puis séquence de reportal

#### 3.1.1.18. Escape 2

Double AAG et on rapporte une tourelle pour tuer GLadOS avec. Chaque balle de
tourette fait tomber un processeur de GLadOS. Edgeglicth pour placer un portail
directement dans l'incinérateur.

## 3.2. Run Any% Inbounds (AGDQ 2023)

### 3.2.0 Informations diverses

#### 3.2.0.0. Description générale

Plateforme : PC

Temps :
- WR (runner AGDQ 2023) : **14m 21s 410ms** RTA, **13m 40s 390ms** LRT par
  Msushi (https://www.youtube.com/watch?v=YhAvTH3EYFo)

Style : Run du mème.

Description : C'est une run du mème, qui utilise une commande console
particulière : `ch_createairboat`. Cette commande génère un hydroglisseur (un
véhicule d'*Half-Life 2*) et on va faire un peu n'importe quoi avec pour
compléter les salles et le jeu. Pour plus de facilités, la commande est associée
à une touche du clavier. Ironiquement, cette commande n'est pas protégée
derrière la commande `sv_cheats 1`.

#### 3.2.0.1. Règles

##### 3.2.0.1.0. Général

Le jeu doit être fini sans tirer un seul portail.

Les *OoBs* sont autorisés.

Le jeu doit être fini sans scripts, macros ou cheats (`sv_cheats` doit être à
0).

Le jeu doit être fini d'une traite, sauvegarde et chargement rapides sont
permis.

Le *yesclip* est interdit (ce clip est un glitch trouvé dans *Half-Life 2*,
qui grosso modo permet d'être en *noclip* sans tricher grâce à un bug de
l'hydroglisseur).

##### 3.2.0.1.2. Chronomètre

Le chronomètre démarre quand la mire apparaît à l'écran.

Le chronomètre termine quand la mire disparaît à la mort de GLaDOS.

#### 3.2.0.2. Techniques spécifiques

L'hydroglisseur débloque une nouvelle mécanique de déplacement : le clip. En
rentrant dans l'hydroglisseur, Chell peut se déplacer à travers les murs pour
s'assoir dedans, et évidement en sortant du véhicule elle se retrouve de l'autre
côté dudit mur.

**Airboat clipping** : la première technique de la run, ça va être grosso modo
de spawn un hydroglisseur à chaque mur ou porte qui nous sépare de la fin du jeu
et clip au travers.

**Airboat boosting** : la deuxième technique va être d'abuser la physique du
jeu en générant plein d'hydroglisseur au même endroit proche de Chell. Le jeu va
essayer d'expulser Chell des objets et va donc générer un maximum de vitesse.

Trivia : alors que dans *Half-Life 2* l'hydroglisseur protège des dégâts de
l'eau toxique, ce n'est pas le cas dans Portal.

### 3.2.1. Route

Note : si pas d'indication dans la route, cela signifie en gros que :
- soit les techniques de déplacement standard sont utilisées ;
- soit les techniques décrites ci-dessus sont utilisées.

De plus, pour accélérer les transitions d'ascenseurs du jeu, un **Airboat Clip**
permet de gagner 1 à 2 secondes pour chacune d'entre elles.

Note : aux commandes de l'hydroglisseur, on peut le conduire et donc avancer
avec. C'est utilisé dans quelques chambres et dans quelques *OoBs*.

#### 3.2.1.0. Chambre 0
#### 3.2.1.1. Chambre 1
#### 3.2.1.2. Chambre 2
#### 3.2.1.3. Chambre 3
#### 3.2.1.4. Chambre 4
#### 3.2.1.5. Chambre 5
#### 3.2.1.6. Chambre 6
#### 3.2.1.7. Chambre 7
#### 3.2.1.8. Chambre 8
#### 3.2.1.9. Chambre 9
#### 3.2.1.10. Chambre 10
#### 3.2.1.11. Chambre 11
#### 3.2.1.12. Chambre 12
#### 3.2.1.13. Chambre 13
#### 3.2.1.14. Chambre 14
#### 3.2.1.15. Chambre 15
#### 3.2.1.16. Chambre 16
#### 3.2.1.17. Chambre 17
#### 3.2.1.18. Chambre 18
#### 3.2.1.19. Chambre 19
#### 3.2.1.20. Escape 0
#### 3.2.1.21. Escape 1
#### 3.2.1.22. Escape 2

À l'instar des autres catégories, on rapporte une tourelle pour tuer GLadOS avec
au lieu d'utiliser les bombes. Chaque balle de tourette fait tomber un
processeur de GLadOS.

# 4. Trivias

Beaucoup trop, demandez à Synahel.

# 5. Références

Wikipedia : https://fr.wikipedia.org/wiki/Portal_(jeu_vid%C3%A9o)

Lien speedrun.com : https://www.speedrun.com/Portal

Speedrun guide : https://docs.google.com/document/d/1H7AHqiSTRzJE2xfqPCGBsUXZay1g9wdUfCbPXoz8S3c/edit

Origin story et tuto Airboat% : https://www.youtube.com/watch?v=H4q3iiAjKcI
