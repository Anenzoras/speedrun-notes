# 0. Metadata

## 0.0.  Versioning

| Date          | Révision                                          |
|-              |-                                                  |
| 2023-05-26    | Update (SGDQ 2023) - Run Co-op Legacy X-row       |
| 2022-06-25    | Update structurelle                               |
| 2019-01-xx    | Création (AGDQ 2019) - Run Coop N++ - A-episodes  |

# 1. Fiche technique

Sortie :
- PlayStation 4 : 28/29 juillet 2015
- Windows : 25 août 2016
- OS X : 26 décembre 2016
- XBox One : 04 octobre 2017
- Switch : 24 mai 2018
- Linux : 31 mai 2018

Plateforme : PlayStation 4, Windows, OS X, Linux, XBox One, Switch

Développé par : Metanet Software

Édité par : Metanet Software

Genre : Plateformer die-and-retry

Note : **N++** fait suite à **N+** qui lui même fait suite à **N**.
**N** était un jeu en Flash disponible sur la plupart des sites aggrégateur de
jeu à l'époque de la bulle Flash. **N+** a été la sortie standalone du jeu sur
console. **N++** est une version améliorée et standalone, un peu à l'instar de
**Super Meat Boy** pour **Meat Boy**.

# 2. Histoire

N++ est un puzzle/parkour-plateformer à niveau à un écran où le but de chaque
niveau est de toucher l'interrupteur pour dévérouiller la porte de sortie et
l'atteindre sans mourir.

Le joueur contrôle un ninja pacifiste et il y a des ennemies et des dangers dans
chaque niveau pour nous empêcher de sortir.

C'est tout.

# 3. Commentaire

## 3.0. Général

### 3.0.0. Mécaniques de jeu

Le gameplay consiste en un parkour-plateformer, où on peut bouger, sauter et
sauter depuis les murs.

Les seuls contrôles sont : gauche/droite, sauter, et reset/exploser.

La hitbox du joueur est un cercle et cela permet une certaine variété
d'interactions lors du saut depuis une pente ou un coin.

Des pièces sont collectibles pour rallonger le temps imposé pour finir un
niveau (symbolisé en haut par la barre rouge).

Des portes et des interrupteurs, des mines, des projectiles, etc.

Pas de points de vie, tout tue en un coup.

Une chute trop haute tue, à moins de pouvoir se réceptionner sur une pente pour
transférer sa vitesse verticale en vitesse horizontale.

Un gameplay relativement difficile.

Dans la coop, il suffit qu'un seul joueur atteigne la sortie pour que le niveau
soit validé.

À noter que la physique possède une inertie très marquée, dans le sens où les
personnages ont une accélération et une décélération lente et conservent
aisément leurs momentum.

### 3.0.1. Techniques émergentes

En coop, l'un des joueurs va souvent se sacrifier pour que l'autre atteigne la
sortie plus vite.

Le plus gros trick pour gagner du temps est le **Transition Skip** qui est fait
à la fin de chaque niveau. Pour pouvoir le faire, il faut joueur en mode
"level", qui permet de sélectionner manuellement les niveau plutot qu'en mode
"episode", qui enchaîne les niveaux mais affiche à la fin un récapitulatif.

### 3.0.2. Règles globales

*(Tirées de speedrun.com, toutefois les règles ne concernant pas le jeu en
lui-même ont été exclues. Les règles concernant les catégories elles-mêmes sont
dans leurs sections respectives.)*

Pour le chronomètrage RTA, le chronomètre commence à la première *frame* ou le
texte disparait au moment de débuter le premier niveau et il s'arrête lorsque
le texte apparaît à la complétion du dernier niveau de la catégorie runnée.

Pour le chronomètrage IGT, il est optionnel. Dans le cas où il est utilisé il
peut être obtenu soit en observant le profil du joueur après la run, soit avec
un autosplitter.

Dans le cas d'une mort au premier niveau le timer RTA peut démarrer à la
première tentative réussie.

### 3.0.3. Explication des catégories

#### 3.0.3.0. Pack de niveau

Le jeu sépare les niveaux par épisode. Les épisodes sont répartis dans un
tableau dont chaque ligne porte un nom qui les identifies. Il existe plusieurs
tableaux qui eux-mêmes sont dans des onglets dans le menu.

Il existe généralement les lignes A, B, C, D, E et X ; X incluant les niveaux
les plus difficiles de chaque onglets.

### 3.0.3.1. Legacy

L'onglet Legacy donne accès aux niveaux des jeux précédents (**N** et **N+**).
Ces niveaux n'avaient dans ces jeux pas de co-op et on été légèrement modifiés
pour la permettre.

## 3.1. Coop N++ - A-episodes (AGDQ 2019)

### 3.1.0. Informations diverses

#### 3.1.0.0. Description générale

Plateforme : PC

Kostucha and Fnyt - WR : **26m43s** RTA

AND4H et Krankdud - ?? : ??

Style : Run du full skill

#### 3.1.0.1. Règles

##### 3.1.0.1.0. Général

- Faire tous les niveaux de l'épisode indiqué.

##### 3.1.0.1.1. Chronomètre

Le chronomètre démarre quand on commence le premier niveau.

Le chronomètre termine quand on atteint la sortie du dernier niveau.

### 3.1.1. Route

Assez visuelle, rien à signaler.

## 3.2. Run Co-op Legacy X-row (SGDQ 2023)

### 3.2.0. Informations diverses

#### 3.2.0.0. Description générale

Plateforme : PC

killingpepsi et XandoToaster - WR : **17m 56s** RTA

Style : Run du full skill

Note : Xandotoaster est le ninja jaune/clair et killingpepsi est le ninja
orange.

#### 3.2.0.1. Règles

##### 3.2.0.1.0. Général

Faire tous les niveaux de chaque épisode de la rangée X (dans le menu) de
l'onglet « Co-op Legacy ».

##### 3.2.0.1.1. Chronomètre

Le chronomètre démarre quand on commence le premier niveau.

Le chronomètre termine quand on atteint la sortie du dernier niveau.

### 3.2.1. Route

Assez visuelle, rien à signaler.

# 4. Trivia

/shrug

# 5. Références

Wikipedia : https://en.wikipedia.org/wiki/N%2B%2B

Lien speedrun.com : https://www.speedrun.com/nplusplus
