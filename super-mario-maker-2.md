Super Mario Maker 2
===

# 0. Metadata

## 0.0.  Versioning

| Date          | Révision                        |
|-              |-                                |
| 2023-12-26    | Création (AGDQ 2024) - Run Any% |

# 1. Fiche technique

Sortie : 2019-06-28

Plateforme : Nintendo Switch

Développé par : Nintendo

Édité par : Nintendo

Compositeur : Koji Kondo

Genre : Plateforme 2D (ou 2.5D selon le mode)

# 2. Histoire

## 2.0. Univers

Ben... c'est Mario, quoi...

## 2.1. Le jeu

Bien que Mario Maker aie un mode solo, il s'agit juste d'un tutoriel pour
apprendre les mécaniques de jeu et l'interface de l'éditeur.

En soi, le jeu n'est qu'un éditeur de niveaux personnalisés plus complet que
Super Mario Maker 1, avec de nouvelles entités. Le premier jeu était lui-même
ce que proposait Nintendo pour satisfaire la communauté qui cherchait un moyen
officiel pour réaliser des niveaux personnalisés de leurs jeux favoris.

# 3. Commentaire

## 3.0. Général

### 3.0.0. Mécaniques de jeu

Ben... c'est Mario, quoi...

C'est la quintescence du jeu de plateforme. Avec un personnage qui court, qui
saute, qui saute depuis les murs, parfois, et qui intérragit avec tout ce que
le royaume champignon a à proposer en mécanique de jeu.

Chaque niveau possède un thème qui le relie aux mécaniques des différents jeu
Mario sorti au fil des ans. Il y a :
- Super Mario Bros. ;
- Super Mario Bros. 3 ;
- Super Mario World ;
- New Super Mario Bros. U ;
- Super Mario 3D World (adapté pour la 2D).

Le thème est choisi dans l'éditeur par le créateur du niveau (*Level Maker*),
qui lui donne accès à différente physiques et objets à utiliser selon le thème
choisi.

Bien que le jeu utilise les ressources graphiques des jeux sus-mentionné, la
physique de chacun des thème a été adapté pour la moderniser et la rendre plus
agréable, bien que des différence fondamentales ont été maintenues entre chaque
thème.

Un point important de l'éditeur et des niveaux est qu'il existe une limite
d'entités placées à l'écran. Il est *normalement* impossible de placer plus
d'entité que la limite ne le permet. Ceci est parfois utilisé par les créateur
de niveau avec les pièces rouges qui ont un suivi entre les checkpoints pour
changer l'état du niveau, car ces pièces ramassées changent alors la limite
d'entité.

### 3.0.1. Techniques émergentes

***N'ayant pas préparé ce document pour le speedrun, les techniques généralement
utilisées par les runners ne sont pas mentionnées ici.***

## 3.1. Glitch showcase (AGDQ 2024)

### 3.1.0. Informations diverses

#### 3.1.0.0. Description générale

Plateforme : Nintendo Switch

Temps : n/a

Style : Run du swag et pour présenter tous les glitchs qu'on ne voit pas dans
les niveaux usuellement présentés en marathon.

#### 3.1.0.1. Règles

n/a

### 3.1.1. ~Route~ Glitches

**Glitched track** : grosso modo, c'est le glitch de base de l'éditeur qui donne
accès à quasiment tous les autres. C'est un abus d'un bug du bouton undo, qui, à
la suite d'objets placés spécifiquement va corrompre une partie de la mémoire
de certains objets.

Il faut :
1. placer un track avec l'objet à glitcher dessus
2. effacer le tout, ce qui stocke l'objet dans la mémoire
3. placer un track en diagonale
4. l'incurver
5. mettre une pince dessus
6. mettre un hammer bro sur la pince
7. enlargir le hammer bro (mushroom)
8. commencer à jouer pour tester
9. arrêter de jouer

La mémoire est maintenant corrompue pour l'objet initial. Il faut ensuite undo
jusqu'à faire réapparaître l'objet avec le track initial, qui est glitché avec
le track du hammer bro.

On peut ensuite prendre l'objet du premier track et le mettre sur le second.
Si c'est un block il se retrouve sur le track sans vraiment y être, et ça change
son alignement sur la grille.

***

**Black hole glitch** : ce glitch est une malfonction du jeu qui permet de
"cacher" des objets au jeu game, ce qui rend possible plein de trucs normalement
impossible, comme dupliquer des checkpoints.

Il existe pleins de méthodes différentes pour mettre en place ce glitch, qui,
comme pour le **glitched track** repose notamment sur l'usage du bouton undo
(et accessoirement placer genre 50 fois le même items overlappé).
Malheureusement, je me vois pas très bien les expliquer parce que je les
comprends pas moi-même.

Parmis la liste des choses que ce glitch permet on trouve :
- des téléportation en utilisant des pinces ;
- des tuyaux et des portes qui ne mènent nulle part (donc softlock) ;
- bypasser la limite d'entité, et faire faire au jeu des trucs très étranges ;
- faire croire qu'un item est sur un track alors qu'en fait non (utilisé pour
  téléporter avec la pince) ;
- faire en sorte qu'un objet (genre tuyau) soit invisible ;
- faire un scrolling forcé beaucoup plus compliqué ;
- stacker des entités (pas forcément les même) au même endroit (notamment il est
  possible de stacker des tuyaux transparent de New Sup avec le décors, et de
  quand même pouvoir passer au travers);

Attention toutefois, ce glitch corrompt la mémoire assez violemment, ce qui peut
résulter en une impossibilité de sauvegarder le niveau. La plupart des vidéos
explicatives que j'ai pu trouver conseillent de supprimer l'entité dite
"*Black Hole*" qui contient le glitch avant de sauvegarder (cela n'entrave pas
les _conséquences_ du glitch).

**Broken track** : il s'agit d'un glitch qui permet de placer un track mais dont
un des côtés n'a pas de bloqueur de track, donc les objets peuvent sortir du
track.

Pour se faire, il faut placer un track puis appliquer les étapes `5.` à `9.` du
**Glitched track**.

Quand certains objets sortent du broken track, plein de trucs incr peuvent
advenir si on place un track incurvé juste à côté. Grosso modo, les objets sur
le track vont, au moment de toucher le track incurvé, se faire téléporter en
(0, 0), activant au passage si applicable les objets sur la route.

Par exemple, un launcheur va déplacer des boules à pic, qui elles-même vont
activer des blocs sur la route. Cela permet par exemple d'activer pleins de
blocs simultanément.

**Claw lick glitch** : Pour ce glitch, il faut Yoshi, un objet et une
pince (claw). Il faut manger d'abord l'objet avec Yoshi puis le recracher et le
re-attraper avec Yoshi au moment précis où l'objet se fait attraper par la
pince.

L'objet se retrouve alors dans un état superposé : il existe dans Yoshi et dans
le monde, cependant son sprite n'apparait pas. Si Mario sort du Yoshi, il peut
marcher sur l'objet qui se trouve à hauteur de la tête du Yoshi.

Il peut se passer différentes choses selon l'objet utilisé.

Un POW permet à Mario sur le Yoshi de superspeed et voler contre un mur.

Un P-Switch est activable depuis l'extérieur.

Un ressort vertical peut être utilisé pour sauter dessus si Mario n'est pas sur
le Yoshi.

Un ressort horizontal bounce constamment le Yoshi.

Etc.

**Fake block** : ce glitch permet de placer un bloc avec son sprite, mais dont
la collision n'est pas active, ce qui permet de passer au travers.

Pour ce faire il faut initier le **Glitched track** avec le bloc à falsifier,
et ensuite le placer entre deux blocs de décor.

Additionnellement, apparaissent hors grille dans l'éditeur.

**Flying machines** : En organisant certains objets d'une certaines façon, il
est possible de les faire voler. La plupart du temps, cela implique des
launchers et des canons, parfois avec des spikies.

**Hardlock** : il existe certains moyens de hardlock le jeu, c'est à dire de
freeze le jeu. Le seul moyen de se défaire du freeze étant de forcer le jeu
à quitter (== "redémarrer" la console).

Une méthode connue est de prendre le thème New Sup, placer un wiggler dans l'eau
mettre un casque sur Mario, et utiliser le casque pour propulser le wiggler
hors de l'eau. À priori, au moment de sortir de l'eau, le jeu essaye de jouer
une animation qui n'existe pas, se bloquant dans une boucle infinie.

**Invisible block** : non, on ne parle pas ici des blocs cachés, objet de base
de Mario, mais bien d'un bloc qui possède une collision mais dont le sprite
ne s'affiche pas. Pour que ce soit le cas, il faut un bloc caché qui contienne
une liane. Le bloc doit être activé mais sans que la liane s'affiche au moment
précis ou Mario déclenche une transition.

Un autre moyen de faire ça est d'utiliser le **black hole glitch**, avec une
pièce glitchée grâce à un setup avec des claws. Placer un bloc au dessus de la
pièce et faire péter un POW rend le bloc invisible. Ne placer aucun bloc au
dessus de la pièce et faire péter un POW fait spawn une autre pièce.

**Shino's 3DW infinite fireball/boomerang** : le setup est pas simple mais pas
aussi compliqué que d'autres glitches.

On a Mario qui tient un spring, Mario qui doit rentrer dans un tuyau, au même
moment qu'il ramasse un chamignon, au même moment qu'il jette le spring.

On peut controler Mario alors que le jeu est en train de le faire rentrer dans
un tuyau. Au sortir du tuyau, Mario doit se faire tuer. Sauf qu'en fait non.

Il est désormais glitché. Dans cet état Mario est capable de voler, s'il
maintient le bouton saut appuyé. De plus Mario est capable d'envoyer des boules
de feu / boomerangs sans limite d'entité dans cet état dans le mode 3DW.

*Side note* : avec un même setup de transformation avant l'entrée dans un tuyau,
on peut porter des items avec le SMB3 mushroom à travers un tuyau, ce qui n'est
normalement pas possible.

# 4. Trivia

No sé.

# 5. Références

Wikipedia : https://en.wikipedia.org/wiki/Super_Mario_Maker_2

Liste de glitches :
- https://supermariomaker2.fandom.com/wiki/Glitches
- https://www.mariowiki.com/List_of_Super_Mario_Maker_2_glitches

Ce qu'on peut faire avec le black hole glitch : https://www.youtube.com/watch?v=7V4_VW48znM

Broken track et dérivé : https://www.youtube.com/watch?v=i-1giw1UsjU&t=1395

Infinite fireball : https://www.youtube.com/watch?v=JUkbAFQTjbo
