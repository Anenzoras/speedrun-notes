Thief Gold (Thief: The Dark Project)
===

# 0. Metadata

## 0.0.  Versioning

| Date          | Révision                        |
|-              |-                                |
| 2023-12-26    | Création (AGDQ 2024) - Run Any% |

# 1. Fiche technique

## 1.0. General

Sortie :
- 01 décembre 1998 (The Dark Project - Amérique du Nord)
- 04 décembre 1998 (The Dark Project - Royaume-Uni (international))
- 02 novembre 1999 (Gold - International)

Plateforme : Windows

Développé par : Looking Glass Studios

Édité par :  Eidos Interactive

Moteur : *Dark Engine*

Genre : Infiltration

Notes : Thief Gold est une réédition du jeu qui améliore les 12 missions du jeu
de base et en ajoute trois nouvelles ; ainsi qu'un éditeur de missions.

## 1.1. Informations additionnelles : version du moteur

Le jeu étant ancien, il y a eu plusieurs patchs non officiels utilisés par la
communauté pour permettre de le faire fonctionner sur des machines modernes.

Utiliser la version Thief Gold de base, soit dans une machine virtuelle soit
avec le correctif non officiel DDFix est considéré comme *OldDark*. Cette
version utilise le moteur *Dark Engine* tel que paru avec le jeu originel et
contient certain glitches et bug bien spécifiques.

Utiliser la version Thief Gold avec le patch TFix permet au jeu de tourner sur
le moteur *Dark Engine* de Thief 2, qui corrige certains bug et inconsistences
graphiques mais en ajoute d'autres. Cette version est appelée *NewDark*.

# 2. Histoire

## 2.0. Univers

*Thief* prend place dans une ville simplement appelée "The City". La ville est
un mélange d'inspirations médiévales, dark fantasy et d'éléments rappelant la
révolution industrielle. Il y a de l'électricité, de la magie et des machines ;
un peu comme une sorte de Steampunk Dark Fantasy.

Le jeu mets en avant trois factions. D'un côté le *Order of the Hammer*, dont
les membres sont appelés "*Hammerites*", est un ordre religieux qui vénère
"*The Builder*" et croit en le progrès technique, l'artisanat et la rectitude.
De l'autre côté les *Pagans* sont décrit comme primitifs, presque animalistiques
et vénèrent le dangereux Dieu *Trickster*, préférant le monde naturel, plus
mystique. Ces deux factions sont généralement présentés comme l'opposition entre
l'Ordre et le Chaos. La troisième des factions est une société secrète, les
*Keepers*, dont la mission est de maintenir l'équilibre entre les forces de cet
univers dans le plus grand secret.

## 2.1. Jeu

### 2.1.0. Note

Le jeu se déroulant de niveau en niveau, cette section est découpée à l'image du
jeu.

### 2.1.1. A Keeper's Training

Le briefing de cette mission présente le prologue de l'histoire.

Garrett, le personnage incarné par le joueur, était initialement un orphelin
laissé pour compte dans les rues de la cité. Il survit de menus larcins.

Alors qu'il tente de voler à la tire un individu suspicieux, celui-ci le prend
sur le fait. L'individu, Artemus, révèle qu'il est un Keeper, et qu'il est
impressionné que le jeune garçon ait pu le voir. Il offre à Garrett la
possibilité de rejoindre l'ordre secret, ce que Garrett accepte.

La mission commence alors, qui sert de tutorial au jeu en prétextant
l'apprentissage de Garrett.

À la fin de plusieurs mois d'entraînement toutefois, Garrett très terre à terre,
refuse de se mêler aux affaires des *Keepers* et quitte l'ordre pour vivre une
vie de voleur.

Pendant un temps, les missions suivantes ne sont pas liés à l'histoire générale
de l'univers mais permettent de l'introduire plus précisément, en plus d'y
introduire la plupart des mécaniques de jeu rencontrées.

### 2.1.2. Lord Bafford's Manor

Des années plus tard, Garrett est un voleur réputé, suffisemment pour qu'un
syndicat du crime souhaite le recruter, peu après.

La première mission du jeu est initié par Garrett, qui vise un riche noble de la
ville : Lord Bafford. Il devra dérober notamment un sceptre serti de joyaux,
fétiche de l'individu.

Il s'agit d'un avant-goût de ce que le jeu a à offrir.

### 2.1.3. Break from Cragscleft Prison

Garrett aimerait receler le sceptre mais toutefois son receleur, Cutty, s'est
fait arrêté et détenir dans la prison de Cragsleft. Garrett va devoir le
libérer. Il s'infiltre dans la prison à partir de la mine en dessous de
celle-ci. Ce sera la première rencontre entre Garrett et des mort-vivants, des
zombies qui hantent la mine.

Une fois dans la prison, malheureusement pour lui, Cutty meurt à cause de ses
conditions d'incarcération quand il le retrouve. Avant de mourir toutefois,
celui-ci lui met sur la piste d'un artéfact précieux, qui pourrait lui ouvrir
les portes auprès d'autres marchands pour revendre ses prises.

Après s'être échappé de la prison, Garrett prend la direction de sa prochaine
mission.

### 2.1.4. Down in the Bonehoard

L'artéfact qu'il recherche, la *Horn of Quintus* est un artéfact appartenant à
la famille Quintus et placée dans leur catacombes dans le cimetierre.

Les catacombes fourmillent de mort-vivant, ce qui explique que l'artéfact,
pourtant réputé parmi les voleurs, n'a jamais pu être dérobé.

### 2.1.5. Assassins

Après avoir récupéré la Corne, Garrett va voir le marchand Farkus pour la vendre
et refaire son inventaire de voleur. Malgré les prix du marchand (car peu dans
la ville acceptent de vendre un à voleur indépendant) Garrett préfère cette
solution plutôt que de travailler pour un syndicat du crime, qui lui prélèverait
une partie de ses prises.

Ayant refusé plusieurs fois les avances des barons du crime, ainsi que par la
suite les "frais de protection", le baron Ramirez envoit des assassins tuer
Garrett. Heureusement pour lui, c'est Farkus qui se prend la flêche qui devait
le tuer.

Outré, Garrett vole la demeure de Ramirez en représaille allant même jusqu'à
dérober la bourse qu'il a la ceinture sans se faire prendre.

### 2.1.6. Thieves' Guild (Thief Gold addition)

Peu après, Garrett convoite le vase précieux de Lord Randall ; cependant il
apparait que la guilde des voleurs l'a dérobé avant lui. Qu'à cela ne tienne,
Garrett vole la guilde des voleurs.

Ce niveau est une addition de la version Gold.

### 2.1.7. The Sword

Garrett est contacté par Viktoria, une femme au service d'un client anonyme,
impressionné par les rumeurs des exploits de Garrett. Son contrat est de dérober
l'épée de Constantine, un noble excentrique récemment arrivé en ville.

La mission est un succès et par la suite, Viktoria lui fait rencontrer...
Constantine ! Il explique qu'il a embauché Garrett pour lui dérober l'épée afin
de le tester. Constantine lui offre alors une fortune (ainsi que l'épée en
question, qui peut tuer les morts vivant sans utiliser d'eau bénite) pour
dérober "*The Eye*", une gemme gardée et scélée dans une cathedrale Hammerite
abandonnée.

### 2.1.8. The Haunted Cathedral

Pour atteindre la cathédrale, Garrett parcours l'Ancien Quartier, un quartier
abandonné et hanté de la ville. À travers une petite ouverture de la cathédrale
scéllée, l'Œil lui-même informe Garrett d'un sanctuaire *Keeper* abandonné, où
il peut apprendre comment ouvrir la cathédrale.

Il y découvre que la cathédrale a été scellée pour empêcher la destruction de
la ville par le *Trickster*. Également, il a besoin de quatre talismans pour
enlever les sceaux qui l'empêche d'entrer.

### 2.1.9. The Mage Towers (Thief Gold addition)

Les mages de la cité possèdent le premier talisman.

Ce niveau est une addition de la version Gold. Dans la version The Dark Project,
ce talisman se trouve dans la Cité Perdue (le niveau suivant).

### 2.1.10. The Lost City

Une cité ancienne perdue, sous La Ville actuelle, possèdes des ruines anciennes
où se trouve le deuxième talisman.

### 2.1.11. Song of the Caverns (Thief Gold addition)

Garrett doit voler le troisième talisman au trésor de la directrice de l'opéra,
qui l'a elle-même récupéré (par ses gardes) de cavernes sous la ville.

Ce niveau est une addition de la version Gold. Dans la version The Dark Project,
ce talisman se trouve dans le Temple des Hammerites.

### 2.1.12. Undercover

Garrett doit infiltrer le Temple des Hammerites pour y récupérer le quatrième
talisman. Cette mission est très particulière par rapport au reste du jeu car
Garrett est déguisé en initié de l'ordre. Il n'attire donc pas l'attention, à
moins de se faire prendre sur le fait.

### 2.1.13. Return to the Cathedral

En possession des quatres artéfacts, Garrett retourne à la cathédrale, ouvre les
portes mais se fait piéger à l'interieur par l'Œil lui-même. Il apprend que les
habitant des lieux sont tous morts, transformés en mort-vivants par l'Œil.

Il parvient finalement à dérober l'Œil et le livre à Constantine.

Soudain, Constantine révèle être le *Trickster*, métamorphosé et Viktoria une
nymphe des bois qui contrôle les plantes et enserre Garrett. Viktoria lui
apprend que l'Œil requiert un œil de chair pour fonctionner, et ponctionne
celui droit de Garrett. Le *Trickster* le place sur la gemme, et les deux
disparaissent à travers un portail, laissant Garrett pour mort.

### 2.1.14. Escape!

Deux *Keepers* trouvent Garrett, le sauvent et le libère, puis disparaissent
aussitôt ; laissant le voleur groggy s'échapper par lui-même.

Alors qu'il s'échappe de la demeure de Constantine à présent envahie par la
nature, il apprend que le *Trickster* planifie d'utiliser l'Œil pour faire
retourner le monde à l'état sauvage.

### 2.1.15. Strange Bedfellows

Après son échappatoire, il cherche l'aide de l'*Order of the Hammer*, mais
découvre que le *Trickster* a attaqué le temple des Hammerites. Sous le temple,
Garrett trouve des survivants qui lui remettent une réplique de l'Œil et
requierrent ses services pour substituer le véritable Œil contre la copie
factice, qui perturbera le rituel et tuera le dieu.

### 2.1.16. Into the Maw of Chaos

Garrett accomplit son plus difficile challenge en entrant dans le domaine même
du *Trickster*, inverse les deux Yeux et trompe le Trompeur !

### 2.1.17. Epilogue

Après ses aventures, Garrett obtient un remplacement mécanique pour son œil
perdu auprès des Hammerites. Dans les rues de la ville, Artemus approche Garrett
et lui dit qu'il aura bientôt besoin de l'aide des *Keepers*. Garrett l'ignore,
et alors qu'il s'éloigne, Artemus le méfie de l'ére qui arrive, le *Metal Age*
(en clin d'œil au jeu suivant).

# 3. Commentaire

## 3.0. Général

### 3.0.0. Mécaniques de jeu

Thief est le premier jeu vu comme un jeu d'infiltration.

Thief se joue à la première personne. Garrett possède une épée, bruyante mais
meurtrière, un gourdin, silencieux et un arc. Si Garrett parvient à attaquer un
adversaire dans le dos sans se faire repérer avec une arme de corps à corps, il
le mets hors combat instantanément (exception des Hammerites mort-vivants).
L'épée est léthale, sauf sur les mort-vivants, sauf s'il s'agit de l'épée de
Constantine, alors que le gourdin assome, sauf les mort-vivants.

Si Garrett se fait repérer et doit combattre, les combats sont terriblements
difficiles ; Garrett prend beaucoup de dégâts des attaques et se fait très vite
surcharger d'ennemis. Le jeu encourage fondamentalement l'approche discrète.

Garret a dix points de vie, qui se rechargent au début de chaque mission où en
buvant des potions de soin ou alors en mangeant la nourriture trouvée dans les
missions.

À distance, Garrett dispose de flêches standard, mais également de flèches
modifiées :
- les flèches à cordes tendent une corde une fois leur cible atteinte et Garrett
  peut s'y accrocher et grimper ;
- les flèches sonores génère un grand bruit en touchant leur cible, attirant
  l'attention ;
- les flèches d'eau permettent d'éteindre les torches mais infligent également
  des dégâts aux élémentaires de feu et peuvent de plus être bénies avec de
  l'eau bénite pour tuer les mort-vivants ;
- les flêches de mousses permettent de faire pousser de la mousse végétale sur
  les surfaces où la flèche attérit qui assourdis les bruits de pas ;
- les flèches de feu allument des torches et infligent des dégâts explosifs ;
- les flèches de gaz étouffent les ennemis et les assoment silencieusement.

Garrett peut être repéré soit s'il se fait voir par les ennemis, le jeu
encourrageant fortement à se déplacer dans les ombres, quitte à éteindre des
torches ; ou bien en faisant du bruit. Marcher et sauter peut générer du bruit
selon les surfaces, et combattre en fait également. Les ennemis sont également
alertés s'ils trouvent un corps assomé ou refroidi.

Garrett peut crocheter les serrures s'il ne trouve pas de clé qui y correspond
avec l'aide de deux crochet, parfois en alternant sur la même porte.

Il existe également d'autres objets utilisables, comme les mines explosives ou
encore les grenades flashs qui aveuglent les ennemis et infligent de lourds
dégâts aux mort-vivant. Des potions de vitesse permettent d'accélérer la
vitesse de course de Garrett pendant un temps et des potions de respiration
permettent de respirer sous l'eau pendant un temps également. Des potions
d'eau bénite permettent de bénir les flèches d'eau pour tuer des mort-vivants.

Les niveaux possèdent des objectifs que Garrett doit remplir et, selon le niveau
de difficulté, certains objectifs bonus s'ajoutent. Dans chaque niveau
également, Garrett doit voler un certain nombre d'objets précieux, qui formeront
son butin, selon le niveau de difficulté.

Le butin d'un niveau est utilisé au début du niveau suivant pour permettre à
Garrett d'acheter du matériel ou des indices avant sa mission. En général, à la
fin de chaque niveau, Garrett doit s'échapper en retournant au début du niveau.

Lorsque Garrett s'approche des PNJ discrètement, il peut voler à la tire
ce qu'ils ont à leur ceinture (clés, bourses, ...).

En ce qui concerne les niveaux de difficulté, voici une liste des mécanique que
le jeu modifie :
- matériel initial
- objectif de butin
- exfiltration nécessaire
- objectifs additionnels
- restrictions additionnelles (ne pas tuer, ne pas se faire repérer, ...)
- nombre de gardes
- sensibilité des gardes
- changement dans la structure des niveaux (passages secrets innaccessibles,
  ...)
- ...

### 3.0.1. Techniques émergentes

*(Si ce n'est mentionné, la technique est valide sur les deux versions du
moteur)*

**Ass manip skip (NewDark only)** : ce skip est en réalité un **Item Deletion**.

En particulier pour celui-ci il s'agit d'une technique assez compliquée pour
manipuler le comportement des assassins de la mission "Assassins" afin de
pouvoir les assomer sans fail la mission. Cependant, le setup se produit à la
mission d'avant. Je n'ai vu aucune explication de pourquoi ça marche, mais ça
marche. À priori ça fait disparaître l'objectif, ce qui empêche son échec.

Dans la mission "Down In The Bonehoard", il faut faire une succession très
précise de tir de flèches à différents endroits du niveau, puis, une fois le
setup préparé, il faut sauvegarder.

Une fois arrivé à la mission "Assassins", il faut sauvegarder, puis charger
la sauvegarde de setup, puis refaire quelques manip et enfin reload la
sauvegarde de "Assassins".

Le comportement des assassins est buggé, l'objectif a disparu, et les
assomer n'échoue pas celui-ci et donc la mission.

Lien de la manip, probablement indescriptible en live, à poster sur le chat
éventuellement : https://www.speedrun.com/thiefgold/guides/qe5u1

Avec *OldDark* à priori le **Block Knockout** est la solution à utiliser.

**Blackjack glitch** : ce glitch permet d'annuler sa vitesse verticale en l'air
et donc de ne pas prendre de dégâts de chute. Il permet également de gagner de
la vitesse horizontale au moment du cancel, et donc également d'allonger la
distance d'un saut. Ce glitch s'exécute différemment en fonction du moteur :
- *OldDark* : dans cette version, le glitch a une phase d'initialisation et
  une fois initialisé peut être utilisé à l'infini.
  Initialisation :
  1. Dégainer le gourdin ;
  2. Initier une attaque mais...
  3. ... avant que l'animation termine, changer l'arme pour l'épée et
    immédiatement changer l'arme pour le gourdin.
  Exécution : pour annuler sa vitesse après l'initialisation, il suffit de
  rengainer le gourdin. Par la suite le glitch est réutilisable à chaque fois
  que le gourdin est dégainé.
- *NewDark* : dans cette version, la première exécution du glitch va également
  l'initialiser, puis chaque exécution qui suivra sera plus simple.
  Initialisation/première exécution :
  1. Dégainer le gourdin ;
  2. Maintenir le bouton d'attaque jusqu'à l'étape 4
  3. Cycler dans la sélection d'objet jusqu'à passer les crochets. L'animation
     d'attaque va jouer, mais maintenir le bouton quand même
  4. Ranger le gourdin pour annuler la vitesse.
  Exécutions suivantes : dégainer le gourdin et ranger le gourdin fonctionne
  ensuite exactement comme après l'initialisation du *OldDark*.

Note : utiliser le glitch avec trop de vitesse tue le joueur de toute façon !

**Block Knockout (OldDark only?)** : débinder la touche de "Block/Saut"
puisqu'il existe un bind indépendant pour chacune de ces actions. Dans cette
situation, bloquer avec le gourdin (normalement impossible) permet d'assomer les
ennemis. Utiliser pour bypass l'objectif de Assassins si on assomme les
assassins.

**Bunny hop** : contrairement à Quake / Half-Life, le *bunny hop* dans ce jeu
n'est pas à proprement parler un glitch, cependant il s'agit tout de même d'un
abus du code concernant les sauts. À chaque fois que Garrett saute dans une
direction, il bénéficie naturellement d'un boost de 1.4 fois sa vitesse (1.05
dans NewDark ?).

Donc, répéter les saut à chaque fois que Garrett touche le sol revient à
multiplier sa vitesse par 1.4 sans subir de friction.

**Gentle clipping** : il existe une technique très simple pour passer à travers
certains murs et portes très fins. Pour clip : se mettre dos au mur en question,
s'accroupir, se pencher *dans* le mur et sauter.

On ne peut pas clip si le mur est trop épais ou si le sol derrière le mur est
plus élevé que le sol où l'on initie le clip. Chaque clip du jeu a des
spécificité de setup car certains clip sont plus récalcitrant. Il est également
possible de rester coincé dans le mur, malgré tout à certains endroit il suffit
de se déplacer le long du mur pour se décoincer.

**Item stack** : les items récoltables ont une hitbox, et il
est possible de les lacher de l'inventaire. En utilisant leur hitbox, on peut
sauter d'un peu plus haut que le sol, ce qui peut aider à certains endroit. On
peut aussi parfois entamer l'animation d'aggripage dessus, comme si l'on
souhaitait grimper une corniche.

À priori, ce trick est aussi utilisé sur les échelles, pour que Garrett s'y
accroche plus vite.

**Item deletion (NewDark only)** : il s'agit d'une technique assez compliquée
pour faire disparaître des items d'un niveau en utilisant un glitch manipulant
la mémoire.

Il faut pour ce faire utiliser des objets (potions...) ainsi qu'un arc et des
flèches dans certains niveaux. Cela va avoir pour effet de faire disparaître
les ID (utilisés en interne par le jeu) qui identifient les items. Ensuite, à
un autre niveau, ces ID sont disponibles et utilisés pour les items que l'on
souhaite supprimer. Il faut ensuite recharger la partie pour retirer à l'arc
ou utiliser la potion qui correspond et recharger la partie au niveau en cours.
Les IDs des deux objets sont perçus comme les même par le jeu et, la flèche
ayant été tirée, l'ID va disparaître, entraînant la disparition de l'autre objet
avec lui.

Cela est utile dans le **Ass manip skip** pour supprimer un objectif (les
objectifs ayant un ID) et dans le dernier niveau pour supprimer les autels du
rituel, par exemple.

**Item duplication** : faire un save/load au même moment duplique les items
de la carte, qui bug.

Il est possible de dupliquer une flèche qu'on est en train de tirer avec la
même exécution, la flèche apparaîtra en place, et une copie sera dans
l'inventaire.

**Object fling (OldDark only)** : c'est un glitch qui abuse de la physique du
jeu pour se propulser dans une direction en utilisant un objet du décors. Il
faut se placer en face d'un objet choisi. Il faut ensuite se pencher et
légèrement se tourner à l'opposer de la direction souhaitée, puis rapidement se
tourner dans l'autre sens. La collision avec l'objet alors que Garrett se penche
va rendre le moteur physique un peu perdu et propulser dans la direction
voulue (bien que le glitch soit très peu précis, il semblerait) .

## 3.1. Any% Normal, xxxDark (last update: AGDQ 2024)

### 3.1.0. Informations diverses

#### 3.1.0.0. Description générale

Plateforme : PC

Version du moteur : NewDark ou OldDark, selon incentive (cf. la sous-partie
*Version* de la fiche technique, ci-dessus).

Temps (AGDQ 2024) :
- OldDark :
  - Psych0sis - WR : **32m 20s** LRT - **32m 40s** RTA
- NewDark :
  - beazzles - WR : **29m 44s** LRT - **30m 09s** RTA
  - Psych0sis - 5ème : **38m 31s** LRT - **38m 48s** RTA

Style : Run du swag et du skill avec des techniques fumées et des glitchs.

Note : Durant l'AGDQ 2024 il y a une incentive sur le moteur utilisé, c'est
pourquoi la route détaille les deux possibilités pour chaque mission, si
applicable.

#### 3.1.0.1. Règles

*(Tirées de speedrun.com, certaines non pertinentes ignorées)*

##### 3.1.0.1.0. Général

Le jeu doit être joué en difficulté Normal.

Le framerate doit être stable à 60fps maximum.

##### 3.1.0.1.1. Chronomètre

Le chronomètre démarre au moment du clic sur "Continue" sur l'écran précédent le
niveau tutoriel (A Keeper's Training).

Le chronomètre se termine lorsque l'écran "Mission Complete" s'affiche à la fin
de la dernière mission (Into the Maw of Chaos).

##### 3.1.0.1.2. Spécifique OldDark

DDFix esl le seul mod autorisé.

Les paramètres du jeu ne doivent pas être modifiés depuis les fichiers,
uniquement depuis l'interface du jeu.

##### 3.1.0.1.3. Spécifique NewDark

N'importe quelle version de TFix est autorisée (1.20 recommendée).

TFix Lite n'est PAS autorisé.

### 3.1.1. Route

#### 3.1.1.0. General

La plupart des tricks et glitches sont décrits dans la partie
**3.0.1. Techniques émergentes**. Le jeu étant très linéaire, les seuls éléments
mentionnés ici seront les objectifs et les techniques spécifique à un niveau en
particulier.

Chaque niveau abuse le plus que possible des *Gentle clips* et des *Bunny hop*.

La version *OldDark* abuse autant que possible des *Object flings*.

La version *NewDark* possède quelques *Out of Bounds* ainsi que des
*Items delete* qui permettent des skip un peu surprenant.

De plus, à la fois parce qu'on est en Normal et aussi parce que l'IA n'est pas
fantastique de toute manière, on va radicalement ignorer la plupart des ennemis
du jeu... et toute l'infiltration en fait !

#### 3.1.1.1. A Keeper's Training

Objectif(s) : passer l'entraînement

**NewDark** : save importante pour un glitch plus tard (Thieves' Den).

#### 3.1.1.2. Lord Bafford's Manor

Objectif(s) :
- S'infiltrer dans le manoir de Lord Bafford
- Trouver le Sceptre serti de joyaux de Lord Bafford

**NewDark** : save importante pour un glitch plus tard (Into the Maw of Chaos).

**OldDark** : à la fin de la mission, il semble possible de mourir mais
continuer à se déplacer pour obtenir le sceptre, ce qui accélère le
*fade out* vers l'écran de fin de mission.

#### 3.1.1.3. Break from Cragscleft Prison

Objectif(s) :
- Trouver la prison et s'y infiltrer
- Libérer Cutty de prison (objectif à fail, scripté)
- (Ajouté en cours de mission :) Trouver le coffre de preuves et récupérer la
  carte de Felix

**OldDark** : On skip la majorité des mines grâce au fling.

Dans la prison, pickpocket de la clé du garde ; qu'on ignore.

Lorsqu'on trouve la cellule de Cutty, il suffit de s'incliner dans
la cellule pour trigger le dialogue qui change les objectifs, pas besoin de
l'ouvrir. Manipulation des gardes avec des flêches sonores pour détourner leur
attention.

#### 3.1.1.4. Down in the Bonehoard

Objectif(s) : trouver la Corne de Quitus

**NewDark** : hyper important, ici on va setup le **Ass manip skip** ! Lien du
tuto de la manipulation pour les curieux :
https://www.speedrun.com/thiefgold/guides/qe5u1

**Item stack** juste avant la Corne pour attraper la corniche.

#### 3.1.1.5. Assassins

Objectif(s) :
- Mettre à sac le temple des Hammerites (c'est une feinte, objectif remplacé)
- (Ajouté au début de mission :) Filer les assassins pour trouver qui les a envoyé
- (Ajouté en cours de mission :) Mettre à sac la maison de Ramirez et lui voler
  sa bourse.
- (Ajouté en cours de mission :) Accumuler un butin d'un valeur de 1,000.

**OldDark** : **Block Knockout** un des assassins.

**NewDark** : on termine le **Ass manip skip** pour pouvoir assomer un des
assassins.

On ammène l'assassin à la demeure Ramirez directement plutôt que se taper toute
la phase de filature et ça trigger le changement d'objectif.

**OldDark** : à la fin de la mission, un nouveau *death abuse* pour le fade out
plus rapide sur l'écran de fin de mission.

#### 3.1.1.6. Thieves' Guild (Thief Gold addition)

Objectif(s) :
- S'infiltrer dans la guilde des voleurs via le casino clandestin "Overlord
  Fancy" ;
- Récupérer le vase de saphir de Lord Randall ;
- Voler 1000 de butin à la guilde en représaille ;
- S'exfiltrer de la guilde.

Au début du niveau : achat de 2 potions de vitesse.

Routes **OldDark** et **NewDark** radicalement différent dans cette mission
(nouvelle route depuis les précédent records ou spécifique au moteur ?)

**OldDark** : Outre les tricks habituels, on consomme une potion de vitesse dans
le souterrain pendant la phase de loot puis une dans les égouts entre les deux
maison des barons du crime. Du reste, on fonce.

**NewDark** : on va looter d'abord le casino clandestin et ensuite on va setup
un OoB appelé **Thieves' Guild Floor Clip**. Apparemment la manip a l'air très
particulière et technique, requerrant une sauvegarde vers le tutoriel
similairement à l'**Ass manip skip** et des **Item stack** pour passer dans un
état clippé avec le sol en faisant dans le même temps croire au jeu qu'on est en
train de monter une échelle. Conséquence : on vole jusqu'à la fin du niveau et
on ramasse le vase et la fin du loot en OoB jusqu'à atteindre la zone
d'extraction.

Lien du tuto de cette manip : https://www.speedrun.com/thiefgold/guides/1onto

(À priori ce skip **NewDark** a été trouvé hyper récemment car la run WR du
niveau individuel en **NewDark** utilise une route similaire à **OldDark**. Le
nouveau skip fait à priori gagner 1 minute sur l'ancienne route).

Dans les 2 version du moteur, mourir alors qu'on a remplis les objectifs que
l'on devait remplir trompe le jeu, et valide le niveau.

#### 3.1.1.7. The Sword

Objectif(s) :
- Trouver et obtenir l'épée ;
- En profiter pour voler un butin de 500 ;
- S'échapper de la demeure avec l'épée.

Dans les deux moteurs : *death abuse* pour faire pop l'écran de fin de niveau
plus vite.

#### 3.1.1.8. The Haunted Cathedral

Objectif(s) :
- Trouver la cathédrale ;
- Trouver un moyen de rentrer dans la cathédrale et voler l'Œil ;
- Voler un butin de 750 ;
- S'échapper ;
- (Ajouté en cours de mission :) Obtenir la Clé du Portail dans la bibliothèque
  des *Keepers*.

En dépit des objectifs donnés par le jeu, seuls 2 sont nécessaire pour finir la
mission : le butin et la Clé du Portail.

**OldDark** : death abuse pour afficher l'écran de fin de niveau plus vite.

#### 3.1.1.9. The Mage Towers (Thief Gold addition)

Objectif(s) :
- Atteindre la bibliothèque des mages pour des indices ;
- Trouver des indices sur l'endroit où est conserver le Talisman de la Terre ;
- Voler le Talisman ;
- Voler un butin de 400.
- S'échapper par le passage secret vers les égouts sous les tours des mages.

**OldDark** : en début de mission, achat de 2 mines.

**NewDark** : en début de mission, achat de 1 potion de soin et 2 bombes flash.

On passe en coup de vent dans la librairie pour valider l'objectif mais en vrai
on en a rien à carer.

**New dark** : on utilise la potion de soin pour **Item stack** afin de clip
plus facilement à travers le mur.

**New dark** : flash bomb pour sortir de la salle du talisman en clip sans que
les ennemis nous fasse chier (j'imagine que c'est par sécurité et pas vraiment
nécessaire).

Après avoir récupéré le talisman, **Blackjack glitch** pour fall cancel de la
tour.

**OldDark** : death abuse en fin de niveau, as usual.

#### 3.1.1.10. The Lost City

(OK ce niveau est trop cool en speedrun haha)

Objectif(s) :
- Trouver l'entrée de la Cité Perdu au fond de la rivière ;
- Voler le Talisman de Feu ;
- S'échapper.

Début du niveau : achat d'une potion de vitesse.

**OldDark** : posage de mine juste après le saut dans la grotte.

Potion de vitesse sur la corniche en face du talisman de feu + **Strafe Jump**
vers le taliman pour le ramasser, et :
- **OldDark** : **Strafe Jump** en sens inverse.
- **NewDark** : **Item stack** en sens inverse.

**OldDark** : death abuse avec la mine.

**NewDark** : death abuse avec les flèches de gaz.

#### 3.1.1.11. Song of the Caverns (Thief Gold addition)

Objectif(s) :
- Trouver l'informateur Giry ;
- Utiliser la carte des Keepers pour trouver le Talisman de l'Eau;
- S'échapper des cavernes.
- Voler un butin d'une valeur de 200.

Objectif(s) après update :
- Trouver et voler le Talisman de l'Eau ;
- Trouver et s'infiltrer dans l'Opéra ;
- Voler un butin d'une valeur de 700.
- S'exfiltrer de l'Opéra.

**NewDark** : on récupère 3 flash bomb dans l'arsenal de l'opéra.

**NewDark** : flash bomb les gardes devant la porte du trésor personnelde Lady
Valerius pour clip tranquille (possible que ce soit par sécurité et même pas
nécessaire)

**OldDark** : à la fin, death abuse into **Object fling** à balle.

#### 3.1.1.12. Undercover

Objectif(s) :
- Se faire passer pour un novice Hammerite, présenter la lettre de
  recommandation au portail et rentrer dans le temple ;
- Chercher et voler le Talisman de l'Air ;
- S'échapper.

C'est le festival du **Gentleclip** ici.

Dans la salle du Talisman; bypass complet du script qui nécessite le marteau
Hammerite pour traverser le vide entre l'entrée de la salle et le talisman grâce
à un **Straffe jump**.

#### 3.1.1.13. Return to the Cathedral

Objectif(s) :
- Attraper l'Œil ;
- S'échapper de la Cathédrale.
- (Ajouté en cours de mission, mais pas en speedrun :) Aider le fantôme de Frêre
  Murus pour pouvoir s'échapper.

**OldDark** : Flash bomb pour stun/tuer les mort-vivants et prendre l'Œil
tranquille (probablement par sécurité).

**Item stack** pour récupérer l'Œil.

Ici on va death abuse à un endroit bien précis pour activer l'objectif
"S'échapper", et donc finir le niveau au lieu d'échouer.

**OldDark** : mega **Object fling** pour se tuer et *death abuse*.

**NewDark** : *death abuse* avec les flèches de feu, explosives.

#### 3.1.1.14. Escape!

Objectif(s) : si vous voulez survivre, il faut vous échapper d'ici !

Objectif(s) après update : sortez de la maison de Constantine en vie (lol).

Ramasser les flash bomb pour flasher les ennemis si la RNG n'est pas favorable.

**NewDark** : le fait à la flèche de gaz.

**OldDark** : **Object fling** pour *death abuse* en touchant le trigger de fin.

#### 3.1.1.15. Strange Bedfellows

Objectif(s) :
- Parler à un Hammerite haut-placé pour obtenir de l'aide ;
- Ne pas déclencher de combats avec eux.

Objectif(s) après update :
- (Échoué) S'infiltrer pour trouver le Haut-Prêtre Hammerite pour le prévenir du
  Trickster
- Investiguer la situation et trouver les Hammerites ;
- Trouver le Haut-Prêtre ;
- Ramener le Haut-Prêtre aux Hammerites assiégés ;
- Ne tuer aucun Hammerites.

On va gruger cette mission en allant cherche le Haut-Prêtre *d'abord*.

Flash bomb sur les ennemis juste avant pour pas se faire embêter.

On gruge, donc, en déclenchant le dialogue avec le Hammerite de l'autre côté du
mur, puis en rammenant le Haut-Prêtre en face de lui avant que le dialogue se
termine.

**OldDark** : death abuse à la fin avec les flèche à gaz.

#### 3.1.1.16. Into the Maw of Chaos

Objectif(s) :
- Chasser Constantine dans la Gueule du Chaos
- Intervertir le vrai Œil et le faux Œil sans se faire prendre.
- Attendre la fin du rituel pour observer le résultat.

**OldDark** et **NewDark** ont deux routes très différentes à nouveau.

***

**OldDark** : niveau très vertical, utilisations répétées du
**Blackjack glitch**.

**OldDark** : à la fin du pont où l'on doit marcher dans l'ombre faute de
déclencher les pièges, on gruge le niveau en sautant dans le vide, avec
**Item stack** avec une mine pour interrompre la chute et atteindre la terre
ferme d'un endroit où on est pas sencé aller, et on **Object fling** contre le
portail pour arriver au bon endroit.

***

**NewDark** : **Item duplication** sur une flèche de mousse.

**NewDark** : **Maw of Chaos Floor Clip** : encore une technique un peu
compliquée pour clipper à travers le mur, après quoi on glisse littéralement
dans le niveau et on OoB à travers le mur pour tomber droit dans un tunnel où
on n'est pas censé allé juste avant le portail.

Tutoriel ici pour les curieux : https://www.speedrun.com/thiefgold/guides/j2hc7

**NewDark** : **Item stack** pour booster le saut pour revenir sur la bonne
plateforme.

**NewDark** : c'est pas fini avec les truc chelou, c'est l'heure du
**Maw of Chaos Item Deletion**. C'est une manip qui implique encore des
save/load vers un autre niveau et des manipulations à l'arc.

Tutoriel ici : https://www.speedrun.com/thiefgold/guides/r65hk

Le but est de supprimer les autels que visite le Trickster pour accélérer le
temps du rituel et passer moins de temps à attendre la fin du niveau.

***

Dans les deux cas : seule utilisation des flèches à mousse de toute la run, pour
que le Trickster ne nous entende pas échanger les Yeux.

On attend que le trickster fasse son biz et death abuse à la fin du niveau.

# 4. Trivia

Thief: The Dark Project avec son focus très avancé sur la discrétion est
considéré comme le premier jeu d'infiltration à la première personne.

Le jeu dispose d'un niveau bonus, "Blooper Reel" qui expose les bugs rencontrés
pendant le développement, ainsi que des parodies de divers éléments du jeu
ou d'ailleurs. Le niveau se déroule dans le même décors que la mission 1 (Lord
Bafford's Manor).

# 5. Références

Wikipedia : https://en.wikipedia.org/wiki/Thief%3A_The_Dark_Project

Fan wiki du jeu : https://thief.fandom.com/wiki/Thief_Gold

Lien speedrun.com : https://www.speedrun.com/thiefgold

DDFix forum thread (seul patch permis pour OldDark) : https://www.ttlg.com/forums/showthread.php?t=121449

TFix forum thread (patch pour NewDark) : https://www.ttlg.com/forums/showthread.php?t=141148

Liste de la plupart des glitches et manipulations : https://www.speedrun.com/thiefgold/guides

En particulier l'**Ass manip skip** : https://www.speedrun.com/thiefgold/guides/qe5u1

En particulier le **Thieves' Guild Floor Clip** : https://www.speedrun.com/thiefgold/guides/1onto

Jump speed boost (bunny hop) : https://www.reddit.com/r/Thief/comments/3mpxiz/the_jump_acceleration_glitch_was_it_ever_fixed/

Tutorial object fling : https://www.youtube.com/watch?v=bpbo5yYJoj4
