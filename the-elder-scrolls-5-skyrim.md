# 0. Fiche technique

Sortie (Classic) :
- (2011-11-11) international Windows, PlayStation 3, Xbox 360
- (2011-12-8) japon
- (2016-10-28) PlayStation 4, Xbox One
- (2017-11-17) Nintendo Switch

Plateforme : Windows - PS3 - PS4 - Xbox 360 - Xbox One - Switch  
Développé par : Bethesda Game Studios  
Édité par : Bethesda Softworks  
Genre : Action-RPG  
Compositeur : Jeremy Soule  

# 1. Histoire

## 1.0. Prélude

La saga des The Elder Scrolls se déroule dans le monde fictif de Tamriel. La saga est riche et extrêmement chargée en lore, ce qui fait que je ne mentionnerai qu’une infime fraction de ce qui est.

200 ans se sont écoulés après la fin de l’opus précédent (Oblivion). Bien que les Elders Scrolls se déroulent dans le même univers, il n’y a pas de lien direct entre les jeux. L’Empire humain ne s’est jamais vraiment remis des événements du dernier jeu, et il a entamé une longue phase de déclin.

Suite à une guerre de longue haleine avec une faction elfe appelée les Thalmors, l’Empire n’était plus que l’ombre de lui même. À la fin de la guerre, le traité de l’Or Blanc signé jouait en la défaveur des humains et résulte en plusieurs interdictions qui irritent les citoyens de l’Empire. Les plus notables étant l’abandon de territoires conquis et l’interdiction du culte de Talos, ancien humain élevé au rang de dieu.

L’ancienne demeure de la faction humaine des Nordiques, Skyrim (ou Bordeciel en français) vacille au bord de la destruction. Avec la mort du haut-roi Torygg, une guerre civile éclate, les frères tuant les frères dans cette province de l’Empire composée de toundras et de montagnes. Beaucoup, en particulier des Nordiques, désirent faire sécession de l’Empire affaibli duquel ils se sentent trahi suite au traité de l’Or Blanc. D’un autre côté, de nombreux loyalistes préfèrent la sécurité de l’Empire. De rares individus voient en les Thalmors les véritables ennemis, et comprennent qu’ils attendent que les provinces soient à genou avant de les annexer entièrement.

À côté de la géopolitique de Tamriel, les Elder Scrolls (parchemins des anciens) ont prédit le retour des premières menaces sombres du monde, les Dragons ; et l’apparition d’un mortel né avec l’âme d’un dragon, le Dovahkiin, Dragonborn (enfant de dragon). C’est un être qui a le pouvoir de défaire pour de bon son plus grand ennemi et mettre fin au noir règne du destructeur de monde, Alduin.

## 1.1. Jeu

Le jeu commence comme tous les Elder Scrolls : emprisonné. Le personnage-joueur essayait de traverser la frontière entre Cyrodiil (la province de la faction des Impériaux) et Skyrim (Nordiques), et s’est fait prendre entre deux feus, puisque l’empire tendait une embuscade contre les renégats Sombrages.

Traité comme les prisonniers de guerre qui l’accompagnent, le sort qui l’attend est l’exécution. La tête sur le billot, il ne doit son évasion qu’à l’intervention salvatrice du boss du jeu (paradoxalement), Alduin le destructeur de monde, le plus puissant des dragons qui avait décidé ce jour-là de détruire l’endroit. À la suite d’une échappée sauvage, le personnage-joueur s’échappe de Helgen, la ville dans laquelle il allait perdre la vie.

Le jeu commence à cet endroit, où le personnage est finalement libre de ses mouvement. Le jeu mettant à disposition un certain nombre de quêtes secondaire, avec régulièrement une histoire propre à plusieurs suites de quêtes, il ne sera ici fait mention que de la quête principale.

Suite aux événements de Helgen, l’allié avec qui le personnage-joueur s’évade lui indique de rapporter la menace et le retour des dragons à la ville de Whiterun (Blancherive). Après avoir rencontré le jarl (titre de noblesse équivalent à comte) Balgruuf qui gouverne Blancherive, ce dernier vous remercie pour l’information et accepte d’envoyer l’armée protéger Whiterun (Rivebois), un village proche de Helgen. Chargé de mission par le mage de la cour, le personnage-joueur retrouve une tablette draconique perdue dans une ruine, ainsi qu’un mot de pouvoir draconique, duquel il ne saisit pas encore le sens.

*La suite est tirée de wikipedia fr*
Au retour du joueur à Blancherive, une des tours de garde de la cité est attaquée par un dragon. Le joueur se rend sur place et aide à vaincre la bête, mais, à la mort du dragon, le joueur absorbe son âme et devient capable de maîtriser son cri. Stupéfaits, les soldats de Blancherive se rendent compte que le joueur est un « enfant de dragon », capable de parler la langue des dragons. Peu après, le jarl est mis au courant, et le joueur est appelé par les Grises-barbes, un ordre de moines reclus dans le temple du Haut-Hrothgar, près du sommet de la plus haute montagne de Bordeciel, la Gorge du monde. Ces érudits sont notamment chargés d'étudier les pouvoirs du Thu'um, et seuls à pouvoir reconnaître le joueur comme Enfant de dragon. Une fois adoubé en ce haut lieu, le héros apprend les secrets du langage draconique et son rôle dans la guerre à venir contre Alduin, le Dévoreur de Mondes, le plus dangereux des dragons.

Pour éprouver ses capacités, les Grises-barbes envoient l'Enfant récupérer une relique, la corne de Jurgen Parlevent à Ustengrav, un ancien tertre nordique près de la ville de Morthal ; celle-ci a en fait déjà été saisie par un inconnu qui donne rendez-vous au héros. Le voleur n'est autre que Delphine, l'aubergiste du village de Rivebois et surtout l'une des dernières représentantes de l'Ordre des Lames, des chevaliers qui furent autrefois dévoués à la protection de l'empereur de Tamriel mais qui agissent désormais en Bordeciel pour l'extermination de la menace que représentent les dragons. Avec Delphine, le héros part au Bosquet de Kyne, près de Vendeaume, à la recherche d'Alduin, et assiste au processus de résurrection d'un dragon, qu'ils tuent avant qu'il ne puisse causer des dommages. Delphine, désormais bien convaincue des capacités de l'Enfant de dragon, l'aide alors à infiltrer l'ambassade des elfes Thalmors auprès de la capitale de Bordeciel, Solitude, afin d’enquêter sur leur possible implication dans le retour des dragons. Sur place, ils apprennent que les Thalmors recherchent activement un homme appelé Esbern, archiviste des Lames, mais qu'eux-mêmes n'ont rien à voir avec le retour des dragons. L'enjeu est alors de retrouver l'archiviste en premier, qui se cache dans la Souricière, un grand réseau de galeries souterraines à Faillaise.

Une fois Esbern retrouvé, l'élu accompagne les Lames vers le Temple de Havreciel, où se trouve le Mur d'Alduin. Il y découvre une gravure historique datant du premier combat entre Alduin et les hommes, faisant référence à l'existence d'un cri extrêmement puissant, seul capable de vaincre le seigneur des dragons. Tandis que les Lames s'organisent, le héros retourne auprès des Grises-Barbes pour en apprendre plus sur l'arme qui a permis la défaite d'Alduin par le passé, ce cri représenté sur la gravure : le Fendragon, un cri du Thu'um créé de toutes pièces par les hommes pour priver un dragon de sa capacité à voler. Il apprend la vérité sur la bataille et le Fendragon par le chef des Grises-Barbes lui-même, le dragon Paarthurnax, général déchu d'Alduin. Le vénérable dragon révèle que dans les temps anciens Alduin ne fut pas réellement vaincu, mais seulement envoyé dans une autre époque grâce à un Parchemin des Anciens, dans l'espoir qu'il ne réapparaîtrait jamais. Le héros parvient à retrouver le précieux Parchemin grâce aux mages de l'Académie de Fortdhiver, au fond des ténébreuses ruines dwemer de Griffenoire. Il lui permet de voyager momentanément dans le temps pour assister au combat mythique des trois légendaires guerriers nordiques face à Alduin, et prendre connaissance du Fendragon.

Immédiatement au sortir de cette expérience mémorielle, Alduin surgit à la Gorge du Monde et attaque le héros ; désormais armé du Fendragon, celui-ci parvient à mettre la créature en déroute, qui fuit vers Sovngarde, le monde légendaire où les Nordiques trouvent le repos après la mort. Déterminé à se lancer à la poursuite, l'Enfant de dragon doit découvrir comment accéder à ce mythique paradis ; il se sert pour cela de Fort-Dragon, le palais du jarl de Blancherive, pour capturer un dragon et le forcer à lui révéler l'accès à Sovngarde. Auparavant, le jarl Balgruuf le Grand aura exigé que le héros prenne parti dans la guerre civile pour conclure une trêve entre les Impériaux et les Sombrages et apaiser la situation en Bordeciel. Grâce à sa réputation, qui fait désormais autorité dans le pays, l'Enfant de dragon est capable d'organiser une rencontre entre le Général Tullius et le Nordique Ulfric Sombrage au Haut-Hrothgar, sous la tutelle des Grises-Barbes, et parvient à obtenir un armistice temporaire jusqu'à ce que la menace d'Alduin soit écartée.

La guerre civile apaisée, le héros capture un dragon, Odahviing, qu'il fait détenir à Fort-Dragon et qui lui révèle qu'Alduin a rejoint Sovngarde par un portail dans les ruines d'un fort inaccessible par voie humaine du nom de Skuldafn. Odahviing, impressionné par le Thu'um de l'Enfant et mettant en doute la supériorité d'Alduin depuis sa défaite à la Gorge du Monde, accepte de l'emmener en ce lieu sur son dos. L'élu rejoint ainsi Sovngarde et le Panthéon des Braves, où il rencontre Ysgramor, le héros nordique des légendes, qui lui révèle qu'Alduin se nourrit des âmes défuntes à l'aide une brume magique. Grâce aux âmes des trois grands héros nordiques qui avaient repoussé Alduin autrefois, l'Enfant de dragon dissipe la brume et vainc le seigneur dragon pour de bon avant de retourner en Bordeciel, que les dragons ne menacent plus d'extermination.

# 2. Commentaire

## 2.0. Informations diverses

Runné sur PC, version classique

Catégorie : Main Quest

Times:
- WR: **23m45s240ms** loadless (**30m43s950ms** RTA) by Waz
- (BubbleDelFuego 2ème à 25m46s090ms / 33m24s resp.)
- Chronos_R 3ème à 26m22s290ms / 37m10s050ms resp.

Run incompréhensible du swag avec des glitchs dans tous les sens

Timer démarre lorsque l’on gagne le contrôle complet du personnage (caméra et mouvement) et s’arrête quand on donne le dernier coup sur Alduin.

Sont interdits :
- *Text Glitch*
When leaving the game open for a long time (3+ hours), a glitch happens where you can skip through conversations much faster than normal. This glitch has an annoying long and inconsistent setup and doesn't work the same on everyone's computer, so it's not allowed in runs. You are advised to restart your game after every completed run or handful of attempts to avoid this.
- *Ctrl+PrtScn Hotkey*
This hotkey changes the game speed to be based on framerate rather than real time, which means the game runs normal speed at 30 FPS. Higher than that the game runs too fast (2x speed at 60 FPS, 4x at 120, etc.) and lower than that it runs slower. Since not everyone can maintain a constant 60 FPS (framedrops in Riften are common) this hotkey is not allowed.
Console Commands
Use of console commands would make the run trivial and boring so no commands are allowed to be executed during a run. Console commands are very helpful during testing / routing, however you are advised to restart your game prior to doing runs to ensure no commands entered will effect anything.
- *Mods*
As Skyrim is already an unstable game, we can't be sure any installed mods will not effect the run in any way or work the same across computers. Therefore any mods must be removed before doing any runs. Even trivial things like having SKSE running is not allowed.

## 2.1. Mécaniques de jeu

Alors, le jeu est un action RPG en vue à la première personne. Vous pouvez courir, sauter, marcher, activer le mode discrétion, vous battre avec les armes que vous pouvez équiper et lancer des sorts. Il y a quelques subtilité à un peu tout, mais globalement c’est ça.

Vous avez une barre de vie, une barre de mana et une barre d’endurance.

Les équipement que vous ramasser sont parfois enchantés, vous rendant encore plus puissant.

Monter de niveau se fait par compétence : quand vous utilisez ou entraînez une compétence, cette compétence gagne de l’expérience. Chaque fois que la compétence gagne un niveau, votre personnage gagne de l’expérience globale. Quand votre personnage gagne un niveau, vous avez l’opportunité de gagner 10 point de vie, mana ou endurance maximum et un point de capacité à dépenser. Les capacités sont des bonus spécifique à chaque compétence et améliorent celle-ci.

De reste, le monde de Skyrim est vaste, jouer-y pour tout découvrir.

## 2.2. Techniques

*soupir* ‘faut y passer, hein ?

Le jeu est gavé, bourré à ras-bord de bugs, glitch et autres délires en tout genre, et le speedrun un joyeux bordel incompréhensible.

**Infinite Sprint** : sprint, quicksave, enlever le doigt de la touche, quickload
*Special Edition* : sprint, quicksave, unsprint, quickload (maislol) (puisque c’est un mode et pas un bouton)

**Vendor Glitch** : dans l’inventaire du vendeur, click-drag une de nos catégorie vers une de leur catégorie et on peut en fait vendre le stuff du vendeur (maislol)

**Weapon Dupe** (dragonbane) : tenir (hold E) l’arme devant soit et E (ramasser) et quicksave, E,E. On a deux armes du coup. Ce glitch vient du fait que la quicksave fait un peu lagger le jeu et du coup on peut faire croire au jeu qu’on a ramasser deux fois l’item.
*Old Method* : prendre l’arme, la foutre dans un coffre, vendor glitch, l’équiper dans une main et dans l’autre et voilà. Défaut : l’arme n’est pas vraiment dans ton inventaire, juste dans tes mains.

**Infinite Power Attack** : au lieu de juste maintenir appuyé sur la touche d’attaque pour faire une power attack, on maintient mais moins longtemps et dès que l’anim est enclenchée on mash l’attaque. Ça fait qu’on fait une power attaque sans consommer d’endurance.

**Infinite Horse Sprint** : horse > sprint + wait menu + let go sprint + confirm wait

**Yolo bucket** : on trouve un setup pour se placer sur le bucket et on hold le bucket.

**Load Warping** : quicksave, entrer dans une zone et appuyer sur la touche de menu en même temps (le chargement se fait mais le menu est quand même ouvert) aller tout en bas du menu pour quitter et revenir au menu principal, charger une autre sauvegarde. Ça fait que ça amène le personnage de l’autre sauvegarde avec son avancée dans le jeu à l’endroit où on chargeait la zone. Marche seulement sur les zone où on rentre en marchant

**Fade-In Skip** : pour skiper les fade in post-chargement de zone, il faut repérer la barre de level en haut à droite et dès qu’elle disparait, il faut quicksave/quickload. Sauve 0.5 secondes à chaque chargement.

**Horse Tilting** : setup un peu compliqué : on a besoin d’un cheval, d’une save avant le tilt (tilt save) et d’une save à l’endroit d’où on veut tilter (tilty save).

La tilty save doit être faite en TPS.

Le tilt est réalisé en prenant le cheval, en l'amenant sur un rocher un peu spécial et en essayant de le faire se retourner et s’enfoncer. Ensuite on sort du cheval et c’est le bordel.

Load tilty save.

Le glitch vient du fait que le jeu essaye de vous remettre au sol, mais comme le cheval est n’importe comment (en vous ne devriez normalement pas pouvoir descendre, mais merci le setup) le jeu ne trouve pas le sol sur lequel vous mettre et il part en sucette, ce qui vous fait gagner de la vitesse en masse.

**Zooming** : nécessite cri du dash (ouragan?) et être en surcharge. Sauver, cri, quickload avant la fin du cri et le personnage avance avec une vitesse de malade.

**Scroll duping** : scroll dans un chest, vendor glitch & equip left & right : utilisation infinie !

## 2.3. Route

Accrochez vous à vos slips, on va absolument pas faire ce que vous pensez qu’il va se passer.

Dès le début : infinite sprint puis on passe par l’endroit normal jusqu’à ce qu’Alduin casse tout et on commence à faire n’imp.

Utilisation de la vitesse pour OOB comme un porc par delà Helgen et setup un glitch tout particulier.

On se pose en face de l’entrée de la grotte de fin d’évasion pour préparer un load warp. Save & load vers le début pour “faire” le tuto (délier les liens) et surtout chopper un seau. Save et...

Ensuite load warp et on fast travel à Helgen pour aller à l’Est (?) setup le bordel. On attend le cheval d’un chasseur qui nous le prête gentiment (non). On setup un tilt de cheval et YOLO.

On fait plusieurs tilt successif pour:
- Activer le waypoint du Haut Hrothgar
- Activer le waypoint de Blancherive (+ étables)
- Activer le waypoint de la ferme des Guerriers-nés
- Activer le waypoint de la gorge du monde
- Activer le waypoint de Red Eagle Redoubt
- Activer le waypoint de Rivebois
- Aller à Karthspire pour activer le waypoint d'Havreciel (10/17 MainQuest)

FT Blancherive pour aller à Dawnstar avec le chariot pour **Vendor Glitch** et setup un autre horse tilt pour :
- Activer le waypoint de Septimus
- Aller à l'ambassade Thalmor pour la quête 8/17.

FT Blancherive pour aller à Faillaise avec le chariot pour aller sauver Esbern (le sage des lames).
Quicksave/load et coups d'épaule pour passer les dialogues.

LW pour aller à Rivebois rendre la quête d'Esbern qu'on n'a jamais reçu et faire Esbern et Delphine parler ensemble.

FT Havreciel pour la quête 10/17. On active la dalle pour voir la prophétie mais en fait en s'en balance car FT Septimus (12/17) pour retrouver le parchemin des anciens :
- On vole la clé pour entrer dans la ruine dwemer à fouiller pour le parchemin parce que pas le temps de discuter.
- Ensuite fast travel pour setup un nouveau tilt vers la ruine dwemer en question, qu’on ouvre par la sortie avec un petit OOB, parce que pourquoi pas c’est gratuit.

Une fois que c'est fait, go Faillaise où on va chercher à se marrier avec un gonz et activer la quête du mariage.

Re fast travel vers Esbern qui a fini son speech et on en profite pour dupliquer Dragonbane, on récupère la quête suivante.

Ensuite on va choper Fendragon vers les grisebarbes mais en fait non c’est Paathurnax mais en fait non c’est dans le passé avec le parchemin.

Là, pour pas se taper toute la cutscene on se marie et on lit le parchemin ce qui déclenche le bordel (?).

En fait on a la cutscene, mais avec le mariage on a pu sortir du script du coup on est libre de faire un peu tout en même temps. Du coup on tue un dragon, on va activer un waypoint, on revient pour la fin de la cutscene pour entendre le Fendragon. On fast travel pour skipper les dialogue parce que en fait on peut pas vraiment save/load pendant le script qu’on a cassé.

On parle au jarl pour l’étape 2/17 de la quête puis on se fait ramener par la fin du script.

On tabasse Alduin dans le monde des vivants et il se planque en Sovngarde.

On va demander l’aide du jarl qu’on vient juste de voir pour maîtriser un dragon et pas le tuer.

Il demande la trève de la guerre du coup on va un peu chopper les leader pour les mettre à discuter avec les grisebarbes en arbitre.

On discute, trève, on choppe le dragon, il nous envoie vers le portail vers Sovngarde, on rentre, on demande l’aide des anciens héros, ils nous aident on les aide, on tue Alduin pour de bon et time.

# 3. Trivia

À la base, l’idée du thème de Skyrim devait être le thème des Elder Scrolls chantés par des barbares.

Bon y’a genre pleins de trivia à chopper sur TES5 mais la flemme.

# 4. Références

- Lien speedrun.com : http://www.speedrun.com/skyrim
- Wikipedia : https://fr.wikipedia.org/wiki/The_Elder_Scrolls_V:_Skyrim
- UESP : http://en.uesp.net/wiki/Skyrim:Skyrim

# Annexe A. Versioning

2019-06-28: Mise-à-jour SGDQ 2019
2019-05: Update du RPGLB 2019
2018: Création
