# 0. Metadata

## 0.0. Versioning

| Date       | Révision                                             |
|-           |-                                                     |
| 2022-10-17 | Création (RPGLB 2022) - Enhanced Edition Run Any%    |

# 1. Fiche technique

Sortie initiale :
- 30 juin 2014
- 27 octobre 2015 (Enhanced Edition)

Plateforme(s) : Windows, OS X, Linux, PlayStation 4, Xbox One, macOS et SteamOS

Développé par :  Larian Studios

Édité par : Larian Studios

Compositeur : Kirill Pokrovsky

Genre : RPG

Note : l'Enhanced Edition inclue du contenu additionnel, ce qui comprend une histoire étendue, de nouvelles options de gameplay et un remaniement de certaines mécaniques de jeu.

# 2. Histoire

## 2.0. Univers

Les protagonistes de l'histoire sont une paire de Traque-Sources ; membres d'une organisation dont le but est de traquer et d'exterminer une magie néfaste et puissante appelée la Source, ainsi que et ses adeptes, les Ensourceleurs.

## 2.1. "Acte I"

Le jeu commence sur l'arrivée des joueurs à Cyséal, ville portuaire de Rivellon, où ils ont été envoyé en mission d'élucider le meurtre d'un conseiller par potentiellement un Ensourceleur. Mais Cyséal, cité autrefois prospère est maintenant au bord de la ruine, car actuellement en proie à un véritable siège mené par les morts-vivants qui envahissent la région, et par les Orcs qui ont lancé des attaques. Ils découvrent vite que ce siège a été orchéstré par une conspiration d'Ensourceleurs liée aux Immaculés, un culte basé dans la forêt de Luculla plus loin dans les terre. Ils trouvent également des preuves impliquant la Sorcière Blanche, gardienne de la forêt de Luculla, dans le meurtre.

Leur recherche la concernant les ammène dans le royaume féérique de Hiberheim, un monde de glace, où ils la découvrent emprisonéee dans un block de glace par le chef des Immaculés, le Conduit. Après sa libération, elle se présente en tant qu'Icara, et plaide coupable d'avoir accidentellement tué le conseiller de Cyséal. Cependant, après leur rencontre avec les Immaculés, les Traque-Sources ont des preuves d'un crime plus grand du culte : meurtres en série, Sourcerie et sacrifices humains. Ils s'allient alors avec Icara contre cet ennemi commun, et elle révèle alors que le Conduit est en réalité sa sœur Leandra. Les deux étaient autrefois très proches et liées par un puissant, mais Leandra a rompu ce lien après d'étranges événements. Icara enjoint alors les Traque-Sources d'infiltrer le Culte pour apprendre leurs plans.

## 2.2. "Acte II"

Les Traque-Sources découvrent que le culte extrayait un métal toxique nommé le Tenebrium dans les mines de Luculla. Ils y découvrent aussi des Chevaliers de la Mort, des mort-vivants indestructibles créés par Leandra avec le Tenebrium et la Source, afin de conquérir Rivellon. Leandra détruit les mines dans une tentative de tuer les Traque-Sources, mais ils parviennent à s'échapper et emporter également son fail-safe qui rendent les Chevalier de la Mort vulnérables.

## 2.3. La Source et les Traque-Sources

Au cours du jeu, les Traque-Sources rencontrent occasionnellement des cristaux magiques connus sous le nom de Pierre Stellaire (ou certains transformées en Pierre de Sang par les sacrifices des Immaculés). Lorsqu'ils découvrent la première, les Traque-Sources sont transportés magiquement à la Résidence de la Fin des Temps, une forteresse mystérieuse au-délà l'espace et le temps, mais pourtant immédiatement familère aux deux individus. Au fur et à mesure de l'obtention des Pierres, ils apprennent que la Source était originellement une magie bénéfique associée à Astarte, la déesse de la vie, avant qu'elle ne soit corrompue par le Vide, une force maléfique qui provient d'au-délà le monde matériel. Les Traque-Sources eux-même sont révélés être les réincarnations de deux anciens généraux, un homme et une femme, qui avaient emprisonnés la corruption de la Source à l'intérieur d'un artéfact, la boîte de Pandore. Les généraux ont reçu en conséquence des pouvoirs divins pour protéger l'artéfact.

Cependant, lorsqu'un être démonique appelé Ornix persuada Astarte d'ouvrir la Boîte, la corruption fut libérée de nouveau et pris la forme d'un dragon, qu'Astarte combattu alors pour l'éternité dans le Vide. En pénitence de leur échec, les Gardiens choisirent d'être dépouillés de leurs pouvoirs et souvenirs et furent ressucités en Rivellon en humains ordinaires. Les Pierres Stellaires sont en fait des fragments cristalisés de leur mémoire perdue.

Déduisant que Ornix manipule maintenant Leandra pour qu'elle affaiblisse Astarte et libère le Dragon du Vide sur le monde, Icara implore les Traque-Sources de la stopper, soit en restaurant le lien psychique entre les sœurs, la Forge des Âmes, soit en la tuant si nécessaire.

## 2.4. "Acte III"

Les Traque-Sources suivent la piste du Conduit jusqu'au village de l'Orée du Chasseur, au bord de la Forêt Spectrale. Le village est désormais controllé par les Immaculés, les orcs et des mercenaires barbares. Ils trouvent des indices laissés par le magicien Zandalor, l'amant d'Icara et le némésis de Leandra, qui les mène vers un ancien Temple de la Source caché plus profond dans les bois. À l'intérieur du Temple, ils découvrent Zandalor et apprenent grâce à lui que l'objectif de Leandra est la Boîte de Pandore dans le Jardin Originel, vers lequel ils peuvent entrer depuis la Résidence de la Fin des Temps.

Dans le Jardin, ils confrontent Léandra et peuvent restaurer la Forge des Âmes entre les deux sœurs s'ils ont découvert comment faire dans la Forêt Spectrale. Dans ce cas, elle prend conscience de sa corruption et part avec Icara et Zandalor pour se racheter ; autrement ils n'ont d'autre choix que la combattre et la tuer. En atteignant la Boîte de Pandore, les Traque-Sources joignent leur forces avec Astarte pour détruire Ornyx, mais celui-ci invoque le Dragon du Vide lui-même avant sa chute. Au bout d'un combat titanesque, ils parviennent à défaire et bannir le Dragon dans la Boîte, à nouveau. Astarte jure alors de garder la Boîte pour l'éternité, remerciant les Traque-Sources et leur révélant que d'autre dieux ont laissé en conscience la Source se faire corrompre.

## 2.5. Epilogue

À la fin de leur mission, les Traque-Sources partent du Jardin Originel faire leur rapport à leur organisation et embarquent dans une nouvelle aventure.

# 3. Commentaire

## 3.0. Général

### 3.0.0. Mécaniques de jeu

C'est un RPG en temps réel avec combat au tour par tour.

Bien que jouable en solo, le jeu est très orienté Co-op car chaque joueur contrôlera un des deux personnages. Le jeu fourmille également de nombreux dialogue entre ceux-ci, qui permettent de détailler leur traits de personnalité en fonction des réponses données.

Les traits de personnalités sont une mécanique des personnages joueurs qui donnent des bonus spécifiques.

Chaque personnage a un certains nombre de caractéristiques, talents et compétences, qui permet au final d'utiliser des capacités. Les capacités sont réparties en classes, qui correspond à une compétence spécifique.

Le combat se déroule au tour par tour, chaque personnage possédant un certains nombre de point d'action pour effectuer des actions. Les actions et capacités coûtent un nombre de points d'actions qui dépend des compétences du type d'arme utilisé, etc. Les compétences sont soumises à des temps de recharge.

Les talents sont des traits spécifique qui changent un peu la façon de jouer un personnage.

Certains compétences concernent du craft.

L'inventaire est soumis à une règle de poids, les joueurs ne pouvant porter uniquement ce que leur force leur permet.

Le jeu possède une physique particulière : on peut déplacer et jeter des objets sur la carte. La distance et le poids des objets est fonction de la force du personnage qui tente cette action. Toutefois, la compétence "Télékinésie" permet de déplacer des objets quels que soit leur poids. La distance est fonction de la capactité toutefois.

À noter : si un objet est déplacé sur le socle d'un personage, l'objet fera des dégâts en fonction de son poids.

Le jeu fournit des points de téléportations à divers endroits du monde, qui permettent de se téléporter vers n'importe quel point déjà découvert.

En plus de cela le jeu donne aussi à chaque personnage-joueur au cours de la quête principale une Pyramide de Téléportation. Chaque pyramide permet au joueur de se téléporter à l'autre ; et chaque Pyramide peut se placer sur la carte du monde.

Il y a plusieurs effets élémentaires disponibles avec des sorts ou des objets, et le jeu est réputé pour des combinaisons détonnante entre différents effets (par exemple, mettre du feu sur du poison fais exploser le poison ; mettre de l'eau sur du feu fait de la vapeur, ce qui cache les gens, etc.)

### 3.0.1. Techniques émergentes

**Crushing chest** (old run) : chopper des items lourds et tout mettre dans un item conteneur et envoyer l'item sur un ennemis via Télékinésie = instant kill

**Chairwarp** (old run) il existe un glitch avec les points de téléportation et les chaises. Si un personnage s'assoit sur une chaise, sa position X Y est fixée par le jeu. Hors, si le personnage est téléporté sur une autre carte avec un point de téléportation, il va bien se téléporter sur la nouvelle carte mais aux coordonnées X Y de l'ancienne carte.

**Magma chest** : Y'a un coffre dans tout le jeu qui, s'il est détruit, fait spawn une flaque de lave en dessous. On s'en sert pour péta le boss final.

**Infinite Toss** : Clic droit alors qu'on clic-déplace un objet au sol et le jeu considère n'importe quelle position comme valide pour lancer l'objet.

**Battering Ram warp**: en utilisant la capacité *Battering ram* sur un objet volant (voir **Infinite Toss**) téléportera le personnage à celui-ci.
Note additionnelle : pour une raison inconnue au recordman du monde, enflammer le lit d'Evelyn dans Cyséal et se faire attraper étend la portée. "It’s basically pure junk".

**Invincibility glitch** : Parler à Nick Sans-tête alors qu'un autre personnage lui parle déjà rend le deuxième personnage à lui parler invincible et permet de tuer n'importe quoi sans rentrer en combat.

Key binding spécial : Option de dialogue 1 bind sur la molette de la souris pour aller plus vite

## 3.1. Enhanced Edition - Run Any%

### 3.1.0 Informations diverses

#### 3.1.0.0. Description générale

Runné sur PC.

Temps :
- WR : **4m 10s** RTA par *ArkEnd*
- Rang inconnu : **-inconnu-** RTA par Raelcun, runner commenté

Type de run : oskour cépété

#### 3.1.0.1. Règles

##### 3.1.0.1.0. Général

Le jeu est verrouillé en 60 FPS et les sauvegardes automatiques sont désactivées.

##### 3.1.0.1.1. Chronomètre

Le chronomètre démarre juste après la confirmation des personnage après la création de personnage.

Le chronomètre s'arrête quand Zixzax prononce sa dernière réplique dans l'Épilogue

### 3.1.1. Route

#### 3.1.1.0. Général

Euh... c'est pété ?

On va abuser donc du **Infinite Toss** glitch avec la pyramide de TP pour complètement *sequence break* le jeu.

#### 3.1.1.1. Début

(Du lancement de la run jusqu'à visite forcée de la Résidence de la Fin des Temps suite à la découverte de la première Pierre Stellaire)

##### 3.1.1.1.0 Nouvelle route

**Infinite Toss** + **Battering Ram warp** pour aller directement chez Evelynn et les blessés afin de se faire envoyer dans la Résidence de la Fin des Temps.

##### 3.1.1.1.1 Ancienne route

On va juste marcher.

Split des perso :

Perso 1 va marcher jusqu'à la porte d'entrée de Ciséal, zapper un combat grâce à la capacité d'Invisibilité du Brigand et crocheter la porte avec l'outil que Perso 2 va lui envoyer.

Perso 2 va faire le début du donjon tuto à l'est du split pour obtenir un kit de crochetage, et l'envoyer au Perso 1 (mécanique de jeu).

On reste sur perso 1 jusqu'à la fin de cette parti, où il va juste marcher tranquilou jusqu'à la zone des blessés pour trigger la Pierre Stellaire et la TP vers la Fin des Temps.

#### 3.1.1.2. Fin

On sort de la fin des temps, on récupère la pierre de TP, on récupère la deuxième pierre de TP et c'est le zbeul.

Pyramide de tp + **Infinite Toss** pour changer de map.

Pyramide de tp + **Infinite Toss** pour aller récupérer le **Magma chest** qui cassera le cul du boss de fin.

Pyramide de tp + **Infinite Toss** pour aller déclencher un trigger qui donne de l'XP après une découverte de zone (et chercher de l'expérience facile). Le level up permet seulement de pousser le jeu à faire en sorte que le piège du coffre fasse plus de dégâts (et donc plus de dégât au boss).

TP fin des temps

Pyramide de tp + **Infinite Toss** pour aller au boss de fin qui existe dans la carte mais loin.

Poser le coffre au sol là où le boss pop. Le boss pop. Envoyer une bombe incendiaire pour détruire le coffre. RIP le dragon.

Pyramide de tp + **Infinite Toss** pour skip l'épilogue parce que bon on a pas que ça à faire.

# 4. Trivia

Ce jeu a été kickstarté.

# 5. Références

Lien speedrun.com : https://www.speedrun.com/divinity_original_sin_enhanced_edition

WR, nouvelle route : https://www.speedrun.com/divinity_original_sin_enhanced_edition/run/m7xo6x4y
Avec notes : https://www.reddit.com/r/DivinityOriginalSin/comments/xzwqk8/dos1_beaten_in_4_minutes_tricks_explanation_in/

"Ancienne nouvelle route" : https://www.speedrun.com/divinity_original_sin_enhanced_edition/run/mk388x3z

Wikipedia : https://en.wikipedia.org/wiki/Divinity:_Original_Sin


