# 0. Metadata

## 0.0. Versioning

| Date       | Révision                                     |
|-           |-                                             |
| 2024-06-24 | Update (SGDQ 2024) - Run Any% No Voidclip  |
| 2022-01-12 | Création (AGDQ 2022) - Run Any% No Voidclip  |

# 0. Fiche technique

Sortie initiale : 16 novembre 2004

Plateformes : PC, Xbox, Xbox 360, PlayStation 3

Développé par : Valve Corporation

Édité par :
- Vivendi Universal Games
- Valve Corporation

Distributeurs :
- Electronic Arts (version boîte)
- Valve Corporation (version Steam)

Compositeur : Kelly Bailey

Genre : FPS

Le jeu existe en deux versions :
- *OldEngine* : la version boîte, sortie en 2004
- *NewEngine* : la version Steam, sortie dans le *Orange Box bundle* en 2007

# 2. Histoire

    (Merci Wiiiiikiiipeeediiiiiaaaa !)

## 2.0. Contexte

Peu après les évènements de Black Mesa survenus dans Half-Life, le Cartel
(*Combine* en anglais), une puissante organisation extraterrestre a pris le
contrôle de la Terre au cours d'une guerre éclair connue sous le nom de Guerre
des Sept Heures. Très vite, le Cartel a commencé à adapter et exploiter les
ressources de la Terre. Les humains ont été parqués dans des cités comme
Cité 17, sous la surveillance étroite de la Protection Civile, une branche
ultra-répressive de la milice qui, elle, est constituée de soldats transhumains,
fruits du croisement entre l'organisme humain et la technologie du Cartel.
L'ancien administrateur de Black Mesa, le Docteur Wallace Breen, a été déclaré
représentant de l'humanité qu'il dirige depuis son bureau au sommet de la
Citadelle, une gigantesque structure au centre de Cité 17.

Le Cartel ayant utilisé Xen comme portail pour son invasion, toutes les
créatures de Xen ont été déportées sur la Terre avec eux. L'espèce dominante de
Xen, les Vortigaunts, a été réduite à l'esclavage avant même son apparition sur
Terre, les humains semblent destinés au même sort tandis que les espèces moins
évoluées se sont implantées dans l'écosystème terrestre et ont anéanti la
majorité de la faune déjà existante. En dehors des Cités, le reste de la
Terre est envahie par ces créatures hostiles ou détient des camps de résistants.

Pour éviter toute résistance, Breen a mis en place une solide dictature qui
tient en différents points :
- une propagande omniprésente : les discours de Breen sont diffusés sur des
  écrans géants disposés partout dans les rues, les bâtiments publics ou
  militaires. L'écrasante présence de la Citadelle reste la représentation
  suprême de la domination de Breen.
- une répression forte : tout opposant (ou considéré comme tel) est puni par la
  privation et par la force. Les cas extrêmes sont envoyés dans la terrible
  prison de Nova Prospekt pour être torturés, interrogés et transformés en
  Stalker.
- une privation totale : un dispositif spécial empêche les êtres humains de se
  reproduire en supprimant leur cycle de reproduction. Le rationnement de la
  nourriture permet quant à lui une emprise totale sur la population et un moyen
  de pression sur la résistance.

Contrairement à son prédécesseur, Half-Life 2 se déroule dans un endroit peu
précis. L'architecture de Cité 17 ressemble cependant à celle des villes
d'Europe de l'Est. Viktor Antonov, le directeur artistique du jeu qui a aussi
passé son enfance en Bulgarie, indique que le choix de l'Europe de l'Est comme
décor du jeu vient du fait qu'elle est capable de combiner les architectures
ancienne et moderne, permettant de créer une ville avec une histoire.

Le décor du jeu montre fréquemment des signes et des graffitis écrits dans
l'alphabet cyrillique (et en langue bulgare). Les vieilles voitures
rencontrées dans le jeu sont des modèles de fabrication soviétique communs en
Europe de l'Est (Moskvitchs, Zaporozhets, Volgas (GAZ-24), Latvias (RAF-2203),
ZIL-130s). Durant le jeu, une base de la résistance s'appelle
« New Little Odessa » ; Little Odessa étant le surnom d'une communauté russe de
Brighton Beach, où de nombreux immigrants d'ex-URSS se sont implantés (la vraie
Odessa est une grande ville d'Ukraine du littoral). Le Père Grégori a un nom
commun dans les pays slaves et un accent stéréotypé (du moins dans la version
originale du jeu), de plus il a été identifié comme un prêtre orthodoxe, la
religion prédominante de nombreux pays d'Europe de l'Est (l'église de
Ravenholm est d'ailleurs typique de l'architecture orthodoxe).

Un nombre indéterminé d'années s'est écoulé entre les événements de Half-Life et
ceux de Half-Life 2. Les sources officielles sont conflictuelles là-dessus et la
période varie entre une à deux décennies.

## 2.1 Chapitre I : Point d'insertion

L'introduction du jeu est une sorte de rêve étrange dans lequel le G-Man parle
à Freeman du rôle qu'il va bientôt jouer, juste avant de sortir celui-ci de
l'état de stase dans lequel il l'a plongé à la fin de Half-Life. Freeman se
retrouve dans un train qui arrive sur Cité 17. À son entrée en gare, Gordon
retrouve rapidement Barney Calhoun, un ancien collègue de Black Mesa infiltré
dans la Protection Civile, qui l'aide à passer les contrôles et entrer dans
Cité 17. Poursuivi par la PC, il fait ensuite la connaissance d'une jeune
femme, Alyx Vance qui l'aide à éliminer quelques soldats. Elle le guide ensuite
vers le laboratoire d'Isaac Kleiner.

## 2.2 Chapitre II : Un jour mémorable

Dans le laboratoire secret du Docteur Kleiner, celui-ci rend la fameuse
combinaison CEH à Gordon avant de tenter de le téléporter vers le repaire
principal de la Résistance : Black Mesa Est où il pourra rencontrer le père
d'Alyx, le Docteur Eli Vance. Mais, si la téléportation d'Alyx réussit, le
crabe de tête domestique de Kleiner, Lamarr, interfère au moment de la
téléportation de Freeman qui échoue et alerte le Cartel de sa présence à
Cité 17. Barney propose alors à Gordon de prendre les canaux de la ville pour
se rendre à Black Mesa Est.

## 2.3 Chapitre III : Itinéraire Kanal

En route vers le canal, Gordon est de nouveau poursuivi par la milice du Cartel.
Il traverse le secteur des voies ferrées jusqu'à un avant-poste rebelle ; à ce
point, il devient de plus en plus évident que les exploits de Freeman à
Black Mesa lui ont donné une réputation de héros légendaire, son retour
quasiment messianique redonne courage à la Résistance et inquiète de plus en
plus le Cartel. Les rebelles lui confient un hydroglisseur avec lequel Gordon
commence sa traversée du canal.

## 2.4 Chapitre IV : Danger aquatique

Après une course poursuite frénétique avec les forces du Cartel,
l'hydroglisseur arrive finalement à une ancienne centrale hydro-électrique.
Dans les souterrains, l'équipe scientifique de la Résistance a implanté son
laboratoire principal, Black Mesa Est.

## 2.5 Chapitre V : Black Mesa Est

Là, Gordon rencontre le Docteur Eli Vance et le Docteur Judith Mossman,
rejoint par Alyx. Ils lui confient une arme développée par leurs soins
appelée « pistolet anti-gravité, » Alyx se charge d'apprendre à Gordon le
maniement de cette arme avec l'aide de Chien, un puissant robot conçu par son
père pour la protéger, lorsque le laboratoire est pris d'assaut par le Cartel,
forçant Gordon à s'enfuir en empruntant un tunnel abandonné menant à Ravenholm.

## 2.6 Chapitre VI : Ravenholm

À la suite d'un bombardement massif du Cartel, Ravenholm est devenue une ville
fantôme. La population entière a été transformée en zombies par les crabes de
tête qui ont envahi la ville, seul a survécu le père Grégori parti en croisade
contre les monstres qui traînent dans les rues de la ville. Le père Grégori
aide Freeman à sortir de Ravenholm et le guide vers le chantier naval à la
périphérie de Cité 17.

## 2.7 Chapitre VII : Autoroute 17

Freeman est alors alerté par radio par Alyx que son père a été capturé par le
Cartel au cours de l'assaut de Black Mesa Est et emmené à la prison
Nova Prospekt. Pour atteindre Nova Prospekt, il doit longer cette région du
littoral appelée La Côte en empruntant l'Autoroute 17. Informée, la Résistance
met à sa disposition un buggy qui lui permet de forcer les nombreux barrages du
Cartel. Sur le chemin, il porte assistance à une base de la résistance, nommée
New Little Odessa, commandée par le colonel Odessa Cubbage, à contenir l'assaut
d'un hélicoptère du Cartel.

## 2.8 Chapitre VIII : Pièges de sable

Plus loin, Freeman rejoint un phare reconverti en avant-poste de la résistance,
où une bataille se prépare au terme de laquelle il y laisse son buggy. Forcé de
traverser les dunes de sables pour rejoindre Nova Prospekt, Gordon doit
ffronter les Fourmilions, des redoutables créatures insectoïdes de Xen enfouie
dans le sable. Amené à éliminer un Garde Fourmilion, Freeman utilise les
phéromones de la créature pour commander les autres Fourmilions qui ne
représentent alors plus une menace et qui vont devenir un soutien de choix.

## 2.9 Chapitre IX : Nova Prospekt

Engagé dans les méandres de Nova Prospekt, Gordon Freeman constate que les
Fourmilions qu'il a attirés avec lui sont en train de submerger les gardes
de Nova Prospket. Dans ce chaos, seules quelques poches de résistances
subsistent, et Gordon Freeman a tôt fait de les dissoudre pour se frayer un
chemin toujours plus profond dans la prison.

## 2.10 Chapitre IX bis : Corrélation quantique

Avec l'aide des Fourmilions, Freeman prend d'assaut Nova Prospekt. Rejoint par
Alyx, ils finissent ensemble par retrouver Eli Vance et Judith Mossman. Mossman,
qui se révèle être une espionne du Cartel, crée une diversion et enlève Eli en
utilisant le téléporteur situé en profondeur dans la prison. Acculés, Gordon et
Alyx doivent reconfigurer le téléporteur pour pouvoir s'enfuir vers le
laboratoire du Docteur Kleiner. Cependant, un dysfonctionnement de l'équipement
entraîne une énorme explosion qui anéantit Nova Prospekt. Ils constatent à leur
arrivée dans le laboratoire de Kleiner qu'une semaine s'est depuis écoulée.
Kleiner explique qu'au cours de cette semaine, la chute de Nova Prospekt a été
tellement symbolique qu'elle a déclenché une révolte massive dans Cité 17 qui
est devenue le théâtre d'une véritable guerre urbaine. Il les informe également
qu'Eli Vance serait détenu en haut de la Citadelle, le bastion général du
Cartel.

## 2.11 Chapitre X : Anticitoyen Un

Chien ouvre la voie à Freeman vers la Citadelle tandis qu'Alyx et Kleiner
s'échappent du laboratoire. Cité 17 est à feu et à sang, et Freeman doit se
frayer un chemin au milieu des combats et est rejoint par tous les résistants
qu'il rencontre devenant l'un des meneurs de cette révolution. Quand Alyx le
rejoint, ils travaillent de concert pour désactiver un générateur du Cartel
afin d'ouvrir un accès ; mais une fois le portail traversé, alors qu'Alyx
cherche une issue, elle est capturée par les soldats.

## 2.12 Chapitre XI : Suivez Freeman

Gordon n'a d'autres choix que de poursuivre sa progression, mais il apprend que
Barney est acculé par des tireurs embusqués. Une fois la route dégagée, Barney
et ses troupes le rejoignent et ils se dirigent vers un ancien bâtiment
administratif qui se trouve être un refuge pour la milice et une cible de choix
pour affaiblir le Cartel. Gordon continue son chemin en direction de la
Citadelle alors que la guérilla urbaine atteint son paroxysme, avec l'arrivée
des Striders. Tandis qu'il permet aux résistants de prendre le contrôle de la
périphérie de la Citadelle, il arrive jusqu'au mur d'enceinte, que Chien l'aide
à franchir.

## 2.13 Chapitre XII : Nos Bienfaiteurs

Gordon entre dans la Citadelle par un passage souterrain et débouche du tunnel
vers un gouffre gigantesque dans lequel est implantée la Citadelle. Alors qu'il
s'y engage, toutes ses armes sont détruites par un dispositif de défense (une
sorte de « champ de confiscation ») à l'exception du pistolet anti-gravité qui,
au contraire, est renforcé par le champ. Freeman parvient grâce à cela à
avancer dans la Citadelle et rentre dans un container qui se verrouille et
l'amène au sommet de la Citadelle, directement dans le bureau de Wallace Breen.

## 2.14 Chapitre XIII : Énergie Noire

Breen se tient aux côtés de Judith Mossman tandis que Eli et Alyx sont
emprisonnés dans des containers similaires à celui qui retient Gordon. Après
une longue discussion entre les cinq protagonistes, Mossman est prise de
remords et se retourne contre Breen, libérant ses trois prisonniers. Breen
parvient à s'échapper jusqu'au « réacteur à énergie noire » au sommet de la
citadelle avec l'intention d'ouvrir un portail pour se téléporter loin de la
Terre. Gordon et Alyx partent à sa poursuite et réussissent à détruire le
réacteur, empêchant la fuite de Breen, ce qui déclenche une gigantesque
explosion dans laquelle Alyx et Gordon sont pris.

## 2.15 Épilogue

Immédiatement après la destruction du réacteur, le temps ralentit puis s'arrête.
Le G-Man apparait, félicitant les exploits de Gordon sans se préoccuper d'Alyx,
et bien décidé à le placer de nouveau en stase. Le jeu se termine exactement
comme il a commencé : par un fondu noir. La suite de l'histoire est à découvrir
dans Half-Life 2: Episode One.

# 3. Commentaire

## 3.0. Général

### 3.0.0. Mécaniques de jeu

First person shooter

Des armes, cac / armes "standard" de FPS / armes plus expérimentales et des
bidules aliens. On verra des utilisations TRÉS détournées de certaines armes.

Mécaniques de vie/armure qui se rechargent via soit des collectibles soit des
distributeurs (oui).

On peut sauter.

On peut s'accroupir.

On peut combiner les 2 pour des sauts accroupis.

On peut prendre des objets en main et les jeter.

Du fun.

### 3.0.1. Techniques émergentes

En fait y'en a plein. Ben ouais c'est le moteur Source.

#### 3.0.1.0. Techniques historiques

Les techniques ci-après sont patchées dans Half-Life 2 mais mentionnées pour
faciliter la compréhension.

**Bunny hoping** : on saute + straffe et on regarde dans la direction du
straffe = vitesse augmente. Slider nous faisant perdre de la friction, c'est
encore mieux accroupi.

**Straffe Jump** : exploitation d'un bogue du moteur Hammer (et donc Source),
en sautant + straffe + direction on peut vraiment accélérer son saut dans un
mouvement circulaire.

#### 3.0.1.1. Techniques inhérentes

**Accelerated Back Hopping (ou ABH)** (et assimilés) : lorsque Valve a patché
le bunny hoping dans le moteur Source, ils ont involontairement introduit un
glitch dans le moteur qui, lorsque le personnage dépasse une vitesse absolue,
le jeu essaye de pousser le joueur :
- Si le joueur se déplace dans une direction, le jeu le pousse dans la direction
  opposée ;
- Si le joueur ne presse pas de touche de déplacement, le jeu le pousse dans la
  dernière direction de poussée enregistrée.
C'est plus efficace que le classique **Bunny Hop**, l'accumulation de vitesse
est plus grande.

Pour info :
- vitesse max de base = ~320 HUPS
- bunny hop (hl1) = ~600/700 HUPS
- **ABH** = >2000/3000 HUPS

**Blast boost** : de manière générale, une propulsion par n'importe quelle
explosion.

**Collision Boosting** : terme général de transformation de vitesse en rentrant
dans des objets ou surfaces.

**Corner boost** : quand on prend certains angles avec une haute vitesse, cela
permet de transférer la vitesse horizontale en vitesse verticale et inversement,
selon angles et direction d'arrivée.

**Floor Warp** : glitch qui consiste en le personnage-joueur passant à travers
le sol dans certaines transition de carte. Pour ce faire, il faut que le PJ se
coince dans le décor alors que la carte transitionne.

**Grenade boost** : quand on lance une grenade, on les dégoupille, et elles ont
un délai. Si on les garde en main longtemps, elles explosent quand on relâche.
Le truc c’est que l’explosion nous boost radicalement les sauts. Par ailleurs,
plus on prend des dégâts de l’explosion sur la vie (et non l'armure) plus le
boost est fort.

**Prop boost** : on peut parfois abuser de la mécanique de grab des props pour
gagner de la vitesse. Beaucoup moins efficace et un poil plus de setup que dans
Half-Life 1 mais tout de même.

**Prop clipping** : on peut parfois clipper à travers le sol si on utilise un
gros prop (genre un baril) et un save/load.

**Ladderspeeding** : en montant une échelle, crouch + utiliser + avancer nous
fait monter plus vite.

**Save deletion** : glitch disponible sur certaines version de Source, qui doit
son nom à la première façon de le faire, en supprimant une sauvegarde en
essayant de charger celle-ci. Le glitch essaye de provoquer le `reload` de la
carte comme si elle venait juste d'être chargée de 0, depuis le menu par
exemple ou encore depuis la console (normalement illégal en run, mais pas avec
le glitch).

En effet, lorsqu'un carte est chargée, le moteur appelle un script spécifique à
chaque map pour donner vie/armure et armes selon la configuration de la carte.
Parfois même avec d'autres bonus.

**Save/load buffering** : l'action de sauvegarder et charger une partie en même
temps. Cela a plusieurs effets kiss-cool notamment quand le personnage-joueur
est coincé dans un item, cela permet de le faire passer à travers des "Brush"
(grosso modo les surfaces) et donc souvent le sol, pour passer outre des parties
du jeu.

En effet, lorsque le joueur est coincé ainsi, au chargement le jeu va tenter de
pousser le personnage-joueur légérement dans une direction (les spécificités
sont apparemment complexes).

**Sharking** : technique utilisée pour maintenir sa vitesse dans l'eau. Il
suffit juste de maintenir la touche de saut et de crouch dans l'eau après y
être entrée avec une forte vélocité.

**Teleporting** (nom non officiel : **NPC Prop Teleport**) : on peut utiliser
des props qu'on a grabbé au préalable en donnant des coup sur la tête des NPC
en pleine animation pour les faire se téléporter et accélérer ladite animation.

Plus généralement, le terme désigne n'importe quelle technique qui permet de
téléporter un personnage non joueur pendant son animation.

**Voidclip** : dans le moteur source, le *Void* est un espace entièrement vide
dans les cartes. Le voidclip est un glitch qui utilise le **save/load
buffering** pour clipper dans le *Void* et maintenir sa vitesse horizontale.

En général, les runs à *Voidclip* sont considérés comme peu intéressantes à voir
car la vaste majorité de ce qu'on voit du jeu sont des écrans de save/load
quasiment tout le temps.

#### 3.0.1.2. LES GROS GLITCHES

**Bill's Big Thrill (BBT)** : le BBT est un glitch nommé en l'honneur du joueur
qui l'a découvert dans un *casual playthrough* du jeu (Bill the Thrill). C'est
un glitch qui permet de faire passer un *prop* dans un état glitché, qui peut
avoir plusieurs impacts :
- le prop reste bloqué en l'air ;
- le prop vole tout seul.

Après avoir touché le prop, le joueur est dans un état glitché. Cela a plusieurs
conséquences :
- lorsque le joueur s'accroupit, le jeu cesse de mettre à jour la position de sa
  hitbox physique, et cela jusqu'à ce qu'il se redresse. Le jeu stocke aussi la
  vitesse à laquelle cette hitbox est figée.
- si le joueur touche un prop, quel qu'il soit, le joueur se téléporte à la
  position de sa hitbox physique, donc potentiellement loin si le joueur ne
  s'est pas relevé depuis longtemps.
- si un prop touche la hitbox physique SÉPARÉE, le joueur cesse d'être soumis
  à la gravité et aux collisions de *brush* jusqu'à ce qu'il se redresse.

(Spécifiquement, en touchant le prop glitché initialement, le joueur va
déclencher ce dernier point s'il s'accroupit avant.)

Le problème de ce glitch, c'est qu'il est instable : le jeu n'aime pas du tout.
Il aime bien crash.
Du coup, pour pouvoir restaurer un état correct, généralement on fait un
**save deletion** juste après avoir utilisé le glitch pour reset la physique.

**Quickclip** : nom donné à la fois à l'état glitché et au clip résultant de
celui-ci.

Pour entrer dans l'état **Quickclip**, il faut sortir d'un **yesclip** (voir
ci-après) à l'aide d'un Barnacle.

Dans cet état glitché trois choses à retenir :
- Les hitboxes du joueur sont déliés. Il existe 2 hitboxes, celle d'Havoc, qui
  gère la physique et celle de Hammer (le moteur de Quake sur lequel est basé
  Source), qui gère les dégâts. Hors dans certaines condition, la hitbox Havoc
  va se déplacer hors de la position réelle du joueur, ce qui peut engendrer
  des téléportation. C'est le **Quickclip** en lui-même et cela arrive quand le
  joueur se place sur un prop ou cherche à se déplacer dedans.
- Les projectiles ne peuvent pas infliger des dégâts au joueur (les explosions
  et dangers de la cartes peuvent toujours).
- L'état glitché est conservé entre les sauvegarde et les chargement et par
  conséquent est disponible jusqu'à la fin du jeu. Toutefois, une
  **save deletion** réinitialise la physique et par conséquent le l'état
  glitché.

**Item Save Glitch** : un glitch initialement découvert dans Portal mais qui
existe aussi dans Half-Life 2. Récemment (2023) découvert comme résultant du
BBT selon certaines conditions, cela nécessite un setup particulier. Toute la
physique du jeu va être bogué et permettre de faire des **Quickclip**s sans
avoir à faire entrer le joueur lui-même dans l'état glitché, (grosso modo,
tous les props sont glitchés au lieu du joueur).

Cet état glitché est concervé entre sauvegarde, le seul moyen de reset est de
redémarrer le jeu.

Dans cet état, la plupart des props du jeu va avoir un comportement étrange,
se mettre à flotter bizarrement.

**Yesclip** : consiste à se placer dans un état de *No clip* (c'est à dire sans
collision) sans tricher. Pour se faire, il faut sortir d'un véhicule à l'endroit
même d'un trigger de changement de zone. En effet, cela va permettre de
conserver la physique de l'intérieur du véhicule (le moteur désactive les
collisions du joueur, par simplicité) tout en en étant sorti grâce à un état
glitché dû à la simultanéité avec le changement de zone.

Voir la route des run, le glitch initial est déclenché au chapitre Water Hazard.

**Wrong warp** : lors d'une transition de carte, consiste à charger la carte
avec une autre position que prévu.

Pour ce faire, il faut :
- utiliser le **BBT** pour glitcher un prop, puis le joueur ;
- il faut se déplacer à un endroit précis sur la carte A puis s'accroupir pour
  figer la hitbox ;
- se déplacer vers la transition vers la carte B accompagné d'un prop ;
- toucher le prop à la même frame que le déclencheur de transition de zone.

Cela va provoquer la téléportation décrite dans le **BBT** en même temps que le
changement de carte, d'où la transition avec d'autre coordonnées que prévues.

## 3.1. Any% No Voidclip (last update: SGDQ 2024)

### 3.1.0 Informations diverses

#### 3.1.0.0. Description générale

Runné sur PC, *New Engine*.

Temps :
- (1er, PenPerson : **38m 04s 065ms** LRT - **46m 45s 010ms** RTA)
- 3ème, Eold : **39m 45s 825ms** LRT - **48m 46s** RTA
- 4ème, Dirtch : **39m 57s 150ms** LRT - **44m 56s 770ms** RTA

Type de run : LE. BORDEL.
(Run du swag et du skill avec des techniques fumées et des glitchs.)

Notes :
- Existe des sous catégories avec scripts (notamment pour faciliter le bunny
  hop).

#### 3.1.0.1. Règles

##### 3.1.0.1.0. Général

La run doit être complétée sans scripts (il existe des script pour rendre
trivial le **Bunny Hop**, le **Ladder Boost**, le **Spam E**, etc.).

Toutes les commandes de la console des développeurs sont interdites, exceptées :
- `use weapon_(weapon name)`
- `cl_showpos`
- `toggle_duck`
- `r_portalsopenall`
- `fps_max`
- `reload`
- `kill`

Il est autorisé d'associer une commande à plusieurs touches (par exemple sauter
sur espace et la molette de la souris).

Il est autorisé d'associer plusieurs commandes à une seule touche (par exemple
`bind c "reload; fps_max 30"`).

La technique **Save Deletion** est autorisée.

Les modifications du HUD qui pourrait donner un avantage sont interdites.

Aggrandir le texte affiché par `cl_showpos` est autorisé.

Désactiver les flashs rouges lors de prises de dégâts est interdit.

L'utilisation de mods est interdite. Le remplacement de maps est interdit.

L'utilisation de deux types de contrôleurs en même temps (clavier + souris et
manette par exemple) est interdit.

L'utilisation de modèles personnalisés pour les PNJ est interdit.

##### 3.1.0.1.1. Chronomètre

Utilisation de Livesplit pour avoir le temps sans les temps de chargement
obligatoire.

Le chronomètre démarre dès que la map `d1_trainstation_01` est chargée.

Le chronomètre termine au moment où le jeu reprend la main sur le joueur au
moment de l'explosion du réacteur à énergie noire.

### 3.1.1. Route

#### 3.1.1.0. Général

C’est le bordel, mais généralement, juste confèrer à la section 3.0.1. pour
tout ce qu'il se passe. Mais 4 points à retenir :
- On **ABH** (ou assimilé) + **Bunny Hop** systématiquement (quand pas de
  meilleur déplacement disponible) ;
- Se rattraper sur une échelle ne provoque aucun dégât après une longue chute.
- Certaines chutes sont rattrapées par un **Collision boosting**.

##### 3.1.1.1. Chapitre I : Point d'insertion

**Train clip** est fait avec des save/load.

Premier ABH.

Premier **Teleporting**.

Si on est assez rapide on peut passer à travers les gardes qui devraient nous
ralentir.

De ce fait on passe outre un trigger qui devrait nous enlever l'invincibilité
(ne fonctionne que pour la première map).

**BBT**.

Alors attention, là ça va setup des trucs de ouf. D'abord on fait une
sauvegarde précise juste après s'être envolé, ça sera utile plus tard.
Cette sauvegarde contient l'état **BBT**.

Ensuite dès la cutscene d'Alyx, le reste du chapitre est un setup avec le
**BBT** pour wrong warp Red Letter Day (le chapitre suivant) grace à une
dislocation de hitbox.

##### 3.1.1.2. Chapitre II : Un jour mémorable

Grâce au setup **BBT** du chapitre précédent, on commence ce chapitre en
skippant toute la phase du laboratoire secret de Kleiner, donc au moins 5 bonnes
minutes de gagnées.

On arrive au moment où on récupère la crowbar, sans récupérer la crowbar.

Ça rushe avec les tricks classiques.

##### 3.1.1.3. Chapitre III : Itinéraire Kanal

**Save deletion** pour obtenir la suit, la crowbar. Also reset du **BBT**, par
conséquent.

À un moment, on ramasse une caisse de supplies. On va la placer à un endroit
précis et sauvegarder. On charge ensuite la sauvegarde BBT faite au chapitre 1 ;
où on fait rentrer un prop dans un autre prop qui passe à travers le sol.

C'est ridicule, mais ce qu'il vient de se passer a mis le jeu dans l'état
**Item Save Glitch**. On peut recharger la sauvegarde de ce chapitre et
conserver cet état.

Maintenant, on va pouvoir faire des **Quickclip**s et la physique va être un peu
chelou.

Dans les tunnels **Quickclip** into **Prop clip** into **Collision boost** into
**save/load** pour sortir du tunnel.

Suivi par une ejection hors de la carte à base d'**ASH** into **propclip** avec
pas mal de **save/load** pour revenir inbound.

**Save deletion** vers la fin du chapitre. On perd l'**ISG** mais le jeu est
plus stable et de toute façon on va réobtenir l'état **quickclip** au chapitre
suivant.

##### 3.1.1.4. Chapitre IV : Danger aquatique

On rush la première map sans l'hydroglisseur.

**Save deletion** à la map suivante pour avoir l'hydroglisseur.

Dans ce chapitre, on va utiliser l'hydroglisseur pour se placer en état
de **Yesclip**. On va utiliser un barnacle pour terminer le setup. L'état
glitché qui permet de faire des **Quickclip** se conserve entre
les sauvegardes et jusqu'à (quasi) la fin du jeu.

L'effet kiss cool notable c'est que les ennemis ne peuvent plus infliger des
dégâts au joueur, jusqu'à la fin du jeu.

##### 3.1.1.5. Chapitre V : Black Mesa Est

Bon.

Ça va chier.

À l'époque on accélérait les cutscene avec manipulation de Mossman +
**quickclip**s + **teleporting**s.

Maintenant on va juste full skip avec **quickclip**s + setup de **quickclip**s
+ on reload la save **BBT** du chapitre I pour redéclencher un **ISG** et on
reload la save de ce chapitre.

Grace à l'**ISG**, un **quickclip** et des **save/load**, on va faire passer
Eli Vance de l'autre côté d'une porte où on est pas censé aller tout de suite.

Le jeu qui ne détecte plus de NPC dans la zone de la cutscene croit qu'elle est
terminée et passe à la séquence suivante, où Alyx arrive de l'autre porte.

Oui, on fait tout ça pour qu'Alyx arrive plus vite.

Ensuite **teleporting**s sur Alyx en série puis setup de **quickclip**s en
attendant que Alyx déclenche le trigger de changement de carte.

On va **Quickclip** à travers un mur invisible pour éviter toute la séquence
avec Chien et un deuxième pour aller à Ravenholm.

##### 3.1.1.6. Chapitre VI : Ravenholm

**Blast boost** en folie, parce que maintenant on a des grenades.

##### 3.1.1.7. Chapitre VII : Autoroute 17

*Niiiiiouuuum*

On voit les **Collision Boosting** très bien illustrés ici, et le **Sharking**.

##### 3.1.1.8. Chapitre VIII : Pièges de sable

*Niiiiiouuuum*

##### 3.1.1.9. Chapitre IX : Nova Prospekt

Ça clippe de partout.

##### 3.1.1.10. Chapitre IX bis : Corrélation quantique

Ça clippe toujours de partout.

Re un **ISG** ici comme les autre avec la save juste pour un clip visiblement
(jsp, là je comprend pas tout).

Juste avant le téléporteur on va clip à travers le sol avec un **propclip**
+ **grenade boost** et toucher une tourelle, ce qui cause la phase de "tower
defense" à s'activer plus vite.

La cutscene de Mossman, Alyx et Eli joue toujours cependant. Il faut mettre une
plaque de métal à un endroit précis pour accélérer les mouvements de Mossman.

**Collision boost** pour clipper hors du téléporteur, pour se launch avec la
plaque de métal qu'on a setup plus tôt pour toucher le trigger un peu plus
vite.

##### 3.1.1.11. Chapitre X : Anticitoyen Un

*Niiiiiouuuum*

##### 3.1.1.12. Chapitre XI : Suivez Freeman

À noter dans ce chapitre un **Grenade boost** immédiatement suivi par un
**Quickclip**. Extrèmement rapide, extrèmement difficile à faire.

##### 3.1.1.13. Chapitre XII : Nos Bienfaiteurs

**Save deletion** pour reset les PV et obtenir le Gravity Gun. Plus de
**Quickclip**s du coup.

En entrant sur la deuxième map avec un Drone Scanner tenu avec le gravity gun
évite de spawner dans la cage-prison, puisqu'on a suivit une route non
conventionnelle pour ça. La deuxième carte doit donc être elle aussi
faite de manière non conventionnelle (mais plus rapide).

**Quick weapons strip** : en s'alignant sur un point précis de la zone de
désarmement, le joueur peut sortir du trigger qui l'empêche de les attraper à
nouveau. Après les avoir réattrapé, la séquence se termine plus vite.

On va se servir du *Super gravity gun* pour faire des **orb launch** spécifique
à la citadelle avec les orbes d'énergie.

##### 3.1.1.14. Chapitre XIII : Énergie Noire

**Breenblast** : un autre **Yesclip** en faisant deux sets de **save/load**
juste avant et au moment du tir du *Super gravity gun* par Breen.

Cela permet de voler directement aux orbes d'énergies en haut de la forteresse
et de détruire le réacteur et de terminer la run.

##### 3.1.1.15. Épilogue

# 4. Trivia

Beaucoup trop, demandez à Synahel.

# 5. Références

BBT et son histoire (en) : https://www.youtube.com/watch?v=Fb5gi-5EjEE

BBT mais mieux expliqué (en) : https://www.youtube.com/watch?v=NYNnYVIXJJ8

Lien speedrun.com : https://www.speedrun.com/hl2

Noclip glitch (en) : https://www.youtube.com/watch?v=07wRq2Wtadw

Wikipedia : https://fr.wikipedia.org/wiki/Half-Life_2

Wiki source run, grosse référence du speedrun de ces moteurs :
https://wiki.sourceruns.org/wiki/Category:Half-life
