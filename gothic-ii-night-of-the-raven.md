# 0. Metadata

## 0.0. Versioning

| Date          | Révision              |
|-              |-                      |
| 2022-06-25    | Update structurelle   |
| 2019-09-28    | Création (ZFM 2019)   |

# 1. Fiche technique

Sortie : 2002

Plateforme : 

Développé par : Piranha Bytes

Édité par : JoWooD Productions

Genre : Action role-playing

Concepteur : Michael Hoge

Compositeur : Kai Rosenkranz

# 2. Histoire

(Wipidia FTW:)

## 2.0. Prélude

La série des Gothic se déroule dans un monde médiéval dark fantasy. Gothic I
tiens lieu dans une enclave de l'île de Khorinis tandis que Gothic II prend
place dans toute l'île.

Pré-Gothic I, le roi de la nation humaine est en guerre contre des envahisseurs
orcs. Alors que le conflit tourne en sa défaveur, il sait que le seul moyen de
renverser la tendance est d'envoyer des gens miner le précieux minerai magique.
Il envoit des prisonier dans la mine pour pouvoir assurer le flux de minerai,
et place une barrière magique pour que personne ne puisse s'en échapper.
Cependant, le plan tourne mal et la barrière s'étends trop, permettant aux
prisoniers de se rebeler et prendre contrôle de la région.

Le héros, un prisonier, arrive dans la mine et va se mêler à la guerre
intérieure entre les 3 factions nouvellement créées. Au fur et à mesure du
premier jeu va se révéler une menace d'un ancien Dieu, qu'il va finir par
combattre et détruire, engeandrant la destruction de la barrière et libérant les
prisoniers.

## 2.1. Jeu

La nuit du corbeau est une extension qui rajoute un peu de contenu, dont on va
se servir et tuer le boss de l'extension mais on va aussi faire l'histoire
originale.

Sauvé des débris de son conflit contre le mauvais Dieu par Xardas, un mage
rénégat (qui l'avait aidé dans le premier jeu) ; celui-ci indique au héros que
sa tâche n'est pas terminée. Une armée démoniaque dirigée par des dragons va
s'abattre sur le monde, réveillée par la mort du Dieu.

Le première étape de sa quête est de récupérer un artefact pour l'aider et
également de prévenir le monde ; notemment Lord Hagen, le
paladin envoyé par le roi pour prendre le contrôle de la région. Il va donc
chercher des indices dans la mine maintenant sous contrôle des orcs. Une fois
cela fait, Hagen le crois et l'envoie au monastère des mages pour récupérer
l'Oeil de Innos, l'artéfact, une amulette.

Sauf qu'à son arrivé, le héros découvre que l'Oeil s'est fait voler, et en
pourchassant le voleur voit que les serviteurs de l'armée détruisent l'amulette.
Le héros va chercher à la reforger, mais cela demande un bon forgeron et surtout
un rituel magique pour rétablir son pouvoir. Ce rituel doit être fait par des
mages représentant chacun un Dieu. On va donc aller les démarcher.

Une fois l'amulette réparée, on va poutrer les 4 dragons qui menacent le monde,
puis on retourne voir Xardas. Il a disparu mais il nous a laissé une piste pour
trouver le repaire du dernier dragon, avatar du Dieu démoniaque Béliar.

De fil en aiguille, on va prendre la mer pour aller le tuer.

Xardas réaparait à la fin du jeu, absorbe l'âme du Dragon mort et devient à son
tour avatar de Béliar. Il nous dit qu'il nous reverra et part.

# 3. Commentaire

## 3.0. Informations diverses

***Qualité : Meublage***

Runné sur PC

Catégorie : any%

Temps :
- WR : **30m53s033ms** by Karstix

Run full abus, full glitché, ce serait bien si j'avais un guide d'ailleurs :-/

Chronomètre :
- Démarre à l'apparition du premier temps de chargement
- Fini à l'apparition de la dernière cutscene

## 3.1. Mécaniques de jeu

Action RPG plus ou moins classique.

De la vie, du mana, des stats, des skills.

On peut lancer des sorts avec des parchemins.

Des potions.

Des armes, des armures, etc.

A noter, il y a une touche action pour tout faire, ce qui est très rigide comme
gameplay au vue des standard aujourd'hui (exemple, pour attaquer il faut :
sortir son arme, maintenir <action> et appuyer sur une touche de direction).

Le jeu est très difficile, il y a beaucoup d'opportunité de clamser pour rien,
sans préparation.

## 3.2. Techniques

Y'a blinde de glitches dans cette run. Sans explication je ne peux que deviner
ce qu'il se passe.

Strafing:
- Strafing is very very slightly faster than walking.
- Strafing against a wall at the right angle is significantly faster than walking.

FPS fixés à 30 sinon c'est la merde (luuul)

Black ore glitch : un glitch qui augmente la vitesse avec le minerai noir.

## 3.3. Route

Parler au gonz, dormir, sacrifier des PV à la statue pour de la thune.

Glitch ?  
Je suppute que c'est un speed buffer glitch, où on accélère à l'infini alors
qu'on se trouve dans un menu.

On va voir un mec pour de la thune.

On va voir un autre mec pour une potion de vitesse et des sorts craqués. On bois
la potion.

Transform into bêbête à côté d'un mur et on zip à travers. On tue des gens pour
le loot et pour une quête qu'on va récup et rendre ensuite (c'est des voleurs).

Apparemment c'est assez facile de passer à travers les murs MDR.

On rejoint la faction de la milice grâce à la quête.

Ensuite, on glitche (je bite rien sérieux) : on va acheter du stuff à un
marchant, le tuer avec de la magie pour pas trigger des fights à côté, et on
récupère le stuff du marchand (avec un petit glitch pour se déplacer avec
l'inventaire ouvert).

On va voir Hagen, le chef des paladins pour lui parler des méchants.

(Quick point histoire)

P'tit zip. On va chercher la black ore pour speed up (apparemment c'est un
glitch).

Et là on revient dans le premier jeu.

On va chercher une rune pour se TP après.

Le passage où on se change en mouton je crois que ça désactive l'aggro du gonze
à qui on parle.

On tue le dragon pour avoir une preuve (la magie C'estPéTé).

On va retourner voir Hagen avec les preuves, on en profite pour refaire le trick
du marchand décédé avant.

Segue sur la quête de l'extension.

Training pour augmenter son mana.

Bon là c'est le bordel, tant pis pour les explications (on va OOB into strafe
WTF).

Raven dead, on reprend l'histoire.

On récupère l'amulette pétée, on va voir les mages.

On va tuer les dragons. Chaque dragon a sa faiblesse.

Après on va prendre le bateau.

Le boss de fin est undead, on a un sort contre les undead lol.

C'pas time tout de suite, 'faut se barrer de l'île avant.

# 4. Trivia

# 5. Références

Lien speedrun.com : https://www.speedrun.com/notr/full_game

Wikipedia : https://en.wikipedia.org/wiki/Gothic_II
