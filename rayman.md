# 0. Metadata

## 0.0.  Versioning

| Date          | Révision                          |
|-              |-                                  |
| 2022-06-25    | Update structurelle               |
| 2018-10-xx    | Création (ZFM 2018) - Run Any%    |

# 1. Fiche technique

Sortie : 1995

Plateforme : Windows, DOS, Jaguar, PlayStation, PlayStation 2, PlayStation 3 (via PSN), Saturn, Game Boy Color, Game Boy Advance, PlayStation Portable (via PSN), Nintendo DS (via le DSiWare), iOS, Android

Développé par : Ubisoft Montpellier (Ludimédia)

Édité par : Ubisoft

Genre : Plateformer Metroid-Vania

Concepteur : Michel Ancel

# 2. Histoire

(Wipidia FTW:)

## 2.0. Prélude

L'équilibre cosmique du monde de Rayman est maintenu par un artéfact, plus précisément une sphère, connu sous le nom de Grand Protoon. Un jour, cependant, l'antagoniste M. Dark décide de le voler après avoir réussi à vaincre Betilla la Fée qui tentait de protéger l'artéfact. Les Électoons – petites créatures roses et jaunes gravitant autour du Grand Protoon – sont alors éparpillés à divers endroits dans le monde. Dans ce monde désormais devenu malsain, des créatures hostiles apparaissent de l'ombre pour mettre en cage les Électoons. Le magicien, grand sorcier, fait appel au protagoniste Rayman pour libérer les Électoons, vaincre Mister Dark et remettre la main sur le Grand Protoon.

## 2.1. Jeu

L'introduction terminée, Rayman commence son périple dans La Foret des Songes. À cet endroit, il y rencontre Betilla la Fée, lui attribuant le pouvoir de frapper ses adversaires à coups de poing. Le premier antagoniste rencontré dans le monde est Bzzit, un moustique aux proportions gigantesques. Après avoir vaincu Bzzit, Rayman se lie d'amitié pour lui ; de ce fait, Bzzit l'aide à traverser le niveau en survolant le lagon sur son dos. Betilla attribue par la suite à Rayman la capacité de s'accrocher à diverses plateformes. Derrière un massif de fleur, Rayman fait la rencontre de Tarayzan, qui lui donne une graine magique permettant à Rayman d'échapper à une soudaine montée des crues dans le Marais de l'Oubli. Dans l'Antre des Moustiques, Rayman réussit à vaincre Moskito, le boss de fin de niveau, et Betilla lui donne le pouvoir de s'accrocher à des anneaux flottants. Rayman parvient ensuite au second niveau, Le Ciel Chromatique, une région principalement composée d'instruments et de notes de musiques. Il fait la rencontre du boss de fin de niveau M. Sax, un saxophone géant. Betilla la Fée lui attribue par la suite le pouvoir de l'hélicoptère, la capacité de voler une petite distance en faisant tournoyer ses cheveux. Rayman atteint par la suite l'antre de M. Sax et parvient à le vaincre.

Dans Les Montagnes Bleues, un niveau majoritairement constitué de montagnes, Rayman fait la rencontre du Musicien, dont la guitare a été écrasée par la chute d'un rocher. Pour le consoler, Rayman lui sculpte à coups de poing une nouvelle guitare à partir d'un rocher placé devant lui. Pour le remercier, le Musicien offre à Rayman une fiole de liquide violet qui, en l'ingérant, lui attribuera le pouvoir du super-hélicoptère, la même capacité de voler mais à longue distance. Arrivé à destination, Rayman fait la rencontre de M. Stone, un homme de pierre géant. Une fois M. Stone vaincu, Betilla attribue à Rayman la capacité de courir à grande vitesse. Au quatrième niveau, Rayman parvient à La Cité des Images, une plateforme entièrement composée de matériels artistiques. Après avoir atteint La Crique aux Crayons, Rayman fait deux fois la rencontre avec Space Mama, une comédienne déguisée en viking puis en spationaute, qu'il parvient à vaincre. Rayman apprend par la suite que M. Dark a enlevé Betilla la Fée. Au cinquième niveau, Rayman parvient aux Caves de Skops, dans lesquelles il fait la rencontre de Joe l'extra-terrestre, propriétaire d'une crêperie-buvette appelée Mangez chez Joe, puis plus tard Skops, un scorpion géant rouge vif et fuchsia. Après la défaite de Skops, Rayman entend Betilla la Fée crier à l'aide.

Dans le but parvenir au niveau final, Le Château des Délices, Rayman se doit de fracasser les 102 cages dans lesquelles les Électoons ont été retenu prisonniers à travers les cinq niveaux précédents. Dans ce niveau constitué de sucreries, après avoir échappé à son double maléfique, Bad Rayman, Rayman fait face à M. Dark qu'il parviendra difficilement à vaincre. Un épilogue apparaît ensuite montrant le prompt rétablissement du Grand Protoon, la libération de Betilla, ainsi que Rayman partageant du bon temps avec ses alliés et ses anciens ennemis.

# 3. Commentaire

## 3.0. Informations diverses

Version PS1 runné sur PS2

Catégorie : any% (qui est également le 100% du coup)

WR is 1h11m55s RTA no loads, 1h18m40s RTA by Thextera (Pokemonrise à ???/??? LUL)

Run du skill avec quand même un zip et quelques clips.

Timer démarre au “start” au menu, fini sur le dernier hit de Mr Dark

## 3.1. Mécaniques de jeu

Un plateformer

Rayman commence en pouvant marcher et sauter. La fée Betilla lui donne des pouvoir au fur et à mesure de son avancée dans les niveaux.

Metroid-vania oblige, certaines cages ne sont brisable qu’avec des pouvoirs obtenus plus tard dans le jeu, du coup backtracking.

Les pouvoirs sont :
- Lancer son poing, en un coup à charger. Rayman lance à longue distance immédiatement s’il est en l’air.
- S’accrocher aux plateformes.
- Attraper des anneaux avec le poing pour se balancer. Lancer son poing pendant le balancement annule le balancement.
- Faire l’hélico avec les cheveux pendant une courte durée (sauf niveau super hélico). Lancer le poing cancel l’hélico.
- Courir.

Après on a le classique des jeux de plateformes : plateformes invisibles qui apparaissent, plateformes qui disparaissent quand on marche dessus, objets cachés qui apparaissent uniquement si on va à un certain endroit

L’eau tue, on passe outre avec des prunes (oui).

Des powerup (poing rapide, poing doré, soin +2pv, full life 5hp) et des vies.

Les lums (petit trucs bleus) qu’on ramasse nous donnent une vie à 100 ramassés.

## 3.2. Techniques

Rien de bien spécifique au speedrun, du par coeur principalement.

Dans les niveaux du Ciel Chromatique, Rayman peut passer, avec un certain angle, à travers certaines plateformes glissantes.

De plus, dans ce niveau, un objet qui nous propulse est utilisé pour zipper à travers un mur.

Dernière technique abusive : le damage boost. On va en profiter à certain moment clés voir la route.

## 3.3. Route

Mostly normal excepté qu’on va rusher les pouvoir (car la course c’est pratique quand même). Dans la plupart des mondes, on essaye de ramasser toutes les cages, sinon le moins possible parce qu’on va backtrack, du coup.

Plusieurs plateformes aux niveaux du Ciel Chromatiques sont clipable, avec le bon angle.

Fast strat sur les boss quand fast strat possible (notamment Mr. Rock, où on va damage boost, Skops et les phases de Mr. Dark).

Sinon la run est suffisamment normale pour expliquer ce qu’il se passe à l’écran calmement.

Note 1 : le premier niveau de la Crique aux Crayons est reset après la 1ère caisse pour retrouver le cycle initial.

# 4. Trivia

Des différences selon les supports, ask Maman ou Synahel.

Rayman 2 avait un prototype en 2D qui ressemble beaucoup à Rayman 1.

# 5. Références

Lien speedrun.com : https://www.speedrun.com/rayman_1

Wikipedia : https://fr.wikipedia.org/wiki/Rayman_(jeu_vid%C3%A9o)
